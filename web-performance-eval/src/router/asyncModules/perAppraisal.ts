//绩效考核相关路由
export default {
  'views/perAppraisal': () => import('@/views/perAppraisal/Index.vue'),
  'views/perAppraisal/perApList': () => import('@/views/perAppraisal/perApList/Index.vue'),
  'views/perAppraisal/perWorkList': () => import('@/views/perAppraisal/perWorkList/Index.vue'),
  'views/perAppraisal/perScoreList': () => import('@/views/perAppraisal/perScoreList/Index.vue'),
  'views/perAppraisal/perEmailList': () => import('@/views/perAppraisal/perEmailList/Index.vue'),
} as const;
