export default {
  'views/perPlan/planList': () => import('@/views/perPlan/planList/Index.vue'),
  'views/perPlan/reportList': () => import('@/views/perPlan/reportList/Index.vue'),
  'views/perPlan/resultList': () => import('@/views/perPlan/resultList/Index.vue'),
  'views/perImbizo/MeetingSetUp': () => import('@/views/perImbizo/meetingList/MeetingSetUp.vue'),
  'views/perImbizo/MeetingShow': () => import('@/views/perImbizo/meetingList/MeetingShow.vue'),
} as const;
