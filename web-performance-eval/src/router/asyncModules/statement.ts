export default {
  'views/statement/reportPresentation/KPI': () =>
    import('@/views/statement/reportPresentation/KPI.vue'),
  'views/statement/reportPresentation/Strategic': () =>
    import('@/views/statement/reportPresentation/Strategic.vue'),
  'views/statement/reportPresentation/Score': () =>
    import('@/views/statement/reportPresentation/Score.vue'),
  'views/statement/reportPresentation/Mark': () =>
    import('@/views/statement/reportPresentation/Mark.vue'),
  'views/statement/reportPresentation/Stimulate': () =>
    import('@/views/statement/reportPresentation/Stimulate.vue'),
} as const;
