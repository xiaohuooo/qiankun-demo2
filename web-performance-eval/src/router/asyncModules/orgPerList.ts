//组织绩效设定相关路由
export default {
  'views/orgPerList/perList/PerList': () => import('@/views/orgPerList/perList/PerList.vue'),
  'views/orgPerList/perSetUp/Setup': () => import('@/views/orgPerList/perSetUp/Setup.vue'),
} as const;
