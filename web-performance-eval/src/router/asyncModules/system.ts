/**
 * system module
 */
export default {
  'views/system/menu/Menu': () => import('@/views/system/menu/Menu.vue'),
  'views/system/role/Role': () => import('@/views/system/role/Role.vue'),
  'views/system/user/User': () => import('@/views/system/user/User.vue'),
  'views/system/dept/Dept': () => import('@/views/system/dept/Dept.vue'),
  'views/system/dict/Dict': () => import('@/views/system/dict/Dict.vue'),
  'views/system/NoticeList': () => import('@/views/account/NoticeList.vue'),
} as const;
