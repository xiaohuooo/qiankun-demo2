import type { RouteRecordRaw } from 'vue-router';
import { LOGIN_NAME } from '@/router/constant';

/**
 * layout布局之外的路由
 */
export const LoginRoute: RouteRecordRaw = {
  path: '/login',
  name: LOGIN_NAME,
  component: () => import(/* webpackChunkName: "login" */ '@/views/login/Index.vue'),
  meta: {
    title: '登录',
  },
};
export const MeetingRoute: RouteRecordRaw = {
  path: '/MeetingShow',
  name: 'MeetingShow',
  component: () =>
    import(/* webpackChunkName: "login" */ '@/views/perImbizo/meetingList/MeetingShow.vue'),
  meta: {
    title: '绩效对话会',
  },
};
export const MeetingPDFRoute: RouteRecordRaw = {
  path: '/PDFShow',
  name: 'PDFShow',
  component: () =>
    import(/* webpackChunkName: "login" */ '@/views/perImbizo/meetingList/PDFShow.vue'),
  meta: {
    title: '绩效对话会',
  },
};

export default [LoginRoute, MeetingRoute, MeetingPDFRoute];
