import { defineStore } from 'pinia';
import { store } from '@/store';
interface insertContentType {
  title: string;
  value: any[];
}

interface chartConfigurationsState {
  insertContent: insertContentType[];
  AddCatalogueFlug: boolean;
  navList: any[];
  page: any;
  datasets: any;
  chartData: any;
  pageData: any;
  fieldArray: any;
  echartConfig: any;
  sqlParams: any;
  newSqlParams: any;
}
const insertContent = [
  {
    title: '文字',
    value: [],
  },
  {
    title: '数据',
    value: [],
  },
  {
    title: '过滤',
    value: [],
  },
];
const chartData = {
  loading: 'error', // error 无数据
  data: {}, // 获取到的数据
  page: {
    pageNum: 1,
    pageSize: 10,
    total: 0,
    pageCount: 1,
  },
};
export const chartConfigurations = defineStore({
  id: 'chartConfigurations',
  state: (): chartConfigurationsState => ({
    AddCatalogueFlug: false,
    insertContent, // 横纵轴配置参数
    navList: [], // 左边所有数据
    page: {},
    pageData: {},
    datasets: [],
    chartData,
    fieldArray: [], // 所有字段
    echartConfig: {}, // 图表配置项
    sqlParams: { datatableList: [], tableRelationList: [] }, //获取sql入参
    newSqlParams: {},
  }),
  actions: {
    AddCatalogueFlugConfig(itemLength) {
      this.AddCatalogueFlug = itemLength > 0;
    },
    navListConfig(data, fieldArray) {
      this.navList = data;
      this.fieldArray = fieldArray;
      if (this.pageData.tableData) {
        this.navList[1] = this.pageData.tableData; // 配置目录
      }
    },
    pageConfig(data) {
      this.page = data;
      this.pageData = data.data.pageData ? JSON.parse(data.data.pageData) : {};
    },
    chartDataConfig(data) {
      this.chartData = data;
    },
    insertContentConfig(data) {
      this.insertContent = [
        {
          title: '文字',
          value: [],
        },
        {
          title: '数据',
          value: [],
        },
        {
          title: '过滤',
          value: [],
        },
      ];
      if (data.pageData) {
        const pageData = JSON.parse(data.pageData);
        console.log(pageData, 'pagesdasdas');
        this.insertContent.forEach((item, index) => {
          item.value = pageData.filterList[index].value;
        });
      }
    },
    onlyAxis(newVal) {
      // 轴字段过滤
      this.insertContent.forEach((item) => {
        item.value.forEach((tItem, tIndex) => {
          // 目录
          if (tItem.newType === 4) {
            tItem.children.forEach((cItem, cIndex) => {
              let flag = false;
              newVal.forEach((fItem) => {
                if (cItem.key == fItem.key) {
                  flag = true;
                }
              });
              if (!flag) {
                tItem.children.splice(cIndex, 1);
              }
            });
          } else {
            let flag = false;
            newVal.forEach((fItem) => {
              if (tItem.key === fItem.key) {
                flag = true;
              }
            });
            if (!flag) {
              item.value.splice(tIndex, 1);
            }
          }
        });
      });
      // 目录过滤
      if (
        this.navList[1] != undefined &&
        this.navList[1].children != undefined &&
        this.navList[1].children.length > 0
      ) {
        this.navList[1]?.children?.[0]?.children.forEach((citem, cIndex) => {
          let flag = false;
          newVal.forEach((fItem) => {
            if (citem.key === fItem.key) {
              flag = true;
            }
          });
          if (!flag) {
            this.navList[1].children?.[0].children.splice(cIndex, 1);
          }
        });
      }
    },
    getnewSqlParams(newSqlParams) {
      this.newSqlParams = newSqlParams;
    },
  },
});

// 在组件setup函数外使用
export function chartConfigurationsWithOut() {
  return chartConfigurations(store);
}
