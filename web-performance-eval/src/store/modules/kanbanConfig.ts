import { defineStore } from 'pinia';
import { store } from '@/store';
interface kanbanConfigState {
  kanbanData: any;
  dragNavList: any;
  boardData: any;
  dimIdMap: any;
}
export const kanbanConfigStore = defineStore({
  id: 'kanbanConfig',
  state: (): kanbanConfigState => ({
    kanbanData: [], // 右边看板所有的数据 只有id以及定位信息。。。
    dragNavList: [], // 左边列表数据
    boardData: {}, //看板配置信息
    dimIdMap: [], //抛出参数集合
  }),
});

// Need to be used outside the setup
export function kanbanConfigStoreWithOut() {
  return kanbanConfigStore(store);
}
