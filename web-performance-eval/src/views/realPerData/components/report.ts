export type reportWorkType = {
  modalVisible?: boolean;
  dataSource?:
    | [
        {
          attestationFiles?: [];
          auditRemark?: string;
          auditStatus?: string;
          description?: string;
          evaluatedOrgName?: string;
          fieldEndValue?: string;
          fieldName?: string;
          fieldNameCn?: string;
          fieldType?: string;
          fieldValue?: string;
          monthPeriod?: string;
          perfEvalScoreFixOpt?: string;
          perfEvalScoreFixValue?: string;
          perfEvalScoreFixValueStr?: string;
          perfGName?: string;
          qFourthEmphasisGoal?: string;
          qOneEmphasisGoal?: string;
          qSedEmphasisGoal?: string;
          qThirdEmphasisGoal?: string;
          yearEmphasisTarget?: string;
        },
      ]
    | any;
  loading?: boolean;
  BASE_URL?: string;
  subVisible?: boolean;
  infoVisible?: boolean;
  formList?: {
    evaluatedOrgName?: string;
    fieldEndValue?: string | number;
    fieldName?: string;
    fieldNameCn?: string;
    fieldType?: string;
    fieldValue?: string;
    attestationFiles?: Array<any>;
    fileList?: Array<any>;
    fileName?: string;
    monthPeriod?: string;
    perfEvalScoreFixOpt?: string;
    perfGName?: string;
    status?: string;
    description?: string;
    unfinishedDescription?: string;
    period?: string;
  };
  editIndex?: number;
  infoHtml?: string;
  saveLoading?: boolean;
  subLoading?: boolean;
  rowIn?: object | string;
  secVisible?: boolean;
  PerformanceType?: string;
  mainLoading?: boolean;
  conLoading?: boolean;
  rowSelectVal?: any;
};
