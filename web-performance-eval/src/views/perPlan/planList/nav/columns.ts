import { isEmpty } from 'lodash-es';
import dict from '@/utils/dict.js';

export const KPIColumns = [
  {
    title: '序号',
    type: 'seq',
    width: 50,
  },
  {
    field: 'perfGCategory',
    title: '类别',
    formatter: ({ cellValue }) => {
      const obj = dict.getValue('INDICATOR_CATEGORY', cellValue);
      return !isEmpty(obj) ? obj.itemText : '';
    },
  },
  {
    field: 'perfGName',
    title: '指标名称',
    showHeaderOverflow: true,
  },
  {
    field: 'perfGOrgCode',
    title: '指标单位',
    showOverflow: true,
    formatter: ({ cellValue }) => {
      const obj = dict.getValue('INDICATOR_UNIT', cellValue);
      return !isEmpty(obj) ? obj.itemText : '';
    },
  },
  { field: 'yearTarget', title: '目标值', showOverflow: true },
  { field: 'score', title: '权重(%)', showOverflow: true },
  { field: 'superinDeptName', title: '集团对口部门', showOverflow: true },
];
export const deptColumns = [
  {
    field: 'evaluatedOrgName',
    title: '被评价单位',
    width: 140,
  },
  {
    field: 'perfGCategory',
    title: '指标类别',
    width: 160,
    formatter: ({ cellValue }) => {
      const obj = dict.getValue('INDICATOR_CATEGORY', cellValue);
      return !isEmpty(obj) ? obj.itemText : '';
    },
  },
  {
    title: '序号',
    type: 'seq',
    width: 60,
  },
  {
    field: 'perfGName',
    title: '指标名称',
    showHeaderOverflow: true,
    width: 160,
  },
  {
    field: 'score',
    title: '权重(%)',
    showOverflow: true,
    width: 110,
  },
  {
    field: 'perfGOrgCode',
    title: '指标单位',
    showOverflow: true,
    width: 110,
    formatter: ({ cellValue }) => {
      const obj = dict.getValue('INDICATOR_UNIT', cellValue);
      return !isEmpty(obj) ? obj.itemText : '';
    },
  },
  {
    field: 'eneityResultFuncDesc',
    title: '计算公式或指标简述',
    showOverflow: true,
    width: 300,
  },
  {
    field: 'timePeriod',
    title: '当期/累计',
    showOverflow: true,
    width: 120,
    formatter: ({ cellValue }) => {
      const obj = dict.getValue('TIME_PERIOD', cellValue);
      return !isEmpty(obj) ? obj.itemText : '';
    },
  },
  {
    field: 'evaluatePeriod',
    title: '评价周期',
    showOverflow: true,
    width: 120,
    formatter: ({ cellValue }) => {
      const obj = dict.getValue('EVALUATION_PERIOD', cellValue);
      return !isEmpty(obj) ? obj.itemText : '';
    },
  },
  {
    field: 'superinDeptName',
    title: '归口管理部门',
    showOverflow: true,
    width: 160,
  },
  {
    field: 'eneityScoreFuncDesc',
    title: '指标记分方式',
    showOverflow: true,
    width: 300,
  },
  {
    field: 'qOneGoal',
    title: '一季度目标',
    width: 160,
    type: 'html',
    formatter: ({ cellValue }) => {
      return cellValue.replace(/\n/g, '<br>');
    },
  },
  {
    field: 'qSedGoal',
    title: '二季度目标',
    width: 160,
    type: 'html',
    formatter: ({ cellValue }) => {
      return cellValue.replace(/\n/g, '<br>');
    },
  },
  {
    field: 'qThirdGoal',
    title: '三季度目标',
    width: 160,
    type: 'html',
    formatter: ({ cellValue }) => {
      return cellValue.replace(/\n/g, '<br>');
    },
  },
  {
    field: 'qFourthGoal',
    title: '四季度目标',
    width: 160,
    type: 'html',
    formatter: ({ cellValue }) => {
      return cellValue.replace(/\n/g, '<br>');
    },
  },
];
