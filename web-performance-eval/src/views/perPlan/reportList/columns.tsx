import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.queryOrgList;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    dataIndex: 'orgCode',
    width: 160,
    align: 'center',
    fixed: 'left',
    formItemProps: {
      colProps: {
        span: 6,
      },
    },
  },
  {
    title: '名称',
    dataIndex: 'orgName',
    ellipsis: true,
    width: 480,
    className: 'text-align',
    formItemProps: {
      colProps: {
        span: 6,
      },
    },
  },
  {
    title: '年份',
    dataIndex: 'period',
    ellipsis: true,
    align: 'center',
    width: 160,
    formItemProps: {
      colProps: {
        span: 6,
      },
    },
  },
];

export const reportColumn = [
  { type: 'seq', width: 60 },
  {
    field: 'title',
    title: '标题',
    width: 240,
  },
  { field: 'evaluateYerPeriod', title: '年份', width: 100 },
  {
    field: 'evaluateSenPeriodName',
    title: '季度',
    width: 100,
  },
  { title: '请示报告', width: 90, slots: { default: 'operateReport' } },
  { title: '公司KPI', width: 90, slots: { default: 'operateKpi' } },
  { title: '公司战略任务', width: 100, slots: { default: 'operateTask' } },
  { title: '部门得分', width: 90, slots: { default: 'operateScore' } },
  { title: '部门KPI', width: 90, slots: { default: 'operateDept' } },
  { title: '重点激励项目', width: 100, slots: { default: 'operateWork' } },
  { title: '附件下载', width: 90, slots: { default: 'operateDownload' } },
  { title: '操作', width: 100, slots: { default: 'operate' } },
];
