import { isEmpty } from 'lodash-es';
import dict from '@/utils/dict.js';

export const baseColumns = [
  {
    title: '部门',
    dataIndex: 'evaluatedOrgName',
    ellipsis: true,
    width: 120,
    formItemProps: {
      component: 'TreeSelect',
    },
  },
  {
    title: '指标类别',
    dataIndex: 'perfGCategory',
    ellipsis: true,
    width: 140,
    customRender({ record }) {
      const obj = dict.getValue('INDICATOR_CATEGORY', record.perfGCategory);
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
    formItemProps: {
      component: 'Select',
      componentProps: () => {
        const options = dict.getKeyValueByDictCode('INDICATOR_CATEGORY');
        return {
          options,
        };
      },
    },
  },
  {
    title: '序号',
    dataIndex: 'ruleCode',
    width: 100,
    hideInSearch: true,
  },
  {
    title: '指标名称',
    dataIndex: 'perfGName',
    width: 160,
  },
  {
    title: '权重',
    dataIndex: 'score',
    width: 100,
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      return record.score ? `${record.score}%` : '-';
    },
  },
  {
    title: '指标单位',
    dataIndex: 'perfGOrgCode',
    width: 80,
    align: 'center',
    customRender: ({ record }) => {
      if (record.perfGOrgCode != null) {
        const obj = dict.getValue('INDICATOR_UNIT', record.perfGOrgCode);
        if (!isEmpty(obj)) {
          return obj.itemText;
        } else {
          return '-';
        }
      } else {
        return '-';
      }
    },
    formItemProps: {
      component: 'Select',
      componentProps: () => {
        const options = dict.getKeyValueByDictCode('INDICATOR_UNIT');
        return {
          options,
        };
      },
    },
  },
  {
    title: '计算公式或指标简述',
    dataIndex: 'eneityResultFuncDesc',
    width: 180,
    hideInSearch: true,
  },
  {
    title: '当期/累计',
    dataIndex: 'timePeriod',
    width: 80,
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('TIME_PERIOD'),
      },
    },
    customRender({ record }) {
      const obj = record?.timePeriod ? dict.getValue('TIME_PERIOD', record?.timePeriod) : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  {
    title: '评价周期',
    dataIndex: 'evaluatePeriod',
    width: 80,
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('EVALUATION_PERIOD'),
      },
    },
    customRender({ record }) {
      const obj = record?.evaluatePeriod
        ? dict.getValue('EVALUATION_PERIOD', record?.evaluatePeriod)
        : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  {
    title: '归口管理部门',
    dataIndex: 'orgPerformanceName',
    width: 140,
    align: 'center',
  },
  {
    title: '指标计分方式',
    dataIndex: 'eneityScoreFunc',
    width: 220,
    hideInSearch: true,
  },
  // {
  //   title: '单项最高得分',
  //   dataIndex: 'period',
  //   width: 100,
  //   align: 'center',
  //   hideInSearch: true,
  //   customRender({ record }) {
  //     return <div style="color:red">{record.period}</div>;
  //   },
  //   customHeaderCell: () => ({
  //     style: {
  //       color: 'red',
  //     },
  //   }),
  // },
  {
    title: '年度目标',
    dataIndex: 'yearTarget',
    width: 240,
    hideInSearch: true,
  },
  {
    title: '第1季度目标',
    dataIndex: 'qOneGoal',
    width: 240,
    hideInSearch: true,
  },
  {
    title: '第2季度目标',
    dataIndex: 'qSedGoal',
    width: 240,
    hideInSearch: true,
  },
  {
    title: '第3季度目标',
    dataIndex: 'qThirdGoal',
    width: 240,
    hideInSearch: true,
  },
  {
    title: '第4季度目标',
    dataIndex: 'qFourthGoal',
    width: 240,
    hideInSearch: true,
  },
];
