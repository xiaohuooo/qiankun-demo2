import type { FormSchema } from '@/components/core/schema-form/';

export const formSchemas: FormSchema<API.CreatePreParams>[] = [
  {
    field: 'orgName',
    component: 'Input',
    label: '名称',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 24,
    },
    componentProps: {
      maxlength: 200,
    },
  },
  {
    field: 'period',
    component: 'DatePicker',
    label: '年份',
    rules: [{ required: true, message: '请选择年份', trigger: 'blur' }],
    colProps: {
      span: 24,
    },
    componentProps: {
      picker: 'year',
    },
  },
];

export const formSchemas1: FormSchema<API.CreatePreParams>[] = [
  {
    field: 'orgCode',
    label: '编号',
    component: 'Input',
    colProps: {
      span: 24,
    },
    componentProps: {
      disabled: true,
    },
  },
  {
    field: 'orgName',
    component: 'Input',
    label: '名称',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 24,
    },
    componentProps: {
      maxlength: 200,
    },
  },
  {
    field: 'period',
    component: 'DatePicker',
    label: '年份',
    rules: [{ required: true, message: '请选择年份', trigger: 'blur' }],
    colProps: {
      span: 24,
    },
    componentProps: {
      picker: 'year',
      disabled: true,
    },
  },
];
