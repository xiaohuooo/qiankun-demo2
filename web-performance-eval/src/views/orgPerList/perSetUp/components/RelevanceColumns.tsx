import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.QueryDefinitionParams;
export type TableColumnItem = TableColumn<TableListItem>;

const IndicatorCategory = dict.getKeyValueByDictCode('INDICATOR_CATEGORY');
export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    width: 110,
    align: 'center',
    dataIndex: 'perfGCode',
    hideInSearch: true,
  },
  {
    title: '目标名称',
    width: 180,
    dataIndex: 'perfGName',
    align: 'center',
  },
  {
    title: '指标类别',
    width: 160,
    dataIndex: 'perfGCategory',
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: IndicatorCategory,
      },
    },
    customRender({ record }) {
      const obj = dict.getValue('INDICATOR_CATEGORY', record?.perfGCategory);
      if (!isEmpty(obj)) {
        return (
          <div>
            <a-tooltip title={obj.itemText}>
              <a-tag>{obj.itemText}</a-tag>
            </a-tooltip>
          </div>
        );
      } else {
        return '-';
      }
    },
  },
  {
    title: '指标类型',
    width: 150,
    dataIndex: 'perfGType',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      const obj = dict.getValue('INDICATOR_TYPE', record?.perfGType);
      if (!isEmpty(obj)) {
        return (
          <div>
            <a-tooltip title={obj.itemText}>
              <a-tag>{obj.itemText}</a-tag>
            </a-tooltip>
          </div>
        );
      } else {
        return '-';
      }
    },
  },
  {
    title: '指标单位',
    width: 120,
    dataIndex: 'perfGOrgCode',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      const obj = record?.perfGOrgCode
        ? dict.getValue('INDICATOR_UNIT', record?.perfGOrgCode)
        : null;
      if (!isEmpty(obj)) {
        return (
          <div>
            <a-tooltip title={obj.itemText || ''}>
              <a-tag>{obj.itemText || ''}</a-tag>
            </a-tooltip>
          </div>
        );
      } else {
        return '-';
      }
    },
  },
  {
    title: '归口管理部门',
    width: 160,
    dataIndex: 'superinDeptName',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建人',
    width: 120,
    dataIndex: 'createName',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建时间',
    width: 180,
    dataIndex: 'createDate',
    align: 'center',
    hideInSearch: true,
  },
];
