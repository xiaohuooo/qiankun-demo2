import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.QuerySetUpListParams;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    width: 110,
    align: 'center',
    dataIndex: 'ruleCode',
  },
  {
    title: '目标名称',
    width: 220,
    dataIndex: 'perfGName',
    className: 'text-align',
  },
  {
    title: '指标类别',
    width: 150,
    dataIndex: 'perfGCategory',
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('INDICATOR_CATEGORY'),
      },
    },
    customRender({ record }) {
      const obj = record?.perfGCategory
        ? dict.getValue('INDICATOR_CATEGORY', record?.perfGCategory)
        : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  // {
  //   title: '指标类型',
  //   width: 100,
  //   dataIndex: 'perfGType',
  //   align: 'center',
  //   hideInTable: true,
  //   formItemProps: {
  //     component: 'Select',
  //     componentProps: {
  //       options: dict.getKeyValueByDictCode('INDICATOR_TYPE'),
  //     },
  //   },
  //   customRender({ record }) {
  //     const obj = record?.perfGType ? dict.getValue('INDICATOR_TYPE', record?.perfGType) : null;
  //     return !isEmpty(obj) ? obj.itemText : '-';
  //   },
  // },
  {
    title: '绩效类型',
    width: 100,
    dataIndex: 'performanceType',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      const obj = record?.performanceType
        ? dict.getValue('PERFORMANCE_TYPE', record?.performanceType)
        : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  {
    title: '权重',
    width: 100,
    dataIndex: 'score',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      return record.score ? `${record.score}%` : '-';
    },
  },
  {
    title: '归口管理部门',
    width: 160,
    dataIndex: 'superinDeptCode',
    align: 'center',
    hideInTable: true,
    formItemProps: {
      component: 'TreeSelect',
    },
  },
  {
    title: '归口管理部门',
    width: 160,
    dataIndex: 'orgPerformanceName',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '指标单位',
    width: 100,
    dataIndex: 'perfGOrgCode',
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('INDICATOR_UNIT'),
      },
    },
    customRender({ record }) {
      const obj = record?.perfGOrgCode
        ? dict.getValue('INDICATOR_UNIT', record?.perfGOrgCode)
        : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  {
    title: '时间周期',
    width: 100,
    dataIndex: 'timePeriod',
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('TIME_PERIOD'),
      },
    },
    customRender({ record }) {
      const obj = record?.timePeriod ? dict.getValue('TIME_PERIOD', record?.timePeriod) : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  {
    title: '评价周期',
    width: 100,
    dataIndex: 'evaluatePeriod',
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('EVALUATION_PERIOD'),
      },
    },
    customRender({ record }) {
      const obj = record?.evaluatePeriod
        ? dict.getValue('EVALUATION_PERIOD', record?.evaluatePeriod)
        : null;
      return !isEmpty(obj) ? obj.itemText : '-';
    },
  },
  {
    title: '审核状态',
    width: 100,
    dataIndex: 'auditStatus',
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('AUDIT_STATUS'),
      },
    },
    customRender({ record }) {
      const obj = record?.auditStatus
        ? dict.getItemText('AUDIT_STATUS', record?.auditStatus)
        : null;
      if (!isEmpty(obj)) {
        return obj;
      } else {
        return '-';
      }
    },
  },
  {
    title: '指标上限',
    width: 140,
    dataIndex: 'perfGUpperLimit',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '年度目标',
    width: 180,
    dataIndex: 'yearTarget',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '指标下限',
    width: 140,
    dataIndex: 'perfGLowerLimit',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '第1季度目标',
    width: 180,
    dataIndex: 'qOneGoal',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '第2季度目标',
    width: 180,
    dataIndex: 'qSedGoal',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '第3季度目标',
    width: 180,
    dataIndex: 'qThirdGoal',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '第4季度目标',
    width: 180,
    dataIndex: 'qFourthGoal',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '实绩公式描述',
    width: 200,
    dataIndex: 'eneityResultFuncDesc',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '计分公式描述',
    width: 200,
    dataIndex: 'eneityScoreFuncDesc',
    hideInSearch: true,
    ellipsis: true,
  },
];
