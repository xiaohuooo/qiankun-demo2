// import { isEmpty } from 'lodash-es';
import type { FormSchema } from '@/components/core/schema-form/';
import type { SelectProps } from 'ant-design-vue';

export const formulaSchemas: FormSchema<API.FormulaParams>[] = [
  {
    field: 'eneityResultFuncDesc',
    component: 'Input',
    label: '实绩公式描述',
    rules: [{ required: true, type: 'string' }],
    componentProps: {
      placeholder: '请输入200字符以内',
    },
  },
  {
    field: 'apiCode',
    component: 'Select',
    label: '实绩公式',
    // rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
    dynamicRules: ({ formModel }) => {
      console.log(formModel);
      return [
        {
          required: true,
          type: 'string',
          max: 100,
        },
      ];
    },
    componentProps: () => {
      const options: SelectProps['options'] = [
        { label: '实绩1(p1)', value: 1 },
        { label: '实绩2(p2)', value: 2 },
        { label: '实绩3(p3)', value: 3 },
        { label: '实绩4(p4)', value: 4 },
        { label: '实绩5(p5)', value: 5 },
      ];
      return {
        options,
      };
    },
  },
  {
    field: 'eneityResultFunc',
    component: 'InputTextArea',
    label: '实绩结果代码',
    // rules: [{ required: true, type: 'string' }],
    componentProps: {
      placeholder: '',
    },
  },
  {
    field: 'eneityScoreFuncDesc',
    component: 'Input',
    label: '计分公式描述',
    rules: [{ required: true, type: 'string' }],
    componentProps: {
      placeholder: '',
    },
  },
  {
    field: 'eneityScoreFunc',
    component: 'InputTextArea',
    label: '计分公式',
    rules: [{ required: true, type: 'string' }],
    componentProps: {
      placeholder: '',
    },
  },
  {
    field: 'ruleCode',
    component: 'Input',
    vShow: false,
  },
];
