import type { FormSchema } from '@/components/core/schema-form/';
import { getRoleListPage } from '@/api/system/role';

export const userSchemas: FormSchema<API.CreateUserParams>[] = [
  {
    field: 'deptId',
    component: 'TreeSelect',
    label: '所属部门',
    componentProps: {
      getPopupContainer: () => document.body,
    },
    rules: [{ required: true, type: 'number' }],
  },
  {
    field: 'roles',
    component: 'Select',
    label: '所属角色',
    rules: [{ required: true, type: 'array' }],
    componentProps: {
      mode: 'multiple',
      request: async () => {
        const data = (await getRoleListPage({ pageNum: 1, pageSize: 10000 })) as any;
        return data.data.map((n) => ({ label: n.roleName, value: n.roleId }));
      },
    },
  },
  {
    field: 'username',
    component: 'Input',
    label: '用户名',
    rules: [{ required: true }],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'employeeCode',
    component: 'Input',
    label: '工号',
    rules: [{ required: true }],
    colProps: {
      span: 12,
    },
    componentProps: {
      disabled: false,
    },
  },
  {
    field: 'email',
    component: 'Input',
    label: '邮箱',
    colProps: {
      span: 12,
    },
    rules: [{ type: 'email', message: '请正确输入邮箱' }],
  },
  {
    field: 'mobile',
    component: 'Input',
    label: '手机',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^1[3|4|5|7|8][0-9]\d{8}$/, message: '请正确输入手机号' }],
  },
  {
    field: 'status',
    component: 'RadioGroup',
    label: '状态',
    defaultValue: '1',
    componentProps: {
      options: [
        {
          label: '启用',
          value: '1',
        },
        {
          label: '禁用',
          value: '0',
        },
      ],
    },
  },
  {
    field: 'ssex',
    component: 'RadioGroup',
    label: '性别',
    defaultValue: '0',
    componentProps: {
      options: [
        {
          label: '男',
          value: '0',
        },
        {
          label: '女',
          value: '1',
        },
        {
          label: '保密',
          value: '2',
        },
      ],
    },
  },
  {
    field: 'deptIds',
    component: 'Tree',
    label: '部门权限',
    colProps: {
      span: 24,
    },
    componentProps: {
      checkable: true,
      checkStrictly: true,
      vModelKey: 'checkedKeys',
      style: {
        height: '350px',
        paddingTop: '5px',
        overflow: 'auto',
        borderRadius: '6px',
        border: '1px solid #dcdfe6',
      },
    },
  },
];
/**
 * @description 更新用户密码
 */
export const updatePswSchemas: FormSchema[] = [
  {
    field: 'password',
    component: 'Input',
    label: '新密码',
    rules: [{ required: true, type: 'string' }],
  },
];
