import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '#',
    dataIndex: 'roleId',
    width: 55,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '名称',
    width: 200,
    align: 'center',
    dataIndex: 'roleName',
  },
  {
    title: '备注',
    dataIndex: 'remark',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '更新时间',
    align: 'center',
    dataIndex: 'modifyTime',
    hideInSearch: true,
  },
];
