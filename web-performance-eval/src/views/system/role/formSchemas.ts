import type { FormSchema } from '@/components/core/schema-form/';

export const roleSchemas: FormSchema<API.CreateRoleParams>[] = [
  {
    field: 'name',
    component: 'Input',
    label: '名称',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 24,
    },
  },
  {
    field: 'remark',
    component: 'InputTextArea',
    label: '备注',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'menus',
    component: 'Tree',
    label: '菜单权限',
    colProps: {
      span: 24,
    },
    componentProps: {
      checkable: true,
      checkStrictly: true,
      vModelKey: 'checkedKeys',
      style: {
        height: '350px',
        paddingTop: '5px',
        overflow: 'auto',
        borderRadius: '6px',
        border: '1px solid #dcdfe6',
      },
    },
  },
];
