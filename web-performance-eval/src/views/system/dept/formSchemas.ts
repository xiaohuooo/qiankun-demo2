import type { FormSchema } from '@/components/core/schema-form/';

export const deptSchemas: FormSchema<API.CreateDeptParams>[] = [
  {
    field: 'title',
    component: 'Input',
    label: '部门名称',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
    vShow: false,
  },
  {
    field: 'parentId',
    component: 'TreeSelect',
    label: '父级部门',
    colProps: {
      span: 12,
    },
    componentProps: {
      fieldNames: {
        label: 'title',
        value: 'value',
      },
      getPopupContainer: () => document.body,
    },
    rules: [{ required: true, type: 'number' }],
    vShow: false,
  },
  {
    field: 'orderNum',
    component: 'InputNumber',
    label: '排序号',
    defaultValue: 255,
    componentProps: {
      style: {
        width: '100%',
      },
    },
  },
];
