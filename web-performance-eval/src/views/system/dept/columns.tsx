import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '#',
    dataIndex: 'id',
    width: 55,
    align: 'center',
  },
  {
    title: '名称',
    width: 150,
    align: 'center',
    dataIndex: 'title',
  },
  {
    title: '备注',
    width: 150,
    dataIndex: 'text',
    align: 'center',
  },
  {
    title: '创建时间',
    width: 120,
    dataIndex: 'createTime',
    align: 'center',
  },
  {
    title: '更新时间',
    width: 120,
    align: 'center',
    dataIndex: 'modifyTime',
  },
];
