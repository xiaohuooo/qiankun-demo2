// import MultipleCascader from './components/multiple-cascader/index.vue';
import type { FormSchema } from '@/components/core/schema-form/';
import IconsSelect from '@/components/basic/icons-select/index.vue';
import { constantRouterComponents } from '@/router/asyncModules';

const getMenuOptions = () => {
  const options = Object.keys(constantRouterComponents).map((n) => ({ label: n, value: n }));

  options.push({
    label: 'PageView',
    value: 'PageView',
  });
  return options;
};

export const menuSchemas: FormSchema<API.MenuAddParams>[] = [
  {
    field: 'type',
    component: 'RadioGroup',
    label: '菜单类型',
    defaultValue: 0,
    rules: [{ required: true, type: 'number' }],
    componentProps: {
      options: [
        {
          label: '菜单',
          value: 0,
        },
        {
          label: '资源',
          value: 1,
        },
      ],
    },
  },
  {
    field: 'menuName',
    component: 'Input',
    label: '节点名称',
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'parentId',
    component: 'TreeSelect',
    label: '上级节点',
    componentProps: {
      fieldNames: {
        label: 'name',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
    rules: [{ required: true, type: 'number' }],
  },
  {
    field: 'path',
    component: 'Input',
    label: '节点路由',
    vIf: ({ formModel }) => formModel['type'] === 0,
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'perms',
    component: 'Input',
    label: '权限',
    vIf: ({ formModel }) => formModel['type'] === 1,
    rules: [{ required: true, type: 'string', message: '请输入权限' }],
  },
  {
    field: 'component',
    component: 'Select',
    label: '文件路径',
    vIf: ({ formModel }) => formModel['type'] === 0,
    componentProps: {
      options: getMenuOptions(),
    },
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'icon',
    component: () => IconsSelect,
    label: '节点图标',
    vIf: ({ formModel }) => formModel['type'] !== 1,
  },
  {
    field: 'keepAlive',
    component: 'Switch',
    label: '是否缓存',
    defaultValue: '10',
    vIf: ({ formModel }) => formModel['type'] == 0,
  },
  {
    field: 'showInSide',
    component: 'Switch',
    label: '是否显示',
    defaultValue: 1,
    vIf: ({ formModel }) => formModel['type'] != 1,
  },
  {
    field: 'orderNum',
    component: 'InputNumber',
    label: '排序号',
    defaultValue: 255,
    componentProps: {
      style: {
        width: '100%',
      },
    },
  },
];
