import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.queryOrgList;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    dataIndex: 'orgCode',
    width: 160,
    align: 'center',
    fixed: 'left',
    formItemProps: {
      colProps: {
        span: 6,
      },
    },
  },
  {
    title: '名称',
    dataIndex: 'orgName',
    ellipsis: true,
    width: 480,
    className: 'text-align',
    formItemProps: {
      colProps: {
        span: 6,
      },
    },
  },
  {
    title: '年份',
    dataIndex: 'period',
    ellipsis: true,
    align: 'center',
    width: 160,
    formItemProps: {
      colProps: {
        span: 6,
      },
    },
  },
];
