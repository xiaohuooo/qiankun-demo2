import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.queryScoreDatalist;
export type TableColumnItem = TableColumn<TableListItem>;

export const scoreColumns = [
  {
    title: '序号',
    width: 80,
    align: 'center',
    fixed: 'left',
    customRender: (column) => {
      return column.index + 1;
    },
  },
  {
    title: '目标名称',
    dataIndex: 'perfGName',
    fixed: 'left',
    width: 160,
  },
  {
    title: '权重',
    dataIndex: 'score',
    ellipsis: true,
    align: 'center',
    width: 80,
    customRender({ record }) {
      return record.score ? `${record.score}%` : '-';
    },
  },
  {
    title: '归口管理部门',
    dataIndex: 'superinDeptName',
    width: 120,
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '指标单位',
    dataIndex: 'perfGOrgCode',
    align: 'center',
    width: 110,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.perfGOrgCode != null) {
        const obj = dict.getValue('INDICATOR_UNIT', record.perfGOrgCode);
        if (!isEmpty(obj)) {
          return obj.itemText;
        } else {
          return '-';
        }
      } else {
        return '-';
      }
    },
  },
  {
    title: '年度目标',
    dataIndex: 'yearTarget',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '本季度目标',
    dataIndex: 'quarterTarget',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '上季度实绩',
    dataIndex: 'upQuarterAchievements',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '本季度实绩',
    dataIndex: 'quarterAchievements',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
];
export const MoreColumns = [
  {
    title: '本季度得分',
    dataIndex: 'quarterScore',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
  {
    title: '调整得分',
    dataIndex: 'perfEvalScoreFixValue',
    align: 'center',
    width: 180,
    hideInSearch: true,
  },
  {
    title: '调整理由',
    dataIndex: 'description',
    align: 'center',
    width: 240,
    hideInSearch: true,
  },
  {
    title: '本季度权重得分',
    dataIndex: 'quarterWeightScore',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
];
/**
 * 绩效查看
 */
export const ViewMoreColumns = [
  {
    title: '本季度权重得分',
    dataIndex: 'quarterWeightScore',
    align: 'center',
    width: 140,
    hideInSearch: true,
  },
];
