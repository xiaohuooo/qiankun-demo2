import type { FormSchema } from '@/components/core/schema-form/';
export const dictSchemas: FormSchema[] = [
  {
    field: 'dictName',
    component: 'Input',
    label: '专项',
  },
  {
    field: 'periodYear',
    component: 'Input',
    label: '年份',
  },
  {
    field: 'dictName',
    component: 'Input',
    label: '原总分数值',
  },
  {
    field: 'dictCode',
    component: 'Input',
    label: '调整值',
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'dictName',
    component: 'Input',
    label: '调整后总分',
  },
  {
    field: 'dictName',
    component: 'InputTextArea',
    label: '情况说明',
  },
];
