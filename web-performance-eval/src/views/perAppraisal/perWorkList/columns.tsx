import { isEmpty } from 'lodash-es';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';

export type TableListItem = API.queryAppraisalList;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '编号',
    width: 110,
    align: 'center',
    dataIndex: 'ruleCode',
    fixed: 'left',
  },
  {
    title: '目标名称',
    width: 150,
    dataIndex: 'perfGName',
    className: 'text-align',
    fixed: 'left',
  },
  {
    title: '指标类别',
    width: 100,
    dataIndex: 'perfGCategory',
    align: 'center',
    fixed: 'left',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('INDICATOR_CATEGORY'),
      },
    },
    customRender({ record }) {
      const obj = record?.perfGCategory
        ? dict.getValue('INDICATOR_CATEGORY', record?.perfGCategory)
        : null;
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '绩效类型',
    width: 100,
    dataIndex: 'performanceType',
    align: 'center',
    fixed: 'left',
    hideInSearch: true,
    customRender({ record }) {
      const obj = record?.performanceType
        ? dict.getValue('PERFORMANCE_TYPE', record?.performanceType)
        : null;
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '归口管理部门',
    width: 160,
    dataIndex: 'superinDeptCode',
    align: 'center',
    hideInTable: true,
    formItemProps: {
      component: 'TreeSelect',
    },
  },
  {
    title: '归口管理部门',
    width: 120,
    dataIndex: 'superinDeptName',
    align: 'center',
    hideInSearch: true,
  },

  {
    title: '年度目标',
    width: 300,
    dataIndex: 'yearTarget',
    hideInSearch: true,
    className: 'text-align',
  },
  {
    title: '第1季度目标',
    width: 300,
    dataIndex: 'qOneGoal',
    hideInSearch: true,
    className: 'text-align',
  },
  {
    title: '第1季度完成情况',
    width: 130,
    dataIndex: 'qOneGoal',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      let obj: any = null;
      if (record?.orgPerfScoreList.length > 0) {
        obj = dict.getValue('EMPHASIS_WORK_SHOW_TYPE', record?.orgPerfScoreList[0].qActualValue);
      }
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '第2季度目标',
    width: 300,
    dataIndex: 'qSedGoal',
    hideInSearch: true,
    className: 'text-align',
  },
  {
    title: '第2季度完成情况',
    width: 130,
    dataIndex: 'qOneGoal',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      let obj: any = null;
      if (record?.orgPerfScoreList.length > 1) {
        obj = dict.getValue('EMPHASIS_WORK_SHOW_TYPE', record?.orgPerfScoreList[1].qActualValue);
      }
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '第3季度目标',
    width: 300,
    dataIndex: 'qThirdGoal',
    hideInSearch: true,
    className: 'text-align',
  },
  {
    title: '第3季度完成情况',
    width: 130,
    dataIndex: 'qOneGoal',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      let obj: any = null;
      if (record?.orgPerfScoreList.length > 2) {
        obj = dict.getValue('EMPHASIS_WORK_SHOW_TYPE', record?.orgPerfScoreList[2].qActualValue);
      }
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '第4季度目标',
    width: 300,
    dataIndex: 'qFourthGoal',
    hideInSearch: true,
    className: 'text-align',
  },
  {
    title: '第4季度完成情况',
    width: 130,
    dataIndex: 'qOneGoal',
    align: 'center',
    hideInSearch: true,
    customRender({ record }) {
      let obj: any = null;
      if (record?.orgPerfScoreList.length > 3) {
        obj = dict.getValue('EMPHASIS_WORK_SHOW_TYPE', record?.orgPerfScoreList[3].qActualValue);
      }
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
];
