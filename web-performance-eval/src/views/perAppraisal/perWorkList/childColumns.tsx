import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.childList;
export type TableColumnItem = TableColumn<TableListItem>;

export const childBaseColumns: TableColumnItem[] = [
  {
    title: '数据项',
    // width: 180,
    dataIndex: 'subtaskName',
    className: 'text-align',
  },
  {
    title: '第一季度实绩',
    width: 160,
    dataIndex: '3',
    align: 'center',
  },
  {
    title: '第二季度实绩',
    width: 160,
    dataIndex: '6',
    align: 'center',
  },
  {
    title: '第三季度实绩',
    width: 160,
    dataIndex: '9',
    align: 'center',
  },
  {
    title: '第四季度实绩',
    width: 160,
    dataIndex: '12',
    align: 'center',
  },
];
