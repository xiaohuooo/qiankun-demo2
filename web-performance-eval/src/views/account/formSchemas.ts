export const userEditorSchemas = [
  {
    field: 'email',
    component: 'Input',
    label: '邮箱',
  },
  {
    field: 'mobile',
    component: 'Input',
    label: '手机',
  },
  {
    field: 'ssex',
    component: 'RadioGroup',
    label: '性别',
    defaultValue: '0',
    componentProps: {
      options: [
        {
          label: '男',
          value: '0',
        },
        {
          label: '女',
          value: '1',
        },
        {
          label: '保密',
          value: '2',
        },
      ],
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '描述',
  },
];
