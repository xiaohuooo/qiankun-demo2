export default function getFilterDataMixin() {
  const filterData = (data: Array<[]>) => {
    const filters1 = JSON.parse(JSON.stringify(data));
    const filters = [] as any[];
    if (filters1 && filters1.length > 0) {
      filters1.forEach((item) => {
        const list = [] as any[];
        item.forEach((v) => {
          let obj = {};
          const val = [] as any[];
          if (v.value1) {
            val.push(v.value1);
          }
          if (v.value2) {
            val.push(v.value2);
          }
          let ctype = '';
          if (v.type === 'number') {
            ctype = '';
          } else if (v.type === 'date') {
            ctype = '4';
          } else if (v.type === 'dict') {
            ctype = '';
          }
          let values = val;
          if (v.type === 'select') {
            values = val[0];
          }
          obj = {
            columnName: v.field,
            filterType: v.operatorValue,
            columnType: ctype,
            values,
          };
          list.push(obj);
        });
        filters.push(list);
      });
    }
    return filters;
  };
  return {
    filterData,
  };
}
