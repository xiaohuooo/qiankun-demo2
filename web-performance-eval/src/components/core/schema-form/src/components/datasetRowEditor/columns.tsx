export const columns = [
  {
    title: '字段中文名',
    align: 'center',
    width: 100,
    dataIndex: 'columnCname',
    editFormItemProps: {
      rules: [{ required: true, message: '请输入中文名' }],
    },
  },
  {
    title: '字段英文名',
    align: 'center',
    width: 150,
    dataIndex: 'columnName',
    editable: () => {
      return false;
    },
  },
  {
    title: '字段类型',
    align: 'center',
    dataIndex: 'type',
    width: 80,
    editable: () => {
      return false;
    },
  },
  {
    title: '值集映射',
    width: 80,
    dataIndex: 'dictId',
    /** 搜索表单配置 */
    formItemProps: {
      component: 'Select',
    },
    /** 可编辑行表单配置 */
    editFormItemProps: {
      /** 继承 formItemProps 的属性配置, 默认就是是true，这里为了演示有这个字段开关，所以特别写上 */
      extendSearchFormProps: true,
    },
  },
  {
    title: '排序',
    width: 70,
    dataIndex: 'sort',
  },
  {
    title: '是否主键',
    width: 90,
    dataIndex: 'isPrikey',
    formItemProps: {
      component: 'Checkbox',
    },
    customRender: ({ record }) => (record.isPrikey ? '是' : '否'),
  },
];
