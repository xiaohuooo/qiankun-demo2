import { computed, getCurrentInstance, ref, onBeforeUnmount, nextTick } from 'vue';
import { debounce } from 'lodash-es';
import type { DynamicTableProps } from '../dynamic-table';

type UseScrollParams = {
  props: DynamicTableProps;
};

export type UseScrollType = ReturnType<typeof useScroll>;

// 获取元素到顶部距离-通用方法
export const getPositionTop = (node: HTMLElement) => {
  let top = node.offsetTop;
  let parent = node.offsetParent as HTMLElement;
  while (parent != null) {
    top += parent.offsetTop;
    parent = parent.offsetParent as HTMLElement;
  }
  return top; // 所有的父元素top和
};

export const useScroll = ({ props }: UseScrollParams) => {
  const currIns = getCurrentInstance();
  const scrollY = ref<number>();
  const withHeader = ref<boolean>(true);

  const scroll = computed(() => {
    return {
      y: scrollY.value,
      ...props.scroll,
    };
  });

  const getScrollY = debounce(() => {
    if (!props.autoHeight) return;
    const compRootEl = currIns?.proxy?.$el as HTMLDivElement;
    const el: HTMLElement | null =
      compRootEl?.querySelector('.ant-table-body') || compRootEl?.querySelector('.ant-table-tbody');
    if (el) {
      let y = document.documentElement.offsetHeight;
      if (withHeader.value) {
        y = y - getPositionTop(el as HTMLDivElement) - 120;
      } else {
        y = y - 180;
      }
      // 简单粗糙的实现
      const height = y;

      // console.log(1111, height);
      scrollY.value = height;
      el.style.height = `${height}px`;
      el.style.maxHeight = `${height}px`;
      // console.log('innerScroll.value', el.style, scrollY.value);
    }
  });

  const redoGetScrollY = () => {
    withHeader.value = true;
    nextTick(() => {
      getScrollY();
    });
  };
  const redoGetWithoutHeaderScrollY = () => {
    withHeader.value = false;
    nextTick(() => {
      getScrollY();
    });
  };

  setTimeout(() => {
    getScrollY();
  });
  window.addEventListener('resize', redoGetScrollY);

  onBeforeUnmount(() => {
    window.removeEventListener('resize', redoGetScrollY);
  });

  return {
    scroll,
    redoGetScrollY,
    redoGetWithoutHeaderScrollY,
  };
};
