import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 实绩管理 - 实绩管理列表;
export function getSetting(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/data/query/setting`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查询组织绩效标签-根据组织编号
export function getBy(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/relation/perf/tag/get/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

/***    二期需求 新接口如下 */
//查询重点工作列表
export function queryWorkList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/emphasis/query/adjust/display`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//重点工作提交审核接口
export function sumbitApproval(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/perf/approval/submit/approval/batch`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//重点工作审核时查询比列接口
export function queryProportion(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/perf/bonus/allocation/ratio/count`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查询实际数据tab数据
export function getTabList() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/goals/tag/get/by/userDeptCode`,
      method: 'get',
    },
    {
      // isPromise: true,
      // isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
