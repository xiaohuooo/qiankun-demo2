import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
export function getDictList(data: API.PageRequest) {
  return request<API.TableListResult<API.DictListPageResult>>(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/query/dataDict`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function createDict(data: API.CreateDictParams) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/add/dataDict`,
      method: 'post',
      data,
    },
    {
      successMsg: '创建成功',
      isJsonBody: true,
    },
  );
}

export function updateDict(data) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/update/dataDict`,
      method: 'post',
      data,
    },
    {
      successMsg: '更新值集成功',
      isJsonBody: true,
    },
  );
}

export function deleteDict(idList) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/delete/dataDict`,
      method: 'post',
      data: idList,
    },
    {
      successMsg: '更新值集成功',
      isJsonBody: true,
    },
  );
}
