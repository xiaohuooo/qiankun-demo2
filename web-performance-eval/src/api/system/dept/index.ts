import { omit } from 'lodash-es';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

// ---------------  added by zhujun
export function getDeptAll() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}dept`,
      method: 'get',
    },
    {},
  );
}
//只查询到一级部门
export function getDeptAllTwo() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}dept?parentId=0&deptId=0`,
      method: 'get',
    },
    {},
  );
}
/** 查询用户配置的部门 **/
export function queryDept(userId) {
  return request({
    url: `${UriPrefix.URL_PORTAL_PREFIX}dept/deptList/${userId}`,
    method: 'get',
  });
}

export function deleteDept(deptIds: number[]) {
  const ids = deptIds.join(',');
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}dept/${ids}`,
      method: 'delete',
    },
    {
      successMsg: '删除成功',
    },
  );
}
export function updateDept(data) {
  const rdata = omit(data, ['title']) as any;
  rdata.deptName = data.title;
  console.log('deptName', data, rdata);

  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}dept`,
      method: 'put',
      data: rdata,
    },
    {},
  );
}

/**
 * @description 创建部门
 * @param {API.CreateDeptParams} data 参数
 * @returns
 */
export function createDept(data: API.CreateDeptParams) {
  const rdata = omit(data, ['title']) as any;
  rdata.deptName = data.title;
  console.log('deptName', data, rdata);

  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}dept`,
      method: 'post',
      data: rdata,
    },
    {
      successMsg: '新增部门成功',
      errorMsg: '新增部门异常',
    },
  );
}
//获取一级部门
export function getDeptOne() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/query/dept/level/one`,
      method: 'get',
    },
    {},
  );
}
