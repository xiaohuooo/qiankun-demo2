import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// ---- added by zhujun
export function getMenuAll() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}menu`,
      method: 'get',
    },
    {},
  );
}

export function createMenu(data: API.MenuAddParams) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}menu`,
      method: 'post',
      data,
    },
    {
      successMsg: '创建成功',
    },
  );
}

export function updateMenu(data: API.MenuUpdateParams) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}menu`,
      method: 'put',
      data,
    },
    {
      successMsg: '更新成功',
    },
  );
}

export function deleteMenu(menuId: number) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}menu/${menuId}`,
      method: 'delete',
    },
    {
      successMsg: '删除成功',
    },
  );
}
