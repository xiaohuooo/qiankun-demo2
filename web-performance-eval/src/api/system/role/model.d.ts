declare namespace API {
  /** 角色列表项 */
  type RoleListResultItem = {
    createTime: Date;
    modifyTime: Date;
    roleId: number;
    roleName: string;
    remark: string;
  };

  /** 角色列表 */
  type RoleListResult = RoleListResultItem[];

  // --------  add by zhujun
  /** 新增角色 */
  type CreateRoleParams = {
    name: string;
    label: string;
    remark: string;
    menus: Key[];
  };

  /** 创建目标定义参数 */
  type QueryDefinitionParams = {
    perfGName: string;
    perfGDesc: string;
    perfGCode: string;
    perfGCategory: string;
    perfGType: string;
    superinDeptCode: string;
    superinDeptName: string;
    perfGOrgCode: string;
    perfGOrgName: string;
    createUser: string;
    createName: string;
    createDate: string;
  };
}
