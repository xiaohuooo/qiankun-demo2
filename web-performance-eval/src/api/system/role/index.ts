import { omit } from 'lodash-es';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// import type { BaseResponse } from '@/utils/request';

// --- added by zhujun
export function getRoleListPage(data: API.PageRequest) {
  return request<API.TableListResult<API.RoleListResult>>(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}role`,
      method: 'get',
      data,
    },
    {},
  );
}

export function createRole(data: any) {
  console.log('hd-角色新增', data);
  const rdata = {
    roleName: data.name,
    remark: data.remark,
    menuId: data.menus.checked.join(','),
  };

  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}role`,
      method: 'post',
      data: rdata,
    },
    {
      successMsg: '创建角色成功',
    },
  );
}

export function updateRole(data) {
  const rdata = omit(data, ['name', 'menus']) as any;
  rdata.roleName = data.name;
  rdata.menuId = data.menus.checked.join(',');

  console.log('data rdata', data, rdata);
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}role`,
      method: 'put',
      data: rdata,
    },
    {
      successMsg: '更新角色成功',
    },
  );
}

export function deleteRole(roleIds: number[]) {
  const ids = roleIds.join(',');
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}role/${ids}`,
      method: 'delete',
    },
    {
      successMsg: '删除角色成功',
    },
  );
}

export function getRoleMenus(data) {
  return request<number[]>(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}role/menu/roleId`,
      method: 'get',
      data,
    },
    {},
  );
}
