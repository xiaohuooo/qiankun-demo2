declare namespace API {
  /** 创建部门参数 */
  type CreatePerforParams = {
    seeValue: string;
    dataSourceCode: number;
    dataSourceDemo: string;
    dataSourceDesc: string;
    dataSourceName: string;
    dataSourceAddr: string;
    manageFields: string;
    createName: string;
    createDate: string;
    updateName: string;
    updateDate: string;
  };
}
