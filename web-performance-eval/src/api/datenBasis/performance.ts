import { omit } from 'lodash-es';
import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
export function getPerformance(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
/**
 * @description 创建部门
 * @param {API.CreatePerforParams} data 参数
 * @returns
 */
export function CreatePerfor(data: API.CreatePerforParams) {
  const rdata = omit(data) as any;
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/upsert`,
      method: 'post',
      data: rdata,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
export function deletePerfor(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/datasource/delete`,
      method: 'post',
      data,
    },
    {
      successMsg: '删除成功',
      errorMsg: '删除失败',
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 字段名称重复校验
export function remoteCheckcolumnName(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/dataColumns/name/check`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 列表名称重复校验
export function remoteCheckSourceName(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/datasource/name/check`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
