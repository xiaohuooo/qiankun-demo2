import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

//会议材料 新增
export function addMeetingItem(data: any) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dialogue/meeting/upsert`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查询会议材料列表
export function queryMeetingList(data: any) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dialogue/meeting/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//删除会议材料列表
export function deleteMeetingItem(data: any) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dialogue/meeting/delete/by/${data}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
