import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 集团KPI指标
export function groupKPI(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/statement/query/group/KPI/index`,
      method: 'post',
      data,
    },
    {
      isPromise: false,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 集团战略任务
export function groupStrategic(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/statement/query/group/strategic/planning`,
      method: 'post',
      data,
    },
    {
      isPromise: false,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 绩效-打分汇总
export function groupSummarizing(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}n/statement/query/mark/summarizing`,
      method: 'post',
      data,
    },
    {
      isPromise: false,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 重点激励项目
export function groupStimulate(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/statement/query/performance/stimulate/project`,
      method: 'post',
      data,
    },
    {
      isPromise: false,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
