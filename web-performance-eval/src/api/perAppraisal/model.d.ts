declare namespace API {
  /** 查询绩效设定列表定义参数 */
  type QuerySetUpListParams = {
    ruleCode: string;
    perfGName: string;
    perfGCategory: string;
    perfGType: string;
    performanceType: string;
    score: string;
    orgPerformanceName: string;
    perfGOrgCode: string;
    timePeriod: string;
    evaluatePeriod: string;
    auditStatus: string;
    perfGUpperLimit: string;
    yearTarget: string;
    perfGLowerLimit: string;
    qOneGoal: string;
    qSedGoal: string;
    qThirdGoal: string;
    qFourthGoal: string;
    eneityResultFuncDesc: string;
    eneityScoreFuncDesc: string;
  };
  /** 查询组织绩效列表定义参数 */
  type queryOrgList = {
    orgCode: string;
    orgName: string;
    period: string;
    createName: string;
    createDate: string;
    updateName: string;
    updateDate: string;
  };
  /** 查询组织绩效列表-绩效考核列表定义参数 */
  type childList = {
    details: Array;
    columnCnName: string;
    oneAchievements: string;
    twoAchievements: string;
    threeAchievements: string;
    fourAchievements: string;
    fiveAchievements: string;
    sixAchievements: string;
    sevenAchievements: string;
    eightAchievements: string;
    nineAchievements: string;
    tenAchievements: string;
    elevenAchievements: string;
    twelveAchievements: string;
    yearAchievements: string;
  };
  type queryAppraisalList = {
    ruleCode: string;
    perfGName: string;
    perfGCategory: string;
    perfGType: string;
    performanceType: string;
    score: string;
    orgPerformanceName: string;
    perfGOrgCode: string;
    timePeriod: string;
    evaluatePeriod: string;
    yearTarget: string;
    qOneGoal: string;
    qSedGoal: string;
    qThirdGoal: string;
    qFourthGoal: string;
    perfScoreDetailList: Array;
    orgPerfScoreList: Array;
    yearAchievements: string;
    yearScore: string;
    drillingDetail: Array;
    qOneGoalScore: {
      qActualValue: string;
      qGoalScore: string;
      qScoreGoalScore: string;
      completionRate: string;
    };
    qSedGoalScore: {
      qActualValue: string;
      qGoalScore: string;
      qScoreGoalScore: string;
      completionRate: string;
    };
    qThirdGoalScore: {
      qActualValue: string;
      qGoalScore: string;
      qScoreGoalScore: string;
      completionRate: string;
    };
    qFourthGoalScore: {
      qActualValue: string;
      qGoalScore: string;
      qScoreGoalScore: string;
      completionRate: string;
    };
  };
  /** 得分汇总列表参数 */
  type summarizingQuery = {
    evaluatedOrgName: string;
    perfGName: string;
    periodYear: string;
    oneGoalScore: number;
    oneGoalScoreSpecial: string;
    oneGoalScoreSum: string;
    sedGoalScore: number;
    sedGoalScoreSpecial: string;
    thirdGoalScore: number;
    thirdGoalScoreSpecial: string;
    thirdGoalScoreSum: string;
    fourthGoalScore: number;
    fourthGoalScoreSpecial: string;
    fourthGoalScoreSum: string;
    yearGoalScoreSum: string;
  };
  /** 实绩管理-填报与调整查询页面 */
  type queryReportAdjust = {
    fieldName: string;
    monthPeriod: string;
    evaluatedOrgName: string;
    perfGName: string;
    fieldType: string;
    fieldValue: string;
    test1: string;
    fieldEndValue: string;
    materialInfo: string;
    description: string;
    perfEvalScoreFixValue: string;
  };
  /** 实绩管理-填报重点工作弹框 */
  type queryReportWork = {
    fieldName: string;
    monthPeriod: string;
    evaluatedOrgName: string;
    perfGName: string;
    perfEvalScoreFixOpt: string;
    fileName: string;
    fileInfo: Array;
    description?: string;
    unfinishedDescription?: string;
    rowSpan?: any;
    yearEmphasisTarget?: any;
    qEmphasisGoal?: any;
  };
  /** 查询得分列表 */
  type queryScoreDatalist = {
    perfGName?: string;
    perfGCategory?: string;
    score?: string;
    superinDeptName?: string;
    perfGOrgCode?: string;
    yearTarget?: string;
    quarterTarget?: string;
    upQuarterAchievements?: string;
    quarterAchievements?: string;
    quarterScore?: string;
    perfEvalScoreFixValue?: string;
    description?: string;
    quarterWeightScore?: string;
  };
}
