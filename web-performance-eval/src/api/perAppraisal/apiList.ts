import { request, getDownload, downloadJson } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 分页查询组织绩效
export function queryOrgList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/query/List`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询绩效考核
export function queryAppraisalList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/appraisal/perf/query/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询重点工作绩效考核
export function queryPerformanceList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/performance/appraisal/perf/query/by/orgCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询得分汇总列表
export function summarizingQuery(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}n/org/goal/score/summarizing/query`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 实绩管理-填报与调整查询页面
export function queryAdjust(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/source/data/query/adjust/display`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 实绩管理-填报与调整修改
export function saveAuditInfo(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/source/data/fieLd/modify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//查询绩效汇总tab详情
export function queryQuarterly(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/appraisal/perf/and/adjust/quarterly/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
// 绩效实绩-调整得分保存
export function SaveAdjust(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/score/approval/adjust`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 绩效汇总-得分调整查询接口
export function QueryAdjust(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/adjust/org/perf/score/approval/detail`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 绩效汇总-得分调整保存接口
export function SureAdjust(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/perf/score/approval/audit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

/******   二期需求  ******/
// 查询得分列表
export function queryScoreList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/perf/score/adjust/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 查询得分列表，仅用于绩效填报人员
export function queryExplainList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/perf/score/explain/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//提交得分调整列表数据
export function submitScoreList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/org/perf/score/adjust/submit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//提交得分调整列表数据
export function refreScoreList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/score/refresh`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
/** 重点工作 **/
//暂存接口 ---新接口，目前仅用于重点工作
export function saveWorkDraft(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/focus/work/nature/audit/save/draft`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//保存接口 ---新接口，目前仅用于重点工作
export function saveWorkData(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/focus/work/nature/audit/saveFocusWorkSubmit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//暂存重点工作数据
export function SaveWorkDraft(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/focus/work/nature/audit/save/draft`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查询绩效上报列表
// /obei-performance-eval/p/quarter/report/query/by/{orgCode}
export function queryEscalationList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/query/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//查询绩效上报详情
export function queryEscalationDetail(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/get/quarter/report/detail/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//绩效上报填报
export function reportPerformance(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/modify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//驳回
export function rejectReport(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/audit/reject`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//通过
export function passReport(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/audit/pass`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//查询履历
export function queryAuditHistory(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/get/quarter/report/history/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}

//查询工业品战略任务
// /obei-performance-eval/p/report/get/group/priority/work/{reportCode}
export function queryWorkChart(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/group/priority/work/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//查询工业品绩效KPI
export function queryGroupKpiList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}n/group/kpi/list`,
      method: 'post',
      data,
    },
    {
      isJsonBody: false,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//查询部门汇总
export function getDeptSum(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/dept/sum/${data}`,
      method: 'get',
    },
    {
      isJsonBody: false,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//查询重点激励项目
export function getEmphasisStimulateTask(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/emphasis/stimulate/task/${data}`,
      method: 'get',
    },
    {
      isJsonBody: false,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}

//部门kpi
export function getByQuarterAndOrgPerfCode(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/kpi/getByQuarterAndOrgperfcode`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
// 锁定&解锁
export function updateLockStatus(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/updateLockStatus`,
      method: 'post',
      data,
    },
    {
      isJsonBody: false,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
// 情况说明保存接口
export function modifyExplain(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/explain/modify`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}

//绩效上报-各部门kpi汇总下载
export function deptKpiDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/kpi/excelExport?${params}`;
  getDownload(url, `${filename}.xls`);
}
//集团KPI指标导出
export function groupKpiDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}n/group/kpi/list/export?${params}`;
  getDownload(url, `${filename}.xls`);
}
//部门汇总导出 /obei-performance-eval/p/report/get/dept/sum/export/{reportCode}
export function deptSumDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/dept/sum/export/${params}`;
  getDownload(url, `${filename}.xls`);
}
//重点激励项目导出 p/report/get/emphasis/stimulate/task/export/{reportCode}
export function emphasisTaskDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/emphasis/stimulate/task/export/${params}`;
  getDownload(url, `${filename}.xls`);
}
//集团战略任务导出 /obei-performance-eval/p/report/get/group/priority/work/export/{reportCode}
export function groupPriorityDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/get/group/priority/work/export/${params}`;
  getDownload(url, `${filename}.xls`);
}

//查询奖金分配详情
export function queryBonusApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/eval/key/tasks/bonus/allocation`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//下载重点工作奖金列表-邮件跳转专用
export function emailDownloadsApi(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/eval/key/tasks/bonus/allocation/export`;
  downloadJson(url, params, `${filename}.xls`);
}

// 查询历史填报接口-重点工作填报
export function queryHistoryReport(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/emphasis/completion/describe/history`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}

// 查询通知消息列表
export function getNoticeList() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/get/user/message`,
      method: 'get',
    },
    {
      isGetDataDirectly: true,
    },
  );
}

// 查询通知消息列表 -带分页
export function queryNoticeList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/query/user/message`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}
// 标记已读状态
export function getReadMessage(messageId) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/update/read/flag/by/${messageId}`,
      method: 'get',
    },
    {
      isGetDataDirectly: true,
    },
  );
}

// 单条信息查询
export function getOneMessage(messageId) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/get/user/message/by/${messageId}`,
      method: 'get',
    },
    {
      isGetDataDirectly: true,
    },
  );
}
// 领导确认
export function confirmApi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/emphasis/dept/leaders/determine`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}
