import { request, getDownload } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
// 绩效档案相关接口

// 查询绩效报告查看接口
export function queryReportList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/report/query/by/${data}`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
// 查询工业品绩效
export function queryCompanyKpi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/group/eval/result/query`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}

// 查询工业品战略任务
export function queryCompanyTask(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/group/eval/key/tasks/query`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}

// 查询部门绩效
export function queryDeptKpi(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/eval/result/query`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}

// 查询部门重点激励项目
export function queryDeptTask(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/eval/key/tasks/query`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}
//批量导出
export function getBatchExport(params, fileName) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/result/bath/export/${params}`;
  getDownload(url, fileName);
}
//附件下载
export function getAnnexExport(params, fileName) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/quarter/report/file/bath/download/${params}`;
  getDownload(url, fileName);
}
