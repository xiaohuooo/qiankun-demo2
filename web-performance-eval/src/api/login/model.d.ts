declare namespace API {
  /** 登录参数 */
  type LoginParams = {
    account: string;
    username: string;
    imageVerificationVo: any;
  };

  /** 登录成功结果 */
  type LoginResult = {
    token: string;
    user: any;
    permissions: string[];
    rolesInfo: any;
  };
}
