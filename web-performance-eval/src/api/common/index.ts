import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';
export function getDictAll() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/query/dataDictAll`,
      method: 'get',
    },
    {},
  );
}

// 获取值集列表（不含具体定义）
export function getDictNameMap() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/query/dataDict`,
      method: 'post',
      data: {
        type: 'tblfield',
        pageSize: 999,
        dictName: '',
      },
    },
    {
      isJsonBody: true,
      isPromise: true,
    },
  );
}

export function getWidget(data) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}n/widget/previewWidget`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

export function queryAggregate(data) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}n/query/aggregate/data/by/param`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getClueDeptDist() {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/department/summary/data/get`,
      method: 'get',
      data: {},
    },
    {
      isPromise: true,
    },
  );
}

export function queryToDo(data) {
  return request(
    {
      url: `${UriPrefix.URL_PORTAL_PREFIX}p/clue/getUnreadPrompt`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}
