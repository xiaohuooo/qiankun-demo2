import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

/** 首页接口 **/
//集团KPI指标
export function queryKpiList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/group/kpi/index`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: true,
    },
  );
}
//集团战略任务
export function queryPriorityList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/group/priority/work`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
//各部门绩效评价
export function queryDeptKpiList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/org/kpi/index`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//重点激励项目
export function queryDeptWorkList() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/org/priority/work`,
      method: 'get',
    },
    {
      isGetDataDirectly: true,
    },
  );
}
/** 绩效分析接口 **/
export function queryAnalysisList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/perf/analysis`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//绩效分析接口 平台营收新
export function queryAnalysisPTList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/platform/revenue/perf/analysis`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function queryAnalysisView(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/group/kpi/situation/get`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//绩效分析接口 季度目标实际
export function queryAnalysisGoalActualList(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/home/query/perf/goalactual/analysis`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function querySituationView(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/dept/kpi/situation/get`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function querySituation(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/dept/kpi/situation/orgCode/get`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}

export function queryAllSituation(year) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/get/dept/kpi/incomplete/goals/by/${year}`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
//绩效改善
export function annotationModify(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/home/annotation/modify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: true,
    },
  );
}

export function annotationQuery() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/home/annotation/query`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: true,
    },
  );
}
//物流经营毛利+成本削减 /obei-performance-eval/p/home/logistics/goals/source/data
export function queryLogisticsGoalsSourceData() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/home/logistics/goals/source/data`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: true,
    },
  );
}

//物流经营毛利+成本削减 /obei-performance-eval/p/home/logistics/goals
export function queryLogisticsGoals() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/home/logistics/goals`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: false,
    },
  );
}

//绩效评价图表填报 /obei-performance-eval/p/home/widget/annotation/modify
export function widgetAnnotationModify(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/home/widget/annotation/modify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: false,
    },
  );
}
export function widgetAnnotationQuery(code) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}/p/home/widget/annotation/query/${code}`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: true,
    },
  );
}
//查询指标数据
export function queryGoalsApi(year) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/goals/group/category/query/${year}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: true,
    },
  );
}
//查询绩效分析数据
export function queryANALApi(data: API.PageRequest) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/score/by/dept/code/goals/code/query`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
