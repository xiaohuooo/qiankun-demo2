import { request } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

/**
 * 绩效目标--分页查询
 * @param data
 */
export function queryList(data: API.PageRequest) {
  return request<API.PageResult<API.DefResultItems>>(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/orgPerfGoals/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isGetDataDirectly: true,
      isJsonBody: true,
    },
  );
}

/**
 * 绩效目标--删除
 * @param code
 */
export function removeByCode(codes: string[]) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/orgPerfGoals/delete`,
      method: 'post',
      data: { perfGCodeList: codes },
    },
    {
      isPromise: true,
      isGetDataDirectly: true,
      // isJsonBody: true,
    },
  );
}

/**
 * 绩效目标--添加编辑
 * @param data
 */
export function save(data: API.UpdateDefinitionParams) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/orgPerfGoals/save`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isGetDataDirectly: true,
      isJsonBody: true,
    },
  );
}

/***
 * 绩效目标--校验名称
 * @param data
 */
export function check(data: any) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/orgPerfGoals/check`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isGetDataDirectly: true,
      isJsonBody: true,
    },
  );
}
