import { request, downloadJson } from '@/utils/request';
import { UriPrefix } from '@/enums/httpEnum';

//查询绩效方案列表
export function queryPlanList() {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/archives/eval/category`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isPromise: true,
      isGetDataDirectly: true,
    },
  );
}
//查看工业品绩效
export function queryKPIList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/archives/group/performance/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//查看各部门绩效
export function queryDeptList(data) {
  return request(
    {
      url: `${UriPrefix.URL_PERFORMANCE_PREFIX}p/archives/performance/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//各部门绩效下载
export function deptDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/archives/performance/export`;
  downloadJson(url, params, `${filename}.xls`);
}

//工业品绩效下载
export function kpiDownloads(params, filename) {
  const url = `${UriPrefix.URL_PERFORMANCE_PREFIX}p/archives/group/performance/export`;
  downloadJson(url, params, `${filename}.xls`);
}

