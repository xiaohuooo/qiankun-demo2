import {
  Modal,
  Table,
  Menu,
  Input,
  Form,
  Card,
  Checkbox,
  Radio,
  Col,
  Row,
  Select,
  DatePicker,
  TimePicker,
  Progress,
  Statistic,
  Tooltip,
  Tabs,
  Badge,
  Tag,
  TreeSelect,
  Collapse,
  Image,
  Upload,
  Cascader,
  Steps,
  Empty,
  Spin,
  Divider,
  Descriptions,
  InputNumber,
  Tree,
  Switch,
  Pagination,
  Dropdown,
  Popover,
  Space,
  Breadcrumb,
  Popconfirm,
  Drawer,
  List,
} from 'ant-design-vue';

import type { App } from 'vue';

import { AButton } from '@/components/basic/button/index';

// import 'ant-design-vue/dist/antd.css';
import 'ant-design-vue/dist/antd.variable.min.css';
import 'dayjs/locale/zh-cn';

export function setupAntd(app: App<Element>) {
  app.component('AButton', AButton);
  app.component('ATooltip', Tooltip);
  app.component('ABadge', Badge);
  app.component('ATag', Tag);
  app.component('ATreeSelect', TreeSelect);
  app.component('ATree', Tree);
  app
    .use(Form)
    .use(Input)
    .use(Modal)
    .use(Table)
    .use(Menu)
    .use(Card)
    .use(Checkbox)
    .use(Radio)
    .use(Col)
    .use(Row)
    .use(Select)
    .use(DatePicker)
    .use(TimePicker)
    .use(Progress)
    .use(Statistic)
    .use(Collapse)
    .use(Switch)
    .use(Divider)
    .use(Image)
    .use(Upload)
    .use(Empty)
    .use(Cascader)
    .use(Steps)
    .use(Spin)
    .use(Tabs)
    .use(InputNumber)
    .use(Descriptions)
    .use(Pagination)
    .use(Dropdown)
    .use(Popover)
    .use(Space)
    .use(Breadcrumb)
    .use(Popconfirm)
    .use(Drawer)
    .use(List);
}
