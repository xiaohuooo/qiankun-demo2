/*
 *数字每千位加逗号
 *
 */
const commafy = (num) => {
  return (
    num &&
    num.toString().replace(/\d+/, function (s) {
      return s.replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    })
  );
};

/**
 * 字母大小写切换
 * @param str 要处理的字符串
 * @param type 1:首字母大写 2：首页母小写 3：大小写转换 4：全部大写 5：全部小写
 */
export function strChangeCase(str, type) {
  function ToggleCase(str) {
    let itemText = '';
    str.split('').forEach(function (item) {
      if (/^([a-z]+)/.test(item)) {
        itemText += item.toUpperCase();
      } else if (/^([A-Z]+)/.test(item)) {
        itemText += item.toLowerCase();
      } else {
        itemText += item;
      }
    });
    return itemText;
  }

  switch (type) {
    case 1:
      return str.replace(/^(\w)(\w+)/, function (v, v1, v2) {
        return v1.toUpperCase() + v2.toLowerCase();
      });
    case 2:
      return str.replace(/^(\w)(\w+)/, function (v, v1, v2) {
        return v1.toLowerCase() + v2.toUpperCase();
      });
    case 3:
      return ToggleCase(str);
    case 4:
      return str.toUpperCase();
    case 5:
      return str.toLowerCase();
    default:
      return str;
  }
}

export default {
  commafy,
  strChangeCase,
};
