import axios from 'axios';
import { message as $message } from 'ant-design-vue';
import { uniqueSlash } from './urlUtils';
import type { AxiosRequestConfig } from 'axios';
import { ACCESS_TOKEN_KEY } from '@/enums/cacheEnum';
import { Storage } from '@/utils/Storage';
import { useUserStore } from '@/store/modules/user';
// import {ExclamationCircleOutlined} from '@ant-design/icons'

declare global {
  interface Navigator {
    msSaveBlob?: (blob: any, defaultName?: string) => boolean;
  }
}

export interface RequestOptions {
  /** 当前接口权限, 不需要鉴权的接口请忽略， 格式：sys:user:add */
  permCode?: string;
  /** 是否直接获取data，而忽略message等 */
  isGetDataDirectly?: boolean;
  /** 请求成功是提示信息 */
  successMsg?: string;
  /** 请求失败是提示信息 */
  errorMsg?: any;
  /** 是否mock数据请求 */
  isMock?: boolean;

  isJsonBody?: boolean;

  isPromise?: boolean;
}

const UNKNOWN_ERROR = '未知错误，请重试';
// 是否生产环境
// const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV);
// console.log(111111, location.origin);
/** 判断是否本地开发*/
const IS_LOCAL = location.origin.endsWith('localhost:8099');
/** 真实请求的路径前缀 */
const baseApiUrl = IS_LOCAL ? `${location.origin}/proxy/` : process.env.VUE_APP_BASE_API;
// const baseApiUrl = process.env.VUE_APP_BASE_API;
/** mock请求路径前缀 */
const baseMockUrl = process.env.VUE_APP_MOCK_API;

const service = axios.create({
  // baseURL: baseApiUrl,
  // timeout: 6000,
  validateStatus(status) {
    // 200 外的状态码都认定为失败
    return status === 200;
  },
});

service.interceptors.request.use(
  (config) => {
    const token = Storage.get(ACCESS_TOKEN_KEY);
    if (token && config.headers) {
      // 请求头token信息，请根据实际情况进行修改
      config.headers['Authorization'] = token;
      config.headers['Authentication'] = token;
    }

    if (config.headers) {
      if (config.url != null && config.url.indexOf('/auth/n/') === -1) {
        config.headers['systemCode'] = 'obei-data-performance-eval'; //系统别
      }
      if (!Object.hasOwn(config.headers, 'Content-Type')) {
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      }
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

service.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // 处理 422 或者 500 的错误异常提示
    const errMsg = error?.response?.data?.message ?? UNKNOWN_ERROR;
    $message.error(errMsg);
    error.message = errMsg;
    return Promise.reject(error);
  },
);

export type Response<T = any> = {
  code: number;
  message: string;
  data: T;
};

export type BaseResponse<T = any> = Promise<Response<T>>;

export const downloadJson = (url, params, filename) => {
  $message.loading('文件传输中');

  const fullUrl = baseApiUrl + url;

  return service
    .post(fullUrl, params, {
      transformRequest: [
        (params) => {
          let result = '';
          Object.keys(params).forEach((key) => {
            if (!Object.is(params[key], undefined) && !Object.is(params[key], null)) {
              result = JSON.stringify(params);
            }
          });
          return result;
        },
      ],
      headers: {
        'Content-Type': 'application/json',
      },
      responseType: 'blob',
    })
    .then((r) => {
      const content = r.data;
      const type = r.headers['content-type'];
      const blob = new Blob([content], {
        type,
      });
      if ('download' in document.createElement('a')) {
        const elink = document.createElement('a');
        elink.download = filename;
        elink.style.display = 'none';
        elink.href = URL.createObjectURL(blob);
        document.body.appendChild(elink);
        elink.click();
        URL.revokeObjectURL(elink.href);
        document.body.removeChild(elink);
      } else {
        if (navigator) {
          navigator.msSaveBlob!(blob, filename);
        }
      }
    })
    .catch((r) => {
      console.error(r);
      $message.error('下载失败');
    });
};

/**
 *
 * @param method - request methods
 * @param url - request url
 * @param data - request data or params
 */
export const request = async <T = any>(
  config: AxiosRequestConfig,
  options: RequestOptions = {},
): Promise<T> => {
  try {
    const {
      successMsg,
      errorMsg,
      permCode,
      isMock,
      isGetDataDirectly = true,
      isJsonBody = false,
      isPromise = false,
    } = options;

    // 如果当前是需要鉴权的接口 并且没有权限的话 则终止请求发起
    if (permCode && !useUserStore().perms.includes(permCode)) {
      return $message.error('你没有访问该接口的权限，请联系管理员！');
    }

    const fullUrl = `${(isMock ? baseMockUrl : baseApiUrl) + config.url}`;
    config.url = uniqueSlash(fullUrl);

    if (config.method == 'get' && config.data) {
      // 参数要拼接
      let _params;
      const data = config.data;

      if (Object.is(data, undefined)) {
        _params = '';
      } else {
        _params = '?';
        for (const key in data) {
          if (Object.hasOwn(data, key) && data[key] !== null && data[key] !== undefined) {
            _params += `${key}=${data[key]}&`;
          }
        }
      }

      config.url = config.url + _params;
    } else if (isJsonBody === true && config.data) {
      const data = config.data;
      // console.log(config.data, JSON.stringify(data));

      config.data = JSON.stringify(data);

      if (config.headers) {
        config.headers['Content-Type'] = 'application/json';
      } else {
        config.headers = {};
        config.headers['Content-Type'] = 'application/json';
      }
    } else if (config.data) {
      let result = '';
      const params = config.data;
      Object.keys(params).forEach((key) => {
        if (!Object.is(params[key], undefined) && !Object.is(params[key], null)) {
          result += `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}&`;
        }
      });

      config.data = result;
    }

    // if (IS_PROD) {
    //   // 保持api请求的协议与当前访问的站点协议一致
    //   config.url.replace(/^https?:/g, location.protocol);
    // }
    const res = await service.request(config);
    // 王朗：按状态返回页面提示
    const resAny = res as any;
    if (resAny?.success == false) {
      if (errorMsg) {
        if (typeof errorMsg === 'function') {
          errorMsg(resAny) && $message.error(errorMsg(resAny));
        } else {
          $message.error(errorMsg);
        }
      }
    } else {
      successMsg && $message.success(successMsg);
    }

    // successMsg && $message.success(successMsg);

    // 处理文件下载逻辑
    if (isPromise) {
      return Promise.resolve(isGetDataDirectly ? res.data : res);
    } else {
      return isGetDataDirectly ? res.data : res;
    }
  } catch (error: any) {
    return Promise.reject(error);
  }
};

export const downIng = (url, params, filename) => {
  console.log(filename);
  const fullUrl = baseApiUrl + url;
  return service
    .post(fullUrl, params, {
      transformRequest: [
        (params) => {
          let result = '';
          Object.keys(params).forEach((key) => {
            if (!Object.is(params[key], undefined) && !Object.is(params[key], null)) {
              result = JSON.stringify(params);
            }
          });
          return result;
        },
      ],
      headers: {
        'Content-Type': 'application/json',
      },
      responseType: 'blob',
    })
    .then((r) => {
      const content = r.data;
      const type = r.headers['content-type'];
      const blob = new Blob([content], {
        type,
      });
      return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          resolve(e);
        };
        // readAsDataURL
        fileReader.readAsDataURL(blob);
        fileReader.onerror = () => {
          reject(new Error('blobToBase64 error'));
        };
      });
    })
    .catch((r) => {
      $message.error(r);
    });
};

export const getDownload = (url, filename) => {
  $message.loading('文件传输中');

  const fullUrl = baseApiUrl + url;

  return service
    .get(fullUrl, {
      headers: {
        'Content-Type': 'application/json',
      },
      responseType: 'blob',
    })
    .then((r) => {
      const content = r.data;
      const type = r.headers['content-type'];
      const blob = new Blob([content], {
        type,
      });
      if ('download' in document.createElement('a')) {
        const elink = document.createElement('a');
        elink.download = filename;
        elink.style.display = 'none';
        elink.href = URL.createObjectURL(blob);
        document.body.appendChild(elink);
        elink.click();
        URL.revokeObjectURL(elink.href);
        document.body.removeChild(elink);
      } else {
        if (navigator) {
          navigator.msSaveBlob!(blob, filename);
        }
      }
    })
    .catch((r) => {
      console.error(r);
      $message.error('下载失败');
    });
};
