// 值集工具类

// import session from './sessionStorage'
import { Storage } from '@/utils/Storage';
import { ACCESS_DICT_ALL_KEY } from '@/enums/cacheEnum';

/**
 * 通过值集code获取对应值集明细
 * @param dictCode
 */
const getItemsByDictCode = function (dictCode) {
  let items = [];
  if (dictCode && dictCode.length > 0) {
    const data = Storage.get(ACCESS_DICT_ALL_KEY);
    if (data && data.length > 0) {
      const dict = data.find((x) => x.dictCode === dictCode);
      if (dict && dict.itemDtoList && dict.itemDtoList.length > 0) {
        items = dict.itemDtoList;
      }
    }
  }
  return items;
};

/**
 * 获取键值对
 * @param dictCode
 * @returns {*[]}
 */
const getKeyValueByDictCode = function (dictCode) {
  const items = getItemsByDictCode(dictCode);
  if (items.length > 0) {
    const list = [];
    items.forEach((x) => {
      list.push({ label: x.itemText, value: x.itemValue });
    });
    return list;
  }
  return [];
};
/**
 * @param dictCode 值集类型
 * @param value 值
 * @returns {*obj}
 */
const getValue = function (dictCode, value) {
  const items = getItemsByDictCode(dictCode);
  if (items.length > 0) {
    let obj = {};
    obj = items.find((item) => {
      return value == item.itemValue || value == item.id;
    });
    return obj;
  }
  return null;
};

/**
 * @param dictCode 值集类型
 * @param value 值
 * @returns {*obj}
 */
const getitemCode = function (dictCode, value) {
  const items = getItemsByDictCode(dictCode);
  if (items.length > 0) {
    let obj = {};
    obj = items.find((item) => {
      return value == item.itemText;
    });
    return obj;
  }
  return null;
};
/**
 * @param dictCode 值集类型
 * @param value 值
 * @returns {*obj}
 */
const getItemText = function (dictCode, value) {
  const items = getItemsByDictCode(dictCode);
  if (items.length > 0) {
    let obj = {};
    obj = items.find((item) => {
      return value == item.itemValue || value == item.id;
    });
    return obj.itemText;
  }
  return null;
};
export default {
  getKeyValueByDictCode,
  getItemsByDictCode,
  getValue,
  getitemCode,
  getItemText,
};
