
// 1. user/username?username=admin
let a = {
    "userId": 13,
    "username": "admin",
    "employeeCode": "admin",
    "password": "6744158b53f0faf54c0c87663c5cb305",
    "deptId": 3,
    "parentId": null,
    "unitId": 1,
    "unitName": "数字科技中心",
    "unitCode": "BWDHLA00",
    "deptName": "数据开发部",
    "deptCode": "BWDHLAE0",
    "email": "18239427579@163.com",
    "mobile": "15134627381",
    "status": "1",
    "createTime": "2022-06-09 01:01:57",
    "modifyTime": null,
    "lastLoginTime": "2023-10-11 09:00:29",
    "ssex": "1",
    "description": "obei",
    "avatar": "o8.png",
    "roleId": "1",
    "roleIds": null,
    "roleName": "管理员",
    "sortField": null,
    "sortOrder": null,
    "createTimeFrom": null,
    "createTimeTo": null,
    "id": null,
    "userLineDefense": "",
    "deptLineDefense": "2",
    "authCacheKey": 13
}

// 2. menu/13
let b = [
    {
        "path": "/",
        "showInSide": 1,
        "name": "主页",
        "component": "MenuView",
        "icon": "none",
        "redirect": "/home",
        "children": [
            {
                "path": "/home",
                "showInSide": 1,
                "name": "系统主页",
                "component": "HomePageView",
                "icon": "home",
                "meta": {
                    "closeable": false,
                    "isShow": true
                }
            },
            {
                "id": "203",
                "parentId": "0",
                "path": "/home1",
                "showInSide": 0,
                "name": "首页",
                "component": "/HomePage1",
                "icon": "rocket",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00"
            },
            {
                "id": "1",
                "parentId": "0",
                "path": "/system",
                "showInSide": 1,
                "name": "系统管理",
                "component": "PageView",
                "icon": "appstore-o",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "10",
                "children": [
                    {
                        "id": "3",
                        "parentId": "1",
                        "path": "/system/user",
                        "showInSide": 1,
                        "name": "用户管理",
                        "component": "system/user/User",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "4",
                        "parentId": "1",
                        "path": "/system/role",
                        "showInSide": 1,
                        "name": "角色管理",
                        "component": "system/role/Role",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "6",
                        "parentId": "1",
                        "path": "/system/dept",
                        "showInSide": 1,
                        "name": "部门管理",
                        "component": "system/dept/Dept",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "64",
                        "parentId": "1",
                        "path": "/system/dict",
                        "showInSide": 0,
                        "name": "字典管理",
                        "component": "system/dict/Dict",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "5",
                        "parentId": "1",
                        "path": "/system/menu",
                        "showInSide": 1,
                        "name": "菜单管理",
                        "component": "system/menu/Menu",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "144",
                "parentId": "0",
                "path": "/dataAdministration",
                "showInSide": 1,
                "name": "数据模型",
                "component": "PageView",
                "icon": "pie-chart",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "10",
                "children": [
                    {
                        "id": "254",
                        "parentId": "144",
                        "path": "/dataAdministration/sortmanagement/SortManagement",
                        "showInSide": 1,
                        "name": "分类管理",
                        "component": "views/dataAdministration/sortmanagement/SortManagement",
                        "icon": "bars",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "151",
                        "parentId": "144",
                        "path": "/dataAdministration/setmanagement/SetManagement",
                        "showInSide": 1,
                        "name": "数据集管理",
                        "component": "dataAdministration/setmanagement/SetManagementNew",
                        "icon": "bar-chart",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "168",
                        "parentId": "144",
                        "path": "/system/dict",
                        "showInSide": 1,
                        "name": "值集管理",
                        "component": "views/system/dict/Dict",
                        "icon": "diff",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "275",
                        "parentId": "144",
                        "path": "/dataAdministration/downloadcenter/DownloadCenter",
                        "showInSide": 1,
                        "name": "下载中心",
                        "component": "views/dataAdministration/downloadcenter/DownloadCenter",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "272",
                        "parentId": "144",
                        "path": "/SupervisionGovern/sqlAnalysis/sqlAnalysis",
                        "showInSide": 1,
                        "name": "SQL解析登记",
                        "component": "views/SupervisionGovern/sqlAnalysis/sqlAnalysis",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "145",
                        "parentId": "144",
                        "path": "/dataAdministration/management/Management",
                        "showInSide": 1,
                        "name": "数据源管理",
                        "component": "dataAdministration/management/Management",
                        "icon": "area-chart",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    }
                ]
            },
            {
                "id": "146",
                "parentId": "0",
                "path": "/SupervisionGovern",
                "showInSide": 1,
                "name": "监督执纪",
                "component": "PageView",
                "icon": "credit-card",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "10",
                "children": [
                    {
                        "id": "147",
                        "parentId": "146",
                        "path": "/SupervisionGovern/modelresult",
                        "showInSide": 1,
                        "name": "模型跟踪",
                        "component": "SupervisionGovern/modelresult/ModelResultNew",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "195",
                        "parentId": "146",
                        "path": "/SupervisionGovern/personalTodo",
                        "showInSide": 1,
                        "name": "模型个人待办",
                        "component": "SupervisionGovern/personalTodo/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "196",
                        "parentId": "146",
                        "path": "/SupervisionGovern/toDoHistory/historyLists",
                        "showInSide": 1,
                        "name": "模型处理记录",
                        "component": "SupervisionGovern/toDoHistory/historyLists",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "169",
                        "parentId": "146",
                        "path": "/SupervisionGovern/InfoAll",
                        "showInSide": 0,
                        "name": "个人疑点业务",
                        "component": "SupervisionGovern/InfoAll",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "421",
                        "parentId": "146",
                        "path": "/SupervisionGovern/repeatSupervise/index",
                        "showInSide": 1,
                        "name": "监督再监督",
                        "component": "views/SupervisionGovern/repeatSupervise/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "406",
                        "parentId": "146",
                        "path": "/SupervisionGovern/reportDownload/index",
                        "showInSide": 1,
                        "name": "监督成果",
                        "component": "views/SupervisionGovern/reportDownload/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "399",
                        "parentId": "146",
                        "path": "/SupervisionGovern/modelGroupTodo/index",
                        "showInSide": 1,
                        "name": "模型组个人待办",
                        "component": "views/SupervisionGovern/modelGroupTodo/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "398",
                        "parentId": "146",
                        "path": "/SupervisionGovern/toDoHistory/modelGroupHistoryLists",
                        "showInSide": 1,
                        "name": "模型组处理记录",
                        "component": "views/SupervisionGovern/toDoHistory/modelGroupHistoryLists",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    }
                ]
            },
            {
                "id": "285",
                "parentId": "0",
                "path": "/SupervisionModel",
                "showInSide": 1,
                "name": "监督模型",
                "component": "PageView",
                "icon": "diff",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "286",
                        "parentId": "285",
                        "path": "/SupervisionModel/modellist",
                        "showInSide": 1,
                        "name": "模型管理",
                        "component": "views/SupervisionModel/ModellistNew",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "400",
                        "parentId": "285",
                        "path": "/supervisionModelGroup/ModelGroupList",
                        "showInSide": 1,
                        "name": "模型组",
                        "component": "views/supervisionModelGroup/ModelGroupList",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    }
                ]
            },
            {
                "id": "152",
                "parentId": "0",
                "path": "/userAnalyze",
                "showInSide": 1,
                "name": "自主分析",
                "component": "PageView",
                "icon": "stock",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "10",
                "children": [
                    {
                        "id": "180",
                        "parentId": "152",
                        "path": "/userAnalyze/dataPreview",
                        "showInSide": 0,
                        "name": "自主查看",
                        "component": "userAnalyze/dataPreview/dataPreview",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "171",
                        "parentId": "152",
                        "path": "userAnalyze/SetManagementList",
                        "showInSide": 1,
                        "name": "数据集预览",
                        "component": "views/userAnalyze/setmanagement/SetManagementList",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "172",
                        "parentId": "152",
                        "path": "/userAnalyze/bulletinBoardList",
                        "showInSide": 1,
                        "name": "图表列表",
                        "component": "userAnalyze/bulletinBoardList/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "173",
                        "parentId": "152",
                        "path": "/userAnalyze/kanbanList",
                        "showInSide": 1,
                        "name": "看板列表",
                        "component": "userAnalyze/kanbanList/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "174",
                        "parentId": "152",
                        "path": "/userAnalyze/ChartConfigurations",
                        "showInSide": 0,
                        "name": "图表配置",
                        "component": "userAnalyze/ChartConfigurations/ChartConfiguration",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "331",
                        "parentId": "152",
                        "path": "userAnalyze/DataSetInfo",
                        "showInSide": 0,
                        "name": "查看数据",
                        "component": "views/userAnalyze/setmanagement/components/DataSetInfo",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "236",
                        "parentId": "152",
                        "path": "userAnalyze/kanbanConfigTabs",
                        "showInSide": 0,
                        "name": "看板配置",
                        "component": "userAnalyze/kanbanList/kanbanConfigTabs",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "422",
                "parentId": "0",
                "path": "/supervisionGovern",
                "showInSide": 1,
                "name": "整改跟踪",
                "component": "PageView",
                "icon": "",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "423",
                        "parentId": "422",
                        "path": "/SupervisionGovern/inspectionSupervision/index",
                        "showInSide": 1,
                        "name": "监督检查",
                        "component": "PageView",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00",
                        "children": [
                            {
                                "id": "425",
                                "parentId": "423",
                                "path": "/SupervisionGovern/inspectionSupervision/index",
                                "showInSide": 1,
                                "name": "项目列表",
                                "component": "views/SupervisionGovern/inspectionSupervision/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            },
                            {
                                "id": "426",
                                "parentId": "423",
                                "path": "/SupervisionGovern/questionInventory/index",
                                "showInSide": 1,
                                "name": "问题清单",
                                "component": "views/SupervisionGovern/questionInventory/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            },
                            {
                                "id": "424",
                                "parentId": "423",
                                "path": "/SupervisionGovern/questionTodo/index",
                                "showInSide": 1,
                                "name": "问题整改个人待办",
                                "component": "views/SupervisionGovern/questionTodo/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            },
                            {
                                "id": "427",
                                "parentId": "423",
                                "path": "/SupervisionGovern/questionHistoryLists/index",
                                "showInSide": 1,
                                "name": "问题处理记录",
                                "component": "views/SupervisionGovern/questionHistoryLists/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            }
                        ]
                    },
                    {
                        "id": "437",
                        "parentId": "422",
                        "path": "/InspectionIssues/index",
                        "showInSide": 1,
                        "name": "巡察反馈",
                        "component": "PageView",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00",
                        "children": [
                            {
                                "id": "438",
                                "parentId": "437",
                                "path": "/SupervisionGovern/InspectionIssues/index",
                                "showInSide": 1,
                                "name": "巡察项目",
                                "component": "views/SupervisionGovern/InspectionIssues/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            },
                            {
                                "id": "439",
                                "parentId": "437",
                                "path": "/SupervisionGovern/InspectionQuestion/index",
                                "showInSide": 1,
                                "name": "巡察问题",
                                "component": "views/SupervisionGovern/InspectionQuestion/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            },
                            {
                                "id": "440",
                                "parentId": "437",
                                "path": "/SupervisionGovern/InspectionTodo/index",
                                "showInSide": 1,
                                "name": "待办处理",
                                "component": "views/SupervisionGovern/InspectionTodo/index",
                                "icon": "",
                                "meta": {
                                    "closeable": true
                                },
                                "keepAlive": "00"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "162",
                "parentId": "0",
                "path": "/businessTracking",
                "showInSide": 0,
                "name": "业务跟踪",
                "component": "/businessTracking",
                "icon": "file-search",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00"
            },
            {
                "id": "348",
                "parentId": "0",
                "path": "/supplier",
                "showInSide": 1,
                "name": "国资委页面",
                "component": "PageView",
                "icon": "",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "350",
                        "parentId": "348",
                        "path": "/supplier/authentication/QueryList",
                        "showInSide": 1,
                        "name": "供应商信用认证查询",
                        "component": "views/supplier/authentication/QueryList",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "351",
                        "parentId": "348",
                        "path": "/supplier/commodity/Commodity",
                        "showInSide": 1,
                        "name": "商品均价智能检索",
                        "component": "views/supplier/commodity/Commodity",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "435",
                "parentId": "0",
                "path": "/algorithmService",
                "showInSide": 1,
                "name": "算法服务",
                "component": "PageView",
                "icon": "",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "436",
                        "parentId": "435",
                        "path": "/algorithmService/material/MaterialComparison",
                        "showInSide": 1,
                        "name": "物料对照",
                        "component": "views/algorithmService/material/MaterialComparison",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "2",
                "parentId": "0",
                "path": "/monitor",
                "showInSide": 1,
                "name": "系统监控",
                "component": "PageView",
                "icon": "dashboard",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "10",
                        "parentId": "2",
                        "path": "/monitor/systemlog",
                        "showInSide": 1,
                        "name": "系统日志",
                        "component": "monitor/SystemLog",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "121",
                        "parentId": "2",
                        "path": "/monitor/httptrace",
                        "showInSide": 0,
                        "name": "请求追踪",
                        "component": "monitor/Httptrace",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "256",
                        "parentId": "2",
                        "path": "/dataAdministration/datalogging/DataLogging",
                        "showInSide": 0,
                        "name": "数据日志",
                        "component": "dataAdministration/datalogging/DataLogging",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "178",
                "parentId": "0",
                "path": "/strategyTracking",
                "showInSide": 1,
                "name": "采购运营管理驾驶舱",
                "component": "PageView",
                "icon": "highlight",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "179",
                        "parentId": "178",
                        "path": "/strategyTracking/businesstracking/OverMonth",
                        "showInSide": 1,
                        "name": "总览",
                        "component": "views/strategyTracking/businesstracking/OverMonth",
                        "icon": "bar-chart",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "201",
                        "parentId": "178",
                        "path": "/strategyTracking/businesstracking/regionalcenter",
                        "showInSide": 1,
                        "name": "各中心各大区",
                        "component": "strategyTracking/businesstracking/regionalcenter",
                        "icon": "bank",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "340",
                "parentId": "0",
                "path": "/obeicockpit",
                "showInSide": 1,
                "name": "欧贝平台管理驾驶舱",
                "component": "PageView",
                "icon": "",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "10",
                "children": [
                    {
                        "id": "341",
                        "parentId": "340",
                        "path": "/obeicockpit/obeitracking/ObeiOverview",
                        "showInSide": 1,
                        "name": "月度例会-总览",
                        "component": "views/obeicockpit/obeitracking/ObeiOverview",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "342",
                        "parentId": "340",
                        "path": "/obeicockpit/obeicockpit/ObeiDepartments",
                        "showInSide": 1,
                        "name": "月度例会-各部门",
                        "component": "views/obeicockpit/obeicockpit/ObeiDepartments",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "364",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/OverIndex",
                        "showInSide": 1,
                        "name": "总览",
                        "component": "views/obeicockpit/obeiplatform/OverIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "365",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/SalesIndex",
                        "showInSide": 1,
                        "name": "业务部门",
                        "component": "views/obeicockpit/obeiplatform/SalesIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "389",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/TescoIndex",
                        "showInSide": 1,
                        "name": "欧贝易购",
                        "component": "views/obeicockpit/obeiplatform/TescoIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "367",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/ShoppingMallIndex",
                        "showInSide": 1,
                        "name": "欧贝商城",
                        "component": "views/obeicockpit/obeiplatform/ShoppingMallIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "390",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/CloudLibraryIndex",
                        "showInSide": 1,
                        "name": "欧贝云库",
                        "component": "views/obeicockpit/obeiplatform/CloudLibraryIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "391",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/EasytoIndex",
                        "showInSide": 1,
                        "name": "欧贝易销",
                        "component": "views/obeicockpit/obeiplatform/EasytoIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "392",
                        "parentId": "340",
                        "path": "/obeicockpit/obeiplatform/ZeroCarbonIndex",
                        "showInSide": 1,
                        "name": "欧贝零碳",
                        "component": "views/obeicockpit/obeiplatform/ZeroCarbonIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "366",
                        "parentId": "340",
                        "path": "obeicockpit/obeiplatform/BusinessIndex",
                        "showInSide": 1,
                        "name": "会员商务中心",
                        "component": "views/obeicockpit/obeiplatform/BusinessIndex",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "395",
                "parentId": "0",
                "path": "/priorityWork",
                "showInSide": 1,
                "name": "公司年度重点任务",
                "component": "PageView",
                "icon": "",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "00",
                "children": [
                    {
                        "id": "396",
                        "parentId": "395",
                        "path": "priorityWork/homePagekanban/index",
                        "showInSide": 1,
                        "name": "首页看板",
                        "component": "views/priorityWork/homePagekanban/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    },
                    {
                        "id": "397",
                        "parentId": "395",
                        "path": "priorityWork/jobFilling/index",
                        "showInSide": 1,
                        "name": "工作填报",
                        "component": "views/priorityWork/jobFilling/index",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "00"
                    }
                ]
            },
            {
                "id": "355",
                "parentId": "0",
                "path": "/manualMaintenance",
                "showInSide": 1,
                "name": "数据维护",
                "component": "PageView",
                "icon": "",
                "meta": {
                    "closeable": true
                },
                "keepAlive": "10",
                "children": [
                    {
                        "id": "401",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/4ba0ee95f08641498e9da561fb564823",
                        "showInSide": 1,
                        "name": "吨钢成本目标维护",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "356",
                        "parentId": "355",
                        "path": "/manualMaintenance/baseProduction",
                        "showInSide": 1,
                        "name": "钢铁基地产量维护表",
                        "component": "views/manualMaintenance/baseProduction",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "357",
                        "parentId": "355",
                        "path": "/manualMaintenance/materialMaintenance",
                        "showInSide": 1,
                        "name": "指标家族物料维护表",
                        "component": "views/manualMaintenance/materialMaintenance",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "369",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/99c5dc0a96d549509da1f33cc7c6b162",
                        "showInSide": 1,
                        "name": "资材品类中心原料维护表",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "373",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/51833fc0d8d94e7c84ee161de32cfd4f",
                        "showInSide": 1,
                        "name": "资材品类中心物料维护表",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "428",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/cadb59efc5c94b6ea17cedc98930a66b",
                        "showInSide": 1,
                        "name": "用户服务反馈问题跟踪清单",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "375",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/529898ca1b924d1981431bec625f1b66",
                        "showInSide": 1,
                        "name": "设备品类中心重点项目跟踪",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "376",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/094d7450d81643f9be66a94d1de8fcc4",
                        "showInSide": 1,
                        "name": "客户服务跟踪",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    },
                    {
                        "id": "374",
                        "parentId": "355",
                        "path": "/manualMaintenance/general/29a370eaa1764d28b996249fec90675d",
                        "showInSide": 1,
                        "name": "平台驾驶舱数据维护",
                        "component": "views/manualMaintenance/general",
                        "icon": "",
                        "meta": {
                            "closeable": true
                        },
                        "keepAlive": "10"
                    }
                ]
            },
            {
                "path": "/profile",
                "showInSide": 1,
                "name": "个人中心",
                "component": "personal/Profile",
                "icon": "none",
                "meta": {
                    "closeable": true,
                    "isShow": false
                }
            }
        ]
    },
    {
        "path": "*",
        "name": "404",
        "component": "error/404"
    }
]

// 3. p/query/dataDictAll
let c = {
    "success": true,
    "code": "OK",
    "message": "查询成功",
    "pageNum": null,
    "pageSize": null,
    "pageCount": null,
    "total": null,
    "data": [
        {
            "id": 224,
            "dictId": "498861204596641792",
            "dictName": "下载文件来源",
            "dictCode": "DownLoadFileSources",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-10-08 14:16:20",
            "itemDtoList": [
                {
                    "id": 1419,
                    "dictItemId": "498861207184527360",
                    "dictId": "498861204596641792",
                    "itemText": "模型结果集",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1420,
                    "dictItemId": "498861207184527361",
                    "dictId": "498861204596641792",
                    "itemText": "数据集",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1421,
                    "dictItemId": "498861207184527362",
                    "dictId": "498861204596641792",
                    "itemText": "图表",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1422,
                    "dictItemId": "498861207184527363",
                    "dictId": "498861204596641792",
                    "itemText": "看板",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 223,
            "dictId": "495163641793187840",
            "dictName": "巡查整改状态",
            "dictCode": "INSPECTION_REMEDIATION_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-28 09:23:31",
            "itemDtoList": [
                {
                    "id": 1416,
                    "dictItemId": "495163641801576448",
                    "dictId": "495163641793187840",
                    "itemText": "未启动",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1417,
                    "dictItemId": "495163641801576449",
                    "dictId": "495163641793187840",
                    "itemText": "整改中",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1418,
                    "dictItemId": "495163641801576450",
                    "dictId": "495163641793187840",
                    "itemText": "已完成",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 222,
            "dictId": "495163401480540160",
            "dictName": "巡察审批状态",
            "dictCode": "INSPECTION_AUDIT_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-28 09:22:34",
            "itemDtoList": [
                {
                    "id": 1414,
                    "dictItemId": "495163401488928768",
                    "dictId": "495163401480540160",
                    "itemText": "审批通过",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1415,
                    "dictItemId": "495163401488928769",
                    "dictId": "495163401480540160",
                    "itemText": "审批驳回",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 221,
            "dictId": "495163119304544256",
            "dictName": "巡察待办环节",
            "dictCode": "INSPECTION_TODO_LINK",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-28 09:21:27",
            "itemDtoList": [
                {
                    "id": 1406,
                    "dictItemId": "495163119312932864",
                    "dictId": "495163119304544256",
                    "itemText": "待办创建",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1407,
                    "dictItemId": "495163119312932865",
                    "dictId": "495163119304544256",
                    "itemText": "整改措施填报",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1408,
                    "dictItemId": "495163119312932866",
                    "dictId": "495163119304544256",
                    "itemText": "整改措施填报领导审核",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1409,
                    "dictItemId": "495163119312932867",
                    "dictId": "495163119304544256",
                    "itemText": "整改措施填报巡查办审核",
                    "itemValue": "30",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1410,
                    "dictItemId": "495163119312932868",
                    "dictId": "495163119304544256",
                    "itemText": "整改情况填报",
                    "itemValue": "40",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1411,
                    "dictItemId": "495163119312932869",
                    "dictId": "495163119304544256",
                    "itemText": "整改情况领导审核",
                    "itemValue": "50",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1412,
                    "dictItemId": "495163119312932870",
                    "dictId": "495163119304544256",
                    "itemText": "整改情况巡查办审核",
                    "itemValue": "60",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1413,
                    "dictItemId": "495163119312932871",
                    "dictId": "495163119304544256",
                    "itemText": "流程结束",
                    "itemValue": "200",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 220,
            "dictId": "495162651559956480",
            "dictName": "项目类别",
            "dictCode": "PROJECT_CATEGORY",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-28 09:19:35",
            "itemDtoList": [
                {
                    "id": 1402,
                    "dictItemId": "495162651572539392",
                    "dictId": "495162651559956480",
                    "itemText": "常规巡察",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1403,
                    "dictItemId": "495162651572539393",
                    "dictId": "495162651559956480",
                    "itemText": "专项巡察",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1404,
                    "dictItemId": "495162651572539394",
                    "dictId": "495162651559956480",
                    "itemText": "机动式巡察",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1405,
                    "dictItemId": "495162651572539395",
                    "dictId": "495162651559956480",
                    "itemText": "巡察回头看",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 219,
            "dictId": "495161804574789632",
            "dictName": "问题大类",
            "dictCode": "ISSUE_LARGE_CATEGORIES",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-28 09:16:13",
            "itemDtoList": [
                {
                    "id": 1398,
                    "dictItemId": "495161804595761152",
                    "dictId": "495161804574789632",
                    "itemText": "聚焦党中央决策部署在基层的落实情况",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1399,
                    "dictItemId": "495161804595761153",
                    "dictId": "495161804574789632",
                    "itemText": "聚焦群众身边腐败问题和不正之风",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1400,
                    "dictItemId": "495161804595761154",
                    "dictId": "495161804574789632",
                    "itemText": "聚焦基层党组织建设情况",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1401,
                    "dictItemId": "495161804595761155",
                    "dictId": "495161804574789632",
                    "itemText": "聚焦巡察整改落实情况",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 218,
            "dictId": "492706270635667456",
            "dictName": "物料对照任务状态",
            "dictCode": "MaterialTaskCode",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-21 14:38:49",
            "itemDtoList": [
                {
                    "id": 1394,
                    "dictItemId": "492706270954434560",
                    "dictId": "492706270635667456",
                    "itemText": "未运行",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1395,
                    "dictItemId": "492706270954434561",
                    "dictId": "492706270635667456",
                    "itemText": "运行中",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1396,
                    "dictItemId": "492706270954434562",
                    "dictId": "492706270635667456",
                    "itemText": "成功",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1397,
                    "dictItemId": "492706270954434563",
                    "dictId": "492706270635667456",
                    "itemText": "失败",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 217,
            "dictId": "489466787881730048",
            "dictName": "HIVE字段类型",
            "dictCode": "HudiDataType",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-12 16:06:16",
            "updateUser": "admin",
            "updateTime": "2023-09-12 16:07:31",
            "itemDtoList": [
                {
                    "id": 1377,
                    "dictItemId": "489467103880593408",
                    "dictId": "489466787881730048",
                    "itemText": "BOOLEAN",
                    "itemValue": "BOOLEAN",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1378,
                    "dictItemId": "489467103888982016",
                    "dictId": "489466787881730048",
                    "itemText": "INT",
                    "itemValue": "INT",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1379,
                    "dictItemId": "489467103893176320",
                    "dictId": "489466787881730048",
                    "itemText": "LONG",
                    "itemValue": "LONG",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1380,
                    "dictItemId": "489467103901564928",
                    "dictId": "489466787881730048",
                    "itemText": "FLOAT",
                    "itemValue": "FLOAT",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1381,
                    "dictItemId": "489467103905759232",
                    "dictId": "489466787881730048",
                    "itemText": "DOUBLE",
                    "itemValue": "DOUBLE",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1382,
                    "dictItemId": "489467103914147840",
                    "dictId": "489466787881730048",
                    "itemText": "DECIMAL",
                    "itemValue": "DECIMAL",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1383,
                    "dictItemId": "489467103918342144",
                    "dictId": "489466787881730048",
                    "itemText": "DATE",
                    "itemValue": "DATE",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1384,
                    "dictItemId": "489467103922536448",
                    "dictId": "489466787881730048",
                    "itemText": "TIMESTAMP",
                    "itemValue": "TIMESTAMP",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1385,
                    "dictItemId": "489467103930925056",
                    "dictId": "489466787881730048",
                    "itemText": "STRING",
                    "itemValue": "STRING",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1386,
                    "dictItemId": "489467103935119360",
                    "dictId": "489466787881730048",
                    "itemText": "BYTES",
                    "itemValue": "BYTES",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1387,
                    "dictItemId": "489467103939313664",
                    "dictId": "489466787881730048",
                    "itemText": "ARRAY",
                    "itemValue": "ARRAY",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1388,
                    "dictItemId": "489467103943507968",
                    "dictId": "489466787881730048",
                    "itemText": "MAP",
                    "itemValue": "MAP",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1389,
                    "dictItemId": "489467103951896576",
                    "dictId": "489466787881730048",
                    "itemText": "STRUCT",
                    "itemValue": "STRUCT",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 216,
            "dictId": "489466591357616128",
            "dictName": "HIVE逻辑分区",
            "dictCode": "hiveLogical",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-12 16:05:29",
            "updateUser": "admin",
            "updateTime": "2023-09-12 16:05:52",
            "itemDtoList": [
                {
                    "id": 1372,
                    "dictItemId": "489466591403753472",
                    "dictId": "489466591357616128",
                    "itemText": "bwdhmajyfx",
                    "itemValue": "bwdhmajyfx",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1373,
                    "dictItemId": "489466690905227264",
                    "dictId": "489466591357616128",
                    "itemText": "obei_superv",
                    "itemValue": "obei_superv",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1374,
                    "dictItemId": "489466690909421568",
                    "dictId": "489466591357616128",
                    "itemText": "obei_superv_dev",
                    "itemValue": "obei_superv_dev",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1375,
                    "dictItemId": "489466690917810176",
                    "dictId": "489466591357616128",
                    "itemText": "obei_superv_test",
                    "itemValue": "obei_superv_test",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1376,
                    "dictItemId": "489466690922004480",
                    "dictId": "489466591357616128",
                    "itemText": "default",
                    "itemValue": "default",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 215,
            "dictId": "485388883851587584",
            "dictName": "历史环节",
            "dictCode": "HISTORY_LINK_CODE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-01 10:02:07",
            "updateUser": "admin",
            "updateTime": "2023-09-01 10:09:40",
            "itemDtoList": [
                {
                    "id": 1357,
                    "dictItemId": "485390781849645056",
                    "dictId": "485388883851587584",
                    "itemText": "业务自查",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1358,
                    "dictItemId": "485390781853839360",
                    "dictId": "485388883851587584",
                    "itemText": "核查处理",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1359,
                    "dictItemId": "485390781862227968",
                    "dictId": "485388883851587584",
                    "itemText": "纪检研判",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1360,
                    "dictItemId": "485390781870616576",
                    "dictId": "485388883851587584",
                    "itemText": "流程结束",
                    "itemValue": "30",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1361,
                    "dictItemId": "485390781874810880",
                    "dictId": "485388883851587584",
                    "itemText": "待办移交",
                    "itemValue": "40",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1362,
                    "dictItemId": "485390781883199488",
                    "dictId": "485388883851587584",
                    "itemText": "管理员初审",
                    "itemValue": "50",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1363,
                    "dictItemId": "485390781887393792",
                    "dictId": "485388883851587584",
                    "itemText": "整改处理",
                    "itemValue": "60",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1364,
                    "dictItemId": "485390781895782400",
                    "dictId": "485388883851587584",
                    "itemText": "部门领导审核",
                    "itemValue": "70",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1365,
                    "dictItemId": "485390781904171008",
                    "dictId": "485388883851587584",
                    "itemText": "监督组织自查",
                    "itemValue": "80",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1366,
                    "dictItemId": "485390781908365312",
                    "dictId": "485388883851587584",
                    "itemText": "联络员审核",
                    "itemValue": "90",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1367,
                    "dictItemId": "485390781920948224",
                    "dictId": "485388883851587584",
                    "itemText": "待办转交",
                    "itemValue": "150",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1368,
                    "dictItemId": "485390781929336832",
                    "dictId": "485388883851587584",
                    "itemText": "待办创建",
                    "itemValue": "200",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 214,
            "dictId": "485388802733748224",
            "dictName": "模型组历史环节",
            "dictCode": "MODEL_GROUP_HISTORY_LINK_CODE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-09-01 10:01:48",
            "updateUser": "admin",
            "updateTime": "2023-09-01 10:03:30",
            "itemDtoList": [
                {
                    "id": 1348,
                    "dictItemId": "485389229168635904",
                    "dictId": "485388802733748224",
                    "itemText": "模型组监督组织自查",
                    "itemValue": "110",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1349,
                    "dictItemId": "485389229193801728",
                    "dictId": "485388802733748224",
                    "itemText": "纪检研判",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1350,
                    "dictItemId": "485389229202190336",
                    "dictId": "485388802733748224",
                    "itemText": "流程结束",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1351,
                    "dictItemId": "485389229206384640",
                    "dictId": "485388802733748224",
                    "itemText": "管理员初审",
                    "itemValue": "50",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1352,
                    "dictItemId": "485389229210578944",
                    "dictId": "485388802733748224",
                    "itemText": "整改处理",
                    "itemValue": "60",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1353,
                    "dictItemId": "485389229214773248",
                    "dictId": "485388802733748224",
                    "itemText": "模型组监督组织自查",
                    "itemValue": "110",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1354,
                    "dictItemId": "485389229474820096",
                    "dictId": "485388802733748224",
                    "itemText": "待办转交",
                    "itemValue": "150",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1355,
                    "dictItemId": "485389229487403008",
                    "dictId": "485388802733748224",
                    "itemText": "业务自查",
                    "itemValue": "00",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1356,
                    "dictItemId": "485389229491597312",
                    "dictId": "485388802733748224",
                    "itemText": "待办创建",
                    "itemValue": "200",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 213,
            "dictId": "485072512331386880",
            "dictName": "整改审批状态",
            "dictCode": "RECTIFICATION_TODO_LINK",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-08-31 13:04:59",
            "updateUser": "admin",
            "updateTime": "2023-09-18 15:33:43",
            "itemDtoList": [
                {
                    "id": 1339,
                    "dictItemId": "485072767420567552",
                    "dictId": "485072512331386880",
                    "itemText": "整改转交",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1340,
                    "dictItemId": "485072767433150464",
                    "dictId": "485072512331386880",
                    "itemText": "整改填报",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1341,
                    "dictItemId": "485072767437344768",
                    "dictId": "485072512331386880",
                    "itemText": "整改待审批",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1342,
                    "dictItemId": "485072767445733376",
                    "dictId": "485072512331386880",
                    "itemText": "整改通过",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1343,
                    "dictItemId": "485072767454121984",
                    "dictId": "485072512331386880",
                    "itemText": "整改驳回",
                    "itemValue": "50",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1344,
                    "dictItemId": "485072767462510592",
                    "dictId": "485072512331386880",
                    "itemText": "已整改待验收",
                    "itemValue": "60",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1345,
                    "dictItemId": "485072767470899200",
                    "dictId": "485072512331386880",
                    "itemText": "纪检验收通过",
                    "itemValue": "70",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1346,
                    "dictItemId": "485072767475093504",
                    "dictId": "485072512331386880",
                    "itemText": "纪检验收驳回",
                    "itemValue": "80",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1347,
                    "dictItemId": "485072767483482112",
                    "dictId": "485072512331386880",
                    "itemText": "整改创建",
                    "itemValue": "200",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1390,
                    "dictItemId": "491632924930777088",
                    "dictId": "485072512331386880",
                    "itemText": "部门领导验收通过",
                    "itemValue": "61",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1391,
                    "dictItemId": "491632930433703936",
                    "dictId": "485072512331386880",
                    "itemText": "部门领导验收驳回",
                    "itemValue": "62",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 212,
            "dictId": "484751759737704448",
            "dictName": "问题类型",
            "dictCode": "PROBLEM_TYPE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-08-30 15:50:25",
            "updateUser": "admin",
            "updateTime": "2023-08-30 15:50:58",
            "itemDtoList": [
                {
                    "id": 1330,
                    "dictItemId": "484751896954359808",
                    "dictId": "484751759737704448",
                    "itemText": "制度缺失",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1331,
                    "dictItemId": "484751896958554112",
                    "dictId": "484751759737704448",
                    "itemText": "违反规程",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1332,
                    "dictItemId": "484751896966942720",
                    "dictId": "484751759737704448",
                    "itemText": "违反制度",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1333,
                    "dictItemId": "484751896971137024",
                    "dictId": "484751759737704448",
                    "itemText": "人为失误",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1334,
                    "dictItemId": "484751896975331328",
                    "dictId": "484751759737704448",
                    "itemText": "其他",
                    "itemValue": "50",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 211,
            "dictId": "484751229258911744",
            "dictName": "问题整改状态",
            "dictCode": "PROBLEM_RECTIFICATION_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-08-30 15:48:19",
            "updateUser": "admin",
            "updateTime": "2023-09-18 15:36:08",
            "itemDtoList": [
                {
                    "id": 1322,
                    "dictItemId": "484751229275688960",
                    "dictId": "484751229258911744",
                    "itemText": "整改创建",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1323,
                    "dictItemId": "484751229275688961",
                    "dictId": "484751229258911744",
                    "itemText": "整改填报",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1324,
                    "dictItemId": "484751229275688962",
                    "dictId": "484751229258911744",
                    "itemText": "整改转交",
                    "itemValue": "15",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1325,
                    "dictItemId": "484751229275688963",
                    "dictId": "484751229258911744",
                    "itemText": "整改审批",
                    "itemValue": "20",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1326,
                    "dictItemId": "484751229275688964",
                    "dictId": "484751229258911744",
                    "itemText": "整改驳回",
                    "itemValue": "25",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1327,
                    "dictItemId": "484751229275688965",
                    "dictId": "484751229258911744",
                    "itemText": "已整改待验收",
                    "itemValue": "30",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1328,
                    "dictItemId": "484751229275688966",
                    "dictId": "484751229258911744",
                    "itemText": "纪检验收驳回",
                    "itemValue": "40",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1329,
                    "dictItemId": "484751229275688967",
                    "dictId": "484751229258911744",
                    "itemText": "纪检已整改验收",
                    "itemValue": "50",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1392,
                    "dictItemId": "491633010301640704",
                    "dictId": "484751229258911744",
                    "itemText": "部门领导整改验收",
                    "itemValue": "31",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1393,
                    "dictItemId": "491633010368749568",
                    "dictId": "484751229258911744",
                    "itemText": "部门领导验收驳回",
                    "itemValue": "32",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 210,
            "dictId": "482196887599927296",
            "dictName": "监督报表",
            "dictCode": "SUPERVISION_REPORT",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-08-23 14:38:16",
            "updateUser": "admin",
            "updateTime": "2023-09-11 13:19:09",
            "itemDtoList": [
                {
                    "id": 1312,
                    "dictItemId": "482196887616704512",
                    "dictId": "482196887599927296",
                    "itemText": "监督模型分布情况",
                    "itemValue": "首页",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1313,
                    "dictItemId": "482196887616704513",
                    "dictId": "482196887599927296",
                    "itemText": "监督模型分布情况详情",
                    "itemValue": "首页",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1314,
                    "dictItemId": "482196887616704514",
                    "dictId": "482196887599927296",
                    "itemText": "各部门监督模型分布情况详情",
                    "itemValue": "首页",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1315,
                    "dictItemId": "482196887616704515",
                    "dictId": "482196887599927296",
                    "itemText": "部门模型拥有人情况详情",
                    "itemValue": "首页",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1316,
                    "dictItemId": "482196887616704516",
                    "dictId": "482196887599927296",
                    "itemText": "监督领域模型情况汇总",
                    "itemValue": "首页",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1317,
                    "dictItemId": "482196887616704517",
                    "dictId": "482196887599927296",
                    "itemText": "监督领域模型详情",
                    "itemValue": "首页",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1318,
                    "dictItemId": "482196887616704518",
                    "dictId": "482196887599927296",
                    "itemText": "监督问题构成分布（按部门）",
                    "itemValue": "首页",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1319,
                    "dictItemId": "482196887616704519",
                    "dictId": "482196887599927296",
                    "itemText": "监督问题构成分布（按业务环节）",
                    "itemValue": "首页",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1320,
                    "dictItemId": "482196887616704520",
                    "dictId": "482196887599927296",
                    "itemText": "各单位模型监督效率统计",
                    "itemValue": "首页",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1321,
                    "dictItemId": "482196887616704521",
                    "dictId": "482196887599927296",
                    "itemText": "明细模型监督效率",
                    "itemValue": "首页",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1335,
                    "dictItemId": "484780071158124544",
                    "dictId": "482196887599927296",
                    "itemText": "监督再监督单位指标",
                    "itemValue": "监督再监督",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1336,
                    "dictItemId": "484780073305608192",
                    "dictId": "482196887599927296",
                    "itemText": "监督再监督部门指标",
                    "itemValue": "监督再监督",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1337,
                    "dictItemId": "484780073313996800",
                    "dictId": "482196887599927296",
                    "itemText": "监督再监督人员指标",
                    "itemValue": "监督再监督",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1338,
                    "dictItemId": "484780073322385408",
                    "dictId": "482196887599927296",
                    "itemText": "监督再监督模型指标",
                    "itemValue": "监督再监督",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1369,
                    "dictItemId": "489062345994735616",
                    "dictId": "482196887599927296",
                    "itemText": "合同检查问题跟踪",
                    "itemValue": "合同检查模型组报表",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1370,
                    "dictItemId": "489062346011512832",
                    "dictId": "482196887599927296",
                    "itemText": "模型出现问题频次",
                    "itemValue": "合同检查模型组报表",
                    "sortOrder": 16,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1371,
                    "dictItemId": "489062346015707136",
                    "dictId": "482196887599927296",
                    "itemText": "检查人效率统计",
                    "itemValue": "合同检查模型组报表",
                    "sortOrder": 17,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 209,
            "dictId": "479960544350425088",
            "dictName": "模型组流程整改状态",
            "dictCode": "MODEL_GROUP_CLUES_RECTIFICATION_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-08-17 10:31:51",
            "updateUser": "admin",
            "updateTime": "2023-08-17 10:35:21",
            "itemDtoList": [
                {
                    "id": 1305,
                    "dictItemId": "479961427805065216",
                    "dictId": "479960544350425088",
                    "itemText": "待整改",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1306,
                    "dictItemId": "479961427855396864",
                    "dictId": "479960544350425088",
                    "itemText": "纪检提交未整改",
                    "itemValue": "05",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1307,
                    "dictItemId": "479961427863785472",
                    "dictId": "479960544350425088",
                    "itemText": "整改中",
                    "itemValue": "10",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1308,
                    "dictItemId": "479961427872174080",
                    "dictId": "479960544350425088",
                    "itemText": "未按计划完成整改",
                    "itemValue": "20",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1309,
                    "dictItemId": "479961427876368384",
                    "dictId": "479960544350425088",
                    "itemText": "整改待核查",
                    "itemValue": "30",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1310,
                    "dictItemId": "479961427880562688",
                    "dictId": "479960544350425088",
                    "itemText": "整改结束",
                    "itemValue": "40",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1311,
                    "dictItemId": "479961427888951296",
                    "dictId": "479960544350425088",
                    "itemText": "驳回待整改",
                    "itemValue": "06",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 208,
            "dictId": "478916229512564736",
            "dictName": "模型组历史环节",
            "dictCode": "MODEL_GROUP_HISTORY_LINK",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-08-14 13:22:06",
            "updateUser": "admin",
            "updateTime": "2023-08-16 15:22:26",
            "itemDtoList": [
                {
                    "id": 1295,
                    "dictItemId": "478916229801971712",
                    "dictId": "478916229512564736",
                    "itemText": "待办创建",
                    "itemValue": "待办创建",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1297,
                    "dictItemId": "478922523778748416",
                    "dictId": "478916229512564736",
                    "itemText": "纪检研判",
                    "itemValue": "纪检研判",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1298,
                    "dictItemId": "478922523787137024",
                    "dictId": "478916229512564736",
                    "itemText": "流程结束",
                    "itemValue": "流程结束",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1299,
                    "dictItemId": "478922523791331328",
                    "dictId": "478916229512564736",
                    "itemText": "管理员初审",
                    "itemValue": "管理员初审",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1300,
                    "dictItemId": "478922616183459840",
                    "dictId": "478916229512564736",
                    "itemText": "整改处理",
                    "itemValue": "整改处理",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1301,
                    "dictItemId": "478922942236069888",
                    "dictId": "478916229512564736",
                    "itemText": "重新自查",
                    "itemValue": "重新自查",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1302,
                    "dictItemId": "478928167801982976",
                    "dictId": "478916229512564736",
                    "itemText": "模型组监督组织自查",
                    "itemValue": "模型组监督组织自查",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1303,
                    "dictItemId": "478928394105655296",
                    "dictId": "478916229512564736",
                    "itemText": "待办转交",
                    "itemValue": "待办转交",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1304,
                    "dictItemId": "478928613400645632",
                    "dictId": "478916229512564736",
                    "itemText": "业务自查",
                    "itemValue": "业务自查",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 207,
            "dictId": "466997418341974016",
            "dictName": "待办环节",
            "dictCode": "todoLink",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-07-12 16:01:00",
            "updateUser": "admin",
            "updateTime": "2023-08-08 14:11:32",
            "itemDtoList": [
                {
                    "id": 1255,
                    "dictItemId": "466997418350362624",
                    "dictId": "466997418341974016",
                    "itemText": "业务自查",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1256,
                    "dictItemId": "467310516738965504",
                    "dictId": "466997418341974016",
                    "itemText": "核查处理",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1257,
                    "dictItemId": "467310596485267456",
                    "dictId": "466997418341974016",
                    "itemText": "纪检研判",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1258,
                    "dictItemId": "467310659659874304",
                    "dictId": "466997418341974016",
                    "itemText": "流程结束",
                    "itemValue": "30",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1259,
                    "dictItemId": "467312996893835264",
                    "dictId": "466997418341974016",
                    "itemText": "待办移交",
                    "itemValue": "40",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1260,
                    "dictItemId": "467312996906418176",
                    "dictId": "466997418341974016",
                    "itemText": "管理员初审",
                    "itemValue": "50",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1261,
                    "dictItemId": "467312996914806784",
                    "dictId": "466997418341974016",
                    "itemText": "整改处理",
                    "itemValue": "60",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1262,
                    "dictItemId": "467312996923195392",
                    "dictId": "466997418341974016",
                    "itemText": "部门领导审核",
                    "itemValue": "70",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1263,
                    "dictItemId": "467312996931584000",
                    "dictId": "466997418341974016",
                    "itemText": "监督组织自查",
                    "itemValue": "80",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1264,
                    "dictItemId": "467312996939972608",
                    "dictId": "466997418341974016",
                    "itemText": "联络员审核",
                    "itemValue": "90",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1265,
                    "dictItemId": "467312996948361216",
                    "dictId": "466997418341974016",
                    "itemText": "检查员自查",
                    "itemValue": "100",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1266,
                    "dictItemId": "467312996952555520",
                    "dictId": "466997418341974016",
                    "itemText": "待办转交",
                    "itemValue": "150",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1267,
                    "dictItemId": "467312996960944128",
                    "dictId": "466997418341974016",
                    "itemText": "待办创建",
                    "itemValue": "200",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1293,
                    "dictItemId": "476406397545168896",
                    "dictId": "466997418341974016",
                    "itemText": "模型组监督组织自查",
                    "itemValue": "110",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1294,
                    "dictItemId": "476754339327250432",
                    "dictId": "466997418341974016",
                    "itemText": "模型组监督组织审核",
                    "itemValue": "120",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 206,
            "dictId": "466997326071480320",
            "dictName": "待办类型",
            "dictCode": "todoType",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-07-12 16:00:38",
            "updateUser": "admin",
            "updateTime": "2023-07-13 12:57:13",
            "itemDtoList": [
                {
                    "id": 1254,
                    "dictItemId": "466997326084063232",
                    "dictId": "466997326071480320",
                    "itemText": "疑似问题",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1268,
                    "dictItemId": "467313552592977920",
                    "dictId": "466997326071480320",
                    "itemText": "移交问题",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1269,
                    "dictItemId": "467313552601366528",
                    "dictId": "466997326071480320",
                    "itemText": "确认问题",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1270,
                    "dictItemId": "467313552613949440",
                    "dictId": "466997326071480320",
                    "itemText": "问题线索",
                    "itemValue": "30",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1271,
                    "dictItemId": "467313552618143744",
                    "dictId": "466997326071480320",
                    "itemText": "不是问题",
                    "itemValue": "40",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1272,
                    "dictItemId": "467313552630726656",
                    "dictId": "466997326071480320",
                    "itemText": "留存待查",
                    "itemValue": "50",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1273,
                    "dictItemId": "467313552639115264",
                    "dictId": "466997326071480320",
                    "itemText": "不是问题线索",
                    "itemValue": "60",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1274,
                    "dictItemId": "467313552643309568",
                    "dictId": "466997326071480320",
                    "itemText": "移交纪检",
                    "itemValue": "110",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1275,
                    "dictItemId": "467313552651698176",
                    "dictId": "466997326071480320",
                    "itemText": "转交业务",
                    "itemValue": "120",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1276,
                    "dictItemId": "467313552660086784",
                    "dictId": "466997326071480320",
                    "itemText": "重新自查",
                    "itemValue": "130",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1277,
                    "dictItemId": "467313552664281088",
                    "dictId": "466997326071480320",
                    "itemText": "转交审计",
                    "itemValue": "140",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1278,
                    "dictItemId": "467313552672669696",
                    "dictId": "466997326071480320",
                    "itemText": "流程结束",
                    "itemValue": "150",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1279,
                    "dictItemId": "467313552681058304",
                    "dictId": "466997326071480320",
                    "itemText": "无需推送",
                    "itemValue": "160",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1280,
                    "dictItemId": "467313552689446912",
                    "dictId": "466997326071480320",
                    "itemText": "整改结束",
                    "itemValue": "170",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1281,
                    "dictItemId": "467313552697835520",
                    "dictId": "466997326071480320",
                    "itemText": "待办创建",
                    "itemValue": "200",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 180,
            "dictId": "458639327375265792",
            "dictName": "审批状态",
            "dictCode": "AUDIT_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-06-19 14:28:56",
            "updateUser": "admin",
            "updateTime": "2023-06-20 13:59:50",
            "itemDtoList": [
                {
                    "id": 1114,
                    "dictItemId": "458639327392043008",
                    "dictId": "458639327375265792",
                    "itemText": "待审批",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1115,
                    "dictItemId": "458639327392043009",
                    "dictId": "458639327375265792",
                    "itemText": "已审批",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 173,
            "dictId": "454695967184420864",
            "dictName": "评价等级",
            "dictCode": "EVALUATION_LEVEL",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-06-08 17:19:26",
            "updateUser": "admin",
            "updateTime": "2023-06-08 18:44:34",
            "itemDtoList": [
                {
                    "id": 1099,
                    "dictItemId": "454695967192809472",
                    "dictId": "454695967184420864",
                    "itemText": "未完成",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1100,
                    "dictItemId": "454695967192809473",
                    "dictId": "454695967184420864",
                    "itemText": "已完成",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1101,
                    "dictItemId": "454695967192809474",
                    "dictId": "454695967184420864",
                    "itemText": "待完善",
                    "itemValue": "3",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 172,
            "dictId": "454683958677696512",
            "dictName": "项目性质",
            "dictCode": "PROJECT_FEATURE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-06-08 16:31:43",
            "itemDtoList": [
                {
                    "id": 1097,
                    "dictItemId": "454683958686085120",
                    "dictId": "454683958677696512",
                    "itemText": "年初计划",
                    "itemValue": "A",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1098,
                    "dictItemId": "454683958686085121",
                    "dictId": "454683958677696512",
                    "itemText": "临时交办",
                    "itemValue": "B",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 171,
            "dictId": "454683686932934656",
            "dictName": "任务状态",
            "dictCode": "TASK_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-06-08 16:30:38",
            "itemDtoList": [
                {
                    "id": 1092,
                    "dictItemId": "454683686941323264",
                    "dictId": "454683686932934656",
                    "itemText": "持续跟踪",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1093,
                    "dictItemId": "454683686941323265",
                    "dictId": "454683686932934656",
                    "itemText": "拟关闭",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1094,
                    "dictItemId": "454683686941323266",
                    "dictId": "454683686932934656",
                    "itemText": "已关闭",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1095,
                    "dictItemId": "454683686941323267",
                    "dictId": "454683686932934656",
                    "itemText": "挂起",
                    "itemValue": "40",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1096,
                    "dictItemId": "454683686941323268",
                    "dictId": "454683686932934656",
                    "itemText": "重点关注",
                    "itemValue": "50",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 170,
            "dictId": "454677415781560320",
            "dictName": "项目月份",
            "dictCode": "PROJECT_DEF_MONTH",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-06-08 16:05:43",
            "itemDtoList": [
                {
                    "id": 1079,
                    "dictItemId": "454677415798337536",
                    "dictId": "454677415781560320",
                    "itemText": "年度",
                    "itemValue": "0",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1080,
                    "dictItemId": "454677415798337537",
                    "dictId": "454677415781560320",
                    "itemText": "1月份",
                    "itemValue": "1",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1081,
                    "dictItemId": "454677415798337538",
                    "dictId": "454677415781560320",
                    "itemText": "2月份",
                    "itemValue": "2",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1082,
                    "dictItemId": "454677415798337539",
                    "dictId": "454677415781560320",
                    "itemText": "3月份",
                    "itemValue": "3",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1083,
                    "dictItemId": "454677415798337540",
                    "dictId": "454677415781560320",
                    "itemText": "4月份",
                    "itemValue": "4",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1084,
                    "dictItemId": "454677415798337541",
                    "dictId": "454677415781560320",
                    "itemText": "5月份",
                    "itemValue": "5",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1085,
                    "dictItemId": "454677415798337542",
                    "dictId": "454677415781560320",
                    "itemText": "6月份",
                    "itemValue": "6",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1086,
                    "dictItemId": "454677415798337543",
                    "dictId": "454677415781560320",
                    "itemText": "7月份",
                    "itemValue": "7",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1087,
                    "dictItemId": "454677415798337544",
                    "dictId": "454677415781560320",
                    "itemText": "8月份",
                    "itemValue": "8",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1088,
                    "dictItemId": "454677415798337545",
                    "dictId": "454677415781560320",
                    "itemText": "9月份",
                    "itemValue": "9",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1089,
                    "dictItemId": "454677415798337546",
                    "dictId": "454677415781560320",
                    "itemText": "10月份",
                    "itemValue": "10",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1090,
                    "dictItemId": "454677415798337547",
                    "dictId": "454677415781560320",
                    "itemText": "11月份",
                    "itemValue": "11",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1091,
                    "dictItemId": "454677415798337548",
                    "dictId": "454677415781560320",
                    "itemText": "12月份",
                    "itemValue": "12",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 169,
            "dictId": "454676923605151744",
            "dictName": "项目类型",
            "dictCode": "PROJECT_TYPE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-06-08 16:03:46",
            "itemDtoList": [
                {
                    "id": 1076,
                    "dictItemId": "454676923621928960",
                    "dictId": "454676923605151744",
                    "itemText": "战略任务",
                    "itemValue": "A",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1077,
                    "dictItemId": "454676923621928961",
                    "dictId": "454676923605151744",
                    "itemText": "重点工作",
                    "itemValue": "B",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1078,
                    "dictItemId": "454676923621928962",
                    "dictId": "454676923605151744",
                    "itemText": "决定事项",
                    "itemValue": "C",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 168,
            "dictId": "448782265152016384",
            "dictName": "字段类型",
            "dictCode": "fieldType",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-05-23 09:40:29",
            "updateUser": "admin",
            "updateTime": "2023-07-20 09:49:26",
            "itemDtoList": [
                {
                    "id": 1048,
                    "dictItemId": "448782265168793600",
                    "dictId": "448782265152016384",
                    "itemText": "文本",
                    "itemValue": "VARCHAR",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1055,
                    "dictItemId": "448871219587026944",
                    "dictId": "448782265152016384",
                    "itemText": "数值",
                    "itemValue": "DECIMAL",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1105,
                    "dictItemId": "456114867740999680",
                    "dictId": "448782265152016384",
                    "itemText": "日期",
                    "itemValue": "DATE",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 167,
            "dictId": "448492731221233664",
            "dictName": "报支状态代码",
            "dictCode": "REPORTEX_STATUS_CODE",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "obei100",
            "createTime": "2023-05-22 14:29:59",
            "updateUser": "obei100",
            "updateTime": "2023-05-23 15:49:14",
            "itemDtoList": [
                {
                    "id": 1063,
                    "dictItemId": "448873804914069504",
                    "dictId": "448492731221233664",
                    "itemText": "草稿",
                    "itemValue": "DRAFT",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1064,
                    "dictItemId": "448875060021792768",
                    "dictId": "448492731221233664",
                    "itemText": "未递交",
                    "itemValue": "CREATED",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1065,
                    "dictItemId": "448875060030181376",
                    "dictId": "448492731221233664",
                    "itemText": "已递交",
                    "itemValue": "SUBMITED",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1066,
                    "dictItemId": "448875060034375680",
                    "dictId": "448492731221233664",
                    "itemText": "待自动发送财务",
                    "itemValue": "HANDOVERING",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1067,
                    "dictItemId": "448875060038569984",
                    "dictId": "448492731221233664",
                    "itemText": "发送财务中",
                    "itemValue": "POSTING",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1068,
                    "dictItemId": "448875060042764288",
                    "dictId": "448492731221233664",
                    "itemText": "财务接收成功",
                    "itemValue": "HANDOVERED",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1069,
                    "dictItemId": "448875060046958592",
                    "dictId": "448492731221233664",
                    "itemText": "财务初核成功",
                    "itemValue": "AUDITED",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1070,
                    "dictItemId": "448875060051152896",
                    "dictId": "448492731221233664",
                    "itemText": "财务复核通过",
                    "itemValue": "REVIEWED",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1071,
                    "dictItemId": "448875060051152897",
                    "dictId": "448492731221233664",
                    "itemText": "部分付款",
                    "itemValue": "PARTIALLY PAID",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1072,
                    "dictItemId": "448875060055347200",
                    "dictId": "448492731221233664",
                    "itemText": "全部付款",
                    "itemValue": "PAID",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1073,
                    "dictItemId": "448875060059541504",
                    "dictId": "448492731221233664",
                    "itemText": "逻辑删除",
                    "itemValue": "CANCELLED",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1074,
                    "dictItemId": "448875060063735808",
                    "dictId": "448492731221233664",
                    "itemText": "红冲",
                    "itemValue": "RED",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                }
            ]
        },
        {
            "id": 166,
            "dictId": "444924828054577152",
            "dictName": "代码类型",
            "dictCode": "MANUAL_CODE_TYPE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-05-12 18:12:25",
            "itemDtoList": [
                {
                    "id": 1032,
                    "dictItemId": "444924828067160064",
                    "dictId": "444924828054577152",
                    "itemText": "物料",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1033,
                    "dictItemId": "444924828067160065",
                    "dictId": "444924828054577152",
                    "itemText": "大类",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1034,
                    "dictItemId": "444924828067160066",
                    "dictId": "444924828054577152",
                    "itemText": "中类",
                    "itemValue": "3",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1035,
                    "dictItemId": "444924828067160067",
                    "dictId": "444924828054577152",
                    "itemText": "叶类",
                    "itemValue": "4",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 165,
            "dictId": "444924737495359488",
            "dictName": "地区",
            "dictCode": "MANUAL_AREA",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-05-12 18:12:03",
            "itemDtoList": [
                {
                    "id": 1020,
                    "dictItemId": "444924737533108224",
                    "dictId": "444924737495359488",
                    "itemText": "股份总部(宝山)",
                    "itemValue": "SH",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1021,
                    "dictItemId": "444924737533108225",
                    "dictId": "444924737495359488",
                    "itemText": "华北大区(TY)",
                    "itemValue": "TY",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1022,
                    "dictItemId": "444924737533108226",
                    "dictId": "444924737495359488",
                    "itemText": "华东大区(MAS)",
                    "itemValue": "MAS",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1023,
                    "dictItemId": "444924737533108227",
                    "dictId": "444924737495359488",
                    "itemText": "华东大区(MS)",
                    "itemValue": "MS",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1024,
                    "dictItemId": "444924737533108228",
                    "dictId": "444924737495359488",
                    "itemText": "华南大区(FZ)",
                    "itemValue": "FZ",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1025,
                    "dictItemId": "444924737533108229",
                    "dictId": "444924737495359488",
                    "itemText": "华南大区(SG)",
                    "itemValue": "SG",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1026,
                    "dictItemId": "444924737533108230",
                    "dictId": "444924737495359488",
                    "itemText": "华南大区(ZJ)",
                    "itemValue": "ZJ",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1027,
                    "dictItemId": "444924737533108231",
                    "dictId": "444924737495359488",
                    "itemText": "华中大区(EZ)",
                    "itemValue": "EZ",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1028,
                    "dictItemId": "444924737533108232",
                    "dictId": "444924737495359488",
                    "itemText": "华中大区(WH)",
                    "itemValue": "WH",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1029,
                    "dictItemId": "444924737533108233",
                    "dictId": "444924737495359488",
                    "itemText": "西北大区(XJ)",
                    "itemValue": "XJ",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1030,
                    "dictItemId": "444924737533108234",
                    "dictId": "444924737495359488",
                    "itemText": "西南大区(CQ)",
                    "itemValue": "CQ",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1031,
                    "dictItemId": "444924737533108235",
                    "dictId": "444924737495359488",
                    "itemText": "西南大区(KM)",
                    "itemValue": "KM",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 164,
            "dictId": "436947523260473344",
            "dictName": "测试",
            "dictCode": "test",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-04-20 17:53:27",
            "updateUser": "admin",
            "updateTime": "2023-06-16 09:25:20",
            "itemDtoList": [
                {
                    "id": 1017,
                    "dictItemId": "436947523268861952",
                    "dictId": "436947523260473344",
                    "itemText": "宝钢",
                    "itemValue": "60",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 162,
            "dictId": "436940476502597632",
            "dictName": "产生方式",
            "dictCode": "generationMethod",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-04-20 17:25:27",
            "itemDtoList": [
                {
                    "id": 1015,
                    "dictItemId": "436940476519374848",
                    "dictId": "436940476502597632",
                    "itemText": "自建",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1016,
                    "dictItemId": "436940476519374849",
                    "dictId": "436940476502597632",
                    "itemText": "开发",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 161,
            "dictId": "436940320289939456",
            "dictName": "整改类型",
            "dictCode": "RECTIFY_TYPE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-04-20 17:24:50",
            "itemDtoList": [
                {
                    "id": 1013,
                    "dictItemId": "436940321581785088",
                    "dictId": "436940320289939456",
                    "itemText": "就事论事整改",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1014,
                    "dictItemId": "436940321581785089",
                    "dictId": "436940320289939456",
                    "itemText": "举一反三整改",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 160,
            "dictId": "436940085856092160",
            "dictName": "所属防线",
            "dictCode": "lineOfDefense",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-04-20 17:23:54",
            "updateUser": "admin",
            "updateTime": "2023-04-20 17:24:00",
            "itemDtoList": [
                {
                    "id": 1010,
                    "dictItemId": "436940086975971328",
                    "dictId": "436940085856092160",
                    "itemText": "第一道防线",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1011,
                    "dictItemId": "436940086975971329",
                    "dictId": "436940085856092160",
                    "itemText": "第二道防线",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1012,
                    "dictItemId": "436940086975971330",
                    "dictId": "436940085856092160",
                    "itemText": "第三道防线",
                    "itemValue": "3",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 159,
            "dictId": "434354936558821376",
            "dictName": "策略状态代码",
            "dictCode": "T924_STRATEGY_STATUS_CODE",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-04-13 14:11:26",
            "updateUser": "admin",
            "updateTime": "2023-04-20 17:53:00",
            "itemDtoList": [
                {
                    "id": 1005,
                    "dictItemId": "434354936571404288",
                    "dictId": "434354936558821376",
                    "itemText": "自动审批通过",
                    "itemValue": "生效",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1006,
                    "dictItemId": "434354936571404289",
                    "dictId": "434354936558821376",
                    "itemText": "突破协议价不突破二级价",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1007,
                    "dictItemId": "434354936571404290",
                    "dictId": "434354936558821376",
                    "itemText": "突破二级价不突破三级价",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1008,
                    "dictItemId": "434354936571404291",
                    "dictId": "434354936558821376",
                    "itemText": "突破三级价不突破战略价VV3",
                    "itemValue": "30",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1009,
                    "dictItemId": "434354936571404292",
                    "dictId": "434354936558821376",
                    "itemText": "突破战略价VV3",
                    "itemValue": "40",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 158,
            "dictId": "428958298014040064",
            "dictName": "报支大类",
            "dictCode": "CATEGORY_MAIN_CODE\t",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "朱锐",
            "createTime": "2023-03-29 16:47:07",
            "updateUser": "朱锐",
            "updateTime": "2023-03-29 16:48:11",
            "itemDtoList": [
                {
                    "id": 999,
                    "dictItemId": "428958298026622976",
                    "dictId": "428958298014040064",
                    "itemText": "差旅费",
                    "itemValue": "FA4",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "朱锐",
                    "updateUser": "朱锐"
                },
                {
                    "id": 1000,
                    "dictItemId": "428958298026622977",
                    "dictId": "428958298014040064",
                    "itemText": "房屋租赁",
                    "itemValue": "FA5",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "朱锐",
                    "updateUser": "朱锐"
                }
            ]
        },
        {
            "id": 157,
            "dictId": "428934770376175616",
            "dictName": "责任中心",
            "dictCode": "T1_ORG_CODE",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-03-29 15:13:38",
            "updateUser": "admin",
            "updateTime": "2023-03-30 09:49:05",
            "itemDtoList": [
                {
                    "id": 998,
                    "dictItemId": "428934770380369920",
                    "dictId": "428934770376175616",
                    "itemText": "重量",
                    "itemValue": "BSWG2006",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 156,
            "dictId": "428920249225965568",
            "dictName": "客商名称",
            "dictCode": "T1_VENDOR_NAME",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-03-29 14:15:56",
            "updateUser": "admin",
            "updateTime": "2023-03-29 14:55:50",
            "itemDtoList": [
                {
                    "id": 997,
                    "dictItemId": "428920249234354176",
                    "dictId": "428920249225965568",
                    "itemText": "欧冶",
                    "itemValue": "欧冶工业品股份有限公司",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 154,
            "dictId": "418354729769328640",
            "dictName": "状态",
            "dictCode": "T1_STATUS",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-02-28 10:32:20",
            "updateUser": "admin",
            "updateTime": "2023-02-28 14:26:26",
            "itemDtoList": [
                {
                    "id": 983,
                    "dictItemId": "418354729786105856",
                    "dictId": "418354729769328640",
                    "itemText": "草稿",
                    "itemValue": "DRAFT",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 984,
                    "dictItemId": "418413643510947840",
                    "dictId": "418354729769328640",
                    "itemText": "审批中",
                    "itemValue": "INPROCESSING",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 985,
                    "dictItemId": "418413643519336448",
                    "dictId": "418354729769328640",
                    "itemText": "驳回",
                    "itemValue": "REJECT",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 986,
                    "dictItemId": "418413643527725056",
                    "dictId": "418354729769328640",
                    "itemText": "删除",
                    "itemValue": "DELETE",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 987,
                    "dictItemId": "418413643531919360",
                    "dictId": "418354729769328640",
                    "itemText": "审核完成",
                    "itemValue": "APPROVED",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 988,
                    "dictItemId": "418413643536113664",
                    "dictId": "418354729769328640",
                    "itemText": "修订",
                    "itemValue": "REVISION",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 989,
                    "dictItemId": "418413643536113665",
                    "dictId": "418354729769328640",
                    "itemText": "已发询",
                    "itemValue": "INQUSENT",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 990,
                    "dictItemId": "418413643540307968",
                    "dictId": "418354729769328640",
                    "itemText": "报价中",
                    "itemValue": "QUOTING",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 991,
                    "dictItemId": "418413643544502272",
                    "dictId": "418354729769328640",
                    "itemText": "已报价",
                    "itemValue": "QUOTED",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 992,
                    "dictItemId": "418413643548696576",
                    "dictId": "418354729769328640",
                    "itemText": "已开标",
                    "itemValue": "CHECKING",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 993,
                    "dictItemId": "418413643557085184",
                    "dictId": "418354729769328640",
                    "itemText": "已核价",
                    "itemValue": "CHECKED",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 994,
                    "dictItemId": "418413643565473792",
                    "dictId": "418354729769328640",
                    "itemText": "已生成拟签",
                    "itemValue": "SIGNED",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 995,
                    "dictItemId": "418413643565473793",
                    "dictId": "418354729769328640",
                    "itemText": "作废",
                    "itemValue": "INVALID",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 153,
            "dictId": "411536196375244800",
            "dictName": "采购方式",
            "dictCode": "PUR_METHOD",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-02-09 14:57:55",
            "updateUser": "admin",
            "updateTime": "2023-02-15 09:35:12",
            "itemDtoList": [
                {
                    "id": 978,
                    "dictItemId": "411536196392022016",
                    "dictId": "411536196375244800",
                    "itemText": "网上比价",
                    "itemValue": "online_compare",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 152,
            "dictId": "408318179109613568",
            "dictName": "合同号",
            "dictCode": "CONTR_NO",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-01-31 17:50:40",
            "updateUser": "admin",
            "updateTime": "2023-03-31 09:06:22",
            "itemDtoList": [
                {
                    "id": 977,
                    "dictItemId": "408318179122196480",
                    "dictId": "408318179109613568",
                    "itemText": "备件",
                    "itemValue": "2201HT08448",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 151,
            "dictId": "400353049665458176",
            "dictName": "审计纪检监督部人员标记",
            "dictCode": "AUDIT_PUSH_FLAG",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2023-01-09 18:20:05",
            "itemDtoList": [
                {
                    "id": 973,
                    "dictItemId": "400353049686429696",
                    "dictId": "400353049665458176",
                    "itemText": "审计纪检监督部人员推送",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 974,
                    "dictItemId": "400353049686429697",
                    "dictId": "400353049665458176",
                    "itemText": "非审计纪检监督部人员推送",
                    "itemValue": "0",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 150,
            "dictId": "394082014829572096",
            "dictName": "角色组编号",
            "dictCode": "ROLE_GROUP_CODE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-23 11:01:14",
            "updateUser": "admin",
            "updateTime": "2023-08-07 14:14:02",
            "itemDtoList": [
                {
                    "id": 957,
                    "dictItemId": "394082014837960704",
                    "dictId": "394082014829572096",
                    "itemText": "核查人员",
                    "itemValue": "82",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 958,
                    "dictItemId": "394082014837960705",
                    "dictId": "394082014829572096",
                    "itemText": "核查人员",
                    "itemValue": "83",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 959,
                    "dictItemId": "394082014837960706",
                    "dictId": "394082014829572096",
                    "itemText": "核查人员",
                    "itemValue": "90",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 960,
                    "dictItemId": "394082014837960707",
                    "dictId": "394082014829572096",
                    "itemText": "纪检组",
                    "itemValue": "83",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 961,
                    "dictItemId": "394082014837960708",
                    "dictId": "394082014829572096",
                    "itemText": "业务组",
                    "itemValue": "84",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 962,
                    "dictItemId": "394082014837960709",
                    "dictId": "394082014829572096",
                    "itemText": "业务组",
                    "itemValue": "85",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 963,
                    "dictItemId": "394082014837960710",
                    "dictId": "394082014829572096",
                    "itemText": "审计组",
                    "itemValue": "82",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 964,
                    "dictItemId": "394082014837960711",
                    "dictId": "394082014829572096",
                    "itemText": "审计组",
                    "itemValue": "90",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 965,
                    "dictItemId": "394082014837960712",
                    "dictId": "394082014829572096",
                    "itemText": "监督联络员",
                    "itemValue": "98",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 967,
                    "dictItemId": "396640845300813824",
                    "dictId": "394082014829572096",
                    "itemText": "业务人员",
                    "itemValue": "84",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 968,
                    "dictItemId": "396640845380505600",
                    "dictId": "394082014829572096",
                    "itemText": "业务人员",
                    "itemValue": "85",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 969,
                    "dictItemId": "396640845393088512",
                    "dictId": "394082014829572096",
                    "itemText": "业务人员",
                    "itemValue": "2",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1004,
                    "dictItemId": "433983349913636864",
                    "dictId": "394082014829572096",
                    "itemText": "部门领导",
                    "itemValue": "102",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1103,
                    "dictItemId": "455064441246588928",
                    "dictId": "394082014829572096",
                    "itemText": "检查员",
                    "itemValue": "118",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1291,
                    "dictItemId": "476392581189025792",
                    "dictId": "394082014829572096",
                    "itemText": "模型组监督联络员",
                    "itemValue": "131",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 149,
            "dictId": "394081302347345920",
            "dictName": "角色参数对照表",
            "dictCode": "ROLE_PARAMS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-23 10:58:24",
            "updateUser": "admin",
            "updateTime": "2023-08-07 14:14:32",
            "itemDtoList": [
                {
                    "id": 952,
                    "dictItemId": "394081302368317440",
                    "dictId": "394081302347345920",
                    "itemText": "inspection",
                    "itemValue": "纪检组",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 953,
                    "dictItemId": "394081302368317441",
                    "dictId": "394081302347345920",
                    "itemText": "business",
                    "itemValue": "业务组",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 954,
                    "dictItemId": "394081302368317442",
                    "dictId": "394081302347345920",
                    "itemText": "audit",
                    "itemValue": "审计组",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 955,
                    "dictItemId": "394081302368317443",
                    "dictId": "394081302347345920",
                    "itemText": "verification",
                    "itemValue": "核查人员",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 956,
                    "dictItemId": "394081302368317444",
                    "dictId": "394081302347345920",
                    "itemText": "rectification",
                    "itemValue": "监督联络员",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 966,
                    "dictItemId": "396640666535075840",
                    "dictId": "394081302347345920",
                    "itemText": "businessPerson",
                    "itemValue": "业务人员",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1104,
                    "dictItemId": "455064778468630528",
                    "dictId": "394081302347345920",
                    "itemText": "inspector",
                    "itemValue": "检查员",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1292,
                    "dictItemId": "476392708167385088",
                    "dictId": "394081302347345920",
                    "itemText": "modelGroupBusinessPerson",
                    "itemValue": "模型组监督联络员",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 148,
            "dictId": "393817452155645952",
            "dictName": "整改日志审批状态",
            "dictCode": "RECTIFICATIONLOG_AUDIT_STATUS",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-22 17:29:57",
            "itemDtoList": [
                {
                    "id": 950,
                    "dictItemId": "393817452159840256",
                    "dictId": "393817452155645952",
                    "itemText": "驳回",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 951,
                    "dictItemId": "393817452159840257",
                    "dictId": "393817452155645952",
                    "itemText": "审核通过",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 147,
            "dictId": "393781513026940928",
            "dictName": "流程整改状态",
            "dictCode": "CLUES_RECTIFICATION_STATUS",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-22 15:07:08",
            "updateUser": "admin",
            "updateTime": "2023-06-26 15:42:08",
            "itemDtoList": [
                {
                    "id": 943,
                    "dictItemId": "393781513035329536",
                    "dictId": "393781513026940928",
                    "itemText": "待整改",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 944,
                    "dictItemId": "393781513035329537",
                    "dictId": "393781513026940928",
                    "itemText": "纪检提交未整改",
                    "itemValue": "05",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 945,
                    "dictItemId": "393781513035329538",
                    "dictId": "393781513026940928",
                    "itemText": "整改中",
                    "itemValue": "10",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 946,
                    "dictItemId": "393781513035329539",
                    "dictId": "393781513026940928",
                    "itemText": "未按计划完成整改",
                    "itemValue": "20",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 947,
                    "dictItemId": "393781513035329540",
                    "dictId": "393781513026940928",
                    "itemText": "整改待核查",
                    "itemValue": "30",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 948,
                    "dictItemId": "393781513035329541",
                    "dictId": "393781513026940928",
                    "itemText": "整改结束",
                    "itemValue": "40",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 949,
                    "dictItemId": "393810618161364992",
                    "dictId": "393781513026940928",
                    "itemText": "驳回待整改",
                    "itemValue": "06",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1116,
                    "dictItemId": "461194464491851776",
                    "dictId": "393781513026940928",
                    "itemText": "部门领导整改",
                    "itemValue": "07",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 146,
            "dictId": "393780906111152128",
            "dictName": "部门类型",
            "dictCode": "DEPT_TYPE",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-22 15:04:44",
            "itemDtoList": [
                {
                    "id": 940,
                    "dictItemId": "393780906119540736",
                    "dictId": "393780906111152128",
                    "itemText": "业务",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 941,
                    "dictItemId": "393780906119540737",
                    "dictId": "393780906111152128",
                    "itemText": "职能",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 942,
                    "dictItemId": "393780906119540738",
                    "dictId": "393780906111152128",
                    "itemText": "审计纪检",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 145,
            "dictId": "393705092002754560",
            "dictName": "推送产生疑点部门",
            "dictCode": "PUSH_SUSPICIOUS_DEPARTMENT",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-22 10:03:28",
            "updateUser": "admin",
            "updateTime": "2022-12-23 10:56:12",
            "itemDtoList": [
                {
                    "id": 938,
                    "dictItemId": "393705092040503296",
                    "dictId": "393705092002754560",
                    "itemText": "审计部（纪检监督部）",
                    "itemValue": "5",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 144,
            "dictId": "392618455890014208",
            "dictName": "监督类别",
            "dictCode": "belongUnit",
            "type": "sys",
            "delFlag": 0,
            "createUser": "时月明",
            "createTime": "2022-12-19 10:05:34",
            "updateUser": "admin",
            "updateTime": "2023-09-12 15:23:37",
            "itemDtoList": [
                {
                    "id": 932,
                    "dictItemId": "392618455894208512",
                    "dictId": "392618455890014208",
                    "itemText": "职能部门",
                    "itemValue": "10",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 933,
                    "dictItemId": "392618455894208513",
                    "dictId": "392618455890014208",
                    "itemText": "业务部门",
                    "itemValue": "20",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 934,
                    "dictItemId": "392618455894208514",
                    "dictId": "392618455890014208",
                    "itemText": "审计监督部门",
                    "itemValue": "30",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                }
            ]
        },
        {
            "id": 143,
            "dictId": "391583622963277824",
            "dictName": "整改状态",
            "dictCode": "RECTIFICATION_STATE",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-16 13:33:30",
            "itemDtoList": [
                {
                    "id": 926,
                    "dictItemId": "391583622975860736",
                    "dictId": "391583622963277824",
                    "itemText": "未整改",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 927,
                    "dictItemId": "391583622975860737",
                    "dictId": "391583622963277824",
                    "itemText": "整改中",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 928,
                    "dictItemId": "391583622975860738",
                    "dictId": "391583622963277824",
                    "itemText": "已整改",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 142,
            "dictId": "391520487127711744",
            "dictName": "数据源分类",
            "dictCode": "dataSource",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-16 09:22:38",
            "updateUser": "admin",
            "updateTime": "2023-05-16 15:50:35",
            "itemDtoList": [
                {
                    "id": 923,
                    "dictItemId": "391520487136100352",
                    "dictId": "391520487127711744",
                    "itemText": "5S通用查询配置",
                    "itemValue": "147",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 924,
                    "dictItemId": "391520487136100353",
                    "dictId": "391520487127711744",
                    "itemText": "5S常规查询配置",
                    "itemValue": "148",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 925,
                    "dictItemId": "391520487136100354",
                    "dictId": "391520487127711744",
                    "itemText": "智能算法",
                    "itemValue": "149",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1037,
                    "dictItemId": "446337254416547840",
                    "dictId": "391520487127711744",
                    "itemText": "采购运营管理驾驶舱数据源",
                    "itemValue": "150",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1038,
                    "dictItemId": "446337254420742144",
                    "dictId": "391520487127711744",
                    "itemText": "物流履约大屏看板数据源",
                    "itemValue": "151",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1039,
                    "dictItemId": "446337254429130752",
                    "dictId": "391520487127711744",
                    "itemText": "大数据穿透式监督JDBC",
                    "itemValue": "152",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1040,
                    "dictItemId": "446337254433325056",
                    "dictId": "391520487127711744",
                    "itemText": "5s(DB2数据库)",
                    "itemValue": "153",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1041,
                    "dictItemId": "446337254437519360",
                    "dictId": "391520487127711744",
                    "itemText": "phoenix数据库",
                    "itemValue": "154",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1042,
                    "dictItemId": "446337254441713664",
                    "dictId": "391520487127711744",
                    "itemText": "mysql数据库",
                    "itemValue": "155",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1043,
                    "dictItemId": "446337254445907968",
                    "dictId": "391520487127711744",
                    "itemText": "数据湖",
                    "itemValue": "156",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1044,
                    "dictItemId": "446337254450102272",
                    "dictId": "391520487127711744",
                    "itemText": "5s(DB2数据库)",
                    "itemValue": "157",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 141,
            "dictId": "387648222152196096",
            "dictName": "数据集逻辑分区",
            "dictCode": "logicalPartition",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-12-05 16:55:38",
            "updateUser": "admin",
            "updateTime": "2022-12-16 09:21:18",
            "itemDtoList": [
                {
                    "id": 908,
                    "dictItemId": "387648222181556224",
                    "dictId": "387648222152196096",
                    "itemText": "BWDHMAJYFX",
                    "itemValue": "ADS",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 909,
                    "dictItemId": "388651301180809216",
                    "dictId": "387648222152196096",
                    "itemText": "BWDHMSCG00",
                    "itemValue": "采购",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 910,
                    "dictItemId": "388651301185003520",
                    "dictId": "387648222152196096",
                    "itemText": "BWDHMSCW00",
                    "itemValue": "财务",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 911,
                    "dictItemId": "388651301189197824",
                    "dictId": "387648222152196096",
                    "itemText": "BWDHMSWL00",
                    "itemValue": "物流",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 912,
                    "dictItemId": "388651301193392128",
                    "dictId": "387648222152196096",
                    "itemText": "BWDHMOSAAS",
                    "itemValue": "商城",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 913,
                    "dictItemId": "388651301197586432",
                    "dictId": "387648222152196096",
                    "itemText": "BWDHMSYX00",
                    "itemValue": "6",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                }
            ]
        },
        {
            "id": 138,
            "dictId": "378864132890124288",
            "dictName": "上线状态",
            "dictCode": "ONLINE_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-11-11 11:10:48",
            "itemDtoList": [
                {
                    "id": 900,
                    "dictItemId": "378864132948844544",
                    "dictId": "378864132890124288",
                    "itemText": "待上线",
                    "itemValue": "-1",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 901,
                    "dictItemId": "378864132948844545",
                    "dictId": "378864132890124288",
                    "itemText": "已下线",
                    "itemValue": "0",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 902,
                    "dictItemId": "378864132948844546",
                    "dictId": "378864132890124288",
                    "itemText": "已上线",
                    "itemValue": "1",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 137,
            "dictId": "378863792014888960",
            "dictName": "模型类型",
            "dictCode": "MODEL_TYPE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-11-11 11:09:27",
            "itemDtoList": [
                {
                    "id": 897,
                    "dictItemId": "378863793369649152",
                    "dictId": "378863792014888960",
                    "itemText": "静态",
                    "itemValue": "static",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 898,
                    "dictItemId": "378863793369649153",
                    "dictId": "378863792014888960",
                    "itemText": "动态",
                    "itemValue": "dynamic",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 899,
                    "dictItemId": "378863793369649154",
                    "dictId": "378863792014888960",
                    "itemText": "算法",
                    "itemValue": "algorithm",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 136,
            "dictId": "378862524944965632",
            "dictName": "运行状态",
            "dictCode": "RUN_STATUS",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-11-11 11:04:25",
            "itemDtoList": [
                {
                    "id": 893,
                    "dictItemId": "378862526635270144",
                    "dictId": "378862524944965632",
                    "itemText": "未运行",
                    "itemValue": "0",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 894,
                    "dictItemId": "378862526635270145",
                    "dictId": "378862524944965632",
                    "itemText": "运行中",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 895,
                    "dictItemId": "378862526635270146",
                    "dictId": "378862524944965632",
                    "itemText": "失败",
                    "itemValue": "6",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 896,
                    "dictItemId": "378862526635270147",
                    "dictId": "378862524944965632",
                    "itemText": "成功",
                    "itemValue": "7",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 135,
            "dictId": "376390004412387328",
            "dictName": "处理意见",
            "dictCode": "HANDLE_OPINION",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-11-04 15:19:30",
            "itemDtoList": [
                {
                    "id": 882,
                    "dictItemId": "376390004437553152",
                    "dictId": "376390004412387328",
                    "itemText": "不是问题",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 883,
                    "dictItemId": "376390004437553153",
                    "dictId": "376390004412387328",
                    "itemText": "是问题",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 884,
                    "dictItemId": "376390004437553154",
                    "dictId": "376390004412387328",
                    "itemText": "留存待查",
                    "itemValue": "3",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 885,
                    "dictItemId": "376390004437553155",
                    "dictId": "376390004412387328",
                    "itemText": "驳回业务自查",
                    "itemValue": "4",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 886,
                    "dictItemId": "376390004437553156",
                    "dictId": "376390004412387328",
                    "itemText": "转交审计",
                    "itemValue": "5",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 887,
                    "dictItemId": "376390004437553157",
                    "dictId": "376390004412387328",
                    "itemText": "移交纪检",
                    "itemValue": "6",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 888,
                    "dictItemId": "376390004437553158",
                    "dictId": "376390004412387328",
                    "itemText": "流程结束",
                    "itemValue": "7",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 889,
                    "dictItemId": "376390004437553159",
                    "dictId": "376390004412387328",
                    "itemText": "予以了结",
                    "itemValue": "8",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 890,
                    "dictItemId": "376390004437553160",
                    "dictId": "376390004412387328",
                    "itemText": "暂存待查",
                    "itemValue": "9",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 891,
                    "dictItemId": "376390004437553161",
                    "dictId": "376390004412387328",
                    "itemText": "谈话函询",
                    "itemValue": "10",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 892,
                    "dictItemId": "376390004437553162",
                    "dictId": "376390004412387328",
                    "itemText": "初步核实",
                    "itemValue": "11",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 134,
            "dictId": "376388935754059776",
            "dictName": "推送标记",
            "dictCode": "PUSH_TAG",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-11-04 15:15:15",
            "updateUser": "admin",
            "updateTime": "2023-07-04 17:56:41",
            "itemDtoList": [
                {
                    "id": 880,
                    "dictItemId": "376388935791808512",
                    "dictId": "376388935754059776",
                    "itemText": "已推送",
                    "itemValue": "0",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 881,
                    "dictItemId": "376388935791808513",
                    "dictId": "376388935754059776",
                    "itemText": "未推送",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 980,
                    "dictItemId": "411937543163502592",
                    "dictId": "376388935754059776",
                    "itemText": "无需推送",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 981,
                    "dictItemId": "411937544560205824",
                    "dictId": "376388935754059776",
                    "itemText": "部分推送",
                    "itemValue": "3",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 133,
            "dictId": "373785497279778816",
            "dictName": "查看所有模型的角色",
            "dictCode": "RolesOfViewingAllModel",
            "type": "sys",
            "delFlag": 0,
            "createUser": "时月明",
            "createTime": "2022-10-28 10:50:07",
            "updateUser": "admin",
            "updateTime": "2023-03-29 18:10:17",
            "itemDtoList": [
                {
                    "id": 874,
                    "dictItemId": "373785497367859200",
                    "dictId": "373785497279778816",
                    "itemText": "纪检人员",
                    "itemValue": "纪检人员",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 875,
                    "dictItemId": "373785497367859201",
                    "dictId": "373785497279778816",
                    "itemText": "公司领导",
                    "itemValue": "公司领导",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 876,
                    "dictItemId": "373785497367859202",
                    "dictId": "373785497279778816",
                    "itemText": "审计主任",
                    "itemValue": "审计主任",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 877,
                    "dictItemId": "373785497367859203",
                    "dictId": "373785497279778816",
                    "itemText": "审计领导",
                    "itemValue": "审计领导",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 878,
                    "dictItemId": "373785497367859204",
                    "dictId": "373785497279778816",
                    "itemText": "管理员",
                    "itemValue": "管理员",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 1001,
                    "dictItemId": "428979224269156352",
                    "dictId": "373785497279778816",
                    "itemText": "集团纪委",
                    "itemValue": "集团纪委",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 132,
            "dictId": "373634783748411392",
            "dictName": "核查人员角色组",
            "dictCode": "VERIFIED_ROLE_GROUP",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-10-28 00:51:14",
            "updateUser": "admin",
            "updateTime": "2022-10-28 13:57:42",
            "itemDtoList": [
                {
                    "id": 871,
                    "dictItemId": "373634783773577216",
                    "dictId": "373634783748411392",
                    "itemText": "核查人员",
                    "itemValue": "82",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 872,
                    "dictItemId": "373634783773577217",
                    "dictId": "373634783748411392",
                    "itemText": "核查人员",
                    "itemValue": "83",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 873,
                    "dictItemId": "373634783773577218",
                    "dictId": "373634783748411392",
                    "itemText": "核查人员",
                    "itemValue": "90",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 131,
            "dictId": "370887823642963968",
            "dictName": "纪检处理意见",
            "dictCode": "handlingOpinionsjijian",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-10-20 10:55:47",
            "updateUser": "admin",
            "updateTime": "2023-09-18 14:04:33",
            "itemDtoList": [
                {
                    "id": 866,
                    "dictItemId": "370887823663935488",
                    "dictId": "370887823642963968",
                    "itemText": "是问题线索",
                    "itemValue": "30",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 867,
                    "dictItemId": "370887823663935489",
                    "dictId": "370887823642963968",
                    "itemText": "不是问题线索",
                    "itemValue": "60",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 130,
            "dictId": "370656586454413312",
            "dictName": "待办类型",
            "dictCode": "TODO_TYPE",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-10-19 19:36:56",
            "updateUser": "admin",
            "updateTime": "2023-07-04 16:59:58",
            "itemDtoList": [
                {
                    "id": 860,
                    "dictItemId": "370656586504744960",
                    "dictId": "370656586454413312",
                    "itemText": "疑似问题",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 861,
                    "dictItemId": "370656586504744961",
                    "dictId": "370656586454413312",
                    "itemText": "移交问题",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 862,
                    "dictItemId": "370656586504744962",
                    "dictId": "370656586454413312",
                    "itemText": "确认问题",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 863,
                    "dictItemId": "370656586504744963",
                    "dictId": "370656586454413312",
                    "itemText": "问题线索",
                    "itemValue": "30",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 864,
                    "dictItemId": "370656586504744964",
                    "dictId": "370656586454413312",
                    "itemText": "不是问题",
                    "itemValue": "40",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 865,
                    "dictItemId": "370656586504744965",
                    "dictId": "370656586454413312",
                    "itemText": "留存待查",
                    "itemValue": "50",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1117,
                    "dictItemId": "464111740427730944",
                    "dictId": "370656586454413312",
                    "itemText": "不是问题线索",
                    "itemValue": "60",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1118,
                    "dictItemId": "464111740444508160",
                    "dictId": "370656586454413312",
                    "itemText": "移交纪检",
                    "itemValue": "110",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1119,
                    "dictItemId": "464111740448702464",
                    "dictId": "370656586454413312",
                    "itemText": "转交业务",
                    "itemValue": "120",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1120,
                    "dictItemId": "464111740452896768",
                    "dictId": "370656586454413312",
                    "itemText": "重新自查",
                    "itemValue": "130",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1121,
                    "dictItemId": "464111740457091072",
                    "dictId": "370656586454413312",
                    "itemText": "转交审计",
                    "itemValue": "140",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1122,
                    "dictItemId": "464111740465479680",
                    "dictId": "370656586454413312",
                    "itemText": "流程结束",
                    "itemValue": "150",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1123,
                    "dictItemId": "464111740469673984",
                    "dictId": "370656586454413312",
                    "itemText": "无需推送",
                    "itemValue": "160",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1124,
                    "dictItemId": "464111740473868288",
                    "dictId": "370656586454413312",
                    "itemText": "整改结束",
                    "itemValue": "170",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1125,
                    "dictItemId": "464111740478062592",
                    "dictId": "370656586454413312",
                    "itemText": "待办创建",
                    "itemValue": "200",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 128,
            "dictId": "370160714188742656",
            "dictName": "历史环节",
            "dictCode": "HISTORY_LINK",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-10-18 10:46:31",
            "updateUser": "admin",
            "updateTime": "2023-08-14 14:12:10",
            "itemDtoList": [
                {
                    "id": 854,
                    "dictItemId": "370160714205519874",
                    "dictId": "370160714188742656",
                    "itemText": "业务自查",
                    "itemValue": "业务自查",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 855,
                    "dictItemId": "370160714205519875",
                    "dictId": "370160714188742656",
                    "itemText": "核查处理",
                    "itemValue": "核查处理",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 856,
                    "dictItemId": "370160714205519876",
                    "dictId": "370160714188742656",
                    "itemText": "纪检研判",
                    "itemValue": "纪检研判",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 929,
                    "dictItemId": "391583817745891328",
                    "dictId": "370160714188742656",
                    "itemText": "流程结束",
                    "itemValue": "流程结束",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1282,
                    "dictItemId": "467332923151335424",
                    "dictId": "370160714188742656",
                    "itemText": "待办移交",
                    "itemValue": "待办移交",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1283,
                    "dictItemId": "467332923163918336",
                    "dictId": "370160714188742656",
                    "itemText": "管理员初审",
                    "itemValue": "管理员初审",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1284,
                    "dictItemId": "467332923176501248",
                    "dictId": "370160714188742656",
                    "itemText": "整改处理",
                    "itemValue": "整改处理",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1285,
                    "dictItemId": "467332923184889856",
                    "dictId": "370160714188742656",
                    "itemText": "部门领导审核",
                    "itemValue": "部门领导审核",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1286,
                    "dictItemId": "467332923193278464",
                    "dictId": "370160714188742656",
                    "itemText": "监督组织自查",
                    "itemValue": "监督组织自查",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1287,
                    "dictItemId": "467332923201667072",
                    "dictId": "370160714188742656",
                    "itemText": "联络员审核",
                    "itemValue": "联络员审核",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1289,
                    "dictItemId": "467332923218444288",
                    "dictId": "370160714188742656",
                    "itemText": "待办转交",
                    "itemValue": "待办转交",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1290,
                    "dictItemId": "467332923226832896",
                    "dictId": "370160714188742656",
                    "itemText": "待办创建",
                    "itemValue": "待办创建",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 126,
            "dictId": "369973889676390400",
            "dictName": "监督领域",
            "dictCode": "bizSystem",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-10-17 22:24:09",
            "updateUser": "admin",
            "updateTime": "2023-09-01 11:17:41",
            "itemDtoList": [
                {
                    "id": 839,
                    "dictItemId": "369973889705750528",
                    "dictId": "369973889676390400",
                    "itemText": "设备采购",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 840,
                    "dictItemId": "369973889705750529",
                    "dictId": "369973889676390400",
                    "itemText": "资材备件采购",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 842,
                    "dictItemId": "369973889705750531",
                    "dictId": "369973889676390400",
                    "itemText": "物流",
                    "itemValue": "4",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 970,
                    "dictItemId": "398491932442021888",
                    "dictId": "369973889676390400",
                    "itemText": "费用",
                    "itemValue": "5",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 971,
                    "dictItemId": "398491932450410496",
                    "dictId": "369973889676390400",
                    "itemText": "财务",
                    "itemValue": "6",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 1045,
                    "dictItemId": "447423108744462336",
                    "dictId": "369973889676390400",
                    "itemText": "平台业务-商城",
                    "itemValue": "7",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1046,
                    "dictItemId": "447423629245370368",
                    "dictId": "369973889676390400",
                    "itemText": "平台业务-易购",
                    "itemValue": "10",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "obei100",
                    "updateUser": "obei100"
                },
                {
                    "id": 1110,
                    "dictItemId": "457484078992973824",
                    "dictId": "369973889676390400",
                    "itemText": "平台业务-云库",
                    "itemValue": "11",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1111,
                    "dictItemId": "457484079001362432",
                    "dictId": "369973889676390400",
                    "itemText": "平台业务-易采",
                    "itemValue": "12",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1112,
                    "dictItemId": "457484079005556736",
                    "dictId": "369973889676390400",
                    "itemText": "平台业务-零碳",
                    "itemValue": "13",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 123,
            "dictId": "123",
            "dictName": "模型业务用途",
            "dictCode": "businessPurpose",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-09-30 15:35:04",
            "itemDtoList": [
                {
                    "id": 736,
                    "dictItemId": "736",
                    "dictId": "123",
                    "itemText": "疑点模型",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 737,
                    "dictItemId": "737",
                    "dictId": "123",
                    "itemText": "分析模型",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 121,
            "dictId": "121",
            "dictName": "图表类型",
            "dictCode": "chartType",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-09-06 15:35:38",
            "updateUser": "admin",
            "updateTime": "2023-04-03 10:17:55",
            "itemDtoList": [
                {
                    "id": 653,
                    "dictItemId": "653",
                    "dictId": "121",
                    "itemText": "饼图",
                    "itemValue": "pie",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 654,
                    "dictItemId": "654",
                    "dictId": "121",
                    "itemText": "明细表",
                    "itemValue": "table",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 655,
                    "dictItemId": "655",
                    "dictId": "121",
                    "itemText": "竖向表",
                    "itemValue": "portraitTable",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 656,
                    "dictItemId": "656",
                    "dictId": "121",
                    "itemText": "折线图",
                    "itemValue": "line",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 657,
                    "dictItemId": "657",
                    "dictId": "121",
                    "itemText": "雷达图",
                    "itemValue": "radar",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 658,
                    "dictItemId": "658",
                    "dictId": "121",
                    "itemText": "柱状图",
                    "itemValue": "bar",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 848,
                    "dictItemId": "370160098821431296",
                    "dictId": "121",
                    "itemText": "组合柱状图",
                    "itemValue": "BspManyBar",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 849,
                    "dictItemId": "370160098859180032",
                    "dictId": "121",
                    "itemText": "散点图",
                    "itemValue": "scatter",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 850,
                    "dictItemId": "370160098880151552",
                    "dictId": "121",
                    "itemText": "条形图",
                    "itemValue": "flatBar",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 851,
                    "dictItemId": "370160098901123072",
                    "dictId": "121",
                    "itemText": "面积图",
                    "itemValue": "seriesLine",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 1003,
                    "dictItemId": "430670405346557952",
                    "dictId": "121",
                    "itemText": "指标卡",
                    "itemValue": "editor",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 115,
            "dictId": "115",
            "dictName": "处理状态",
            "dictCode": "handleStatus",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-06-28 17:03:54",
            "updateUser": "admin",
            "updateTime": "2022-11-04 15:12:59",
            "itemDtoList": [
                {
                    "id": 646,
                    "dictItemId": "646",
                    "dictId": "115",
                    "itemText": "未处理",
                    "itemValue": "00",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 647,
                    "dictItemId": "647",
                    "dictId": "115",
                    "itemText": "无需处理",
                    "itemValue": "10",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 648,
                    "dictItemId": "648",
                    "dictId": "115",
                    "itemText": "已处理",
                    "itemValue": "20",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 879,
                    "dictItemId": "376368084190486528",
                    "dictId": "115",
                    "itemText": "处理中",
                    "itemValue": "15",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 114,
            "dictId": "114",
            "dictName": "监督业务对象",
            "dictCode": "businessObject",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-06-28 16:41:39",
            "updateUser": "吴游旻",
            "updateTime": "2023-06-08 17:25:23",
            "itemDtoList": [
                {
                    "id": 721,
                    "dictItemId": "721",
                    "dictId": "114",
                    "itemText": "订单",
                    "itemValue": "order",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 722,
                    "dictItemId": "722",
                    "dictId": "114",
                    "itemText": "合同",
                    "itemValue": "contract",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 723,
                    "dictItemId": "723",
                    "dictId": "114",
                    "itemText": "会员",
                    "itemValue": "member",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 724,
                    "dictItemId": "724",
                    "dictId": "114",
                    "itemText": "商品",
                    "itemValue": "commodity",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 725,
                    "dictItemId": "725",
                    "dictId": "114",
                    "itemText": "物料",
                    "itemValue": "material",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 726,
                    "dictItemId": "726",
                    "dictId": "114",
                    "itemText": "质量",
                    "itemValue": "quality",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 727,
                    "dictItemId": "727",
                    "dictId": "114",
                    "itemText": "计划",
                    "itemValue": "plan",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 728,
                    "dictItemId": "728",
                    "dictId": "114",
                    "itemText": "结算",
                    "itemValue": "settlement",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 729,
                    "dictItemId": "729",
                    "dictId": "114",
                    "itemText": "供应商",
                    "itemValue": "supplier",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 730,
                    "dictItemId": "730",
                    "dictId": "114",
                    "itemText": "库存",
                    "itemValue": "stock",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 731,
                    "dictItemId": "731",
                    "dictId": "114",
                    "itemText": "授权",
                    "itemValue": "authority",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 732,
                    "dictItemId": "732",
                    "dictId": "114",
                    "itemText": "费用",
                    "itemValue": "fee",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 733,
                    "dictItemId": "733",
                    "dictId": "114",
                    "itemText": "交易",
                    "itemValue": "trade",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 734,
                    "dictItemId": "734",
                    "dictId": "114",
                    "itemText": "询价单",
                    "itemValue": "INQUIRY",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 735,
                    "dictItemId": "735",
                    "dictId": "114",
                    "itemText": "拟签单",
                    "itemValue": "presign",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 870,
                    "dictItemId": "372325587970273280",
                    "dictId": "114",
                    "itemText": "其他",
                    "itemValue": "Other",
                    "sortOrder": 16,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 914,
                    "dictItemId": "388668329501401088",
                    "dictId": "114",
                    "itemText": "报价单",
                    "itemValue": "quotation",
                    "sortOrder": 17,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 915,
                    "dictItemId": "388668329505595392",
                    "dictId": "114",
                    "itemText": "到货单",
                    "itemValue": "receive",
                    "sortOrder": 18,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 916,
                    "dictItemId": "388668329509789696",
                    "dictId": "114",
                    "itemText": "入库单",
                    "itemValue": "instock",
                    "sortOrder": 19,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 917,
                    "dictItemId": "388668329513984000",
                    "dictId": "114",
                    "itemText": "领用单",
                    "itemValue": "Takeout",
                    "sortOrder": 20,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 918,
                    "dictItemId": "388668329518178304",
                    "dictId": "114",
                    "itemText": "出库单",
                    "itemValue": "Outstock",
                    "sortOrder": 21,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 919,
                    "dictItemId": "388668329522372608",
                    "dictId": "114",
                    "itemText": "财务票据",
                    "itemValue": "Finance",
                    "sortOrder": 22,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 920,
                    "dictItemId": "388668329526566912",
                    "dictId": "114",
                    "itemText": "运输单",
                    "itemValue": "transport",
                    "sortOrder": 23,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 922,
                    "dictItemId": "388668329534955520",
                    "dictId": "114",
                    "itemText": "采购方",
                    "itemValue": "purchaser",
                    "sortOrder": 25,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 936,
                    "dictItemId": "393342443243057152",
                    "dictId": "114",
                    "itemText": "送货单",
                    "itemValue": "delivery",
                    "sortOrder": 27,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 1075,
                    "dictItemId": "454606308479631360",
                    "dictId": "114",
                    "itemText": "通用管理",
                    "itemValue": "common",
                    "sortOrder": 28,
                    "status": 1,
                    "createUser": "吴游旻",
                    "updateUser": "吴游旻"
                },
                {
                    "id": 1102,
                    "dictItemId": "454696958209912832",
                    "dictId": "114",
                    "itemText": "物流设施",
                    "itemValue": "logiFacility",
                    "sortOrder": 29,
                    "status": 1,
                    "createUser": "吴游旻",
                    "updateUser": "吴游旻"
                }
            ]
        },
        {
            "id": 113,
            "dictId": "113",
            "dictName": "业务过程",
            "dictCode": "businessProcess",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-06-28 16:39:05",
            "updateUser": "李静",
            "updateTime": "2022-08-03 15:50:51",
            "itemDtoList": [
                {
                    "id": 603,
                    "dictItemId": "603",
                    "dictId": "113",
                    "itemText": "供应商准入",
                    "itemValue": "supplierAdmittance",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 604,
                    "dictItemId": "604",
                    "dictId": "113",
                    "itemText": "供应商评价",
                    "itemValue": "supplierEvaluation",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 605,
                    "dictItemId": "605",
                    "dictId": "113",
                    "itemText": "合同执行",
                    "itemValue": "contractExecute",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 606,
                    "dictItemId": "606",
                    "dictId": "113",
                    "itemText": "供应商选择",
                    "itemValue": "supplierChoose",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 607,
                    "dictItemId": "607",
                    "dictId": "113",
                    "itemText": "计划编制",
                    "itemValue": "planEstablish",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 608,
                    "dictItemId": "608",
                    "dictId": "113",
                    "itemText": "计划执行",
                    "itemValue": "planExecute",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 609,
                    "dictItemId": "609",
                    "dictId": "113",
                    "itemText": "交易方式选择",
                    "itemValue": "tradeMethodChoose",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 610,
                    "dictItemId": "610",
                    "dictId": "113",
                    "itemText": "交易方式应用",
                    "itemValue": "tradeMethodApply",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 611,
                    "dictItemId": "611",
                    "dictId": "113",
                    "itemText": "交易定价",
                    "itemValue": "tradePriceSet",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 612,
                    "dictItemId": "612",
                    "dictId": "113",
                    "itemText": "合同签署",
                    "itemValue": "contractSign",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 613,
                    "dictItemId": "613",
                    "dictId": "113",
                    "itemText": "合同变更",
                    "itemValue": "contractChange",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 614,
                    "dictItemId": "614",
                    "dictId": "113",
                    "itemText": "合同归档",
                    "itemValue": "contractArchive",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 615,
                    "dictItemId": "615",
                    "dictId": "113",
                    "itemText": "付款",
                    "itemValue": "payment",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 616,
                    "dictItemId": "616",
                    "dictId": "113",
                    "itemText": "结算",
                    "itemValue": "settlement",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 617,
                    "dictItemId": "617",
                    "dictId": "113",
                    "itemText": "授信",
                    "itemValue": "creditGranting",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 618,
                    "dictItemId": "618",
                    "dictId": "113",
                    "itemText": "收款",
                    "itemValue": "gathering",
                    "sortOrder": 16,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 619,
                    "dictItemId": "619",
                    "dictId": "113",
                    "itemText": "质量验收",
                    "itemValue": "qualityInspection",
                    "sortOrder": 17,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 620,
                    "dictItemId": "620",
                    "dictId": "113",
                    "itemText": "质量异议",
                    "itemValue": "qualityObjection",
                    "sortOrder": 18,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 621,
                    "dictItemId": "621",
                    "dictId": "113",
                    "itemText": "库存管理",
                    "itemValue": "stockManage",
                    "sortOrder": 19,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 622,
                    "dictItemId": "622",
                    "dictId": "113",
                    "itemText": "库存清理",
                    "itemValue": "stockCleanup",
                    "sortOrder": 20,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 623,
                    "dictItemId": "623",
                    "dictId": "113",
                    "itemText": "物料代码新增",
                    "itemValue": "MaterialAddup",
                    "sortOrder": 21,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 624,
                    "dictItemId": "624",
                    "dictId": "113",
                    "itemText": "物料代码属性变更",
                    "itemValue": "MaterialAttrChange",
                    "sortOrder": 22,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 625,
                    "dictItemId": "625",
                    "dictId": "113",
                    "itemText": "授权设置",
                    "itemValue": "authoritySetting",
                    "sortOrder": 23,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 626,
                    "dictItemId": "626",
                    "dictId": "113",
                    "itemText": "授权取消",
                    "itemValue": "authorityCancel",
                    "sortOrder": 24,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 627,
                    "dictItemId": "627",
                    "dictId": "113",
                    "itemText": "费用报支",
                    "itemValue": "expenseReport",
                    "sortOrder": 25,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 628,
                    "dictItemId": "628",
                    "dictId": "113",
                    "itemText": "费用补贴",
                    "itemValue": "expenseSubsidy",
                    "sortOrder": 26,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 629,
                    "dictItemId": "629",
                    "dictId": "113",
                    "itemText": "经商办企业",
                    "itemValue": "Doingbusiness",
                    "sortOrder": 27,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                }
            ]
        },
        {
            "id": 94,
            "dictId": "94",
            "dictName": "查询类型值集",
            "dictCode": "QUERY_TYPE_DICT",
            "type": "sys",
            "delFlag": 0,
            "createUser": "admin",
            "createTime": "2022-06-24 15:56:51",
            "itemDtoList": [
                {
                    "id": 148,
                    "dictItemId": "148",
                    "dictId": "94",
                    "itemText": "包含",
                    "itemValue": "in",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 149,
                    "dictItemId": "149",
                    "dictId": "94",
                    "itemText": "等于",
                    "itemValue": "=",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 150,
                    "dictItemId": "150",
                    "dictId": "94",
                    "itemText": "大于",
                    "itemValue": ">",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 151,
                    "dictItemId": "151",
                    "dictId": "94",
                    "itemText": "小于",
                    "itemValue": "<",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 152,
                    "dictItemId": "152",
                    "dictId": "94",
                    "itemText": "区间",
                    "itemValue": "range",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 153,
                    "dictItemId": "153",
                    "dictId": "94",
                    "itemText": "不等于",
                    "itemValue": "<>",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                }
            ]
        },
        {
            "id": 80,
            "dictId": "80",
            "dictName": "数据源类型",
            "dictCode": "sourceType",
            "type": "sys",
            "delFlag": 0,
            "createUser": "mrbird",
            "createTime": "2022-06-20 15:52:31",
            "updateUser": "admin",
            "updateTime": "2022-06-22 08:58:17",
            "itemDtoList": [
                {
                    "id": 123,
                    "dictItemId": "123",
                    "dictId": "80",
                    "itemText": "5sAPI",
                    "itemValue": "5S",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "mrbird",
                    "updateUser": "mrbird"
                },
                {
                    "id": 124,
                    "dictItemId": "124",
                    "dictId": "80",
                    "itemText": "Oracle",
                    "itemValue": "ocacl",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "mrbird",
                    "updateUser": "mrbird"
                },
                {
                    "id": 125,
                    "dictItemId": "125",
                    "dictId": "80",
                    "itemText": "Hive",
                    "itemValue": "hive",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "mrbird",
                    "updateUser": "mrbird"
                },
                {
                    "id": 126,
                    "dictItemId": "126",
                    "dictId": "80",
                    "itemText": "DB2",
                    "itemValue": "db2",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "mrbird",
                    "updateUser": "mrbird"
                },
                {
                    "id": 127,
                    "dictItemId": "127",
                    "dictId": "80",
                    "itemText": "MySQL",
                    "itemValue": "mysql",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "mrbird",
                    "updateUser": "mrbird"
                }
            ]
        },
        {
            "id": 7,
            "dictId": "7",
            "dictName": "值集映射",
            "dictCode": "valueMapping",
            "description": "值集映射值集",
            "type": "tblfield",
            "delFlag": 0,
            "createUser": "6020",
            "createTime": "2022-06-14 13:05:39",
            "updateUser": "6020",
            "updateTime": "2022-06-14 13:05:44",
            "itemDtoList": [
                {
                    "id": 72,
                    "dictItemId": "72",
                    "dictId": "7",
                    "itemText": "布尔映射",
                    "itemValue": "boolean",
                    "description": "布尔值",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "6020",
                    "updateUser": "6020"
                },
                {
                    "id": 73,
                    "dictItemId": "73",
                    "dictId": "7",
                    "itemText": "字符串",
                    "itemValue": "string",
                    "description": "字符型",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "6020",
                    "updateUser": "6020"
                }
            ]
        },
        {
            "id": 6,
            "dictId": "6",
            "dictName": "监督业务环节",
            "dictCode": "bizIds",
            "description": "监督业务环节",
            "type": "sys",
            "delFlag": 0,
            "createUser": "6021",
            "createTime": "2022-06-13 16:50:18",
            "updateUser": "时月明",
            "updateTime": "2023-06-16 10:36:32",
            "itemDtoList": [
                {
                    "id": 807,
                    "dictItemId": "807",
                    "dictId": "6",
                    "itemText": "采购执行",
                    "itemValue": "PurchaseExecution",
                    "sortOrder": 0,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 808,
                    "dictItemId": "808",
                    "dictId": "6",
                    "itemText": "询单报价",
                    "itemValue": "RequestQuotePrice",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 809,
                    "dictItemId": "809",
                    "dictId": "6",
                    "itemText": "物料管理",
                    "itemValue": "MaterialManagement",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 810,
                    "dictItemId": "810",
                    "dictId": "6",
                    "itemText": "合同签署",
                    "itemValue": "ContractSign",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 811,
                    "dictItemId": "811",
                    "dictId": "6",
                    "itemText": "采购策略选择",
                    "itemValue": "PurchaseStrategyChoose",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 812,
                    "dictItemId": "812",
                    "dictId": "6",
                    "itemText": "供应商选择",
                    "itemValue": "SupplierChoose",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 813,
                    "dictItemId": "813",
                    "dictId": "6",
                    "itemText": "授权管理",
                    "itemValue": "AuthenticationManage",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 814,
                    "dictItemId": "814",
                    "dictId": "6",
                    "itemText": "廉洁风控",
                    "itemValue": "Integrityriskcontrol",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 815,
                    "dictItemId": "815",
                    "dictId": "6",
                    "itemText": "采购定价",
                    "itemValue": "purchaseprice",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 816,
                    "dictItemId": "816",
                    "dictId": "6",
                    "itemText": "合同执行",
                    "itemValue": "contractprocess",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 817,
                    "dictItemId": "817",
                    "dictId": "6",
                    "itemText": "合同付款",
                    "itemValue": "contractpay",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 818,
                    "dictItemId": "818",
                    "dictId": "6",
                    "itemText": "合同变更",
                    "itemValue": "contractchange",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 819,
                    "dictItemId": "819",
                    "dictId": "6",
                    "itemText": "计划执行",
                    "itemValue": "planprocess",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 820,
                    "dictItemId": "820",
                    "dictId": "6",
                    "itemText": "授权设置",
                    "itemValue": "AuthenticationSet",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 821,
                    "dictItemId": "821",
                    "dictId": "6",
                    "itemText": "异议管理",
                    "itemValue": "ObjectionManagement",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 857,
                    "dictItemId": "370300604485500928",
                    "dictId": "6",
                    "itemText": "采购策略应用",
                    "itemValue": "purchasestrategyapply",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 859,
                    "dictItemId": "370522853988212736",
                    "dictId": "6",
                    "itemText": "供应商管理",
                    "itemValue": "Suppliermanagement",
                    "sortOrder": 16,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 931,
                    "dictItemId": "391591224170393600",
                    "dictId": "6",
                    "itemText": "采购寻源",
                    "itemValue": "PurchaseInqury",
                    "sortOrder": 17,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 972,
                    "dictItemId": "398792151251296256",
                    "dictId": "6",
                    "itemText": "物流4SP",
                    "itemValue": "logistics",
                    "sortOrder": 18,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 975,
                    "dictItemId": "408272519580725248",
                    "dictId": "6",
                    "itemText": "授信管控",
                    "itemValue": "Controlofcreditgranting",
                    "sortOrder": 19,
                    "status": 1,
                    "createUser": "admin",
                    "updateUser": "admin"
                },
                {
                    "id": 996,
                    "dictItemId": "425951171408142336",
                    "dictId": "6",
                    "itemText": "库存管理",
                    "itemValue": "stockmanagement",
                    "sortOrder": 20,
                    "status": 1,
                    "createUser": "吴游旻",
                    "updateUser": "吴游旻"
                },
                {
                    "id": 1018,
                    "dictItemId": "437181716211482624",
                    "dictId": "6",
                    "itemText": "业务审批",
                    "itemValue": "bussiness examine and approve",
                    "sortOrder": 21,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 1019,
                    "dictItemId": "443812272618708992",
                    "dictId": "6",
                    "itemText": "采购发票",
                    "itemValue": "Purchase invoice",
                    "sortOrder": 22,
                    "status": 1,
                    "createUser": "李静",
                    "updateUser": "李静"
                },
                {
                    "id": 1036,
                    "dictItemId": "445972975681589248",
                    "dictId": "6",
                    "itemText": "应收账款",
                    "itemValue": "Accounts receivable",
                    "sortOrder": 23,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                },
                {
                    "id": 1113,
                    "dictItemId": "457493677226909696",
                    "dictId": "6",
                    "itemText": "财务管理",
                    "itemValue": "Accounts mangement",
                    "sortOrder": 24,
                    "status": 1,
                    "createUser": "时月明",
                    "updateUser": "时月明"
                }
            ]
        },
        {
            "id": 5,
            "dictId": "5",
            "dictName": "月",
            "dictCode": "month",
            "description": "月值集",
            "type": "sys",
            "delFlag": 0,
            "createUser": "6020",
            "createTime": "2022-06-12 16:50:18",
            "updateUser": "5sdev",
            "updateTime": "2022-07-12 15:47:58",
            "itemDtoList": [
                {
                    "id": 338,
                    "dictItemId": "338",
                    "dictId": "5",
                    "itemText": "每月1号",
                    "itemValue": "1",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 339,
                    "dictItemId": "339",
                    "dictId": "5",
                    "itemText": "每月2号",
                    "itemValue": "2",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 340,
                    "dictItemId": "340",
                    "dictId": "5",
                    "itemText": "每月3号",
                    "itemValue": "3",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 341,
                    "dictItemId": "341",
                    "dictId": "5",
                    "itemText": "每月4号",
                    "itemValue": "4",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 342,
                    "dictItemId": "342",
                    "dictId": "5",
                    "itemText": "每月5号",
                    "itemValue": "5",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 343,
                    "dictItemId": "343",
                    "dictId": "5",
                    "itemText": "每月6号",
                    "itemValue": "6",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 344,
                    "dictItemId": "344",
                    "dictId": "5",
                    "itemText": "每月7号",
                    "itemValue": "7",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 345,
                    "dictItemId": "345",
                    "dictId": "5",
                    "itemText": "每月8号",
                    "itemValue": "8",
                    "sortOrder": 8,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 346,
                    "dictItemId": "346",
                    "dictId": "5",
                    "itemText": "每月9号",
                    "itemValue": "9",
                    "sortOrder": 9,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 347,
                    "dictItemId": "347",
                    "dictId": "5",
                    "itemText": "每月10号",
                    "itemValue": "10",
                    "sortOrder": 10,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 348,
                    "dictItemId": "348",
                    "dictId": "5",
                    "itemText": "每月11号",
                    "itemValue": "11",
                    "sortOrder": 11,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 349,
                    "dictItemId": "349",
                    "dictId": "5",
                    "itemText": "每月12号",
                    "itemValue": "12",
                    "sortOrder": 12,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 350,
                    "dictItemId": "350",
                    "dictId": "5",
                    "itemText": "每月13号",
                    "itemValue": "13",
                    "sortOrder": 13,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 351,
                    "dictItemId": "351",
                    "dictId": "5",
                    "itemText": "每月14号",
                    "itemValue": "14",
                    "sortOrder": 14,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 352,
                    "dictItemId": "352",
                    "dictId": "5",
                    "itemText": "每月15号",
                    "itemValue": "15",
                    "sortOrder": 15,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 353,
                    "dictItemId": "353",
                    "dictId": "5",
                    "itemText": "每月16号",
                    "itemValue": "16",
                    "sortOrder": 16,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 354,
                    "dictItemId": "354",
                    "dictId": "5",
                    "itemText": "每月17号",
                    "itemValue": "17",
                    "sortOrder": 17,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 355,
                    "dictItemId": "355",
                    "dictId": "5",
                    "itemText": "每月18号",
                    "itemValue": "18",
                    "sortOrder": 18,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 356,
                    "dictItemId": "356",
                    "dictId": "5",
                    "itemText": "每月19号",
                    "itemValue": "19",
                    "sortOrder": 19,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 357,
                    "dictItemId": "357",
                    "dictId": "5",
                    "itemText": "每月20号",
                    "itemValue": "20",
                    "sortOrder": 20,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 358,
                    "dictItemId": "358",
                    "dictId": "5",
                    "itemText": "每月21号",
                    "itemValue": "21",
                    "sortOrder": 21,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 359,
                    "dictItemId": "359",
                    "dictId": "5",
                    "itemText": "每月22号",
                    "itemValue": "22",
                    "sortOrder": 22,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 360,
                    "dictItemId": "360",
                    "dictId": "5",
                    "itemText": "每月23号",
                    "itemValue": "23",
                    "sortOrder": 23,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 361,
                    "dictItemId": "361",
                    "dictId": "5",
                    "itemText": "每月24号",
                    "itemValue": "24",
                    "sortOrder": 24,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 362,
                    "dictItemId": "362",
                    "dictId": "5",
                    "itemText": "每月25号",
                    "itemValue": "25",
                    "sortOrder": 25,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 363,
                    "dictItemId": "363",
                    "dictId": "5",
                    "itemText": "每月26号",
                    "itemValue": "26",
                    "sortOrder": 26,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 364,
                    "dictItemId": "364",
                    "dictId": "5",
                    "itemText": "每月27号",
                    "itemValue": "27",
                    "sortOrder": 27,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 365,
                    "dictItemId": "365",
                    "dictId": "5",
                    "itemText": "每月28号",
                    "itemValue": "28",
                    "sortOrder": 28,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 366,
                    "dictItemId": "366",
                    "dictId": "5",
                    "itemText": "每月29号",
                    "itemValue": "29",
                    "sortOrder": 29,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 367,
                    "dictItemId": "367",
                    "dictId": "5",
                    "itemText": "每月30号",
                    "itemValue": "30",
                    "sortOrder": 30,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 368,
                    "dictItemId": "368",
                    "dictId": "5",
                    "itemText": "每月31号",
                    "itemValue": "31",
                    "sortOrder": 31,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 369,
                    "dictItemId": "369",
                    "dictId": "5",
                    "itemText": "每月最后一天",
                    "itemValue": "L",
                    "sortOrder": 32,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                }
            ]
        },
        {
            "id": 4,
            "dictId": "4",
            "dictName": "周",
            "dictCode": "week",
            "description": "周值集",
            "type": "sys",
            "delFlag": 0,
            "createUser": "6020",
            "createTime": "2022-06-11 16:50:18",
            "updateUser": "5sdev",
            "updateTime": "2022-07-12 15:50:00",
            "itemDtoList": [
                {
                    "id": 384,
                    "dictItemId": "384",
                    "dictId": "4",
                    "itemText": "周一",
                    "itemValue": "2",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 385,
                    "dictItemId": "385",
                    "dictId": "4",
                    "itemText": "周二",
                    "itemValue": "3",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 386,
                    "dictItemId": "386",
                    "dictId": "4",
                    "itemText": "周三",
                    "itemValue": "4",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 387,
                    "dictItemId": "387",
                    "dictId": "4",
                    "itemText": "周四",
                    "itemValue": "5",
                    "sortOrder": 4,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 388,
                    "dictItemId": "388",
                    "dictId": "4",
                    "itemText": "周五",
                    "itemValue": "6",
                    "sortOrder": 5,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 389,
                    "dictItemId": "389",
                    "dictId": "4",
                    "itemText": "周六",
                    "itemValue": "7",
                    "sortOrder": 6,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                },
                {
                    "id": 390,
                    "dictItemId": "390",
                    "dictId": "4",
                    "itemText": "周日",
                    "itemValue": "1",
                    "sortOrder": 7,
                    "status": 1,
                    "createUser": "5sdev",
                    "updateUser": "5sdev"
                }
            ]
        },
        {
            "id": 3,
            "dictId": "3",
            "dictName": "日",
            "dictCode": "day",
            "description": "日值集",
            "type": "sys",
            "delFlag": 0,
            "createUser": "6020",
            "createTime": "2022-06-10 16:50:18",
            "updateUser": "6020",
            "updateTime": "2022-06-10 16:50:18",
            "itemDtoList": []
        },
        {
            "id": 1,
            "dictId": "1",
            "dictName": "调度周期",
            "dictCode": "dispatchingCycle",
            "description": "模型调度周期",
            "type": "sys",
            "delFlag": 0,
            "createUser": "6020",
            "createTime": "2022-06-08 16:50:18",
            "updateUser": "6020",
            "updateTime": "2022-06-08 16:50:18",
            "itemDtoList": [
                {
                    "id": 66,
                    "dictItemId": "66",
                    "dictId": "1",
                    "itemText": "日",
                    "itemValue": "day",
                    "description": "日",
                    "sortOrder": 1,
                    "status": 1,
                    "createUser": "6020",
                    "updateUser": "6020"
                },
                {
                    "id": 67,
                    "dictItemId": "67",
                    "dictId": "1",
                    "itemText": "周",
                    "itemValue": "week",
                    "description": "周",
                    "sortOrder": 2,
                    "status": 1,
                    "createUser": "6020",
                    "updateUser": "6020"
                },
                {
                    "id": 68,
                    "dictItemId": "68",
                    "dictId": "1",
                    "itemText": "月",
                    "itemValue": "month",
                    "description": "月",
                    "sortOrder": 3,
                    "status": 1,
                    "createUser": "6020",
                    "updateUser": "6020"
                }
            ]
        }
    ],
    "fieldErrors": null,
    "meta": null
}