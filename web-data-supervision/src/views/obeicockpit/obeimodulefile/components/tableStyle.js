const formatter = {
  rate1: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(2);
      return `${parseFloat(resdata).toLocaleString()}%`; // 保留两位有效小数位
    }
  },
  rate2: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(2);
      return parseFloat(resdata).toLocaleString(); // 金额保留两位有效小数位
    }
  },
};

// 总览产品行和并
const rspans = {
  // 经营目标
  rows1: [
    { start: 0, span: 4 },
    { start: 4, span: 6 },
    { start: 10, span: 1 },
    { start: 11, span: 1 },
    { start: 12, span: 1 },
  ],
  rows2: [
    { start: 0, span: 7 },
    { start: 7, span: 2 },
    { start: 9, span: 4 },
  ],
  rows3: [
    { start: 0, span: 9 },
    { start: 9, span: 9 },
    { start: 18, span: 7 },
    { start: 25, span: 4 },
    { start: 29, span: 2 },
    { start: 31, span: 2 },
  ],
};

export default {
  formatter,
  rspans,
};
