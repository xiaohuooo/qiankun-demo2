import * as echarts from 'echarts';
// 目标，实绩，完成率(柱折混合图)
const seriesData = (data1, data2, data3, unit) => {
  const series = [
    {
      name: '目标',
      type: 'bar',
      data: data1,
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
        },
      },
      barGap: 0,
      barWidth: '20px',
      color: '#ffa042',
    },
    {
      name: '实绩',
      type: 'bar',
      data: data2,
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
        },
      },
      barWidth: '20px',
      color: 'MediumSpringGreen',
    },
    {
      name: '完成率',
      type: 'line',
      data: data3,
      yAxisIndex: 1,
      tooltip: {
        valueFormatter(value) {
          return `${Xiaoshu(value)} %`;
        },
      },
      itemStyle: {
        normal: {
          color: 'dodgerblue',
          label: {
            show: true,
            position: 'top',
            textStyle: {
              color: 'black',
            },
            formatter(p) {
              return `${Xiaoshu(p.value)}%`;
            },
          },
        },
      },
    },
  ];
  return series;
};
// 实绩（柱状图）
const seriesData1 = (data1, unit, name1) => {
  const series = [
    {
      name: name1,
      type: 'bar',
      barGap: '-100%',
      barWidth: '20px',
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },

      data: data1,
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
          color: new echarts.graphic.LinearGradient(1, 0, 0, 0, [
            {
              offset: 0,
              color: '#83bff6',
            },
            {
              offset: 0.5,
              color: '#188df0',
            },
            {
              offset: 1,
              color: '#188df0',
            },
          ]),
        },
      },
    },
  ];
  return series;
};

// 目标，实绩（柱状图）
const seriesData2 = (data1, data2, unit) => {
  const series = [
    {
      name: '目标',
      type: 'bar',
      data: data1,
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
        },
      },
      barGap: 0,
      barWidth: '20px',
      color: '#ffa042',
    },
    {
      name: '实绩',
      type: 'bar',
      data: data2,
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
        },
      },
      barWidth: '20px',
      color: 'MediumSpringGreen',
    },
  ];
  return series;
};

// 完成率/实绩（折线图）
const seriesData3 = (data1, unit, name1) => {
  const series = [
    {
      name: name1,
      type: 'line',
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      data: data1,
      itemStyle: {
        normal: {
          color: 'dodgerblue',
          label: {
            show: true,
            position: 'top',
            textStyle: {
              color: 'black',
            },
            formatter(p) {
              return Xiaoshu(p.value) + unit;
            },
          },
        },
      },
    },
  ];
  return series;
};

//保留两位有效小数
function Xiaoshu(text) {
  if (text === '-') {
    return text;
  } else {
    const datatext = parseFloat(text).toFixed(2);
    return parseFloat(datatext).toLocaleString();
  }
}

export default {
  seriesData,
  seriesData1,
  seriesData2,
  seriesData3,
};
