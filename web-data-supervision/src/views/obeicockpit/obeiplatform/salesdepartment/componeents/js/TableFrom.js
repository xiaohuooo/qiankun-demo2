const toTableData = (inputdata, inputid) => {
  const data = [];
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (returnData[i][j] === '' || returnData[i][j] === '-' || returnData[i][j] == null) {
        dtt[columnList[j].name] = '-';
      } else {
        dtt[columnList[j].name] = returnData[i][j];
      }
    }
    data.push(dtt);
  }
  return data;
};

export default {
  toTableData,
};
