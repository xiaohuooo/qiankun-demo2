// const sharedOnCell = (_) => {
//   return { rowSpan: _.projectCodeRowSpan };
// };
export const baseColumns = [
  // {
  //   title: '项目名称',
  //   dataIndex: 'projectName',
  //   width: 200,
  //   align: 'center',
  //   customCell: sharedOnCell,
  // },
  {
    title: '编号',
    dataIndex: 'taskCode',
    width: 60,
    align: 'center',
    hideInSearch: true,
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 6 };
      }
    },
  },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    width: 350,
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      const title = record.taskTitle;
      const text = record.taskContent?.replace(/\n/g, '<br>');
      return (
        <div>
          <div class="task-content-title" innerHTML={title}></div>
          <div class="task-content-detail" innerHTML={text}></div>
        </div>
      );
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '进度',
    dataIndex: 'fillingContent',
    width: 350,
    align: 'left',
    hideInSearch: true,
    customRender: ({ record }) => {
      const text = record.fillingContent;
      return (
        <div>
          <div class="task-content-title-1" innerHTML={text}></div>
        </div>
      );
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '责任单位',
    dataIndex: 'responsibilityDeptCode',
    align: 'center',
    width: 100,
    formItemProps: {
      component: 'Select',
      componentProps: { options: [], mode: 'multiple', optionFilterProp: 'label' },
    },
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.responsibilityDeptName} placement="topLeft">
          {record.responsibilityDeptName}
        </a-tooltip>
      );
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 80,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '备注',
    dataIndex: 'fillAppraise',
    width: 200,
    align: 'left',
    hideInSearch: true,
    customRender: ({ record }) => {
      const title = record.fillAppraise;
      return <div class="task-content-title-1" innerHTML={title}></div>;
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
];
export const baseColumns60 = [
  // {
  //   title: '项目名称',
  //   dataIndex: 'projectName',
  //   width: 200,
  //   align: 'center',
  //   customCell: sharedOnCell,
  // },
  {
    title: '编号',
    dataIndex: 'taskCode',
    width: 100,
    align: 'center',
    hideInSearch: true,
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 6 };
      }
    },
  },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    width: 350,
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      const title = record.taskTitle;
      const text = record.taskContent?.replace(/\n/g, '<br>');
      return (
        <div>
          <div class="task-content-title" innerHTML={title}></div>
          <div class="task-content-detail" innerHTML={text}></div>
        </div>
      );
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '进度',
    dataIndex: 'fillingContent',
    width: 350,
    align: 'left',
    hideInSearch: true,
    customRender: ({ record }) => {
      const text = record.fillingContent;
      return (
        <div>
          <div class="task-content-title-1" innerHTML={text}></div>
        </div>
      );
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '责任单位',
    dataIndex: 'responsibilityDeptCode',
    align: 'center',
    width: 100,
    formItemProps: {
      component: 'Select',
      componentProps: { options: [], mode: 'multiple', optionFilterProp: 'label' },
    },
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.responsibilityDeptName} placement="topLeft">
          {record.responsibilityDeptName}
        </a-tooltip>
      );
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '状态',
    dataIndex: 'taskStatus',
    align: 'center',
    width: 200,
    hideInTable: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [
          { label: '持续跟踪', value: '10' },
          { label: '重点关注', value: '50' },
        ],
      },
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 80,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
  {
    title: '备注',
    dataIndex: 'fillAppraise',
    align: 'left',
    width: 200,
    hideInSearch: true,
    customRender: ({ record }) => {
      const title = record.fillAppraise;
      return <div class="task-content-title-1" innerHTML={title}></div>;
    },
    customCell: (_, _index) => {
      if (!_.pkId) {
        return { colSpan: 0 };
      }
    },
  },
];
export const columnModal = [
  { title: '序号', dataIndex: 'taskIndex', width: 50, align: 'center' },
  // { title: '项目名称', dataIndex: 'projectName', width: 200, align: 'center' },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    width: 320,
    customRender: ({ record }) => {
      const title = record.taskTitle;
      const text = record.taskContent?.replace(/\n/g, '<br>');
      return (
        <div>
          <div class="task-content-title text-center" innerHTML={title}></div>
          <div class="task-content-detail" innerHTML={text}></div>
        </div>
      );
    },
  },
  {
    title: '进度',
    dataIndex: 'fillingContent',
    align: 'left',
    width: 300,
    customRender: ({ record }) => {
      const text = record.fillingContent;
      return <div class="task-content-title-1" innerHTML={text}></div>;
    },
  },
  {
    title: '责任单位',
    dataIndex: 'responsibilityDeptName',
    align: 'center',
    width: 100,
    customRender: ({ record }) => {
      const text = record.responsibilityDeptName;
      return <div class="task-content">{text}</div>;
    },
  },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 60,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
  },
  {
    title: '备注',
    dataIndex: 'fillAppraise',
    align: 'left',
    width: 200,
    customRender: ({ record }) => {
      const title = record.fillAppraise;
      return <div class="task-content-title-1" innerHTML={title}></div>;
    },
  },
];
