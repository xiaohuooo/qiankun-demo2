import { isEmpty } from 'lodash-es';
import dayjs from 'dayjs';
import { Storage } from '@/utils/Storage';
import { ACCESS_DICT_ALL_KEY } from '@/enums/cacheEnum';
import dict from '@/utils/dict.js';

const USERINFO =
  localStorage.getItem('ACCESS_USER_INFO_KEY') ?? sessionStorage.getItem('ACCESS_USER_INFO_KEY');
const userDetail = USERINFO ? JSON.parse(USERINFO ?? '')?.value : '';
const roleName = userDetail ? userDetail?.roleName : '';
const unitCode = roleName
  ? roleName.indexOf('重点工作管理员') != -1
    ? null
    : [userDetail?.unitCode]
  : null;

const codeValue = Storage.get(ACCESS_DICT_ALL_KEY);
const transfer = (name, label, value) => {
  return codeValue
    ?.find((x) => x.dictCode === name)
    ?.itemDtoList?.map((x, index) => {
      return {
        label: x[label],
        value: x[value],
        key: index,
      };
    });
};
export const baseColumns = [
  {
    title: '编号',
    dataIndex: 'taskCode',
    width: 180,
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
  },
  {
    title: '填报月份',
    dataIndex: 'fillMonth',
    width: 120,
    align: 'center',
    hideInTable: true,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        picker: 'month',
        defaultValue: dayjs(),
      },
    },
  },
  {
    title: '项目类型',
    dataIndex: 'projectType',
    width: 120,
    align: 'center',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: transfer('PROJECT_TYPE', 'itemText', 'itemValue'),
      },
    },
    customRender: (record) => {
      if (record != null) {
        const obj = dict.getValue('PROJECT_TYPE', record.text);
        return (
          <a-tooltip title={obj.itemText} placement="topLeft">
            {obj.itemText}
          </a-tooltip>
        );
      }
    },
  },
  {
    title: '项目月份',
    dataIndex: 'projectDefMonth',
    width: 120,
    align: 'center',
    ellipsis: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: transfer('PROJECT_DEF_MONTH', 'itemText', 'itemValue'),
      },
    },
    customRender: (record) => {
      if (record != null) {
        const obj = dict.getValue('PROJECT_DEF_MONTH', record.text);
        return (
          <a-tooltip title={obj.itemText} placement="topLeft">
            {obj.itemText}
          </a-tooltip>
        );
      }
    },
  },
  {
    title: '项目名称',
    dataIndex: 'projectName',
    width: 250,
    align: 'center',
    ellipsis: true,
    customRender: (record) => {
      return (
        <a-tooltip title={record.text} placement="topLeft">
          {record.text}
        </a-tooltip>
      );
    },
  },
  {
    title: '任务标题',
    dataIndex: 'taskTitle',
    width: 200,
    align: 'center',
    ellipsis: true,
    customRender: (record) => {
      return (
        <a-tooltip title={record.text} placement="topLeft">
          {record.text}
        </a-tooltip>
      );
    },
  },
  {
    title: '项目内容',
    dataIndex: 'taskContent',
    align: 'center',
    width: 250,
    ellipsis: true,
    customRender: (record) => {
      return (
        <a-tooltip title={record.text} placement="topLeft">
          {record.text}
        </a-tooltip>
      );
    },
  },
  {
    title: '责任单位',
    dataIndex: 'responsibilityDeptCode',
    align: 'center',
    width: 200,
    ellipsis: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [],
        mode: 'multiple',
        optionFilterProp: 'label',
      },
    },
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.responsibilityDeptName} placement="topLeft">
          {record.responsibilityDeptName}
        </a-tooltip>
      );
    },
  },
  { title: '责任领导', dataIndex: 'responsLeader', align: 'center', width: 200, ellipsis: true },
  { title: '协助领导', dataIndex: 'assistLeaders', align: 'center', width: 200, ellipsis: true },
  {
    title: '反馈单位',
    dataIndex: 'fillingDeptCode',
    align: 'center',
    width: 200,
    ellipsis: true,
    formItemProps: {
      defaultValue: unitCode,
      component: 'Select',
      componentProps: {
        options: [],
        mode: 'multiple',
        optionFilterProp: 'label',
      },
    },
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.fillingDeptName} placement="topLeft">
          {record.fillingDeptName}
        </a-tooltip>
      );
    },
  },
  {
    title: '项目性质',
    dataIndex: 'projectFeature',
    align: 'center',
    width: 200,
    hideInTable: true,
    ellipsis: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: transfer('PROJECT_FEATURE', 'itemText', 'itemValue'),
      },
    },
    customRender: (record) => {
      if (record != null) {
        const obj = dict.getValue('PROJECT_FEATURE', record.text);
        return (
          <a-tooltip title={obj.itemText} placement="topLeft">
            {obj.itemText}
          </a-tooltip>
        );
      }
    },
  },
  {
    title: '进展',
    dataIndex: 'taskProgress',
    align: 'center',
    width: 80,
    fixed: 'right',
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.taskProgress == 10) {
        return <bell-filled style="color: green;" />;
      } else if (record.taskProgress == 20) {
        return <bell-filled style="color: orange;" />;
      } else if (record.taskProgress == 30) {
        return <bell-filled style="color: red;" />;
      }
    },
  },
  {
    title: '审批状态',
    dataIndex: 'auditStatus',
    align: 'center',
    width: 100,
    fixed: 'right',
    ellipsis: true,
    hideInSearch: true,
    customRender: ({ record }) => {
      const obj = dict.getValue('AUDIT_STATUS', record.auditStatus);
      if (!isEmpty(obj)) {
        return obj.itemText;
      } else {
        return '-';
      }
    },
  },
  {
    title: '任务状态',
    dataIndex: 'taskStatus',
    align: 'center',
    width: 140,
    fixed: 'right',
    ellipsis: true,
    hideInSearch: true,
  },
];
