import { kanbanConfigStore } from '@/store/modules/kanbanConfig';
const store = kanbanConfigStore();

export const judgeNeedReset = (item, queryParams) => {
  const local = store?.kanbanData?.find((x) => x?.widgetId === queryParams?.widgetId);
  // 如果没有关联关系,直接return
  if (local) {
    const link = local?.relations?.links?.find((x) => x?.conditions && x?.conditions?.length > 0);
    if (!link) return false;
  }
  if (item?.relations?.links) {
    const link = item?.relations?.links?.find((x) => x?.conditions && x?.conditions?.length > 0);
    if (link?.linkType === 'consumer') {
      // 如果是消费者,那去找它对应的提供者
      const tmp = link?.conditions?.map((z) => z?.linkTarget).map((x) => x?.targetAlias);
      const data = store?.kanbanData?.find((x) => {
        return x?.relations?.links.find(
          (y) =>
            y?.linkType === 'provider' &&
            y?.conditions &&
            y?.conditions?.length > 0 &&
            y?.conditions?.find((z) => tmp?.includes(z?.linkTarget?.targetAlias)),
        );
      });
      // 找到提供者,则找到下面的所有的消费者
      if (data) {
        const link = data?.relations?.links?.find((x) => x?.conditions && x?.conditions.length > 0);
        if (link) {
          const targetAlias = link?.conditions
            ?.map((x) => x?.linkTarget)
            .map((x) => x?.targetAlias);
          // 再去找到widgetIds
          const data = store?.kanbanData?.filter((x) => {
            if (
              x?.relations?.links?.find((y) => {
                if (
                  y?.linkType === 'consumer' &&
                  y?.conditions &&
                  y?.conditions.length > 0 &&
                  y?.conditions?.find((z) => targetAlias?.includes(z?.linkTarget?.targetAlias))
                ) {
                  return y;
                }
              })
            )
              return x;
          });
          if (data && data?.length > 0) {
            const widgetIds = data?.map((x) => x?.widgetId);
            if (!widgetIds?.includes(queryParams?.widgetId)) {
              return false;
            }
          }
        }
      }
    } else if (link?.linkType === 'provider') {
      // 如果是提供者,则直接拿所有匹配的消费者
      const link = item?.relations?.links?.find((x) => x?.conditions && x?.conditions?.length > 0);
      if (link) {
        const targetAlias = link?.conditions?.map((x) => x?.linkTarget).map((x) => x?.targetAlias);
        const data = store?.kanbanData?.filter((x) => {
          if (
            x.relations.links.find((y) => {
              if (
                y.linkType === 'consumer' &&
                y.conditions &&
                y.conditions.length > 0 &&
                y.conditions?.find((z) => targetAlias.includes(z.linkTarget.targetAlias))
              ) {
                return y;
              }
            })
          )
            return x;
        });
        if (data && data?.length > 0) {
          const widgetIds = data?.map((x) => x?.widgetId);
          if (!widgetIds?.includes(queryParams?.widgetId)) {
            return false;
          }
        }
      }
    } else {
      // 如果啥都不是,则还原所有的消费者
      const data = store?.kanbanData?.filter((x) => {
        if (
          x?.relations?.links?.find((y) => {
            if (y?.linkType === 'consumer' && y?.conditions && y?.conditions?.length > 0) {
              return y;
            }
          })
        )
          return x;
      });
      if (data && data?.length > 0) {
        const widgetIds = data?.map((x) => x?.widgetId);
        if (!widgetIds?.includes(queryParams?.widgetId)) {
          return false;
        }
      }
    }
  }
  return true;
};
