import type { FormSchema } from '@/components/core/schema-form/';
import dict from '@/utils/dict';
import { allRoleUsers } from '@/api/userAnalyze/bulletinBoardList'; // api 接口

const businessObjsOptions = dict.getItemsByDictCode('businessObject').map((item) => {
  return {
    label: item.itemText,
    value: item.itemValue,
  };
});
const bizIdsOptions = dict.getItemsByDictCode('bizIds').map((item) => {
  return {
    label: item.itemText,
    value: item.itemValue,
  };
});

export const bulletinBoardListSchemas: FormSchema<API.kanbanListSchemas>[] = [
  {
    field: 'categoryCode',
    component: 'Cascader',
    label: '看板分类',
    componentProps: {
      fieldNames: {
        label: 'categoryName',
        value: 'categoryCode',
      },
      options: [],
    },
    rules: [{ required: true, type: 'array' }],
  },
  {
    field: 'boardName',
    component: 'Input',
    label: '看板名称',
    rules: [{ required: true, type: 'string' }],
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '看板描述',
  },
  {
    field: 'businessObjs',
    component: 'Select',
    label: '监督业务对象',
    colProps: {
      span: 24,
    },
    rules: [{ required: true }],
    componentProps: {
      options: businessObjsOptions,
    },
  },
  {
    field: 'bizIds',
    component: 'Select',
    label: '监督业务环节',
    rules: [{ required: true }],
    componentProps: {
      options: bizIdsOptions,
    },
  },

  {
    field: 'jurisdiction',
    component: 'RadioGroup',
    label: '访问权限',
    rules: [{ required: true }],
    defaultValue: '2',
    colProps: {
      span: 24,
    },
    componentProps: {
      options: [
        {
          label: '所有人可见',
          value: '1',
        },
        {
          label: '仅自己可见',
          value: '2',
        },
        {
          label: '部分人可见',
          value: '3',
        },
      ],
    },
  },
  {
    field: 'roleIds',
    component: 'ApiSelect',
    label: '角色',
    componentProps: {
      api: allRoleUsers,
      labelField: 'roleName',
      valueField: 'roleId',
      placeholder: '请选择角色',
      mode: 'multiple',
    },
    vShow: ({ formModel }) => {
      return formModel.jurisdiction == 3;
    },
  },
  {
    field: 'userIds',
    component: 'Select',
    label: '用户',
    componentProps: {
      options: [],
      mode: 'multiple',
    },
    vShow: ({ formModel }) => {
      return formModel.jurisdiction == 3;
    },
  },
];
