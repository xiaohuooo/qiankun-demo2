import navData from './navData';
import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();
// 字段全部配置
const navDataConfig = () => {
  store.navList[2] = JSON.parse(JSON.stringify(navData[2]));
  store.fieldArray.forEach((item) => {
    store.navList[2].children[item.newType - 1].children.push(item);
  });
};
const locationItem = (data, item) => {
  data.forEach((t, i) => {
    if (t?.children?.length) {
      locationItem(t.children, item);
    } else {
      if (t.key === item.key) {
        data.splice(i, 1);
      }
    }
  });
};
const FieldConfig = () => {
  navDataConfig();
  //  从目录中删除
  if (store.navList[1]?.children?.length) {
    store.navList[1]?.children[0]?.children.forEach((item) => {
      locationItem(store.navList[2].children, item);
    });
  }
  // 从配置轴上删除
  store.insertContent.forEach((item) => {
    item.value.forEach((t) => {
      if (t.type.split(',')[0] === 'catalogue') {
        t.children.forEach((it) => {
          locationItem(store.navList[2].children, it);
        });
      } else if (t.type === 'addField') {
        locationItem(store.navList[0].children, t);
      } else {
        locationItem(store.navList[2].children, t);
      }
    });
  });
};
export default FieldConfig;
