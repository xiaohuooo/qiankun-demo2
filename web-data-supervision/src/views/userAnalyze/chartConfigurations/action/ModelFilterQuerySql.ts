import { getSqlInfo } from '@/api/userAnalyze/chartConfigurations'; // api 接口
import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();
const unique = (arr) => {
  if (!Array.isArray(arr)) {
    console.log('type error!');
    return;
  }
  const res = [arr[0]];
  for (let i = 1; i < arr.length; i++) {
    let flag = true;
    for (let j = 0; j < res.length; j++) {
      if (arr[i].colAli === res[j].colAli) {
        flag = false;
        break;
      }
    }
    if (flag) {
      res.push(arr[i]);
    }
  }
  return res;
};
const queryConfigList = () => {
  const list = [] as any;
  store.insertContent.forEach((it) => {
    it.value.forEach((ct: any) => {
      if (ct.type == 'addField') {
        list.push({ colAli: ct.name, colExp: `(${ct.text})` });
      }
    });
  });
  return list.length ? unique(list) : [];
};
const paramsCofig = (val) => {
  const sqlParams = {
    compositeColumnList: val?.columnType == '10' ? queryConfigList() : [],
    datatableList: [],
    tableRelationList: [],
    mergeCols: [],
  } as any;

  // store.sqlParams = sqlParams;
  sqlParams.tableRelationList = store.sqlParams.tableRelationList;
  store.datasets.forEach((item, index) => {
    const datatableList = {
      sourceId: item.sourceId,
      tableId: item.id,
      tableAlias: `t${index + 1}`,
      tableCName: item.tableCname,
      tableName: item.tableName,
      tableScheme: item.tableScheme,
      tableColumnList: [] as any,
    };
    const column = item.columnsList
      .filter((x) => x.columnName === val.columnName)
      ?.map((x) => {
        return {
          ...x,
          tableColumnName: x.columnName,
        };
      });
    if (column && column.length > 0) {
      datatableList.tableColumnList = column;
    }
    sqlParams.datatableList.push(datatableList);
  });
  return sqlParams;
};
const querySql = async (val) => {
  let previewSql: any = '';
  const data = await getSqlInfo(paramsCofig(val));
  if (data.code === 'OK') {
    previewSql = data.data.sqlStr;
  }
  return previewSql;
};

export default querySql;
