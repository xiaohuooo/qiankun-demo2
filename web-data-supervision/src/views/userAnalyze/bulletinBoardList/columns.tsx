import { Tag } from 'ant-design-vue';
import type { TableColumn } from '@/components/core/dynamic-table';
import { dataSetAll } from '@/api/userAnalyze/bulletinBoardList'; // api 接口
import dict from '@/utils/dict';
export type TableListItem = API.bulletinBoardListColumns;
export type TableColumnItem = TableColumn<TableListItem>;
const chartTypeOption = [
  {
    label: '全部',
    value: '',
  },
];
dict.getItemsByDictCode('chartType').forEach((item) => {
  chartTypeOption.push({
    label: item.itemText,
    value: item.itemValue,
  });
});

export const baseColumns: TableColumnItem[] = [
  // {
  //   title: 'ID',
  //   dataIndex: 'widgetId',
  //   width: 200,
  //   hideInSearch: true, // 是否关闭头部搜索
  // },
  {
    title: '图表名称',
    width: 200,
    ellipsis: true,
    dataIndex: 'widgetName',
  },
  {
    title: '图表描述',
    width: 200,
    ellipsis: true,
    dataIndex: 'description',
    hideInSearch: true, // 是否关闭头部搜索
  },
  {
    title: '监督业务对象',
    dataIndex: 'businessObjs',
    width: 100,
    ellipsis: true,
    hideInSearch: true, // 是否关闭头部搜索
    customRender: ({ record }) => {
      //值集转换列表显示
      if (record.businessObjs != null && record.businessObjs != undefined) {
        return <Tag>{dict.getItemText('businessObject', record.businessObjs)}</Tag>;
      }
    },
  },
  {
    title: '数据集',
    width: 220,
    ellipsis: true,
    dataIndex: 'datasets',
    key: 'datasetId',
    formItemProps: {
      component: 'ApiSelect',
      componentProps: {
        api: dataSetAll,
        resultField: 'data',
        labelField: 'tableCname',
        valueField: 'id',
        placeholder: '请选择数据集',
      },
    },
    customRender: ({ record }) => (
      <div>
        {record.datasets.map((item) => (
          <Tag key={item}>{item}</Tag>
        ))}
      </div>
    ),
  },
  {
    title: '图表类型',
    width: 80,
    dataIndex: 'chartType',
    hideInSearch: true,
    customRender: ({ record }) => {
      //值集转换列表显示
      if (record.chartType != null && record.chartType != undefined) {
        return <Tag>{dict.getItemText('chartType', record.chartType)}</Tag>;
      }
    },
    // customRender: ({ record }) => {
    //   let label = '未配置';
    //   dict.getItemsByDictCode('chartType').forEach((t) => {
    //     if (t.itemValue === record.chartType) {
    //       label = t.itemText;
    //     }
    //   });
    //   return label;
    // },
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: chartTypeOption,
      },
    },
  },
  {
    title: '可见范围',
    width: 80,
    dataIndex: 'jurisdiction',
    hideInSearch: true,
    customRender: ({ record }) => {
      let text = '所有人可见';
      if (record.jurisdiction === '2') {
        text = '仅自己可见';
      } else if (record.jurisdiction === '3') {
        text = '部分人可见';
      }
      return text;
    },
  },
  {
    title: '创建人',
    width: 80,
    dataIndex: 'createUserName',
    // hideInSearch: true, // 是否关闭头部搜索
  },
  {
    title: '创建时间',
    width: 160,
    dataIndex: 'createDate',
    hideInSearch: true, // 是否关闭头部搜索
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
  {
    title: '修改人',
    width: 80,
    dataIndex: 'updateUserName',
    hideInSearch: true, // 是否关闭头部搜索
  },
  {
    title: '修改时间',
    width: 160,
    dataIndex: 'updateDate',
    hideInSearch: true,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
