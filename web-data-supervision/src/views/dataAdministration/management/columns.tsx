import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.DictListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '数据源名称',
    width: 50,
    ellipsis: true,
    dataIndex: 'sourceName',
  },
  {
    title: '数据源类型',
    hideInSearch: true,
    width: 40,
    ellipsis: true,
    dataIndex: 'sourceType',
  },
  {
    title: '数据源地址',
    hideInSearch: true,
    ellipsis: true,
    dataIndex: 'url',
    width: 180,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 90,
    hideInSearch: true,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
