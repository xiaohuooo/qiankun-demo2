import type { FormSchema } from '@/components/core/schema-form/';

export const datasetSchemas: FormSchema<API.CreateDatasetParams>[] = [
  {
    field: 'sourceId',
    component: 'Select',
    label: '数据源',
    rules: [
      {
        required: true,
        type: 'number',
        message: '请选择数据源类型',
      },
    ],
    colProps: {
      span: 8,
    },
    // componentProps: ({ formModel }) => ({
    //   onChange(e, op) {
    //     console.log('formInstance1===e', e, op);
    //     formModel.sourceName = op.label;
    //   },
    // }),
  },

  {
    field: 'tableCname',
    component: 'Input',
    label: '数据集名称',
    rules: [
      { required: true, message: '请输入数据集名称' },
      { max: 50, message: '50字符以内' },
    ],
    colProps: {
      span: 8,
    },
  },
  {
    field: 'tableName',
    component: 'Input',
    label: '数据集英文名',
    rules: [
      { required: true, message: '请输入英文字符' },
      { max: 100, message: '100字符以内' },
    ],
    colProps: {
      span: 8,
    },
  },
  {
    field: 'apiName',
    component: 'Input',
    label: '数据API名称',
    dynamicRules: ({ formModel }) => {
      return formModel.sourceName === '5S常规查询配置'
        ? [{ required: true, message: '数据API名称' }]
        : [];
    },
    colProps: {
      span: 8,
    },
    vIf: ({ formModel }) => {
      return formModel.sourceName == '5S通用查询配置' || formModel.sourceName == '5S常规查询配置';
    },
  },
  {
    field: 'algorithmClass',
    component: 'Input',
    label: '算法路径',
    rules: [{ required: true, message: '算法路径' }],
    colProps: {
      span: 8,
    },
    vIf: ({ formModel }) => {
      return formModel.sourceName == '智能算法';
    },
  },
  {
    field: 'tableScheme',
    component: 'Select',
    label: '数据集逻辑分区',
    dynamicRules: ({ formModel }) => {
      return formModel.sourceName != '智能算法'
        ? [{ required: true, message: '请输入数据集逻辑分区' }]
        : [];
    },
    colProps: {
      span: 8,
    },
  },
  {
    field: 'applicationScenario',
    component: 'Select',
    label: '应用场景',
    rules: [{ required: true, message: '请选择数据集应用场景' }],
    colProps: {
      span: 8,
    },
    componentProps: {
      options: [
        {
          label: '数据治理成果',
          value: '数据治理成果',
        },
        {
          label: '静态监督模型',
          value: '静态监督模型',
        },
        {
          label: '算法监督模型',
          value: '算法监督模型',
        },
      ],
    },
  },
  {
    field: 'governanceObject',
    component: 'Select',
    label: '业务对象',
    rules: [{ required: true, type: 'array', message: '请选择治理业务对象' }],
    colProps: {
      span: 8,
    },
    componentProps: {
      mode: 'multiple',
    },
  },
  {
    field: 'dataCategory',
    component: 'Cascader',
    label: '数据集分类',
    rules: [{ required: true, type: 'array', message: '请选择分类' }],
    colProps: {
      span: 16,
    },
  },
  {
    field: 'voList',
    component: 'DatasetRowEditor',
    label: '字段信息',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'sourceName', //数据源切换显示
    component: 'Input',
    vShow: false,
  },
];
