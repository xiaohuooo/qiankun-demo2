// import dict from '@/utils/dict.js';

export const base2Columns = [
  {
    title: '分类',
    dataIndex: 'CLASS_NAME',
    key: 'CLASS_NAME',
    ellipsis: false,
    rspans: [
      { start: 0, span: 14 },
      { start: 14, span: 6 },
      // { start: 1, span: 1 },
    ],
  },
  {
    title: '用户',
    dataIndex: 'USER_NAME',
    key: 'USER_NAME',
    width: '10%',
  },
  {
    title: '公司分管领导',
    dataIndex: 'COMPANY_LEADER',
    key: 'COMPANY_LEADER',
  },
  {
    title: '大客户总监',
    dataIndex: 'CUST_MANAGER',
    key: 'CUST_MANAGER',
  },
  {
    title: '一',
    dataIndex: 'one',
    key: 'one',
  },
  {
    title: '二',
    dataIndex: 'two',
    key: 'two',
  },
  {
    title: '三',
    dataIndex: 'third',
    key: 'third',
  },
  {
    title: '四',
    dataIndex: 'fourth',
    key: 'fourth',
    // customRender: ({ value }) => {
    //   if (value) {
    //     let color = '#87d068';
    //     if (value == 2) {
    //       color = '#87d068';
    //     } else if (value == 3) {
    //       color = '#FFFF00';
    //     } else if (value == 1) {
    //       color = '#f50';
    //     }
    //     return <a-badge color={color}></a-badge>;
    //   } else {
    //     return '--';
    //   }
    // },
  },
];
