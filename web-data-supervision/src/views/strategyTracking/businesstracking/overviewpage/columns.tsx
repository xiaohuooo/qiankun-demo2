// import dict from '@/utils/dict.js';

export const baseColumns = [
  {
    title: '分类',
    dataIndex: 'CLASS_NAME',
    key: 'CLASS_NAME',
    ellipsis: false,
    width: 100,
    rspans: [
      { start: 0, span: 17 },
      { start: 17, span: 6 },
      // { start: 1, span: 1 },
    ],
  },
  {
    title: '用户',
    dataIndex: 'USER_NAME',
    key: 'USER_NAME',
    width: '12%',
  },
  {
    title: '公司分管领导',
    dataIndex: 'COMPANY_LEADER',
    key: 'COMPANY_LEADER',
  },
  {
    title: '大客户总监',
    dataIndex: 'CUST_MANAGER',
    key: 'CUST_MANAGER',
  },
  {
    title: '1',
    dataIndex: 'one',
    key: 'one',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '2',
    dataIndex: 'two',
    key: 'two',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '3',
    dataIndex: 'third',
    key: 'third',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '4',
    dataIndex: 'fourth',
    key: 'fourth',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '5',
    dataIndex: 'fifth',
    key: 'fifth',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '6',
    dataIndex: 'sixth',
    key: 'sixth',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '7',
    dataIndex: 'seventh',
    key: 'seventh',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '8',
    dataIndex: 'eighth',
    key: 'eighth',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '9',
    dataIndex: 'ninth',
    key: 'ninth',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '10',
    dataIndex: 'tenth',
    key: 'tenth',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '11',
    dataIndex: 'eleventh',
    key: 'eleventh',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
  {
    title: '12',
    dataIndex: 'twelve',
    key: 'twelve',
    customRender: ({ value }) => {
      if (value) {
        let color = '#87d068';
        if (value == 2) {
          color = '#87d068';
        } else if (value == 3) {
          color = '#FFFF00';
        } else if (value == 1) {
          color = '#f50';
        }
        return <a-badge color={color}></a-badge>;
      } else {
        return '--';
      }
    },
  },
];
