const toTableData = (inputdata, inputid, keyColumnNameList = [], columnKeyList = []) => {
  const data = [];
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (returnData[i][j] === '' || returnData[i][j] === '-' || returnData[i][j] == null) {
        dtt[columnList[j].name] = '-';
      } else {
        dtt[columnList[j].name] = returnData[i][j];
      }
    }
    data.push(dtt);
  }
  return setData(data, keyColumnNameList, columnKeyList);
};

const toTableData1 = (
  inputdata,
  inputid,
  inputidboll,
  parseFunc,
  keyColumnNameList = [],
  columnKeyList = [],
) => {
  const data = [];
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (returnData[i][j] === '' || returnData[i][j] === '-' || returnData[i][j] == null) {
        dtt[columnList[j].name] = '-';
      } else {
        if (parseFunc(columnList[j].name)) {
          // 标题表头等无需数据处理
          dtt[columnList[j].name] = returnData[i][j];
        } else {
          if (inputidboll === 1) {
            dtt[columnList[j].name] = parseFloat(returnData[i][j]).toFixed(1);
          } else if (inputidboll === 2) {
            const indata = parseFloat(returnData[i][j]).toFixed(0);
            dtt[columnList[j].name] = parseFloat(indata).toLocaleString();
          } else {
            dtt[columnList[j].name] = returnData[i][j];
          }
        }
      }
    }
    data.push(dtt);
  }
  return setData(data, keyColumnNameList, columnKeyList);
};

const setData = (data, keyColumnNameList, columnKeyList) => {
  if (keyColumnNameList.length > 0 && columnKeyList.length > 0) {
    columnKeyList.forEach((v) => {
      if (v.columnKey.indexOf(',')) {
        v.columnKey1 = v.columnKey.split(',');
      } else {
        v.columnKey1 = [v.columnKey];
      }
    });
    data.forEach((v) => {
      v.flagKey = [];
    });
    columnKeyList.forEach((k) => {
      k.columnKey1.forEach((h) => {
        data.forEach((v) => {
          keyColumnNameList[0].keyColumnName.forEach((j) => {
            if (v[j] == h) {
              v.flagKey.push(h);
            }
          });
        });
      });
    });
    data.forEach((v) => {
      if (v.flagKey.length > 0) {
        v.flagKey = v.flagKey.join(',');
        columnKeyList.forEach((j) => {
          if (v.flagKey === j.columnKey) {
            v.flag = true;
          }
        });
      } else {
        v.flagKey = null;
      }
    });
    return data;
  } else {
    return data;
  }
};

const toChartxAxis = () => {
  const xAxis = [];
  for (let i = 0; i < 12; i++) {
    xAxis.push(`${i + 1}月`);
  }
  return xAxis;
};
const toChartSeries = (inputdata) => {
  const data = inputdata;
  for (let i = data.length; i < 12; i++) {
    data.push('-');
  }
  return data;
};

const toChartsDataxAxis = (xAxis1) => {
  const xAxis = [];
  for (let i = 0; i < xAxis1.length; i++) {
    const jqdata = xAxis1[i].substring(4);
    switch (jqdata) {
      case '01':
        xAxis.push(`${i + 1}月`);
        break;
      case '02':
        xAxis.push(`${i + 1}月`);
        break;
      case '03':
        xAxis.push(`${i + 1}月`);
        break;
      case '04':
        xAxis.push(`${i + 1}月`);
        break;
      case '05':
        xAxis.push(`${i + 1}月`);
        break;
      case '06':
        xAxis.push(`${i + 1}月`);
        break;
      case '07':
        xAxis.push(`${i + 1}月`);
        break;
      case '08':
        xAxis.push(`${i + 1}月`);
        break;
      case '09':
        xAxis.push(`${i + 1}月`);
        break;
      case '10':
        xAxis.push(`${i + 1}月`);
        break;
      case '11':
        xAxis.push(`${i + 1}月`);
        break;
      case '12':
        xAxis.push(`${i + 1}月`);
        break;
      default:
        break;
    }
  }
  return xAxis;
};

export default {
  toTableData,
  toTableData1,
  toChartxAxis,
  toChartSeries,
  toChartsDataxAxis,
};
