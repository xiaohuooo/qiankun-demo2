// 完成率保留一位小数位
const XiaoText = (text) => {
  if (text === '-') {
    return text;
  } else {
    return parseFloat(text).toFixed(1);
  }
};
// 金额保留整数
const MonText = (text) => {
  if (text === '-') {
    return text;
  } else {
    const datatext = parseFloat(text).toFixed(0);
    return parseInt(datatext).toLocaleString();
  }
};

// 一个入参
const parameter = (username, uservalue, usrtID) => {
  const cans = {
    relations: [
      {
        filterField: username,
        filterValues: [uservalue],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 区间入参
const parameter1 = (username, uservalue, usrtID) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 两个入参
const parameter2 = (username, uservalue, usrtID, username1, uservalue1) => {
  const cans = {
    relations: [
      {
        filterField: username,
        filterValues: [uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};

const parameter3 = (username, uservalue, usrtID, username1, uservalue1) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};

const parameter4 = (username, uservalue, usrtID, username1, uservalue1, username2, uservalue2) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
      {
        filterField: username2,
        filterValues: [uservalue2],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};

export default {
  XiaoText,
  MonText,
  parameter,
  parameter1,
  parameter2,
  parameter3,
  parameter4,
};
