import dict from '@/utils/dict.js';

export const baseColumns = [
  {
    title: '模型名称',
    dataIndex: 'modelName',
    width: 250,
    ellipsis: true,
    fixed: 'left',
    sorter: true,
  },
  {
    title: '监督业务对象',
    dataIndex: 'businessObjs',
    ellipsis: true,
    fixed: 'left',
    width: 110,
    customRender({ record }) {
      if (record.businessObjs !== null) {
        const obj = dict.getValue('businessObject', record.businessObjs);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '监督业务环节',
    dataIndex: 'bizIds',
    ellipsis: true,
    width: 160,
    fixed: 'left',
    customRender({ record }) {
      if (record.bizIds !== null) {
        const obj = dict.getValue('bizIds', record.bizIds);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '运行状态',
    dataIndex: 'runStatus',
    width: 100,
    fixed: 'left',
    customRender: ({ value }) => {
      let color = '#87d068';
      let text = '成功';

      if (value == 7) {
        color = '#87d068';
        text = '成功';
      } else if (value == 1) {
        color = '#87d068';
        text = '运行中';
      } else if (value == 6) {
        color = '#f50';
        text = '失败';
      } else if (value == 0) {
        color = '#f50';
        text = '未运行';
      }

      return <a-badge color={color} text={text}></a-badge>;
    },
  },
  {
    title: '上线状态',
    dataIndex: 'onlineStatus',
    width: 100,
    fixed: 'left',
    customRender: ({ value }) => {
      let color = '#87d068';
      let text = '成功';

      if (value == 1) {
        color = '#87d068';
        text = '已上线';
      } else if (value == -1) {
        color = '#87d068';
        text = '待上线';
      } else if (value == 0) {
        color = '#f50';
        text = '已下线';
      }

      return <a-badge color={color} text={text}></a-badge>;
    },
  },
  {
    title: '模型描述',
    dataIndex: 'description',
    ellipsis: true,
    width: 150,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.description} placement="topLeft">
          {record.description}
        </a-tooltip>
      );
    },
  },
  {
    title: '类型',
    dataIndex: 'modelType',
    width: 50,
    customRender({ record }) {
      const text = record.modelType;
      if (text == 'static') {
        return '静态';
      } else if (text == 'dynamic') {
        return '动态';
      } else if (text == 'algorithm') {
        return '算法';
      }
    },
  },
  {
    title: '模型记录数',
    dataIndex: 'suspectedRecords',
    ellipsis: true,
    width: 100,
  },
  {
    title: '模型最新记录数',
    dataIndex: 'latestRecords',
    ellipsis: true,
    width: 120,
  },
  {
    title: '模型管理员',
    dataIndex: 'targetUsernames', //sendTargetUsers
    width: 100,
  },
  {
    title: '所属防线',
    dataIndex: 'lineOfDefenseType',
    ellipsis: true,
    width: 80,
    customRender: ({ record }) => {
      if (record.lineOfDefenseType != null) {
        const obj = dict.getValue('lineOfDefense', record.lineOfDefenseType);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '监督领域',
    dataIndex: 'bizSystem',
    ellipsis: true,
    width: 110,
    customRender({ record }) {
      if (record.bizSystem !== null) {
        const obj = dict.getValue('bizSystem', record.bizSystem);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '业务用途',
    dataIndex: 'businessPurpose',
    width: 100,
    customRender: ({ value }) => {
      let text = '分析模型';

      if (value == 1) {
        text = '疑点模型';
      }

      return (
        <a-tooltip title={text} placement="topLeft">
          <span>{text}</span>
        </a-tooltip>
      );
    },
  },
  {
    title: '模型归属单位',
    dataIndex: 'modelBelongUnit',
    ellipsis: true,
    width: 150,
  },
  {
    title: '模型归属部门',
    dataIndex: 'modelBelongDept',
    ellipsis: true,
    width: 150,
  },
  {
    title: '模型编号',
    dataIndex: 'modelCode',
    width: 150,
    ellipsis: true,
  },
  {
    title: '模型产生方式',
    dataIndex: 'generationMethod',
    ellipsis: true,
    width: 110,
    customRender({ record }) {
      if (record.generationMethod !== null) {
        const obj = dict.getValue('generationMethod', record.generationMethod);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },

  {
    title: '监督类别',
    dataIndex: 'belongUnit',
    ellipsis: true,
    width: 80,
    customRender: ({ record }) => {
      if (record.belongUnit != null) {
        const obj = dict.getValue('belongUnit', record.belongUnit);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },

  {
    title: '数据集',
    dataIndex: 'datatableNames',
    width: 100,
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.datatableNames.join()} placement="topLeft">
          {record.datatableNames.join()}
        </a-tooltip>
      );
    },
  },
  {
    title: '修改人',
    dataIndex: 'updateUser',
    width: 80,
    ellipsis: true,
  },
  {
    title: '修改时间',
    dataIndex: 'updateTime',
    width: 160,
    ellipsis: true,
  },
  {
    title: '创建人',
    dataIndex: 'createUser',
    width: 80,
    ellipsis: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 160,
    ellipsis: true,
  },
];
