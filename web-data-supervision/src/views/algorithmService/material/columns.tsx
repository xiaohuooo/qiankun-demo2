import dict from '@/utils/dict';

export const baseColumns = [
  {
    title: '编号',
    dataIndex: 'taskCode',
    width: 200,
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '文件名称',
    width: 200,
    dataIndex: 'fileName',
    align: 'center',
    ellipsis: true,
  },
  {
    title: '任务状态',
    width: 200,
    ellipsis: true,
    align: 'center',
    dataIndex: 'taskStatus',
    hideInSearch: true,
    customRender: ({ record }) => {
      //值集转换列表显示
      return dict.getItemText('MaterialTaskCode', record.taskStatus);
    },
  },
  {
    title: '上传人',
    dataIndex: 'createName',
    align: 'center',
    ellipsis: true,
    width: 200,
  },
  {
    title: '上传人单位/部门',
    dataIndex: 'deptName',
    align: 'center',
    ellipsis: true,
    width: 200,
    hideInSearch: true,
  },
  {
    title: '上传时间',
    width: 280,
    ellipsis: true,
    align: 'center',
    dataIndex: 'createDate',
    hideInSearch: true,
    formItemProps: {
      component: 'RangePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
  {
    title: '文件生成时间',
    width: 200,
    align: 'center',
    hideInSearch: true,
    dataIndex: 'fileGenerationTime',
  },
  {
    title: '上传时间',
    width: 200,
    align: 'center',
    dataIndex: 'extend1',
    hideInTable: true,
    formItemProps: {
      component: 'RangePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
