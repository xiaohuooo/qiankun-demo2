import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.MenuListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

/**
 * 将对应菜单类型转为字符串字意
 */
const getMenuType = (type) => {
  switch (type) {
    case '0':
    case 0:
      return '菜单';
    case '1':
    case 1:
      return '资源';
    default:
      return '';
  }
};

export function getBaseColumns(getIcon: Function): TableColumnItem[] {
  return [
    {
      title: '名称',
      dataIndex: 'title',
      width: 240,
      fixed: 'left',
    },
    {
      title: '图标',
      width: 80,
      dataIndex: 'icon',
      align: 'center',
      // customRender: ({ record }) =>
      //   record.icon && <icon-font type="icon-kehucanshupeizhi" size="22" />,
      customRender: () => {
        const cicon = getIcon('CalendarOutlined');
        return <component is={cicon} />;
      },
    },
    {
      title: '类型',
      width: 80,
      align: 'center',
      dataIndex: 'type',
      customRender: ({ record }) => getMenuType(record.type),
    },
    {
      title: '节点路由',
      dataIndex: 'path',
      align: 'center',
      width: 240,
    },
    {
      title: '路由缓存',
      dataIndex: 'keepAlive',
      align: 'center',
      width: 80,
      customRender: ({ record }) => record.type == 0 && (record.keepAlive == '10' ? '是' : '否'),
    },
    {
      title: '文件路径',
      width: 280,
      align: 'center',
      dataIndex: 'component',
    },
    {
      title: '排序号',
      width: 80,
      align: 'center',
      dataIndex: 'order',
    },
    {
      title: '更新时间',
      width: 180,
      align: 'center',
      dataIndex: 'modifyTime',
    },
  ];
}

export const baseColumns: TableColumnItem[] = [
  {
    title: '名称',
    dataIndex: 'title',
    width: 240,
    ellipsis: true,
    fixed: 'left',
  },
  {
    title: '图标',
    width: 80,
    dataIndex: 'icon',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
    // customRender: ({ record }) => record.icon && <icon-font type={record.icon} size="22" />,
    customRender: ({ record }) => <icon-font type={record.icon} size="22" />,
  },
  {
    title: '类型',
    width: 80,
    ellipsis: true,
    align: 'center',
    dataIndex: 'type',
    hideInSearch: true,
    customRender: ({ record }) => {
      const text = getMenuType(record.type);
      return <span>{text}</span>;
    },
  },
  {
    title: '节点路由',
    dataIndex: 'path',
    align: 'center',
    ellipsis: true,
    width: 240,
    hideInSearch: true,
  },
  {
    title: '权限',
    dataIndex: 'perms',
    align: 'center',
    ellipsis: true,
    width: 200,
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.perms != undefined) {
        return record.perms;
      } else {
        return '';
      }
    },
  },
  {
    title: '路由缓存',
    dataIndex: 'keepAlive',
    align: 'center',
    ellipsis: true,
    width: 80,
    hideInSearch: true,
    customRender: ({ record }) => record.type === 0 && (record.keepAlive == '10' ? '是' : '否'),
  },
  {
    title: '文件路径',
    width: 280,
    ellipsis: true,
    align: 'center',
    hideInSearch: true,
    dataIndex: 'component',
  },
  {
    title: '排序号',
    width: 80,
    align: 'center',
    hideInSearch: true,
    dataIndex: 'orderNum',
  },
  {
    title: '更新时间',
    width: 180,
    align: 'center',
    hideInSearch: true,
    dataIndex: 'modifyTime',
  },
];
