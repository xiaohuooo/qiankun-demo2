import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '项目编号',
      columnName: 'projectCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目名称',
      columnName: 'projectName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目类别',
      columnName: 'projectCategory',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('PROJECT_CATEGORY'),
    },
    {
      columnCname: '问题子项编号',
      columnName: 'problemDetailCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题名称',
      columnName: 'problemName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题大类',
      columnName: 'problemType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('ISSUE_LARGE_CATEGORIES'),
    },
    {
      columnCname: '问题子项详情',
      columnName: 'problemDetail',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '被巡察单位',
      columnName: 'inspectedUnits',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '整改状态',
      columnName: 'remediationStatus',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('INSPECTION_REMEDIATION_STATUS'),
    },
    {
      columnCname: '整改期限状态',
      columnName: 'rectificationPeriodStatus',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '被巡察单位联系人',
      columnName: 'inspectedUnitsPerson',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建人',
      columnName: 'createName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建时间',
      columnName: 'createDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
