// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns = [
  {
    title: '项目编号',
    width: 90,
    dataIndex: 'projectCode',
    ellipsis: true,
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '问题子项编号',
    width: 80,
    dataIndex: 'problemDetailCode',
    ellipsis: true,
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '项目名称',
    width: 90,
    dataIndex: 'projectName',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '项目类别',
    width: 80,
    ellipsis: true,
    dataIndex: 'projectCategory',
    customRender: ({ record }) => {
      return dict.getItemText('PROJECT_CATEGORY', record.projectCategory);
    },
  },
  {
    title: '问题名称',
    width: 120,
    dataIndex: 'problemName',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题大类',
    width: 100,
    ellipsis: true,
    dataIndex: 'problemType',
    customRender: ({ record }) => {
      return dict.getItemText('ISSUE_LARGE_CATEGORIES', record.problemType);
    },
  },
  {
    title: '问题子项详情',
    width: 100,
    ellipsis: true,
    dataIndex: 'problemDetail',
    hideInSearch: true,
  },
  {
    title: '被巡察单位',
    width: 100,
    ellipsis: true,
    dataIndex: 'inspectedUnits',
    hideInSearch: true,
  },
  {
    title: '整改状态',
    width: 80,
    dataIndex: 'remediationStatus',
    customRender: ({ record }) => {
      return dict.getItemText('INSPECTION_REMEDIATION_STATUS', record.remediationStatus);
    },
  },
  {
    title: '整改期限状态',
    width: 80,
    dataIndex: 'rectificationPeriodStatus',
  },
  {
    title: '被巡察单位联系人',
    width: 120,
    dataIndex: 'inspectedUnitsPerson',
    ellipsis: true,
    hideInSearch: true,
  },

  {
    title: '创建人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createName',
  },
  {
    title: '创建时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createDate',
  },
];
