// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns = [
  {
    title: '项目编号',
    width: 90,
    dataIndex: 'projectCode',
    ellipsis: true,
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '问题编号',
    width: 100,
    dataIndex: 'problemCode',
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '问题类型',
    width: 80,
    dataIndex: 'problemType',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_TYPE', record.problemType);
    },
  },
  {
    title: '检查内容简述',
    width: 120,
    dataIndex: 'problemContent',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '发现问题简述',
    width: 80,
    dataIndex: 'problemFind',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题责任部门',
    width: 80,
    dataIndex: 'problemPersonUnitName',
    hideInSearch: true,
  },
  {
    title: '问题责任人',
    width: 80,
    dataIndex: 'problemPerson',
    hideInSearch: true,
  },
  { title: '问题整改要求', width: 90, dataIndex: 'problemAsk', ellipsis: true, hideInSearch: true },
  { title: '问题整改部门', width: 120, dataIndex: 'problemDeptName', hideInSearch: true },
  { title: '问题整改人员', width: 90, dataIndex: 'handleUsername', hideInSearch: true },
  {
    title: '问题整改状态',
    width: 90,
    dataIndex: 'status',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_RECTIFICATION_STATUS', record.status);
    },
  },
  {
    title: '创建人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createName',
  },
  {
    title: '创建时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createDate',
  },
  {
    title: '更新人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'updateName',
  },
  {
    title: '更新时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'updateDate',
  },
];
