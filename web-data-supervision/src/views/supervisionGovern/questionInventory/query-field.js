import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '项目编号',
      columnName: 'projectCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题编号',
      columnName: 'problemCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题类型',
      columnName: 'problemType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('PROBLEM_TYPE'),
    },

    {
      columnCname: '检查内容简述',
      columnName: 'problemContent',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '发现问题简述',
      columnName: 'problemFind',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题责任部门',
      columnName: 'problemPersonUnitName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题责任人',
      columnName: 'problemPerson',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题整改要求',
      columnName: 'problemAsk',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题整改部门',
      columnName: 'problemDeptName',
      newType: '1',
      example: null,
      dictCode: null,
    },

    {
      columnCname: '问题整改人员',
      columnName: 'handleUsername',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题整改状态',
      columnName: 'status',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('PROBLEM_RECTIFICATION_STATUS'),
    },
    {
      columnCname: '创建人',
      columnName: 'createName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建时间',
      columnName: 'createDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '更新人',
      columnName: 'updateName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '更新时间',
      columnName: 'updateDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
