// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns = [
  {
    title: '待办编号',
    width: 80,
    dataIndex: 'clueId',
    ellipsis: true,
    hideInSearch: true,
    fixed: true,
  },
  {
    title: '问题子项编号',
    width: 120,
    dataIndex: 'problemDetailCode',
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '项目名称',
    width: 120,
    dataIndex: 'projectName',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '项目类别',
    width: 80,
    ellipsis: true,
    dataIndex: 'projectCategory',
    customRender: ({ record }) => {
      return dict.getItemText('PROJECT_CATEGORY', record.projectCategory);
    },
  },
  {
    title: '问题大类',
    width: 100,
    ellipsis: true,
    dataIndex: 'problemType',
    customRender: ({ record }) => {
      return dict.getItemText('ISSUE_LARGE_CATEGORIES', record.problemType);
    },
  },
  {
    title: '问题名称',
    width: 90,
    dataIndex: 'problemName',
    hideInSearch: true,
    ellipsis: true,
  },

  {
    title: '问题子项详情',
    width: 120,
    dataIndex: 'problemDetail',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题责任部门',
    width: 120,
    dataIndex: 'problemLiabilityDept',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题责任人',
    width: 120,
    dataIndex: 'problemLiabilityPerson',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '整改状态',
    width: 80,
    dataIndex: 'remediationStatus',
    customRender: ({ record }) => {
      return dict.getItemText('INSPECTION_REMEDIATION_STATUS', record.remediationStatus);
    },
  },
  { title: '上级推送人', width: 100, dataIndex: 'beforeUser', hideInSearch: true },
  { title: '上级推送时间', width: 150, dataIndex: 'updateDate', hideInSearch: true },
  { title: '待办创建人', width: 100, dataIndex: 'createName', hideInSearch: true },
  { title: '待办创建时间', width: 150, dataIndex: 'createDate', hideInSearch: true },
];
