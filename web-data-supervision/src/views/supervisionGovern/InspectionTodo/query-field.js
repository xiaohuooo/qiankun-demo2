import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '待办编号',
      columnName: 'clueId',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题子项编号',
      columnName: 'problemDetailCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目名称',
      columnName: 'projectName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目类别',
      columnName: 'projectCategory',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('PROJECT_CATEGORY'),
    },
    {
      columnCname: '问题大类',
      columnName: 'problemType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('ISSUE_LARGE_CATEGORIES'),
    },
    {
      columnCname: '问题名称',
      columnName: 'problemName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题子项详情',
      columnName: 'problemDetail',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题责任部门',
      columnName: 'problemLiabilityDept',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题责任人',
      columnName: 'problemLiabilityPerson',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题整改状态',
      columnName: 'status',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('INSPECTION_REMEDIATION_STATUS'),
    },
    {
      columnCname: '上级推送人',
      columnName: 'beforeUser',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '上级推送时间',
      columnName: 'updateDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办创建人',
      columnName: 'createName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办创建时间',
      columnName: 'createDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
