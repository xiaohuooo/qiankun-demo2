import dict from '@/utils/dict.js';

export const baseColumns = [
  {
    title: '待办编号',
    dataIndex: 'id',
    width: 50,
    fixed: 'left',
  },
  {
    title: '关键信息',
    dataIndex: 'recordKeyChinese',
    ellipsis: true,
    width: 120,
    fixed: 'left',
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.recordKeyChinese} placement="topLeft">
          {record.recordKeyChinese}
        </a-tooltip>
      );
    },
  },
  {
    title: '所属模型',
    dataIndex: 'modelName',
    ellipsis: true,
    width: 100,
    fixed: 'left',
  },
  {
    title: '所属防线',
    dataIndex: 'lineOfDefenseType',
    ellipsis: true,
    width: 60,
    customRender: ({ record }) => {
      return dict.getItemText('lineOfDefense', record.lineOfDefenseType);
    },
  },
  {
    title: '监督领域',
    dataIndex: 'bizSystem',
    ellipsis: true,
    width: 80,
    customRender: ({ record }) => {
      return dict.getItemText('bizSystem', record.bizSystem);
    },
  },
  {
    title: '问题类型',
    dataIndex: 'problemType',
    ellipsis: true,
    width: 60,
  },
  {
    title: '待办类型',
    ellipsis: true,
    dataIndex: 'todoType',
    width: 60,
  },
  {
    title: '业务用途',
    dataIndex: 'businessPurpose',
    width: 60,
    ellipsis: true,
    customRender: ({ record }) => {
      return record.businessPurpose == '1' ? '疑点模型' : '分析模型';
    },
  },
  {
    title: '上级推送人',
    dataIndex: 'beforeUser',
    width: 80,
    ellipsis: true,
  },
  {
    title: '当前环节',
    dataIndex: 'todoLink',
    ellipsis: true,
    width: 60,
  },
  {
    title: '整改状态',
    dataIndex: 'rectifyStatus',
    ellipsis: true,
    width: 80,
    customRender: ({ record }) => {
      return dict.getItemText('CLUES_RECTIFICATION_STATUS', record.rectifyStatus);
    },
  },
  {
    title: '处理时间',
    dataIndex: 'updateTime',
    width: 100,
    ellipsis: true,
  },
];
