export { default as basicNews } from '../components/basicNews.vue';
export { default as firstProcess } from './firstBatching/FirstProcess.vue';
export { default as batchFirstCheckVisible } from './firstBatching/FirstCheckVisible.vue';
export { default as secondProcess } from './secondBatching/SecondProcess.vue';
export { default as batchSecondCheckVisible } from './secondBatching/SecondCheckVisible.vue';
export { default as thirdProcess } from './thirdBatching/ThirdProcess.vue';
export { default as ThirdCheckVisible } from './thirdBatching/ThirdCheckVisible.vue';
