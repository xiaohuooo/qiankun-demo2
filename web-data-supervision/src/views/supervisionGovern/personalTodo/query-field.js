import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '待办编号',
      columnName: 'id',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '关键信息',
      columnName: 'recordKeyChinese',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '所属模型',
      columnName: 'modelName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '所属防线',
      columnName: 'lineOfDefenseType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('lineOfDefense'),
    },
    {
      columnCname: '监督领域',
      columnName: 'bizSystem',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('bizSystem'),
    },
    {
      columnCname: '问题类型',
      columnName: 'problemType',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办类型',
      columnName: 'todoType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('TODO_TYPE'),
    },
    {
      columnCname: '业务用途',
      columnName: 'businessPurpose',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('businessPurpose'),
    },
    {
      columnCname: '上级推送人',
      columnName: 'beforeUser',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '当前环节',
      columnName: 'todoLink',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('HISTORY_LINK_CODE'),
    },
    {
      columnCname: '整改状态',
      columnName: 'rectifyStatus',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('CLUES_RECTIFICATION_STATUS'),
    },
    {
      columnCname: '处理时间',
      columnName: 'updateTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
