import dict from '@/utils/dict.js';
const queryFields = {
  fieldsTwo: [
    {
      columnCname: '模型组名称',
      columnName: 'modelGroupName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '模型组编号',
      columnName: 'modelGroupCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '包含模型',
      columnName: 'modelNames',
      newType: '1',
      example: null,
    },
    {
      columnCname: '运行状态',
      columnName: 'runStatus',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('RUN_STATUS'),
    },
    {
      columnCname: '模型组管理员',
      columnName: 'targetUsernames',
      newType: '1',
      example: null,
    },
    {
      columnCname: '最近一次运行时间',
      columnName: 'lastRunTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建人',
      columnName: 'createUserName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建时间',
      columnName: 'createTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '修改人',
      columnName: 'updateUserName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '修改时间',
      columnName: 'updateTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
