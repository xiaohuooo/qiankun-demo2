export const baseColumns = [
  {
    title: '模型组名称',
    dataIndex: 'modelGroupName',
    width: 200,
    ellipsis: true,
    fixed: 'left',
  },
  {
    title: '包含模型',
    dataIndex: 'modelNames',
    width: 250,
    ellipsis: true,
  },

  {
    title: '运行状态',
    dataIndex: 'runStatus',
    width: 100,
    customRender: ({ value }) => {
      let color = '#87d068';
      let text = '成功';
      if (value == 7) {
        color = '#87d068';
        text = '成功';
      } else if (value == 1) {
        color = '#87d068';
        text = '运行中';
      } else if (value == 6) {
        color = '#f50';
        text = '失败';
      } else if (value == 0) {
        color = '#f50';
        text = '未运行';
      }

      return <a-badge color={color} text={text}></a-badge>;
    },
  },
  {
    title: '模型组管理员',
    dataIndex: 'targetUsernames',
    width: 100,
  },
  {
    title: '最近一次运行时间',
    dataIndex: 'lastRunTime',
    width: 160,
    ellipsis: true,
  },
  {
    title: '运行周期',
    dataIndex: 'cronDesc',
    width: 160,
    ellipsis: true,
  },
  {
    title: '创建人',
    dataIndex: 'createUserName',
    width: 80,
    ellipsis: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 160,
    ellipsis: true,
  },
  {
    title: '修改人',
    dataIndex: 'updateUserName',
    width: 80,
    ellipsis: true,
  },
  {
    title: '修改时间',
    dataIndex: 'updateTime',
    width: 160,
    ellipsis: true,
  },
  {
    title: '模型组编号',
    dataIndex: 'modelGroupCode',
    width: 140,
    ellipsis: true,
  },
];
