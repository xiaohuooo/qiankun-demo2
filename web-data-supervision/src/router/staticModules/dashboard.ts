import type { RouteRecordRaw } from 'vue-router';
// import RouterView from '@/layout/routerView/index.vue';
// import { t } from '@/hooks/useI18n';

const moduleName = 'dashboard';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    name: moduleName,
    // redirect: '/dashboard/welcome',
    component: () =>
      process.env.NODE_ENV === 'smartdash'
        ? import(
            /* webpackChunkName: "dashboard-welcome" */ '@/views/dashboard/welcome/SmartDashIndex.vue'
          )
        : import(/* webpackChunkName: "dashboard-welcome" */ '@/views/dashboard/welcome/index.vue'),
    meta: {
      // title: t('routes.dashboard.dashboard'),
      title: '系统主页',
      icon: 'icon-shouye',
    },
    children: [
      // {
      //   path: 'welcome',
      //   name: `${moduleName}-welcome`,
      //   meta: {
      //     title: t('routes.dashboard.workbench'),
      //     icon: 'icon-shouye',
      //   },
      //   component: () =>
      //     import(/* webpackChunkName: "dashboard-welcome" */ '@/views/dashboard/welcome/index.vue'),
      // },
    ],
  },
];

export default routes;
