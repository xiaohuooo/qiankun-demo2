/**
 * 物料对照
 */
export default {
  'views/algorithmService/material/MaterialComparison': () =>
    import('@/views/algorithmService/material/MaterialComparison.vue'),
} as const;
