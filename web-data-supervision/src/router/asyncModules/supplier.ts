export default {
  'views/supplier/authentication/QueryList': () =>
    import('@/views/supplier/authentication/QueryList.vue'),
  'views/supplier/commodity/Commodity': () => import('@/views/supplier/commodity/Commodity.vue'),
} as const;
