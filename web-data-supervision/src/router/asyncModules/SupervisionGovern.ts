/* SupervisionGovern module
 */
export default {
  'views/SupervisionGovern/modelresult/ModelResultNew': () =>
    import('@/views/supervisionGovern/modelresult/ModelResultNew.vue'),
  'views/SupervisionGovern/personalTodo/index': () =>
    import('@/views/supervisionGovern/personalTodo/index.vue'),
  'views/SupervisionGovern/modelGroupTodo/index': () =>
    import('@/views/supervisionGovern/modelGroupTodo/index.vue'),
  'views/SupervisionGovern/toDoHistory/historyLists': () =>
    import('@/views/supervisionGovern/toDoHistory/historyLists.vue'),
  'views/SupervisionGovern/toDoHistory/modelGroupHistoryLists': () =>
    import('@/views/supervisionGovern/toDoHistory/modelGroupHistoryLists.vue'),
  'views/SupervisionGovern/sqlAnalysis/sqlAnalysis': () =>
    import('@/views/supervisionGovern/sqlAnalysis/sqlAnalysis.vue'),
  'views/SupervisionGovern/InfoAll': () => import('@/views/supervisionGovern/InfoAll.vue'),
  'views/SupervisionGovern/reportDownload/index': () =>
    import('@/views/supervisionGovern/reportDownload/index.vue'),
  'views/SupervisionGovern/repeatSupervise/index': () =>
    import('@/views/supervisionGovern/repeatSupervise/index.vue'),
  'views/SupervisionGovern/inspectionSupervision/index': () =>
    import('@/views/supervisionGovern/inspectionSupervision/index.vue'),
  'views/SupervisionGovern/questionInventory/index': () =>
    import('@/views/supervisionGovern/questionInventory/index.vue'),
  'views/SupervisionGovern/questionTodo/index': () =>
    import('@/views/supervisionGovern/questionTodo/index.vue'),
  'views/SupervisionGovern/questionHistoryLists/index': () =>
    import('@/views/supervisionGovern/questionHistoryLists/index.vue'),
  'views/SupervisionGovern/InspectionIssues/index': () =>
    import('@/views/supervisionGovern/InspectionIssues/index.vue'),
  'views/SupervisionGovern/InspectionQuestion/index': () =>
    import('@/views/supervisionGovern/InspectionQuestion/index.vue'),
  'views/SupervisionGovern/InspectionTodo/index': () =>
    import('@/views/supervisionGovern/InspectionTodo/index.vue'),
} as const;
// 'views/supervisionGovern/SupervisionReport/index': () =>
//   import('@/views/supervisionGovern/SupervisionReport/index.vue'),
