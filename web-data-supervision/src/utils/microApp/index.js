// 引入 qiankun  应用注册函数   开启函数
import { registerMicroApps, start } from "qiankun";
// 引入 微应用配置文件
import microAppSetting from "./microAppSetting";
//注册子应用

registerMicroApps(microAppSetting.microApps());
//开启
start({
    prefetch: "all", // 可选，是否开启预加载，默认为 true。
    sandbox: true, // 可选，是否开启沙箱，默认为 true。// 从而确保微应用的样式不会对全局造成影响。
    singular: true, // 可选，是否为单实例场景，单实例指的是同一时间只会渲染一个微应用。默认为 true。
});