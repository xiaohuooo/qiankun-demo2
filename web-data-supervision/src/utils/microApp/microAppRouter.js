import { Storage } from '@/utils/Storage';
import {
  ACCESS_TOKEN_KEY,
  ACCESS_USERNAME_KEY,
  ACCESS_PERMS_KEY,
  ACCESS_USER_INFO_KEY,
  ACCESS_DICT_ALL_KEY,
} from '@/enums/cacheEnum';
// 微应用路由
const microAppRouter = [
  // {
  //   name: 'web-performance-eval', //用于应用名   容器id   应用路由基地址
  //   url: 'http://localhost:8099/web-performance-eval/', //应用路径（ip与端口）
  //   props: {
  //     propsName: '8099',
  //     token: Storage.get(ACCESS_TOKEN_KEY, null),
  //     name: Storage.get(ACCESS_USERNAME_KEY, ''),
  //   }, //初始化时需要传递给微应用的数据
  //   // hidden: false,//是否启用该应用，默认false
  //   menuName: 'web-performance-eval', //自定义属性 根据需要自己配置（用在了菜单导航的名称）
  // },
  {
    name: 'web-data-supervision2', //用于应用名   容器id   应用路由基地址
    url: 'http://localhost:8100/web-data-supervision2/', //应用路径（ip与端口）
    container: `#appContainer1`, //容器id
    props: {
      propsName: '8100',
      token: Storage.get(ACCESS_TOKEN_KEY, null),
      name: Storage.get(ACCESS_USERNAME_KEY, ''),
    }, //初始化时需要传递给微应用的数据
    // hidden: false,//是否启用该应用，默认false
    menuName: 'web-data-supervision2', //自定义属性 根据需要自己配置（用在了菜单导航的名称）
  },
  {
    name: 'web-data-supervision3', //用于应用名   容器id   应用路由基地址
    url: 'http://localhost:8101/web-data-supervision3/', //应用路径（ip与端口）
    container: `#appContainer2`, //容器id
    props: {
      propsName: '8100',
      token: Storage.get(ACCESS_TOKEN_KEY, null),
      name: Storage.get(ACCESS_USERNAME_KEY, ''),
    }, //初始化时需要传递给微应用的数据
    // hidden: false,//是否启用该应用，默认false
    menuName: 'web-data-supervision3', //自定义属性 根据需要自己配置（用在了菜单导航的名称）
  },
];
export default microAppRouter;
