import { defineStore } from 'pinia';

// import { useWsStore } from './ws';
import dayjs from 'dayjs';
import type { RouteRecordRaw } from 'vue-router';
import { store } from '@/store';
import { login } from '@/api/login';
import {
  ACCESS_TOKEN_KEY,
  ACCESS_USERNAME_KEY,
  ACCESS_PERMS_KEY,
  ACCESS_USER_INFO_KEY,
  ACCESS_DICT_ALL_KEY,
} from '@/enums/cacheEnum';
import { Storage } from '@/utils/Storage';
import { getUsermenu, logout } from '@/api/account';
import { getUserDetail } from '@/api/system/user';
import { getDictAll } from '@/api/common';

import { generatorDynamicRouter } from '@/router/generator-router';
import { resetRouter } from '@/router';

interface UserState {
  token: string;
  name: string;
  avatar: string;
  // like [ 'sys:user:add', 'sys:user:update' ]
  perms: string[];
  menus: RouteRecordRaw[];
  userInfo: any;
  yearDate: string;
}
//黑名单菜单 拆分系统用
const blackMenuPath = ['/SupervisionModel', '/SupervisionGovern'];

export const useUserStore = defineStore({
  id: 'user',
  state: (): UserState => ({
    token: Storage.get(ACCESS_TOKEN_KEY, null),
    name: Storage.get(ACCESS_USERNAME_KEY, ''),
    avatar: '',
    perms: [],
    menus: [],
    userInfo: {},
    yearDate: '',
  }),
  getters: {
    getToken(): string {
      return this.token;
    },
    getAvatar(): string {
      return this.avatar;
    },
    getName(): string {
      return this.name;
    },
    getPerms(): string[] {
      return this.perms;
    },
  },
  actions: {
    /** 清空token及用户信息 */
    resetToken() {
      this.avatar = this.token = this.name = '';
      this.perms = [];
      this.menus = [];
      this.userInfo = {};
      Storage.clear();
    },
    setUserInfo(info) {
      this.userInfo = info;
    },
    setAvatar(avatar) {
      this.userInfo.avatar = avatar;
    },
    /** 登录成功保存token */
    setToken(token: string, expireTime: string | null) {
      this.token = token ?? '';
      let ex = 7 * 24 * 60 * 60 * 1000;
      if (expireTime && expireTime.length > 0) {
        ex = dayjs(expireTime).valueOf();
      }
      Storage.set(ACCESS_TOKEN_KEY, this.token, ex);
    },
    /** 登录成功保存用户名 */
    setUserName(name: string) {
      this.name = name ?? '';
      Storage.set(ACCESS_USERNAME_KEY, this.name);
    },
    /** 登录成功保存权限 */
    setPerms(perms: string[]) {
      this.perms = perms ?? [];
      Storage.set(ACCESS_PERMS_KEY, this.perms);
    },
    /** 登录 */
    async login(params: API.LoginParams) {
      try {
        // const { data } = await login(params);
        const res = await login(params);
        if (`${res.code}` != '200') {
          throw new Error(res.message);
        }
        this.setToken(res.data.token, res.data.exipreTime);

        Storage.set(ACCESS_USER_INFO_KEY, res.data.user);

        this.setUserName(params.username);

        const perms = res.data.permissions;
        this.setPerms(perms);

        return this.afterLogin();
      } catch (error) {
        return Promise.reject(error);
      }
    },
    /** 登录成功之后, 获取用户信息以及生成权限路由 */
    async afterLogin() {
      try {
        const name = Storage.get(ACCESS_USERNAME_KEY, '');

        // const wsStore = useWsStore();
        const env = process.env.NODE_ENV;
        console.log('env', env);

        const user = Storage.get(ACCESS_USER_INFO_KEY, null);

        let [userInfo, vuerouters, dictAll]: any = await Promise.all([
          getUserDetail(name),
          getUsermenu(user.userId),
          getDictAll(),
        ]);
        vuerouters = [
          {
            path: '/',
            showInSide: 1,
            name: '主页',
            component: 'MenuView',
            icon: 'none',
            redirect: '/home',
            children: [
              {
                path: '/home',
                showInSide: 1,
                name: '系统主页',
                component: 'HomePageView',
                icon: 'home',
                meta: {
                  closeable: false,
                  isShow: true,
                },
              },
              {
                id: '203',
                parentId: '0',
                path: '/home1',
                showInSide: 0,
                name: '首页',
                component: '/HomePage1',
                icon: 'rocket',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
              },
              {
                id: '1',
                parentId: '0',
                path: '/system',
                showInSide: 1,
                name: '系统管理',
                component: 'PageView',
                icon: 'appstore-o',
                meta: {
                  closeable: true,
                },
                keepAlive: '10',
                children: [
                  {
                    id: '3',
                    parentId: '1',
                    path: '/system/user',
                    showInSide: 1,
                    name: '用户管理',
                    component: 'system/user/User',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '4',
                    parentId: '1',
                    path: '/system/role',
                    showInSide: 1,
                    name: '角色管理',
                    component: 'system/role/Role',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '6',
                    parentId: '1',
                    path: '/system/dept',
                    showInSide: 1,
                    name: '部门管理',
                    component: 'system/dept/Dept',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '64',
                    parentId: '1',
                    path: '/system/dict',
                    showInSide: 0,
                    name: '字典管理',
                    component: 'system/dict/Dict',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '5',
                    parentId: '1',
                    path: '/system/menu',
                    showInSide: 1,
                    name: '菜单管理',
                    component: 'system/menu/Menu',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '144',
                parentId: '0',
                path: '/web-data-supervision2/dataAdministration',
                showInSide: 1,
                name: '数据模型',
                component: 'layout',
                icon: 'pie-chart',
                meta: {
                  closeable: true,
                },
                keepAlive: '10',
                children: [
                  {
                    id: '254',
                    parentId: '144',
                    path: '/dataAdministration/sortmanagement/SortManagement',
                    showInSide: 1,
                    name: '分类管理',
                    component: 'layout',
                    icon: 'bars',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '145',
                    parentId: '144',
                    path: '/dataAdministration/management/Management',
                    showInSide: 1,
                    name: '数据源管理',
                    component: 'layout',
                    icon: 'area-chart',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '151',
                    parentId: '144',
                    path: '/dataAdministration/setmanagement/SetManagement',
                    showInSide: 1,
                    name: '数据集管理',
                    component: 'dataAdministration/setmanagement/SetManagementNew',
                    icon: 'bar-chart',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '168',
                    parentId: '144',
                    path: '/system/dict',
                    showInSide: 1,
                    name: '值集管理',
                    component: 'views/system/dict/Dict',
                    icon: 'diff',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '275',
                    parentId: '144',
                    path: '/dataAdministration/downloadcenter/DownloadCenter',
                    showInSide: 1,
                    name: '下载中心',
                    component: 'views/dataAdministration/downloadcenter/DownloadCenter',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '272',
                    parentId: '144',
                    path: '/SupervisionGovern/sqlAnalysis/sqlAnalysis',
                    showInSide: 1,
                    name: 'SQL解析登记',
                    component: 'views/SupervisionGovern/sqlAnalysis/sqlAnalysis',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '285',
                parentId: '0',
                path: '/web-data-supervision3',
                showInSide: 1,
                name: '监督模型',
                component: 'layout',
                icon: 'diff',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '286',
                    parentId: '285',
                    path: '/SupervisionModel/modellist',
                    showInSide: 1,
                    name: '模型管理',
                    component: 'layout',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '400',
                    parentId: '285',
                    path: '/SupervisionModel/supervisionModelGroup/ModelGroupList',
                    showInSide: 1,
                    name: '模型组',
                    component: 'views/supervisionModelGroup/ModelGroupList',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                ],
              },
              {
                id: '146',
                parentId: '0',
                path: '/SupervisionGovern',
                showInSide: 1,
                name: '监督执纪',
                component: 'PageView',
                icon: 'credit-card',
                meta: {
                  closeable: true,
                },
                keepAlive: '10',
                children: [
                  {
                    id: '147',
                    parentId: '146',
                    path: '/SupervisionGovern/modelresult',
                    showInSide: 1,
                    name: '模型跟踪',
                    component: 'SupervisionGovern/modelresult/ModelResultNew',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '195',
                    parentId: '146',
                    path: '/SupervisionGovern/personalTodo',
                    showInSide: 1,
                    name: '模型个人待办',
                    component: 'SupervisionGovern/personalTodo/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '196',
                    parentId: '146',
                    path: '/SupervisionGovern/toDoHistory/historyLists',
                    showInSide: 1,
                    name: '模型处理记录',
                    component: 'SupervisionGovern/toDoHistory/historyLists',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '406',
                    parentId: '146',
                    path: '/SupervisionGovern/reportDownload/index',
                    showInSide: 1,
                    name: '监督成果',
                    component: 'views/SupervisionGovern/reportDownload/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '169',
                    parentId: '146',
                    path: '/SupervisionGovern/InfoAll',
                    showInSide: 0,
                    name: '个人疑点业务',
                    component: 'SupervisionGovern/InfoAll',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '421',
                    parentId: '146',
                    path: '/SupervisionGovern/repeatSupervise/index',
                    showInSide: 1,
                    name: '监督再监督',
                    component: 'views/SupervisionGovern/repeatSupervise/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '399',
                    parentId: '146',
                    path: '/SupervisionGovern/modelGroupTodo/index',
                    showInSide: 1,
                    name: '模型组个人待办',
                    component: 'views/SupervisionGovern/modelGroupTodo/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '398',
                    parentId: '146',
                    path: '/SupervisionGovern/toDoHistory/modelGroupHistoryLists',
                    showInSide: 1,
                    name: '模型组处理记录',
                    component: 'views/SupervisionGovern/toDoHistory/modelGroupHistoryLists',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                ],
              },
              {
                id: '152',
                parentId: '0',
                path: '/userAnalyze',
                showInSide: 1,
                name: '自主分析',
                component: 'PageView',
                icon: 'stock',
                meta: {
                  closeable: true,
                },
                keepAlive: '10',
                children: [
                  {
                    id: '180',
                    parentId: '152',
                    path: '/userAnalyze/dataPreview',
                    showInSide: 0,
                    name: '自主查看',
                    component: 'userAnalyze/dataPreview/dataPreview',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '171',
                    parentId: '152',
                    path: 'userAnalyze/SetManagementList',
                    showInSide: 1,
                    name: '数据集预览',
                    component: 'views/userAnalyze/setmanagement/SetManagementList',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '172',
                    parentId: '152',
                    path: '/userAnalyze/bulletinBoardList',
                    showInSide: 1,
                    name: '图表列表',
                    component: 'userAnalyze/bulletinBoardList/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '173',
                    parentId: '152',
                    path: '/userAnalyze/kanbanList',
                    showInSide: 1,
                    name: '看板列表',
                    component: 'userAnalyze/kanbanList/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '236',
                    parentId: '152',
                    path: 'userAnalyze/kanbanConfigTabs',
                    showInSide: 0,
                    name: '看板配置',
                    component: 'userAnalyze/kanbanList/kanbanConfigTabs',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '174',
                    parentId: '152',
                    path: '/userAnalyze/ChartConfigurations',
                    showInSide: 0,
                    name: '图表配置',
                    component: 'userAnalyze/ChartConfigurations/ChartConfiguration',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '331',
                    parentId: '152',
                    path: 'userAnalyze/DataSetInfo',
                    showInSide: 0,
                    name: '查看数据',
                    component: 'views/userAnalyze/setmanagement/components/DataSetInfo',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                ],
              },
              {
                id: '422',
                parentId: '0',
                path: '/supervisionGovern',
                showInSide: 1,
                name: '整改跟踪',
                component: 'PageView',
                icon: '',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '423',
                    parentId: '422',
                    path: '/SupervisionGovern/inspectionSupervision/index',
                    showInSide: 1,
                    name: '监督检查',
                    component: 'PageView',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                    children: [
                      {
                        id: '425',
                        parentId: '423',
                        path: '/SupervisionGovern/inspectionSupervision/index',
                        showInSide: 1,
                        name: '项目列表',
                        component: 'views/SupervisionGovern/inspectionSupervision/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                      {
                        id: '426',
                        parentId: '423',
                        path: '/SupervisionGovern/questionInventory/index',
                        showInSide: 1,
                        name: '问题清单',
                        component: 'views/SupervisionGovern/questionInventory/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                      {
                        id: '424',
                        parentId: '423',
                        path: '/SupervisionGovern/questionTodo/index',
                        showInSide: 1,
                        name: '问题整改个人待办',
                        component: 'views/SupervisionGovern/questionTodo/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                      {
                        id: '427',
                        parentId: '423',
                        path: '/SupervisionGovern/questionHistoryLists/index',
                        showInSide: 1,
                        name: '问题处理记录',
                        component: 'views/SupervisionGovern/questionHistoryLists/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                    ],
                  },
                  {
                    id: '437',
                    parentId: '422',
                    path: '/InspectionIssues/index',
                    showInSide: 1,
                    name: '巡察反馈',
                    component: 'PageView',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                    children: [
                      {
                        id: '438',
                        parentId: '437',
                        path: '/SupervisionGovern/InspectionIssues/index',
                        showInSide: 1,
                        name: '巡察项目',
                        component: 'views/SupervisionGovern/InspectionIssues/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                      {
                        id: '439',
                        parentId: '437',
                        path: '/SupervisionGovern/InspectionQuestion/index',
                        showInSide: 1,
                        name: '巡察问题',
                        component: 'views/SupervisionGovern/InspectionQuestion/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                      {
                        id: '440',
                        parentId: '437',
                        path: '/SupervisionGovern/InspectionTodo/index',
                        showInSide: 1,
                        name: '待办处理',
                        component: 'views/SupervisionGovern/InspectionTodo/index',
                        icon: '',
                        meta: {
                          closeable: true,
                        },
                        keepAlive: '00',
                      },
                    ],
                  },
                ],
              },
              {
                id: '162',
                parentId: '0',
                path: '/businessTracking',
                showInSide: 0,
                name: '业务跟踪',
                component: '/businessTracking',
                icon: 'file-search',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
              },
              {
                id: '178',
                parentId: '0',
                path: '/strategyTracking',
                showInSide: 1,
                name: '采购运营管理驾驶舱',
                component: 'PageView',
                icon: 'highlight',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '179',
                    parentId: '178',
                    path: '/strategyTracking/businesstracking/OverMonth',
                    showInSide: 1,
                    name: '总览',
                    component: 'views/strategyTracking/businesstracking/OverMonth',
                    icon: 'bar-chart',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '201',
                    parentId: '178',
                    path: '/strategyTracking/businesstracking/regionalcenter',
                    showInSide: 1,
                    name: '各中心各大区',
                    component: 'strategyTracking/businesstracking/regionalcenter',
                    icon: 'bank',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '348',
                parentId: '0',
                path: '/supplier',
                showInSide: 1,
                name: '国资委页面',
                component: 'PageView',
                icon: '',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '350',
                    parentId: '348',
                    path: '/supplier/authentication/QueryList',
                    showInSide: 1,
                    name: '供应商信用认证查询',
                    component: 'views/supplier/authentication/QueryList',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '351',
                    parentId: '348',
                    path: '/supplier/commodity/Commodity',
                    showInSide: 1,
                    name: '商品均价智能检索',
                    component: 'views/supplier/commodity/Commodity',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '435',
                parentId: '0',
                path: '/algorithmService',
                showInSide: 1,
                name: '算法服务',
                component: 'PageView',
                icon: '',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '436',
                    parentId: '435',
                    path: '/algorithmService/material/MaterialComparison',
                    showInSide: 1,
                    name: '物料对照',
                    component: 'views/algorithmService/material/MaterialComparison',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '2',
                parentId: '0',
                path: '/monitor',
                showInSide: 1,
                name: '系统监控',
                component: 'PageView',
                icon: 'dashboard',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '10',
                    parentId: '2',
                    path: '/test/TestCron',
                    showInSide: 1,
                    name: '系统日志',
                    component: 'views/test/TestCron',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '121',
                    parentId: '2',
                    path: '/monitor/httptrace',
                    showInSide: 0,
                    name: '请求追踪',
                    component: 'monitor/Httptrace',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '256',
                    parentId: '2',
                    path: '/dataAdministration/datalogging/DataLogging',
                    showInSide: 0,
                    name: '数据日志',
                    component: 'dataAdministration/datalogging/DataLogging',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '340',
                parentId: '0',
                path: '/obeicockpit',
                showInSide: 1,
                name: '欧贝平台管理驾驶舱',
                component: 'PageView',
                icon: '',
                meta: {
                  closeable: true,
                },
                keepAlive: '10',
                children: [
                  {
                    id: '341',
                    parentId: '340',
                    path: '/obeicockpit/obeitracking/ObeiOverview',
                    showInSide: 1,
                    name: '月度例会-总览',
                    component: 'views/obeicockpit/obeitracking/ObeiOverview',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '342',
                    parentId: '340',
                    path: '/obeicockpit/obeicockpit/ObeiDepartments',
                    showInSide: 1,
                    name: '月度例会-各部门',
                    component: 'views/obeicockpit/obeicockpit/ObeiDepartments',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '364',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/OverIndex',
                    showInSide: 1,
                    name: '总览',
                    component: 'views/obeicockpit/obeiplatform/OverIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '389',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/TescoIndex',
                    showInSide: 1,
                    name: '欧贝易购',
                    component: 'views/obeicockpit/obeiplatform/TescoIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '365',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/SalesIndex',
                    showInSide: 1,
                    name: '业务部门',
                    component: 'views/obeicockpit/obeiplatform/SalesIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '367',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/ShoppingMallIndex',
                    showInSide: 1,
                    name: '欧贝商城',
                    component: 'views/obeicockpit/obeiplatform/ShoppingMallIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '390',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/CloudLibraryIndex',
                    showInSide: 1,
                    name: '欧贝云库',
                    component: 'views/obeicockpit/obeiplatform/CloudLibraryIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '391',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/EasytoIndex',
                    showInSide: 1,
                    name: '欧贝易销',
                    component: 'views/obeicockpit/obeiplatform/EasytoIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '392',
                    parentId: '340',
                    path: '/obeicockpit/obeiplatform/ZeroCarbonIndex',
                    showInSide: 1,
                    name: '欧贝零碳',
                    component: 'views/obeicockpit/obeiplatform/ZeroCarbonIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '366',
                    parentId: '340',
                    path: 'obeicockpit/obeiplatform/BusinessIndex',
                    showInSide: 1,
                    name: '会员商务中心',
                    component: 'views/obeicockpit/obeiplatform/BusinessIndex',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '395',
                parentId: '0',
                path: '/priorityWork',
                showInSide: 1,
                name: '公司年度重点任务',
                component: 'PageView',
                icon: '',
                meta: {
                  closeable: true,
                },
                keepAlive: '00',
                children: [
                  {
                    id: '396',
                    parentId: '395',
                    path: 'priorityWork/homePagekanban/index',
                    showInSide: 1,
                    name: '首页看板',
                    component: 'views/priorityWork/homePagekanban/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                  {
                    id: '397',
                    parentId: '395',
                    path: 'priorityWork/jobFilling/index',
                    showInSide: 1,
                    name: '工作填报',
                    component: 'views/priorityWork/jobFilling/index',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '00',
                  },
                ],
              },
              {
                id: '355',
                parentId: '0',
                path: '/manualMaintenance',
                showInSide: 1,
                name: '数据维护',
                component: 'PageView',
                icon: '',
                meta: {
                  closeable: true,
                },
                keepAlive: '10',
                children: [
                  {
                    id: '401',
                    parentId: '355',
                    path: '/manualMaintenance/general/4ba0ee95f08641498e9da561fb564823',
                    showInSide: 1,
                    name: '吨钢成本目标维护',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '356',
                    parentId: '355',
                    path: '/manualMaintenance/baseProduction',
                    showInSide: 1,
                    name: '钢铁基地产量维护表',
                    component: 'views/manualMaintenance/baseProduction',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '357',
                    parentId: '355',
                    path: '/manualMaintenance/materialMaintenance',
                    showInSide: 1,
                    name: '指标家族物料维护表',
                    component: 'views/manualMaintenance/materialMaintenance',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '369',
                    parentId: '355',
                    path: '/manualMaintenance/general/99c5dc0a96d549509da1f33cc7c6b162',
                    showInSide: 1,
                    name: '资材品类中心原料维护表',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '373',
                    parentId: '355',
                    path: '/manualMaintenance/general/51833fc0d8d94e7c84ee161de32cfd4f',
                    showInSide: 1,
                    name: '资材品类中心物料维护表',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '428',
                    parentId: '355',
                    path: '/manualMaintenance/general/cadb59efc5c94b6ea17cedc98930a66b',
                    showInSide: 1,
                    name: '用户服务反馈问题跟踪清单',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '375',
                    parentId: '355',
                    path: '/manualMaintenance/general/529898ca1b924d1981431bec625f1b66',
                    showInSide: 1,
                    name: '设备品类中心重点项目跟踪',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '376',
                    parentId: '355',
                    path: '/manualMaintenance/general/094d7450d81643f9be66a94d1de8fcc4',
                    showInSide: 1,
                    name: '客户服务跟踪',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                  {
                    id: '374',
                    parentId: '355',
                    path: '/manualMaintenance/general/29a370eaa1764d28b996249fec90675d',
                    showInSide: 1,
                    name: '平台驾驶舱数据维护',
                    component: 'views/manualMaintenance/general',
                    icon: '',
                    meta: {
                      closeable: true,
                    },
                    keepAlive: '10',
                  },
                ],
              },
              {
                path: '/profile',
                showInSide: 1,
                name: '个人中心',
                component: 'personal/Profile',
                icon: 'none',
                meta: {
                  closeable: true,
                  isShow: false,
                },
              },
            ],
          },
        ];
        Storage.set(ACCESS_DICT_ALL_KEY, dictAll.data);

        const perms = Storage.get(ACCESS_PERMS_KEY, []);
        this.setPerms(perms);

        this.name = userInfo.username;
        this.avatar = userInfo.avatar;
        this.userInfo = userInfo;

        const menus: API.Menu[] = [];

        //region 分系统拆分菜单
        const tempRouters: any = vuerouters[0];
        if (env === 'smartdash' && vuerouters[0].children) {
          const temp = vuerouters[0].children.filter((x) => blackMenuPath.indexOf(x.path) < 0);
          tempRouters.children = temp;
        }
        //endregion
        const realRouters = tempRouters;

        for (let i = 0; i < realRouters.children.length; i++) {
          const tmpMenus = this.toFlatMenu(realRouters.children[i]);
          menus.push(...tmpMenus);
        }

        const generatorResult = await generatorDynamicRouter(menus);
        this.menus = generatorResult.menus.filter((item) => !item.meta?.hideInMenu);

        // !wsStore.client && wsStore.initSocket();

        return { menus, perms, userInfo };
      } catch (error) {
        return Promise.reject(error);
      }
    },
    /** 登出 */
    async logout() {
      const userInfo = Storage.get(ACCESS_USER_INFO_KEY, null);

      if (userInfo) {
        await logout(userInfo.userId);
      }

      // const wsStore = useWsStore();
      // wsStore.closeSocket();
      this.resetToken();
      resetRouter();
    },
    toFlatMenu(router: API.VueRouter): API.Menu[] {
      const menus: API.Menu[] = [];
      const menu: API.Menu = {
        createTime: new Date(new Date().getTime()),
        updateTime: new Date(new Date().getTime()),
        id: router.id,
        parentId: router.parentId,
        name: router.name,
        router: router.path,
        type: router.children?.length > 0 ? 0 : 1,
        icon: router.icon ? router.icon : 'right',
        orderNum: 1000,
        perms: '',
        keepAlive: router.keepAlive, //之前是固定值10，所以怎么配置也没用  api/account/model.d.ts新增两个参数-30。31行
        showInSide: router.showInSide, //之前是固定值1，所以怎么配置也没用
        viewPath: router.component,
      };

      menus.push(menu);

      if (router.children) {
        for (let i = 0; i < router.children.length; i++) {
          const childMenus = this.toFlatMenu(router.children[i]);
          menus.push(...childMenus);
        }
      }

      return menus;
    },
    setYearDate(yearDate) {
      this.yearDate = yearDate;
    },
  },
});

// 在组件setup函数外使用
export function useUserStoreWithOut() {
  return useUserStore(store);
}
