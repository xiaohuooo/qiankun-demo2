"use strict";
exports.__esModule = true;
exports.isDayAlias = exports.toDayAlias = exports.toDayNumber = exports.aliasToNumberMapping = void 0;
exports.aliasToNumberMapping = {
    MON: 1,
    TUE: 2,
    WED: 3,
    THU: 4,
    FRI: 5,
    SAT: 6,
    SUN: 7
};

function toDayNumber(alias) {
    var number = exports.aliasToNumberMapping[alias];
    if (number == undefined) {
        throw new Error("unhandled alias " + alias);
    }
    return number;
}
exports.toDayNumber = toDayNumber;

function toDayAlias(num) {
    var alias = Object.keys(exports.aliasToNumberMapping).find(function(k) { return exports.aliasToNumberMapping[k] === num; });
    if (alias == undefined) {
        throw new Error("unhandled number ".concat(num));
    }
    return alias;
}
exports.toDayAlias = toDayAlias;

function isDayAlias(s) {
    return Object.keys(exports.aliasToNumberMapping).includes(s);
}
exports.isDayAlias = isDayAlias;