import dict from '@/utils/dict';
export const columns = [
  {
    title: '字段中文名',
    align: 'center',
    width: 100,
    dataIndex: 'columnCname',
    editFormItemProps: {
      rules: [{ required: true, message: '请输入中文名' }],
    },
  },
  {
    title: '字段英文名',
    align: 'center',
    width: 150,
    dataIndex: 'columnName',
    editable: ({}) => {
      return false;
    },
  },
  {
    title: '字段类型',
    align: 'center',
    dataIndex: 'type',
    width: 130,

    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('fieldType'),
      },
    },
    /** 可编辑行表单配置 */
    editFormItemProps: {
      /** 继承 formItemProps 的属性配置, 默认就是是true，这里为了演示有这个字段开关，所以特别写上 */
      extendSearchFormProps: true,
    },
  },
  {
    title: '值集映射',
    width: 130,
    dataIndex: 'dictId',
    /** 搜索表单配置 */
    formItemProps: {
      component: 'Select',
    },
    /** 可编辑行表单配置 */
    editFormItemProps: {
      /** 继承 formItemProps 的属性配置, 默认就是是true，这里为了演示有这个字段开关，所以特别写上 */
      extendSearchFormProps: true,
    },
  },
  {
    title: '排序',
    width: 70,
    dataIndex: 'sort',
  },
  {
    title: '是否新增字段',
    width: 100,
    dataIndex: 'isAdd',
    customRender: ({ record }) => (record.isAdd ? '是' : '否'),
    editable: ({}) => {
      return false;
    },
  },
  {
    title: '是否主键',
    width: 90,
    dataIndex: 'isPrikey',
    formItemProps: {
      component: 'Checkbox',
    },
    customRender: ({ record }) => (record.isPrikey == '1' ? '是' : '否'),
  },
];
