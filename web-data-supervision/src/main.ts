// with polyfills
import 'core-js/stable';
import 'regenerator-runtime/runtime';

import { createApp } from 'vue';
import * as antIcons from '@ant-design/icons-vue';
import * as echarts from 'echarts';
import VueGridLayout from 'vue-grid-layout';
import VXETable from 'vxe-table';
import App from './App.vue';
import { setupRouter } from './router';
import dict from '@/utils/dict';
import { setupStore } from '@/store';
import { setupI18n } from '@/locales';
import { setupAntd, setupAssets, setupGlobalMethods, setupCustomComponents } from '@/plugins';
import '@/assets/less/Style.less';
// echarts 主题
import 'echarts/theme/macarons.js';
import 'echarts/theme/vintage.js';
import 'echarts/theme/infographic.js';
import 'echarts/theme/dark.js';
import '@/plugins/chartsTheme/index';
import 'vxe-table/lib/style.css';
import  "@/utils/microApp/index"
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('./mock');
//   mockXHR();
// }

const app = createApp(App);

const icons: any = antIcons;
// 注册组件
Object.keys(icons).forEach((key) => {
  app.component(key, icons[key]);
});

// 添加到全局
app.config.globalProperties.$antIcons = antIcons;
app.config.globalProperties.$dict = dict;
app.config.globalProperties.$echarts = echarts;
app.use(VueGridLayout);
app.use(VXETable);

function setupPlugins() {
  // 注册全局常用的ant-design-vue组件
  setupAntd(app);
  // 引入静态资源
  setupAssets();
  // 注册全局自定义组件,如：<svg-icon />
  setupCustomComponents(app);
  // 注册全局方法，如：app.config.globalProperties.$message = message
  setupGlobalMethods(app);
}

async function setupApp() {
  // 挂载vuex状态管理
  setupStore(app);
  // Multilingual configuration
  // Asynchronous case: language files may be obtained from the server side
  await setupI18n(app);
  // 挂载路由
  await setupRouter(app);

  app.mount('#app');
}

setupPlugins();

setupApp();
