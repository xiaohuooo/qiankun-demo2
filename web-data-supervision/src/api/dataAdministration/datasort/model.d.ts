declare namespace API {
  /** 创建数据分类参数 */
  type CreateDataSortParams = {
    categoryName: string;
    parentId: number;
    orderNum: number;
  };
}
