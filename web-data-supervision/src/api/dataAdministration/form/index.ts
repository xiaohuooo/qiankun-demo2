import { downloadJson, request } from '@/utils/request';

export function getTableConfig(data) {
  return request(
    {
      url: `p/tableConfig/dynamics/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function saveTableData(id, data) {
  return request(
    {
      url: `p/tableConfig/syncData/${id}`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function delTableData(id, data) {
  return request(
    {
      url: `p/tableConfig/deleteData/${id}`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function exportTableData(data, fileName) {
  downloadJson('p/tableConfig/export/data', data, fileName);
}
