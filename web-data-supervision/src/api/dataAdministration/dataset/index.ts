import { request } from '@/utils/request';

export function getDatasetPreview(data) {
  return request(
    {
      url: `p/dataset/preview`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getDSAll() {
  const data = {
    pageSize: 999,
    pageNum: 1,
  };

  return request(
    {
      url: `p/query/source`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getDatasetList(data: API.PageRequest) {
  return request<API.TableListResult<API.DatasetListPageResult>>(
    {
      url: `p/query/dataSet`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function getDatasetCategoryTree(data) {
  return request(
    {
      url: `p/query/DataCategory/tree`,
      method: 'post',
      data,
    },
    {
      // isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getDatasetRawInfo(tableName) {
  return request(
    {
      url: `p/query/dataSetTable?tableName=${tableName}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getDatasetItemList(id, columnCname) {
  return request(
    {
      url: `p/query/dataSetItem?id=${id}&columnCname=${columnCname}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function deleteDataset(idList) {
  return request(
    {
      url: 'p/delete/dataSet',
      method: 'post',
      data: idList,
    },
    {
      successMsg: '删除数据集成功',
      isJsonBody: true,
    },
  );
}

export function createDataset(data) {
  return request(
    {
      url: 'p/add/dataSet',
      method: 'post',
      data,
    },
    {
      // successMsg: '创建成功',
      errorMsg: '服务端异常',
      isJsonBody: true,
    },
  );
}

export function updateDataset(data) {
  return request(
    {
      url: 'p/update/dataSet',
      method: 'post',
      data,
    },
    {
      // successMsg: '更新成功',
      errorMsg: '服务端异常',
      isJsonBody: true,
    },
  );
}

export function checkDatasetNames(data) {
  return request(
    {
      url: 'p/check/dataDict',
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function getDataSetTable(data) {
  return request(
    {
      url: 'p/query/dataSetTable',
      method: 'post',
      data,
    },
    {
      isJsonBody: false,
    },
  );
}
/**
 * 查询数据集在监督模型中使用情况
 */
export function datatableAtModelCircumstance(data) {
  return request(
    {
      url: 'n/model/datatableAtModelCircumstance',
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
/**
 * 查询数据集在自主分析图表中使用情况
 */
export function datatableAtChartCircumstance(data) {
  return request(
    {
      url: 'n/model/datatableAtChartCircumstance',
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function getTablecolumns(data) {
  return request(
    {
      url: 'p/query/data/table/columns',
      method: 'post',
      data,
    },
    {
      isJsonBody: false,
    },
  );
}
