declare namespace API {
  type UserListPageResultItem = {
    createTime: string;
    deptId: number;
    email: string;
    avatar: string;
    id: number;
    description: string;
    userId: number;
    username: string;
    employeeCode: string;
    mobile: string;
    status: number;
    deptName: string;
    roleName: string;
  };

  /** 获取用户列表结果 */
  type UserListPageResult = UserListPageResultItem[];

  /** 管理员用户详情 */
  type AdminUserInfo = {
    createTime: string;
    updateTime: string;
    id: number;
    departmentId: number;
    name: string;
    username: string;
    password: string;
    psalt: string;
    nickName: string;
    headImg: string;
    email: string;
    mobile: string;
    remark: string;
    status: number;
    roles: string[];
    departmentName: string;
  };

  //  ------------zhujun 分隔线---------------------
  type UserDetail = {
    userId: number;
    username: string;
    employeeCode: string;
    password: string;
    deptId: number;
    deptName: string;
    email: string;
    mobile: string;
    status: number;
    createTime: Date;
    modifyTime: Date;
    lastLoginTime: string;
    ssex: string;
    description: string;
    avatar: string;
    roleId: string;
    roleName: string;
  };

  /** 创建用户参数 */
  type CreateUserParams = {
    deptId: number;
    username: string;
    employeeCode: string;
    roles: number[];
    email: string;
    mobile: string;
    status: number;
    ssex: number;
    userLineDefense: number;
  };

  /** 创建用户参数 */
  type UpdateUserParams = {
    deptId: number;
    username: string;
    roles: number[];
    email: string;
    mobile: string;
    status: number;
    ssex: number;
  };

  /** 更新管理员用户参数 */
  type UpdateAdminInfoParams = {
    deptId: number;
    username: string;
    roles: number[];
    email: string;
    mobile: string;
    remark: string;
    status: number;
    userId: number;
    ssex: number;
    id: number;
  };

  /** 更新管理员密码 */
  type UpdateAdminUserPassword = {
    username: string;
    password: string;
  };
}
