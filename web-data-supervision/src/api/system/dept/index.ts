import { omit } from 'lodash-es';
import { request, downloadJson } from '@/utils/request';

// ---------------  added by zhujun
// export function getDeptAll() {
//   return request(
//     {
//       url: 'dept',
//       method: 'get',
//     },
//     {},
//   );
// }
export function getDeptAll(data: API.PageRequest) {
  return request<API.TableListResult>(
    {
      url: `dept`,
      method: 'get',
      data,
    },
    {},
  );
}

export function deleteDept(deptIds: number[]) {
  const ids = deptIds.join(',');
  return request(
    {
      url: `dept/${ids}`,
      method: 'delete',
    },
    {
      successMsg: '删除成功',
    },
  );
}
export function updateDept(data) {
  const rdata = omit(data, ['title']) as any;
  rdata.deptName = data.title;
  console.log('deptName', data, rdata);

  return request(
    {
      url: 'dept',
      method: 'put',
      data: rdata,
    },
    {},
  );
}

/**
 * @description 创建部门
 * @param {API.CreateDeptParams} data 参数
 * @returns
 */
export function createDept(data: API.CreateDeptParams) {
  const rdata = omit(data, ['title']) as any;
  rdata.deptName = data.title;
  console.log('deptName', data, rdata);

  return request(
    {
      url: 'dept',
      method: 'post',
      data: rdata,
    },
    {
      successMsg: '新增部门成功',
      errorMsg: '新增部门异常',
    },
  );
}
//数据表配置-查询配置数据
export function queryConfigList(data) {
  return request(
    {
      url: 'p/datatable/authorized/list',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

//数据表配置-保存配置数据
export function saveConfigList(data) {
  return request(
    {
      url: 'p/datatable/authorized/save',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function downloadUserList(params, filename) {
  const url = `user/excel`;
  downloadJson(url, params, filename);
}
