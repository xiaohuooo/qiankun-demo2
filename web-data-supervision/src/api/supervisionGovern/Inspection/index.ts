import { request } from '@/utils/request';
//巡察单位-树形
export function partyOrganizations(data: API.PageRequest) {
  return request(
    {
      url: `/p/query/partyOrganizations`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getProjectList(data: API.PageRequest) {
  return request(
    {
      url: `/p/query/inspectionProjectList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getInventoryList(data: API.PageRequest) {
  return request(
    {
      url: `/p/query/inspectionProblemDetail`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function inspectionProjects(data: API.PageRequest) {
  return request(
    {
      url: `/p/query/inspectionProjects`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**问题保存 */
export function problemAndSubkey(data: API.PageRequest) {
  return request(
    {
      url: `/p/insert/problemAndSubkey`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**问题删除  */
export function getInventoryDelete(data: API.PageRequest) {
  return request(
    {
      url: `/p/remove/problemAndSubkey`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**编辑问题  */
export function getInventoryEdit(data: API.PageRequest) {
  return request(
    {
      url: `/p/update/problemAndSubkey`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**详情  */
export function inspectionProblemSubkey(data) {
  return request(
    {
      url: `/p/query/inspectionProblemSubkey?problemDetailCode=${data}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**推送部门人员下拉  */
export function problemLiabilityPerson() {
  return request(
    {
      url: `/p/query/problemLiabilityPerson`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 个人待办
export function inspectionToDo(data) {
  return request(
    {
      url: `/p/clue/query/inspectionToDo`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//待办历史
export function inspectionRecording(data) {
  return request(
    {
      url: `/p/clue/query/inspectionRecording`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//推送
export function pushClue(data) {
  return request(
    {
      url: `/p/problem/inspection/push/clue`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function handClue(data) {
  return request(
    {
      url: `/p/problem/inspection/hand/clue`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
