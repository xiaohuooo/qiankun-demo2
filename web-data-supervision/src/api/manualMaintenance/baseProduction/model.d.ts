declare namespace API {
  /** 基地产量列表项 */
  type BaseProductionListResultItem = {
    ym: string;
    base: string;
    acctCode: string;
    unitType: string;
    unitName: string;
    isExistsUnitName: string;
    output: number;
    createDate: Date;
    updateUserName: string;
    id: number;
  };

  /** 基地产量列表 */
  type BaseProductionListResult = BaseProductionListResultItem[];

  /** 新增基地产量参数 */
  type CreateBaseProductionParams = {
    id: number;
    output: number;
  };
}
