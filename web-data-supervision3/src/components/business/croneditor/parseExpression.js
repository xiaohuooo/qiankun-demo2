'use strict';
/**
 * parseExpression
 * Parses given expression and picks a matching tab for it.
 * It would be best if it was not dependent on cron syntax or too many configurable options.
 */
exports.__esModule = true;
exports.parseExpression = void 0;
const dayAliases_1 = require('./dayAliases');

function parseSubExpr(expr) {
  expr = expr.trim();
  let match;
  if ((match = expr.match(/\*\/(\d+)/)) != null) {
    return {
      type: 'cronNumber',
      at: { type: 'asterisk' },
      every: { type: 'number', value: parseInt(match[1]) },
    };
  }
  if ((match = expr.match(/(\d+)\/(\d+)/)) != null) {
    return {
      type: 'cronNumber',
      at: { type: 'number', value: parseInt(match[1]) },
      every: { type: 'number', value: parseInt(match[2]) },
    };
  }
  if ((match = expr.match(/(\d+)/)) != null) {
    return {
      type: 'number',
      value: parseInt(match[1]),
    };
  }
  if (expr == 'L') {
    return {
      type: 'last',
      value: 'L',
    };
  }
  if (expr == '?') {
    return { type: 'question' };
  }
  if (expr == '*') {
    return { type: 'asterisk' };
  }
  throw new Error('Unhandled subexpression: '.concat(expr));
}

function parseDayOfMonth(expr) {
  expr = expr.trim();
  if (expr == '*')
    return {
      type: 'asterisk',
    };
  if (expr == 'L')
    return {
      type: 'last',
    };
  if (expr == '?')
    return {
      type: 'question',
    };

  let match;
  if ((match = expr.match(/\*\/(\d+)/)) != null) {
    return {
      type: 'cronNumber',
      at: { type: 'asterisk' },
      every: { type: 'number', value: parseInt(match[1]) },
    };
  }
  if ((match = expr.match(/(\d+)\/(\d+)/)) != null) {
    return {
      type: 'cronNumber',
      at: { type: 'number', value: parseInt(match[1]) },
      every: { type: 'number', value: parseInt(match[2]) },
    };
  }
  // if ((match = expr.match(/(\d+)/)) != null) {
  //     return {
  //         type: "number",
  //         value: parseInt(match[1])
  //     };
  // }

  const groups = expr.split(','); // 添加正则校验

  if (groups == null) throw new Error('invalid days expression: '.concat(expr));
  return {
    type: 'setOfDays',
    days: groups,
  };
}

function parseDayOfWeek(expr) {
  expr = expr.trim();
  if (expr == '*')
    return {
      type: 'asterisk',
    };
  if (expr == '?')
    return {
      type: 'question',
    };
  const groups = expr.match(
    /([a-zA-Z0-9]+)(,[a-zA-Z0-9]+)?(,[a-zA-Z0-9]+)?(,[a-zA-Z0-9]+)?(,[a-zA-Z0-9]+)?(,[a-zA-Z0-9]+)?(,[a-zA-Z0-9]+)?/,
  );
  if (groups == null) throw new Error('invalid days expression: '.concat(expr));
  return {
    type: 'setOfDays',
    days: groups
      .slice(1)
      .map(function (d) {
        return d && d.replace(/,/, '');
      })
      .filter(function (d) {
        return d;
      })
      .map(function (d) {
        return !(0, dayAliases_1.isDayAlias)(d) ? (0, dayAliases_1.toDayAlias)(parseInt(d)) : d;
      }),
  };
}
const isAny = function (token) {
  return token.type == 'question' || token.type == 'asterisk';
};
const isAnyTime = function (token) {
  return token.type == 'asterisk' || (token.type == 'number' && token.value == 0);
};
const parseExpression = function (expression) {
  const advanced = {
    type: 'advanced',
    cronExpression: expression,
  };
  const groups = expression.split(' ');
  // if (groups.length != 5 && groups.length != 6) {
  //     return advanced;
  // }
  if (groups.length < 5) {
    return advanced;
  }

  if (groups.length > 6) {
    groups.splice(6, groups.length);
  }

  const cron =
    groups.length == 6
      ? {
          seconds: parseSubExpr(groups[0]),
          minutes: parseSubExpr(groups[1]),
          hours: parseSubExpr(groups[2]),
          dayOfTheMonth: parseDayOfMonth(groups[3]),
          month: parseSubExpr(groups[4]),
          dayOfWeek: parseDayOfWeek(groups[5]),
        }
      : {
          minutes: parseSubExpr(groups[0]),
          hours: parseSubExpr(groups[1]),
          dayOfTheMonth: parseDayOfMonth(groups[2]),
          month: parseSubExpr(groups[3]),
          dayOfWeek: parseDayOfWeek(groups[4]),
        };

  if (
    cron.minutes.type == 'cronNumber' &&
    isAnyTime(cron.minutes.at) &&
    cron.hours.type == 'asterisk' &&
    cron.dayOfTheMonth.type == 'asterisk' &&
    cron.month.type == 'asterisk' &&
    isAny(cron.dayOfWeek)
  ) {
    return {
      type: 'minutes',
      minuteInterval: cron.minutes.every.value,
    };
  }
  if (
    cron.minutes.type == 'number' &&
    cron.hours.type == 'cronNumber' &&
    isAnyTime(cron.hours.at) &&
    cron.dayOfTheMonth.type == 'asterisk' &&
    cron.month.type == 'asterisk' &&
    isAny(cron.dayOfWeek)
  ) {
    return {
      type: 'hourly',
      minutes: cron.minutes.value,
      hourInterval: cron.hours.every.value,
    };
  }
  if (
    cron.minutes.type == 'number' &&
    cron.hours.type == 'number' &&
    (cron.dayOfTheMonth.type == 'asterisk' ||
      cron.dayOfTheMonth.type == 'question' ||
      (cron.dayOfTheMonth.type == 'cronNumber' && cron.dayOfTheMonth.at.type == 'asterisk')) &&
    cron.month.type == 'asterisk' &&
    isAny(cron.dayOfWeek)
  ) {
    return {
      type: 'daily',
      minutes: cron.minutes.value,
      hours: cron.hours.value,
      dayInterval: cron.dayOfTheMonth.type == 'cronNumber' ? cron.dayOfTheMonth.every.value : 1,
    };
  }
  if (
    cron.minutes.type == 'number' &&
    cron.hours.type == 'number' &&
    isAny(cron.dayOfTheMonth) &&
    cron.month.type == 'asterisk' &&
    cron.dayOfWeek.type == 'setOfDays'
  ) {
    return {
      type: 'weekly',
      minutes: cron.minutes.value,
      hours: cron.hours.value,
      days: cron.dayOfWeek.days,
    };
  }

  if (
    cron.minutes.type == 'number' &&
    cron.hours.type == 'number' &&
    // cron.dayOfTheMonth.type == "number" &&
    (cron.dayOfTheMonth.type == 'number' ||
      cron.dayOfTheMonth.type == 'setOfDays' ||
      cron.dayOfTheMonth.type == 'last') &&
    (cron.month.type =
      'asterisk' || (cron.month.type == 'cronNumber' && cron.month.at.type == 'asterisk')) &&
    isAny(cron.dayOfWeek)
  ) {
    let lastDay = false;
    let selectDays = [];
    if (cron.dayOfTheMonth.type == 'last') {
      lastDay = true;
      selectDays = [];
    } else if (cron.dayOfTheMonth.type == 'setOfDays') {
      lastDay = false;
      selectDays = cron.dayOfTheMonth.days;
    } else if (cron.dayOfTheMonth.type == 'number') {
      lastDay = false;
      selectDays = [cron.dayOfTheMonth.value];
    }
    // console.log(cron, 'cron');

    return {
      type: 'monthly',
      minutes: cron.minutes.value,
      hours: cron.hours.value,
      day: cron.dayOfTheMonth.value,
      lastDay,
      days: selectDays,
      dgad: '',
      monthInterval: cron.month.every.value,
    };
  }
  return advanced;
};
exports.parseExpression = parseExpression;
