import { request, downloadJson } from '@/utils/request';

export function getProjectList(data: API.PageRequest) {
  return request(
    {
      url: `/p/supervise/project/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getProjectSave(data: API.PageRequest) {
  return request(
    {
      url: `p/supervise/project/save`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getProjectEdit(data: API.PageRequest) {
  return request(
    {
      url: `/p/supervise/project/edit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getProjectDelete(data: API.PageRequest) {
  return request(
    {
      url: `p/supervise/project/delete`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//校验项目名称是否重名
export function getProjectCheck(data) {
  return request(
    {
      url: `p/supervise/project/check`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**问题清单相关接口start */
export function getInventorySave(data) {
  return request(
    {
      url: `p/problem/inventory/save`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getInventoryList(data) {
  return request(
    {
      url: `p/problem/inventory/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getInventoryEdit(data) {
  return request(
    {
      url: `p/problem/inventory/edit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getInventoryDelete(data) {
  return request(
    {
      url: `p/problem/inventory/delete`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getInventoryPush(data) {
  return request(
    {
      url: `p/problem/inventory/push/clue`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getQueryList(data) {
  return request(
    {
      url: `p/clue/person/dealWith/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getQueryHistoryList(data) {
  return request(
    {
      url: `p/clue/dealWith/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/** 项目详情*/
export function getProjectByCode(data) {
  return request(
    {
      url: `p/supervise/project/byCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/** 问题详情*/
export function getProblemByCode(data) {
  return request(
    {
      url: `p/problem/inventory/byCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/** 整改信息*/
export function getClueInventory(data) {
  return request(
    {
      url: `p/clue/inventory/byCode`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/** 整改日志*/
export function getLog(data) {
  return request(
    {
      url: `p/log/dealWith/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**审核- 验收保存，转交*/
export function getHandClue(data) {
  return request(
    {
      url: `p/problem/inventory/handClue`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 项目导出
export function getProjectExport(params, filename) {
  const url = `p/supervise/project/export`;
  downloadJson(url, params, filename);
}
// 问题导出
export function getInventoryExport(params, filename) {
  const url = `p/problem/inventory/export`;
  downloadJson(url, params, filename);
}
// 问题待办导出
export function getPersonDealWithExport(params, filename) {
  const url = `p/clue/person/dealWith/export`;
  downloadJson(url, params, filename);
}
// 问题待办历史
export function getClueDealWithExport(params, filename) {
  const url = `p/clue/dealWith/export`;
  downloadJson(url, params, filename);
}
