import { request, downloadJson } from '@/utils/request';
//个人待办列表
export function getTodoList(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//模型组个人待办列表
export function getModelGroupTodoList(data: API.PageRequest) {
  return request(
    {
      url: `p/process/modelGroup/todo/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 整改日志
export function getLogList(data: API.PageRequest) {
  return request(
    {
      url: `p/rectification/log/query`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 提交整改
export function saveLog(data: API.PageRequest) {
  return request(
    {
      url: `p/rectification/log/save`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 核查处理整改
export function saveInspectRectify(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/handle/inspectRectify`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 审批整改保存
export function saveLogAudit(data: API.PageRequest) {
  return request(
    {
      url: `p/rectification/log/audit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

// 核查处理整改
export function getDetailById(data: API.PageRequest) {
  return request(
    {
      url: `p/query/disciplinary/datamodel/detail/by/id`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//下载文件
export function downLoadFile(params, filename) {
  const url = `p/clue/file/downLoad`;
  downloadJson(url, params, filename);
}
//查询待办人员列表选择
export function getUsers(data) {
  return request(
    {
      url: `p/process/todo/handle/transfer`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}
export function getUsersSecond(data) {
  return request(
    {
      url: `p/process/todo/push/person`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}
//查询部门
export function getDep() {
  return request(
    {
      url: `/dept`,
      method: 'get',
    },
    {
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}

export function getdetail(data: API.PageRequest) {
  return request(
    {
      url: `p/query/disciplinary/datamodel/detail/by/id`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//业务自查提交数据
export function handleBusiness(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/handle/business`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//纪检核查提交数据
export function handleInspections(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/handle/inspections`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function handleInspectionsSecond(data: API.PageRequest) {
  return request(
    {
      url: `p/first/clue/hand`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getTodoHistory(data: API.PageRequest) {
  return request(
    {
      url: `p/key/information/todo/history`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: false,
    },
  );
}

export function getDealWithLog(data: API.PageRequest) {
  return request(
    {
      url: `p/clue/getDealWithLog`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
      isGetDataDirectly: false,
    },
  );
}
// 管理员初审暂存
export function handleModelManager(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/handle/modelManager`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 审计检查
export function handleAudit(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/handle/audit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 获取模型信息
export function getDatamodel(data: API.PageRequest) {
  return request(
    {
      url: `p/query/datamodel/noRole`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
export function getUnreadPrompt(data: any) {
  return request(
    {
      url: `p/clue/getUnreadPrompt`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
export function updateNotifyStatus(data: any) {
  return request(
    {
      url: `p/clue/updateNotifyStatus`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

//个人疑点业务列表
export function getHistory(data: API.PageRequest) {
  return request(
    {
      url: `p/clue/getHistory`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//个人疑点业务用户数据
export function allRoleUsers() {
  return request(
    {
      url: `role/list/allRoleUsers`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 列表点击转交保存
export function transfer(data) {
  return request(
    {
      url: `p/process/todo/transfer`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

//批量处理
export function batch(data) {
  return request(
    {
      url: `p/process/todo/batch`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//批量处理
export function batchDetail(data) {
  return request(
    {
      url: `p/process/todo/batchDetail`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//第3道防线处理提交-监督联络员审核-云涛
export function superviseAudit(data) {
  return request(
    {
      url: `/p/process/todo/superviseAudit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

//第3道防线是否显示驳回按钮-云涛
export function isTransmittedBusiness(data) {
  return request(
    {
      url: `/p/process/todo/isTransmittedBusiness?sid=${data}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//第3道防线是否显示驳回监督联络员:李东接口-云涛
export function isOverrule(data) {
  return request(
    {
      url: `/p/process/todo/isOverrule?sid=${data}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
// 处理待办详情查询模型组基本信息
export function getModelGroupInfo(data) {
  return request(
    {
      url: `/p/modelGroup/queryById`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
// 处理待办详情查询模型组关键信息
export function modelGroupDetail(data) {
  return request(
    {
      url: `/p/query/modelGroup/modelDetail`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 批量整改提交资料
export function batchSave(data) {
  return request(
    {
      url: `p/rectification/log/batchSave`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 批量整改审核
export function batchAudit(data) {
  return request(
    {
      url: `/p/rectification/log/batchAudit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 批量整改中最新日志获取
export function queryNew(data) {
  return request(
    {
      url: `p/rectification/log/queryNew`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
