declare namespace API {
  /** 角色列表项 */
  type RoleListResultItem = {
    createTime: Date;
    modifyTime: Date;
    roleId: number;
    roleName: string;
    remark: string;
  };

  /** 角色列表 */
  type RoleListResult = RoleListResultItem[];

  // --------  add by zhujun
  /** 新增角色 */
  type CreateRoleParams = {
    name: string;
    label: string;
    remark: string;
    menus: any;
  };
}
