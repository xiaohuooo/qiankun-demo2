import { request } from '@/utils/request';
/**
 * 查询模型组列表
 */
export function getModelGroupList(data: API.PageRequest) {
  return request(
    {
      url: `p/query/modelGroup/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**
 * 根据模型ID查询模型数据明细
 */
export function getModelGroupInfo(data: API.PageRequest) {
  return request(
    {
      url: `p/query/modelGroup/detail/by/id`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 获取模型列表
export function getUsableDataModel(data) {
  return request(
    {
      url: `p/list/usable/datamodel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 保存模型组
export function saveModelGroup(data) {
  return request(
    {
      url: `p/add/modelGroup`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 编辑模型组
export function updateModelGroup(data) {
  return request(
    {
      url: `p/update/modelGroup`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
/**
 * 删除模型组
 */
export function deleteModelGroup(data: API.PageRequest) {
  return request(
    {
      url: `/p/delete/modelGroup`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
/**
 * 运行模型组
 */
export function runModelGroup(data: API.PageRequest) {
  return request(
    {
      url: `p/run/modelGroup`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}

// 校验模型组名称是否重复
export function checkModelGroupName(data) {
  return request(
    {
      url: `p/check/modelGroupName`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
    },
  );
}
// 模型推送规则设置保存
export function automaticPush(data) {
  return request(
    {
      url: `p/model/automaticPush/insert`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function automaticPushQuery(data) {
  return request(
    {
      url: `p/model/automaticPush/query`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}
