import { request } from '@/utils/request';

export function getDataLoggingList(data: API.PageRequest) {
  return request<API.TableListResult<API.DSListPageResult>>(
    {
      url: `log`,
      method: 'get',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
