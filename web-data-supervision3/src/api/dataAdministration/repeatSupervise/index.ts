import { request } from '@/utils/request';
// 单位
export function getUnitTable(data) {
  return request(
    {
      url: `p/query/supervisionAndSupervisionUnit`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getDeptTable(data) {
  return request(
    {
      url: `p/query/supervisionAndSupervisionDept`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getPersonTable(data) {
  return request(
    {
      url: `p/query/supervisionAndSupervisionPerson`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getSupervisionModelTable(data) {
  return request(
    {
      url: `p/query/supervisionAndSupervisionModel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
