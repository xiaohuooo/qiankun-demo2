import { request } from '@/utils/request';

export function getDataSortAll(data) {
  return request(
    {
      url: 'p/query/DataCategory/tree',
      method: 'post',
      data: {
        pageSize: 9999,
        pageNum: 1,
        type: data.type,
      },
    },
    {
      isJsonBody: true,
    },
  );
}

export function deleteDataSort(ids: number[]) {
  return request(
    {
      url: `p/delete/DataCategory`,
      method: 'post',
      data: ids,
    },
    {
      isJsonBody: true,
    },
  );
}

export function updateDataSort(data) {
  return request(
    {
      url: 'p/update/DataCategory',
      method: 'post',
      data,
    },
    {
      successMsg: '编辑分类成功',
      errorMsg: '编辑分类异常',
      isJsonBody: true,
    },
  );
}

export function createDataSort(data: API.CreateDataSortParams) {
  return request(
    {
      url: 'p/add/DataCategory',
      method: 'post',
      data,
    },
    {
      successMsg: '新增分类成功',
      errorMsg: '新增分类异常',
      isJsonBody: true,
    },
  );
}
