import { message as $message } from 'ant-design-vue';
import { request } from '@/utils/request';
// 查询
export function getWidgetList(data: API.PageRequest) {
  return request<API.TableListResult<API.DSListPageResult>>(
    {
      url: `p/widget/getWidgetList`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 保存
export function saveWidget(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/widget/saveWidget`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 编辑
export function updateWidge(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/widget/updateWidget`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 查询所有数据集
export function dataSetAll() {
  return request(
    {
      url: `p/query/dataSet/all`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 查询所有用户
export function allRoleUsers() {
  return request(
    {
      url: `role/list/allRoleUsers`,
      method: 'get',
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 删除
export function deleteWidget(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/widget/deleteWidget`,
      method: 'post',
      data,
    },
    {
      successMsg: '数据删除成功',
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 改变上下线状态
export function updateWidgetStatus(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/widget/updateWidgetStatus`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 拷贝数据
export function copyWidget(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/widget/copyWidget`,
      method: 'get',
      data,
    },
    {
      successMsg: '拷贝成功',
      errorMsg: (res) => {
        return res.message;
      },
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 下载数据
export function downloadItem(data: API.saveWidgetParmas) {
  return request(
    {
      url: `p/getDatasetQuerySql/by/widgetId`,
      method: 'post',
      data,
    },
    {
      // successMsg: '下载成功',
      errorMsg: (res) => {
        return res.message;
      },
      isGetDataDirectly: true,
    },
  ).then((res) => db2(res, data));
}
// 下载数据二道接口
const db2 = (res, item) => {
  if (res.code == 'OK') {
    const data = {
      fileName: item.title ? item.title : item.widgetName,
      querySql: res.data,
      widgetId: item.widgetId,
      dataSourceType: item.dataSourceType,
    };
    return request(
      {
        url: `p/file/upload/hdfs/by/db2`,
        method: 'post',
        data,
      },
      {
        // successMsg: '异步下载操作成功，请前往数据下载中心查看并下载数据',
        // errorMsg: (r) => {
        //   return r.message;
        // },
        isJsonBody: true,
        isGetDataDirectly: true,
      },
    )
      .then((r) => {
        if (r.success) {
          $message.info('异步下载操作成功，请前往数据下载中心查看并下载数据');
        } else {
          $message.error(r.message);
        }
      })
      .catch(() => {
        $message.error('下载失败');
      });
  } else {
    $message.error(res['message']);
  }
};
