import { request } from '@/utils/request';
//https://apiprotest.obei.com.cn/obei-gateway/obei-data-hub/

const IS_LOCAL = location.origin.endsWith('localhost:8098');
/** 真实请求的路径前缀 */
const baseApiUrl = IS_LOCAL
  ? `${location.origin}/proxy/obei-data-hub/`
  : process.env.VUE_APP_BASE_GZW_API;

export function getCertificate(data: API.PageRequest) {
  return request(
    {
      url: `${baseApiUrl}n/search/supplier/certificate`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isFullPath: true,
      isJsonBody: true,
    },
  );
}

export function getAverageprice(data: API.PageRequest) {
  return request(
    {
      url: `${baseApiUrl}n/search/product/average/price`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isFullPath: true,
      isJsonBody: true,
    },
  );
}
export function getGoods(data: API.PageRequest) {
  return request(
    {
      url: `${baseApiUrl}n/search/goods/supplier`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isFullPath: true,
    },
  );
}
