/** 用户token */
export const ACCESS_TOKEN_KEY = 'ACCESS_TOKEN';

export const ACCESS_USER_INFO_KEY = 'ACCESS_USER_INFO_KEY';

export const ACCESS_USERNAME_KEY = 'ACCESS_USER_NAME_KEY';

export const ACCESS_PERMS_KEY = 'ACCESS_PERMS_KEY';

export const ACCESS_DICT_ALL_KEY = 'ACCESS_DICT_ALL_KEY';

/** 国际化 */
export const LOCALE_KEY = 'LOCALE__';

/** 主题色 */
export const THEME_KEY = 'THEME__';

/** 用户信息 */
export const USER_INFO_KEY = 'USER__INFO__';

// role info key
export const ROLES_KEY = 'ROLES__KEY__';
/** 是否锁屏 */
export const IS_LOCKSCREEN = 'IS_LOCKSCREEN';
/** 标签页 */
export const TABS_ROUTES = 'TABS_ROUTES';
