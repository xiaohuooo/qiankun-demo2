/**
 * dataAdministration module
 */
export default {
  'views/dataAdministration/management/Management': () =>
    import('@/views/dataAdministration/management/Management.vue'),
  'views/dataAdministration/setmanagement/SetManagementNew': () =>
    import('@/views/dataAdministration/setmanagement/SetManagementNew.vue'),
  'views/dataAdministration/sortmanagement/SortManagement': () =>
    import('@/views/dataAdministration/sortmanagement/SortManagement.vue'),
  'views/dataAdministration/downloadcenter/DownloadCenter': () =>
    import('@/views/dataAdministration/downloadcenter/DownloadCenter.vue'),
  'views/dataAdministration/datalogging/DataLogging': () =>
    import('@/views/dataAdministration/datalogging/DataLogging.vue'),
  'views/dataAdministration/form/fill': () => import('@/views/dataAdministration/form/Fill.vue'),
} as const;
