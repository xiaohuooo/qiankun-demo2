/**
 * test module
 */
export default {
  'views/obeicockpit/obeitracking/ObeiOverview': () =>
    import('@/views/obeicockpit/obeitracking/ObeiOverview.vue'),
  'views/obeicockpit/obeicockpit/ObeiDepartments': () =>
    import('@/views/obeicockpit/obeitracking/ObeiDepartments.vue'),
  'views/obeicockpit/obeiplatform/OverIndex': () =>
    import('@/views/obeicockpit/obeiplatform/OverIndex.vue'),
  'views/obeicockpit/obeiplatform/DeviceIndex': () =>
    import('@/views/obeicockpit/obeiplatform/DeviceIndex.vue'),
  'views/obeicockpit/obeiplatform/ShoppingMallIndex': () =>
    import('@/views/obeicockpit/obeiplatform/ShoppingMallIndex.vue'),
  'views/obeicockpit/obeiplatform/SalesIndex': () =>
    import('@/views/obeicockpit/obeiplatform/SalesIndex.vue'),
  'views/obeicockpit/obeiplatform/BusinessIndex': () =>
    import('@/views/obeicockpit/obeiplatform/BusinessIndex.vue'),
  'views/obeicockpit/obeiplatform/TescoIndex': () =>
    import('@/views/obeicockpit/obeiplatform/TescoIndex.vue'),
  'views/obeicockpit/obeiplatform/CloudLibraryIndex': () =>
    import('@/views/obeicockpit/obeiplatform/CloudLibraryIndex.vue'),
  'views/obeicockpit/obeiplatform/EasytoIndex': () =>
    import('@/views/obeicockpit/obeiplatform/EasytoIndex.vue'),
  'views/obeicockpit/obeiplatform/ZeroCarbonIndex': () =>
    import('@/views/obeicockpit/obeiplatform/ZeroCarbonIndex.vue'),
  // 重构2.0
  'views/obeicockpit/obeiplatform2/OverIndex': () =>
    import('@/views/obeicockpit/obeiplatform2/OverIndex.vue'),
  'views/obeicockpit/obeiplatform2/DepartmentIndex': () =>
    import('@/views/obeicockpit/obeiplatform2/DepartmentIndex.vue'),
  'views/obeicockpit/obeiplatform/Customer': () =>
    import('@/views/obeicockpit/obeiplatform/Customer.vue'),
} as const;
