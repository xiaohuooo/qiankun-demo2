/**
 * test module
 */
export default {
  'views/test/TestMergeTable2': () => import('@/views/test/TestMergeTable.vue'),
  'views/test/TestCron': () => import('@/views/test/TestCron.vue'),
  'views/test/TestTableJoin': () => import('@/views/test/TestTableJoin.vue'),
  'views/test/TestQueryFilter': () => import('@/views/test/TestQueryFilter.vue'),
  'views/test/charts': () => import('@/views/test/Charts.vue'),
} as const;
