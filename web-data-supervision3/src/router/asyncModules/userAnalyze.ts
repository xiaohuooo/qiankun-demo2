/**
 * system module
 */
export default {
  'views/userAnalyze/bulletinBoardList/index': () =>
    import('@/views/userAnalyze/bulletinBoardList/Index.vue'),
  'views/userAnalyze/kanbanList/index': () => import('@/views/userAnalyze/kanbanList/Index.vue'),
  'views/userAnalyze/ChartConfigurations/ChartConfiguration': () =>
    import('@/views/userAnalyze/chartConfigurations/Index.vue'),
  'views/userAnalyze/kanbanList/kanbanConfigTabs': () =>
    import('@/views/userAnalyze/kanbanConfig/Index.vue'),
  'views/userAnalyze/dataPreview/dataPreview': () =>
    import('@/views/userAnalyze/kanbanConfig/Index.vue'),
  'views/userAnalyze/setmanagement/SetManagementList': () =>
    import('@/views/userAnalyze/setmanagement/Index.vue'),
  'views/userAnalyze/setmanagement/components/DataSetInfo': () =>
    import('@/views/userAnalyze/setmanagement/components/DataSetInfo.vue'),
} as const;
