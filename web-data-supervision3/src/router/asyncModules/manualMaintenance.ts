/**
 * 手动维护表管理路由
 */
export default {
  'views/manualMaintenance/baseProduction': () =>
    import('@/views/manualMaintenance/baseProduction/index.vue'),
  'views/manualMaintenance/materialMaintenance': () =>
    import('@/views/manualMaintenance/materialMaintenance/index.vue'),
  'views/manualMaintenance/general': () => import('@/views/manualMaintenance/general/index.vue'),
} as const;
