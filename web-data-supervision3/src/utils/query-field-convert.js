/**
 * 映射转换
 */
import dict from './dict';

/**
 * 数据集字段列表转换成查询字段列表
 * @param columnsList
 * @returns {[]}
 */
const columnFields2QueryFields = function (columnsList) {
  const list = [];
  if (Array.isArray(columnsList) && columnsList.length > 0) {
    columnsList.forEach((x) => {
      const f = columnField2QueryField(x);
      if (f) {
        list.push(f);
      }
    });
  }
  return list;
};

/**
 * 数据集字段转换成查询字段
 * @param filed
 */
const columnField2QueryField = function (filed) {
  if (filed && typeof filed !== 'undefined') {
    const type = columnFieldNewType2QueryFieldType(filed);
    const f = {
      name: filed.columnName,
      cName: filed.columnCname,
      type,
      example: filed.example,
      dictCode: filed.dictCode,
      itemDtoList: filed.itemDtoList,
      queryType: filed.queryType,
    };
    if (type === 'dict') {
      const codeList = dict.getKeyValueByDictCode(filed.dictCode);
      f.codeList = codeList;
    }
    return f;
  }
  return null;
};

/**
 * 数据集字段类型转换成查询字段类型
 * @param newType
 * @returns {string}
 */
const columnFieldNewType2QueryFieldType = function (filed) {
  if ((filed.dictId && filed.dictId.length > 0) || (filed.dictCode && filed.dictCode.length > 0)) {
    return 'dict';
  } else {
    if (filed.newType === '2') {
      return 'number';
    } else if (filed.newType === '3') {
      return 'date';
    } else if (filed.newType === '4') {
      return 'select';
    } else if (filed.newType === 'tree') {
      return 'tree';
    } else {
      return 'text';
    }
  }
};

/**
 * 判断条件转成对应子集字符串
 * @param filterType
 * @returns {string}
 */
const setConditionValue = function (filed) {
  filed.forEach((item, index) => {
    item.forEach((it, j) => {
      switch (it.filterType) {
        case 'NA':
          it.filterType = '';
          break;
        case 'CT':
          it.filterType = 'like';
          break;
        case 'NCT':
          it.filterType = 'not like';
          break;
        case 'LS':
          it.filterType = 'left';
          break;
        case 'RS':
          it.filterType = 'right';
          break;
        case 'EQ':
          it.filterType = '=';
          break;
        case 'NEQ':
          it.filterType = '≠';
          break;
        case 'GT':
          it.filterType = '>';
          break;
        case 'LT':
          it.filterType = '<';
          break;
        case 'GE':
          it.filterType = '≥';
          break;
        case 'LE':
          it.filterType = '≤';
          break;
        case 'BTW':
          it.filterType = '[a,b]';
          break;
        case 'ZLB':
          it.filterType = 'eq';
          break;
        case 'BZLB':
          it.filterType = 'ne';
          break;
      }
    });
  });
  // console.log('条件', filed);
  return filed;
};

export default {
  columnField2QueryField,
  columnFields2QueryFields,
  columnFieldNewType2QueryFieldType,
  setConditionValue,
};
