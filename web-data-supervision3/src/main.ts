// with polyfills
import 'core-js/stable';
import 'regenerator-runtime/runtime';

import { createApp } from 'vue';
import * as antIcons from '@ant-design/icons-vue';
import * as echarts from 'echarts';
import VueGridLayout from 'vue-grid-layout';
import VXETable from 'vxe-table';
import App from './App.vue';
import { setupRouter } from './router';
import dict from '@/utils/dict';
import { setupStore } from '@/store';
import { setupI18n } from '@/locales';
import { setupAntd, setupAssets, setupGlobalMethods, setupCustomComponents } from '@/plugins';
import '@/assets/less/Style.less';
// echarts 主题
import 'echarts/theme/macarons.js';
import 'echarts/theme/vintage.js';
import 'echarts/theme/infographic.js';
import 'echarts/theme/dark.js';
import '@/plugins/chartsTheme/index';
import 'vxe-table/lib/style.css';
import './public-path';
// import { ACCESS_TOKEN_KEY } from '@/enums/cacheEnum';
// import { Storage } from '@/utils/Storage';
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('./mock');
//   mockXHR();
// }
let app;

function setupPlugins(app) {
  // 注册全局常用的ant-design-vue组件
  setupAntd(app);
  // 引入静态资源
  setupAssets();
  // 注册全局自定义组件,如：<svg-icon />
  setupCustomComponents(app);
  // 注册全局方法，如：app.config.globalProperties.$message = message
  setupGlobalMethods(app);
}

async function setupApp(props: any) {
  app = createApp(App);
  const icons: any = antIcons;
  // 注册组件
  Object.keys(icons).forEach((key) => {
    app.component(key, icons[key]);
  });

  // 添加到全局
  app.config.globalProperties.$antIcons = antIcons;
  app.config.globalProperties.$dict = dict;
  app.config.globalProperties.$echarts = echarts;
  app.use(VueGridLayout);
  app.use(VXETable);
  setupPlugins(app);
  // 挂载vuex状态管理
  setupStore(app);
  const { container, getGlobalState, token } = props;
  // const ex = 7 * 24 * 60 * 60 * 1000;
  // Storage.set(ACCESS_TOKEN_KEY, token, ex);
  console.log(container, getGlobalState, token, '-- container, data ');
  app.config.globalProperties.parentVuex = getGlobalState;
  // Multilingual configuration
  // Asynchronous case: language files may be obtained from the server side
  await setupI18n(app);
  // 挂载路由
  await setupRouter(app);

  app.mount(container ? container.querySelector('#app') : '#app');
}

// setupPlugins();

// 独立运行时
if (!(window as any).__POWERED_BY_QIANKUN__) {
  setupApp({});
}

/**
 * bootstrap ： 在微应用初始化的时候调用一次，之后的生命周期里不再调用
 */
export async function bootstrap() {
  console.log('bootstrap');
}

/**
 * mount ： 在应用每次进入时调用
 */
export async function mount(props: any) {
  console.log('mount', props);
  setupApp(props);
}

/**
 * unmount ：应用每次 切出/卸载 均会调用
 */
export async function unmount() {
  console.log('unmount');
  app.unmount();
  if (app._container) {
    app._container.innerHTML = '';
    app = null
  }
}

if (process.env.NODE_ENV === 'development') {
  //@ts-ignore
  window.qiankunLifecycle = {
    bootstrap,
    mount,
    unmount,
  };
}
