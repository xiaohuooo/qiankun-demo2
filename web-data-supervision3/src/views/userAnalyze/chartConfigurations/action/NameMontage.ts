const NameMontage = (item) => {
  const nameList = [] as any;
  for (const key in item) {
    if (key == 'sort') {
      if (item[key] == 'ASC') {
        nameList.push('升序');
      } else if (item[key] == 'DESC') {
        nameList.push('降序');
      }
    }
    if (key == 'aggregateType') {
      if (item[key] == 'SUM') {
        nameList.push(`求和`);
      } else if (item[key] == 'AVG') {
        nameList.push(`平均`);
      } else if (item[key] == 'MAX') {
        nameList.push(`最大值`);
      } else if (item[key] == 'MIN') {
        nameList.push(`最小值`);
      } else if (item[key] == 'COUNT') {
        nameList.push(`计数`);
      }
    }
  }
  return nameList.length ? `${item.sourceName}(${nameList.join()})` : item.sourceName;
};
export default NameMontage;
