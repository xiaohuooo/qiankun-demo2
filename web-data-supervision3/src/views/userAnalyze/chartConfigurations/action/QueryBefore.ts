import { message } from 'ant-design-vue';
import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();

const QueryBefore = () => {
  let flag = true;
  if (!store.pageData.datasetIds?.length) {
    message.error('请选择数据集');
    flag = false;
  }

  if (!store.page.widgetName) {
    message.error('请输入图表名称');
    flag = false;
  }
  if (
    store.pageData.datasetIds?.length > 1 &&
    store.pageData.datasetIds?.length - 1 != store.sqlParams.tableRelationList.length
  ) {
    message.error('请配置关联字段');
    flag = false;
  }
  store.insertContent[2].value.forEach((item) => {
    if (!item?.filters?.length) {
      message.error(`过滤轴"${item.sourceName}"数据请配置过滤条件`);
      flag = false;
    }
  });
  return flag;
};
export default QueryBefore;
