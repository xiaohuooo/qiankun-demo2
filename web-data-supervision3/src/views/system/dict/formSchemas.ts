import type { FormSchema } from '@/components/core/schema-form/';

export const dictSchemas: FormSchema<API.CreateDictParams>[] = [
  {
    field: 'dictName',
    component: 'Input',
    label: '值集名称',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'dictCode',
    component: 'Input',
    label: '值集英文名',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'type',
    component: 'RadioGroup',
    label: '状态',
    rules: [{ required: true }],
    defaultValue: 'sys',
    colProps: {
      span: 24,
    },
    componentProps: {
      options: [
        {
          label: '系统数据类',
          value: 'sys',
        },
        {
          label: '数据表枚举类',
          value: 'tblfield',
        },
      ],
    },
  },
  {
    field: 'itemList',
    component: 'EditTable',
    label: '值集项',
    colProps: {
      span: 24,
    },
    componentProps: {
      itemKey: 'dictItemId',
      columns: [
        {
          title: '名称',
          key: 'itemText',
          width: '30%',
          align: 'center',
          type: 'input',
        },
        {
          title: '值',
          key: 'itemValue',
          align: 'center',
          width: '30%',
          type: 'input',
        },
        {
          title: '顺序号',
          key: 'sortOrder',
          align: 'center',
          type: 'input',
        },
      ],
      data: [],
    },
  },
];
