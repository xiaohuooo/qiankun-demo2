import { Avatar, Tag } from 'ant-design-vue';
import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.UserListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

const getAvatar = (avatar) => {
  if (avatar) {
    return `${process.env.BASE_URL}static/avatar/${avatar}`;
  } else {
    return '';
  }
};

export const baseColumns: TableColumnItem[] = [
  {
    title: '头像',
    width: 50,
    align: 'center',
    dataIndex: 'avatar',
    hideInSearch: true,
    customRender: ({ record }) => <Avatar src={getAvatar(record.avatar)} />,
  },
  {
    title: '用户名',
    width: 70,
    align: 'center',
    ellipsis: true,
    dataIndex: 'username',
  },
  {
    title: '工号',
    ellipsis: true,
    width: 70,
    align: 'center',
    dataIndex: 'employeeCode',
  },
  {
    title: '所属单位',
    dataIndex: 'unitName',
    ellipsis: true,
    align: 'center',
    width: 90,
  },
  {
    title: '所在部门',
    dataIndex: 'deptName',
    ellipsis: true,
    align: 'center',
    width: 90,
  },

  {
    title: '所属角色',
    dataIndex: 'roleName',
    align: 'center',
    ellipsis: true,
    width: 150,
    // customRender: ({ record }) => (
    //   <Space>
    //     {record.roleName?.split(',').map((item) => (
    //       <Tag color={'success'} key={item}>
    //         {item}
    //       </Tag>
    //     ))}
    //   </Space>
    // ),
  },
  {
    title: '邮箱',
    width: 100,
    ellipsis: true,
    align: 'center',
    dataIndex: 'email',
    hideInSearch: true,
  },
  {
    title: '手机',
    width: 80,
    ellipsis: true,
    align: 'center',
    dataIndex: 'mobile',
    hideInSearch: true,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 50,
    ellipsis: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [
          {
            label: '启用',
            value: 1,
          },
          {
            label: '禁用',
            value: 0,
          },
        ],
      },
    },
    customRender: ({ record }) => {
      const isEnable = parseInt(record.status) === 1;
      return <Tag color={isEnable ? 'success' : 'red'}>{isEnable ? '启用' : '禁用'}</Tag>;
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 90,
    ellipsis: true,
    hideInSearch: true,
    formItemProps: {
      component: 'RangePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
