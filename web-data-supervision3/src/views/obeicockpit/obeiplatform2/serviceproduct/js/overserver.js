// 一个入参
const parameter = (username, uservalue, usrtID) => {
  const cans = {
    relations: [
      {
        filterField: username,
        filterValues: [uservalue],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 区间入参
const parameter1 = (username, uservalue, usrtID) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 两个入参
const parameter2 = (username, uservalue, usrtID, username1, uservalue1) => {
  const cans = {
    relations: [
      {
        filterField: username,
        filterValues: [uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};
// 两个入参（区间）
const parameter3 = (username, uservalue, usrtID, username1, uservalue1) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};

// 三个入参（区间）
const parameter4 = (username, uservalue, usrtID, username1, uservalue1, username2, uservalue2) => {
  const cans = {
    relations: [
      {
        filterType: 'ragen',
        filterField: username,
        filterValues: [`${uservalue.substring(0, 4)}01`, uservalue],
        widgetId: usrtID,
      },
      {
        filterField: username1,
        filterValues: [uservalue1],
        widgetId: usrtID,
      },
      {
        filterField: username2,
        filterValues: [uservalue2],
        widgetId: usrtID,
      },
    ],
  };
  return cans;
};

// 处理echarts数据
const echartserver = (datachar, dataxAxis, dataser, ind1, ind2) => {
  const dataserver = {
    char: [],
    xAxis: [],
    series: [],
  };
  dataserver.char = Array.from(new Set(datachar));
  dataserver.xAxis = Array.from(new Set(dataxAxis));
  for (let i = 0; i < dataserver.char.length; i++) {
    dataserver.series.push({
      name: dataserver.char[i],
      type: 'line',
      data: [],
    });
  }
  for (let i = 0; i < dataser.length; i++) {
    const index = dataserver.char.indexOf(dataser[i][ind1]);
    dataserver.series[index].data.push(MonText(dataser[i][ind2]));
  }
  return dataserver;
};

// 完成率保留一位小数位
function XiaoText(text) {
  if (text === '-') {
    return text;
  } else {
    return parseFloat(text).toFixed(1);
  }
}

// 金额保留整数
function MonText1(text) {
  if (text === '-') {
    return text;
  } else {
    const data = parseFloat(text).toFixed(2);
    return data.toLocaleString();
  }
}

function MonText(text) {
  if (text === '-') {
    return text;
  } else {
    return parseInt(text);
  }
}

const toTableData = (inputdata, inputid) => {
  const data = [];
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (returnData[i][j] === '' || returnData[i][j] === '-' || returnData[i][j] == null) {
        dtt[columnList[j].name] = '-';
      } else {
        dtt[columnList[j].name] = returnData[i][j];
      }
    }
    data.push(dtt);
  }
  return data;
};

export default {
  parameter,
  parameter1,
  parameter2,
  parameter3,
  parameter4,
  XiaoText,
  MonText,
  MonText1,
  echartserver,
  toTableData,
};
