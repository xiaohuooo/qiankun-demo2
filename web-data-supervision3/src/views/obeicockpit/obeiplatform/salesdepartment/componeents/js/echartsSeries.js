const seriesData = (data1, data2, data3, unit) => {
  const series = [
    {
      name: '目标',
      type: 'bar',
      data: [],
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
        },
      },
      barGap: 0,
      barWidth: '25px',
    },
    {
      name: '实绩',
      type: 'bar',
      data: [],
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value) + unit;
        },
      },
      itemStyle: {
        normal: {
          barBorderRadius: [40, 40, 0, 0],
        },
      },
      barWidth: '25px',
    },
    {
      name: '完成率',
      type: 'line',
      data: [],
      yAxisIndex: 1,
      tooltip: {
        valueFormatter(value) {
          return `${Xiaoshu(value)} %`;
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: {
              color: 'black',
            },
            formatter(p) {
              return `${Xiaoshu(p.value)}%`;
            },
          },
        },
      },
    },
  ];
  series[0].data = data1;
  series[1].data = data2;
  series[2].data = data3;
  return series;
};

//保留两位有效小数
function Xiaoshu(text) {
  if (text === '-') {
    return text;
  } else {
    const datatext = parseFloat(text).toFixed(2);
    return parseFloat(datatext).toLocaleString();
  }
}

export default {
  seriesData,
};
