const formatter = {
  rate1: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(2); // 保留一位小数
      return `${parseFloat(resdata).toLocaleString()}%`;
    }
  },
  rate2: (text) => {
    if (text == '-') {
      return text;
    } else {
      const resdata = parseFloat(text).toFixed(2);
      return parseFloat(resdata).toLocaleString();
    }
  },
};

export default {
  formatter,
};
