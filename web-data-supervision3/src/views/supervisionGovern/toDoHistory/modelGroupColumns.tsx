import dict from '@/utils/dict.js';

export const baseColumns = [
  { title: '待办编号', width: 80, key: 'id', scopedSlots: { customRender: 'id' }, fixed: 'left' },

  {
    title: '模型组名称',
    width: 150,
    dataIndex: 'modelName',
    key: 'modelName',
    ellipsis: true,
  },
  {
    title: '排序字段',
    dataIndex: 'sort',
    width: 150,
    ellipsis: true,
  },
  {
    title: '处理意见',
    width: 100,
    dataIndex: 'handleRemark',
    key: 'handleRemark',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.handleRemark} placement="topLeft">
          {record.handleRemark}
        </a-tooltip>
      );
    },
  },

  {
    title: '模型组管理员',
    dataIndex: 'targetUsernames',
    ellipsis: true,
    width: 100,
  },
  { title: '处理状态', width: 100, dataIndex: 'handleStatus', key: 'handleStatus', ellipsis: true },
  {
    title: '模型组详情',
    width: 120,
    dataIndex: 'recordKeyChinese',
    key: 'recordKeyChinese',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip
          title={record.recordKeyChinese}
          placement="topLeft"
          overlay-class-name="overlayClassName"
        >
          {record.recordKeyChinese}
        </a-tooltip>
      );
    },
  },
  {
    title: '当前处理人',
    width: 100,
    dataIndex: 'handleUsername',
    key: 'handleUsername',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.handleUsername} placement="topLeft">
          {record.handleUsername}
        </a-tooltip>
      );
    },
  },
  {
    title: '历史处理意见',
    width: 120,
    dataIndex: 'allHandleRemark',
    key: 'allHandleRemark',
    ellipsis: true,
  },
  { title: '当前环节', width: 100, dataIndex: 'todoLink', key: 'todoLink', ellipsis: true },
  {
    title: '整改状态',
    width: 100,
    ellipsis: true,
    key: 'rectifyStatus',
    customRender: ({ record }) => {
      return record.rectifyStatus != '-'
        ? dict.getValue('MODEL_GROUP_CLUES_RECTIFICATION_STATUS', record.rectifyStatus).itemText
        : record.rectifyStatus;
    },
  },
  { title: '发起时间', width: 140, dataIndex: 'createTime', key: 'createTime', ellipsis: true },
  { title: '办结时间', width: 180, dataIndex: 'closeTime', key: 'closeTime', ellipsis: true },
  { title: '耗时', width: 130, dataIndex: 'timeConsumer', key: 'timeConsumer', ellipsis: true },
  {
    title: '历史环节',
    width: 150,
    dataIndex: 'historyLink',
    key: 'historyLink',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.historyLink} placement="topLeft">
          {record.historyLink}
        </a-tooltip>
      );
    },
  },
];
