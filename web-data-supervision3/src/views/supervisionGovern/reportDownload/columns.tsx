export const baseColumns = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 30,
    fixed: 'left',
    customRender({ index }) {
      return index + 1;
    },
  },
  {
    title: '报表名称',
    dataIndex: 'reportName',
    ellipsis: true,
    width: 120,
  },
  {
    title: '时间范围',
    dataIndex: 'timeRange',
    ellipsis: true,
    width: 100,
  },
  {
    title: '数据来源',
    dataIndex: 'dataSources',
    ellipsis: true,
    width: 80,
  },
];
