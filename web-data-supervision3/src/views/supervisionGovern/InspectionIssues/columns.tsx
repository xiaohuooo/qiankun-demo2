// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns = [
  {
    title: '项目编号',
    width: 120,
    dataIndex: 'projectCode',
    ellipsis: true,
    fixed: true,
  },
  { title: '项目名称', width: 100, dataIndex: 'projectName', ellipsis: true },
  {
    title: '项目内容',
    width: 120,
    ellipsis: true,
    dataIndex: 'projectContent',
  },
  {
    title: '项目类别',
    width: 120,
    ellipsis: true,
    dataIndex: 'projectCategory',
    customRender: ({ record }) => {
      return dict.getItemText('PROJECT_CATEGORY', record.projectCategory);
    },
  },

  // { title: '项目部门', width: 120, dataIndex: 'projectDeptName', ellipsis: true },
  { title: '被巡察单位', width: 120, dataIndex: 'inspectedUnits', ellipsis: true },
  {
    title: '项目组长',
    width: 80,
    dataIndex: 'projectTeamLeader',
    ellipsis: true,
  },
  {
    title: '项目副组长',
    width: 90,
    dataIndex: 'projectDeputyLeader',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题数',
    width: 120,
    dataIndex: 'inspectionNum',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题子项数',
    width: 120,
    dataIndex: 'inspectionDetailNum',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '项目开始时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'projectStartDate',
  },
  {
    title: '项目结束时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'projectEndDate',
  },
  {
    title: '创建人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createName',
  },
  {
    title: '创建时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createDate',
  },
  {
    title: '修改人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'updateName',
  },
  {
    title: '修改时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'updateDate',
  },
];
