// import { Tag } from 'ant-design-vue';
import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.DictListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  // {
  //   title: 'ID',
  //   width: 80,
  //   dataIndex: 'registerId',
  //   hideInSearch: true,
  // },
  {
    title: '语句描述',
    width: 120,
    ellipsis: true,
    dataIndex: 'description',
  },
  { title: '查询语句', width: 80, hideInSearch: true, dataIndex: 'registerSql', ellipsis: true },
  {
    title: '关联数据表',
    width: 100,
    ellipsis: true,
    dataIndex: 'registerFrom',
  },
  {
    title: '登记人',
    width: 80,
    dataIndex: 'updateUser',
  },
  {
    title: '更新时间',
    dataIndex: 'updateDate',
    width: 180,
    hideInSearch: true,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
