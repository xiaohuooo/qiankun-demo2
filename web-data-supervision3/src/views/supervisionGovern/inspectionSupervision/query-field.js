import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '项目编号',
      columnName: 'projectCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目名称',
      columnName: 'projectName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目内容',
      columnName: 'projectContent',
      newType: '1',
      example: null,
      dictCode: null,
    },

    {
      columnCname: '项目部门',
      columnName: 'projectDeptName',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('lineOfDefense'),
    },
    {
      columnCname: '项目责任人',
      columnName: 'projectPerson',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '团队成员',
      columnName: 'projectMember',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '立项理由',
      columnName: 'reason',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '备注',
      columnName: 'remark',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目开始时间',
      columnName: 'projectStartDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目完成时间',
      columnName: 'projectEndDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建人',
      columnName: 'createName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '创建时间',
      columnName: 'createDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '更新人',
      columnName: 'updateName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '更新时间',
      columnName: 'updateDate',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
