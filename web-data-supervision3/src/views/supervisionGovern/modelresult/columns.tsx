import dict from '@/utils/dict.js';

export const baseColumns = [
  {
    title: '模型名称',
    dataIndex: 'modelName',
    width: 250,
    ellipsis: true,
    fixed: 'left',
    sorter: true,
  },
  {
    title: '模型编号',
    dataIndex: 'modelCode',
    width: 150,
    ellipsis: true,
    fixed: 'left',
  },
  {
    title: '模型描述',
    dataIndex: 'description',
    ellipsis: true,
    width: 200,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.description} placement="topLeft">
          {record.description}
        </a-tooltip>
      );
    },
  },
  {
    title: '监督类别',
    dataIndex: 'belongUnit',
    ellipsis: true,
    width: 80,
    customRender: ({ record }) => {
      if (record.belongUnit != null) {
        const obj = dict.getValue('belongUnit', record.belongUnit);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '所属防线',
    dataIndex: 'lineOfDefenseType',
    ellipsis: true,
    width: 100,
    customRender: ({ record }) => {
      if (record.lineOfDefenseType != null) {
        const obj = dict.getValue('lineOfDefense', record.lineOfDefenseType);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <span>{obj.itemText}</span>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '模型归属单位',
    ellipsis: true,
    width: 120,
    dataIndex: 'modelBelongUnit',
    // customRender({ record }) {
    //   const text = record.belongUnit;
    //   if (text != undefined && text > 0) {
    //     const obj = dict.getValue('DEPT_TYPE', text);
    //     return obj ? obj.itemText : '-';
    //   } else {
    //     return '-';
    //   }
    // },
  },
  {
    title: '模型归属部门',
    ellipsis: true,
    width: 120,
    dataIndex: 'modelBelongDept',
  },

  {
    title: '监督领域',
    dataIndex: 'bizSystem',
    ellipsis: true,
    width: 100,
    customRender: ({ record }) => {
      const text = record.bizSystem;
      if (text != null) {
        const obj = dict.getValue('bizSystem', text);
        return (
          <div>
            <a-tooltip title={obj.itemText || ''} placement="topLeft">
              <span>{obj.itemText || ''}</span>
            </a-tooltip>
          </div>
        );
      }
    },
  },
  {
    title: '监督业务对象',
    dataIndex: 'businessObjs',
    ellipsis: true,
    width: 120,
    customRender({ record }) {
      const text = record.businessObjs;
      if (text != null) {
        const obj = dict.getValue('businessObject', text);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <a-tag>{obj.itemText}</a-tag>
            </a-tooltip>
          </div>
        );
      } else {
        return '-';
      }
    },
  },
  {
    title: '监督业务环节',
    dataIndex: 'bizIds',
    width: 120,
    ellipsis: true,
    customRender: ({ record }) => {
      if (record.bizIds != null) {
        const obj = dict.getValue('bizIds', record.bizIds);
        return (
          <div>
            <a-tooltip title={obj.itemText} placement="topLeft">
              <a-tag>{obj.itemText}</a-tag>
            </a-tooltip>
          </div>
        );
      } else {
        return '';
      }
    },
  },
  {
    title: '推送状态',
    width: 100,
    dataIndex: 'pushTag',
    ellipsis: true,
    customRender: ({ record }) => {
      if (record.pushTag != null) {
        const obj = dict.getValue('PUSH_TAG', record.pushTag);
        return obj.itemText || '';
      }
    },
  },
  {
    title: '未推送记录数',
    dataIndex: 'notPushedNum',
    ellipsis: true,
    width: 120,
  },
  {
    title: '模型类型',
    dataIndex: 'modelType',
    width: 100,
    ellipsis: true,
    customRender({ record }) {
      const obj = {
        static: '静态',
        dynamic: '动态',
        algorithm: '算法',
      };
      return obj[record.modelType];
      //   if (record.modelType == 'static') {
      //     return '静态';
      //   } else if (record.modelType == 'dynamic') {
      //     return '动态';
      //   }
    },
  },
  {
    title: '运行状态',
    dataIndex: 'runStatus',
    width: 100,
    customRender: ({ record }) => {
      let color = '#87d068';
      let text = '成功';
      if (record.runStatus == 7) {
        color = '#87d068';
        text = '成功';
      } else if (record.runStatus == 1) {
        color = '#87d068';
        text = '运行中';
      } else if (record.runStatus == 6) {
        color = '#f50';
        text = '失败';
      } else if (record.runStatus == 0) {
        color = '#f50';
        text = '未运行';
      }
      return <a-badge color={color} text={text}></a-badge>;
    },
  },
  {
    title: '上线状态',
    dataIndex: 'onlineStatus',
    width: 100,
    customRender: ({ record }) => {
      let color = '#87d068';
      let text = '成功';
      if (record.onlineStatus == 1) {
        color = '#87d068';
        text = '已上线';
      } else if (record.onlineStatus == -1) {
        color = '#87d068';
        text = '待上线';
      } else if (record.onlineStatus == 0) {
        color = '#f50';
        text = '已下线';
      }
      return <a-badge color={color} text={text}></a-badge>;
    },
  },
  {
    title: '业务用途',
    dataIndex: 'businessPurpose',
    width: 100,
    customRender: ({ record }) => {
      let text = '分析模型';
      if (record.businessPurpose == 1) {
        text = '疑点模型';
      }
      return (
        <a-tooltip title={text} placement="topLeft">
          <span>{text}</span>
        </a-tooltip>
      );
    },
  },
  {
    title: '模型管理员',
    dataIndex: 'targetUsernames', //sendTargetUsers
    ellipsis: true,
    width: 100,
  },
  {
    title: '模型记录数',
    dataIndex: 'suspectedRecords',
    width: 100,
  },
  {
    title: '模型最新记录数',
    dataIndex: 'latestRecords',
    width: 120,
  },
  {
    title: '未处理记录数',
    dataIndex: 'untreatedClueNum',
    width: 120,
  },
  {
    title: '模型组已推送记录数',
    dataIndex: 'modelPushCount',
    width: 90,
  },
  {
    title: '修改人',
    dataIndex: 'updateUser',
    width: 120,
  },
  {
    title: '修改时间',
    dataIndex: 'updateTime',
    width: 200,
  },
  {
    title: '创建人',
    dataIndex: 'createUser',
    width: 120,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    key: 'createTime',
    width: 200,
  },
];
