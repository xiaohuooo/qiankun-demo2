import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '待办编号',
      columnName: 'clueId',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题编号',
      columnName: 'problemCode',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '项目名称',
      columnName: 'projectName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题类型',
      columnName: 'problemType',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题责任人',
      columnName: 'problemPerson',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '检查内容简述',
      columnName: 'problemContent',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '发现问题简述',
      columnName: 'problemFind',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '问题整改状态',
      columnName: 'status',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('PROBLEM_RECTIFICATION_STATUS'),
    },

    {
      columnCname: '当前处理人',
      columnName: 'handleUsername',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办创建人',
      columnName: 'createName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办创建时间',
      columnName: 'createName',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办完结时间',
      columnName: 'closeTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '耗时',
      columnName: 'timeConsumerNum',
      newType: '1',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
