// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns = [
  {
    title: '待办编号',
    width: 80,
    dataIndex: 'clueId',
    ellipsis: true,
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '问题编号',
    width: 80,
    dataIndex: 'problemCode',
    hideInSearch: true,
  },
  {
    title: '项目名称',
    width: 120,
    dataIndex: 'projectName',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题类型',
    width: 80,
    dataIndex: 'problemType',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_TYPE', record.problemType);
    },
  },
  {
    title: '问题责任人',
    width: 80,
    dataIndex: 'problemPerson',
    hideInSearch: true,
  },

  {
    title: '检查内容简述',
    width: 120,
    dataIndex: 'problemContent',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '发现问题简述',
    width: 80,
    dataIndex: 'problemFind',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题整改状态',
    width: 90,
    dataIndex: 'status',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_RECTIFICATION_STATUS', record.status);
    },
  },
  { title: '当前处理人', width: 90, dataIndex: 'handleUsername', hideInSearch: true },
  { title: '待办创建人', width: 100, dataIndex: 'createName', hideInSearch: true },
  { title: '待办创建时间', width: 120, dataIndex: 'createDate', hideInSearch: true },
  { title: '待办完结时间', width: 120, dataIndex: 'closeTime', hideInSearch: true },
  { title: '耗时', width: 90, dataIndex: 'timeConsumer', hideInSearch: true },
];
