const echarts01 = (data1, data2) => {
  const option = [
    {
      name: '吨钢成本目标',
      type: 'line',
      smooth: false,
      data: data1,
      color: '#ffd44a',
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
    },
    {
      name: '吨钢成本',
      type: 'bar',
      data: data2,
      color: '#5470c6',
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      barGap: 0,
    },
  ];
  return option;
};

const echarts02 = (data1, data2, data3) => {
  const option = [
    {
      name: '损益口径(不含锌锡)',
      type: 'bar',
      stack: 'total',
      data: data1,
    },
    {
      name: '锌锡',
      type: 'bar',
      stack: 'total',
      data: data2,
    },
    {
      name: '合计',
      type: 'line',
      symbolSize: 0, // 设置symbol的大小设置为0
      showSymbol: false, // 不显示symbol
      lineStyle: {
        width: 0, //设置线宽为0
        color: 'rgba(0, 0, 0, 0)', // s设置线的颜色为透明
      },
      data: data3,
    },
  ];
  return option;
};

const echarts03 = (data1, data2, data3) => {
  const option = [
    {
      name: '钢铁基地',
      type: 'bar',
      stack: 'total',
      data: data1,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
    },
    {
      name: '欧冶工业品',
      type: 'bar',
      stack: 'total',
      data: data2,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
    },
    {
      name: '季度目标',
      type: 'line',
      data: data3,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
    },
  ];
  return option;
};

const echarts04 = (data1) => {
  const option = [
    {
      type: 'line',
      data: data1,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return MonText(p.value);
            },
          },
        },
      },
    },
  ];
  return option;
};

const echarts05 = (data1, data2) => {
  const option = [
    {
      name: '目标',
      type: 'line',
      smooth: false,
      data: data1,
      itemStyle: { color: '#ffd44a' },
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
    },
    {
      name: '实绩',
      type: 'bar',
      data: data2,
      barWidth: 30,
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      itemStyle: { color: '#59c4e6' },
      label: {
        show: true,
        position: 'top',
        formatter(p) {
          return XiaoShuText(p.value);
        },
      },
      barGap: 0,
    },
  ];
  return option;
};

const echarts06 = (data1, data2) => {
  const option = [
    {
      name: '合计库存金额',
      type: 'bar',
      barWidth: '55%',
      data: data1,
      zlevel: '1',
      color: '#EEBB00',
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
    },
    {
      name: '季末库存指标',
      type: 'bar',
      barWidth: '80%',
      color: '#888888',
      barGap: '-123%',
      itemStyle: { opacity: 0.5 },
      data: data2,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
    },
  ];
  return option;
};

const echarts07 = (data1) => {
  const option = [
    {
      itemStyle: {
        borderRadius: 5,
        borderColor: 'white',
        borderWidth: 1,
      },
      type: 'pie',
      radius: ['40%', '80%'],
      data: data1,
      label: {
        normal: {
          show: true,
          position: 'inner', // 数值显示在内部
          formatter(p) {
            const text = parseFloat(parseFloat(p.value).toFixed(1));
            return text.toLocaleString('en-US');
          },
          textStyle: {
            color: 'white', // 改变标示文字的颜色
          },
        },
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ];
  return option;
};

const echarts08 = (data1, data2) => {
  const option = [
    {
      name: '合计',
      type: 'bar',
      barWidth: '80%',
      zlevel: '-1',
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      data: data1,
    },
    {
      name: '超期金额',
      type: 'bar',
      barWidth: '50%',
      barGap: '-85%',
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      data: data2,
    },
  ];
  return option;
};

const echarts09 = (data1, data2) => {
  const option = [
    {
      name: '应收累计',
      type: 'bar',
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      data: data1,
      color: '#91cc75',
    },
    {
      name: '超期金额',
      type: 'bar',
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      data: data2,
      color: '#ee6666',
    },
  ];
  return option;
};

const echarts10 = (data1, data2) => {
  const option = [
    { name: '未超期', type: 'bar', data: data1, color: '#91cc75' },
    { name: '超期', type: 'bar', barGap: '0%', data: data2, color: '#ee6666' },
  ];
  return option;
};

// 处理echarts数据
const echarts11 = (datachar, dataxAxis, dataser, ind1, ind2) => {
  const dataserver = {
    char: [],
    xAxis: [],
    series: [],
  };
  dataserver.char = Array.from(new Set(datachar));
  dataserver.xAxis = Array.from(new Set(dataxAxis));
  for (let i = 0; i < dataserver.char.length; i++) {
    dataserver.series.push({
      name: dataserver.char[i],
      type: 'line',
      data: [],
    });
  }
  for (let i = 0; i < dataser.length; i++) {
    const index = dataserver.char.indexOf(dataser[i][ind1]);
    dataserver.series[index].data.push(XiaoShuText(dataser[i][ind2]));
  }
  return dataserver;
};

const echarts12 = (data1) => {
  const option = [
    {
      type: 'line',
      data: data1,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return XiaoShuText(p.value);
            },
          },
        },
      },
    },
  ];
  return option;
};

const echarts13 = (data1, data2) => {
  const option = [
    {
      name: '订货',
      type: 'line',
      data: data1,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return MonText(p.value);
            },
          },
        },
      },
    },
    {
      name: '到货',
      type: 'line',
      data: data2,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return MonText(value);
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return MonText(p.value);
            },
          },
        },
      },
    },
  ];
  return option;
};

const echarts14 = (data1, data2) => {
  const option = [
    {
      name: '采购实绩',
      type: 'line',
      data: data1,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return XiaoShuText(p.value);
            },
          },
        },
      },
    },
    {
      name: '钢产量',
      type: 'line',
      data: data2,
      smooth: false,
      tooltip: {
        valueFormatter(value) {
          return XiaoShuText(value);
        },
      },
      yAxisIndex: 1,
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return XiaoShuText(p.value);
            },
          },
        },
      },
    },
  ];
  return option;
};

// 完成率保留一位小数位
function XiaoShuText(text) {
  if (text === '-') {
    return text;
  } else {
    return parseFloat(text).toFixed(1);
  }
}

// 金额保留整数
function MonText(text) {
  if (text === '-') {
    return text;
  } else {
    const datatext = parseFloat(text).toFixed(0);
    return parseInt(datatext).toLocaleString();
  }
}

export default {
  echarts01,
  echarts02,
  echarts03,
  echarts04,
  echarts05,
  echarts06,
  echarts07,
  echarts08,
  echarts09,
  echarts10,
  echarts11,
  echarts12,
  echarts13,
  echarts14,
};
