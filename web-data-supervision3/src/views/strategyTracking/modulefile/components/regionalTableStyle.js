const cellColor = {
  // 资材品类中心,备件品类中心
  colorall: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      7: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 华南大区
  colorone: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      6: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      7: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      9: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      10: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 设备品类中心
  colortwo: {
    complex: 'true',
    color: {
      3: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 华东大区，华中大区，西南大区
  colorthree: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      6: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      9: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 西北大区，华北大区
  colorfour: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      7: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'MONTH_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },

  // 资材品类中心,备件品类中心
  colorallyear: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      7: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 华南大区
  coloroneyear: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      6: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      7: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      9: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      10: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 设备品类中心
  colortwoyear: {
    complex: 'true',
    color: {
      3: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      9: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 华东大区，华中大区，西南大区
  colorthreeyear: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      6: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      9: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  // 西北大区，华北大区
  colorfouryear: {
    complex: 'true',
    color: {
      4: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      5: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      7: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      8: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
      all: {
        columns: ['NEW_INDICATOR_PROGRESS_TARGET', 'YEAR_TOTAL_INDICATOR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },

  colorcb01month: {
    complex: 'true',
    color: {
      all: {
        columns: ['TARGET_STEEL_COST', 'MONTH_STEEL_COST'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
    },
  },
  colorcb01year: {
    complex: 'true',
    color: {
      all: {
        columns: ['TARGET_STEEL_COST', 'YEAR_TOTAL_STEEL_COST'],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
    },
  },
  colorxl01one: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURYEAR_PLAN_LINE_CPLTRATE'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorxl01two: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURMONTH_PLAN_LINE_CPLTRATE'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorlj01one: [['GT', 5, 'red']],
  colorzl01one: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURMONTH_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl01two: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURYEAR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl02one: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURMONTH_FIRST_CHK_QUALIFIED_RATE'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl02two: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURYEAR_FIRST_CHK_QUALIFIED_RATE'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl03one: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURMONTH_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorzl03two: {
    complex: 'true',
    color: {
      all: {
        columns: ['INDICATOR_MONTHLY_TARGET', 'CURYEAR_PRFM'],
        expression: '{1}',
        rule: [['LT', '{0}', 'red']],
      },
    },
  },
  colorsc02page01: {
    complex: 'true',
    color: {
      all: {
        columns: [
          'BDGT_PRICE_COMPARE_LASTYEAR_AVG_PRICE_DIFF',
          'CURYEAR_AVG_PRICE_COMPARE_LASTYEAR_AVG_PRICE_DIFF',
        ],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
    },
  },
  colorsc02page02: {
    complex: 'true',
    color: {
      all: {
        columns: [
          'BDGT_PRICE_COMPARE_LASTYEAR_AVG_PRICE_DIFF',
          'MONTH_AVG_PRICE_COMPARE_LASTYEAR_AVG_PRICE_DIFF',
        ],
        expression: '{1}',
        rule: [['GT', '{0}', 'red']],
      },
    },
  },
};

export default {
  cellColor,
};
