const toTableData = (inputdata, parseFunc, parseFunc1, parseFunc3) => {
  const data = [];
  const columnList = inputdata.columnList;
  const returnData = inputdata.data;
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (parseFunc3) {
        // 数据无需处理直接写入
        if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
          dtt[columnList[j].name] = '-';
        } else {
          dtt[columnList[j].name] = returnData[i][j];
        }
      } else {
        if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
          dtt[columnList[j].name] = '-';
        } else {
          if (parseFunc(columnList[j].name)) {
            // 标题表头等无需数据处理
            dtt[columnList[j].name] = returnData[i][j];
          } else {
            if (parseFunc1(columnList[j].name)) {
              // 数据需要处理加%号
              const indata = parseFloat(returnData[i][j]).toFixed(1);
              dtt[columnList[j].name] = `${indata}%`;
            } else {
              const indata = parseFloat(returnData[i][j]).toFixed(0);
              dtt[columnList[j].name] = parseFloat(indata).toLocaleString();
            }
          }
        }
      }
    }
    data.push(dtt);
  }
  return data;
};

const toTableData2 = (inputdata, inputid, parseFunc, parseFunc1, parseFunc3) => {
  const data = [];
  const columnList = inputdata[inputid].columnList;
  const returnData = inputdata[inputid].data;
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (parseFunc3) {
        // 数据无需处理直接写入
        if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
          dtt[columnList[j].name] = '-';
        } else {
          dtt[columnList[j].name] = returnData[i][j];
        }
      } else {
        if (parseFunc(columnList[j].name)) {
          // 标题表头等无需数据处理
          dtt[columnList[j].name] = returnData[i][j];
        } else {
          if (returnData[i][j] == '' || returnData[i][j] == '-' || returnData[i][j] == null) {
            dtt[columnList[j].name] = '-';
          } else {
            if (parseFunc1(columnList[j].name)) {
              // 数据需要处理加%号
              const indata = parseFloat(returnData[i][j]).toFixed(1);
              dtt[columnList[j].name] = `${indata}%`;
            } else {
              const indata = parseFloat(returnData[i][j]).toFixed(0);
              dtt[columnList[j].name] = parseFloat(indata).toLocaleString();
            }
          }
        }
      }
    }
    data.push(dtt);
  }
  return data;
};

const toTableData3 = (inputdata, inputid, keyColumnNameList = [], columnKeyList = []) => {
  const data = [];
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (returnData[i][j] === '' || returnData[i][j] === '-' || returnData[i][j] == null) {
        dtt[columnList[j].name] = '-';
      } else {
        dtt[columnList[j].name] = returnData[i][j];
      }
    }
    data.push(dtt);
  }
  return setData(data, keyColumnNameList, columnKeyList);
};

const toTableData4 = (
  inputdata,
  inputid,
  inputidboll,
  parseFunc,
  keyColumnNameList = [],
  columnKeyList = [],
) => {
  const data = [];
  let columnList = [];
  let returnData = [];
  if (inputid === '') {
    columnList = inputdata.columnList;
    returnData = inputdata.data;
  } else {
    columnList = inputdata[inputid].columnList;
    returnData = inputdata[inputid].data;
  }
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (returnData[i][j] === '' || returnData[i][j] === '-' || returnData[i][j] == null) {
        dtt[columnList[j].name] = '-';
      } else {
        if (parseFunc(columnList[j].name)) {
          // 标题表头等无需数据处理
          dtt[columnList[j].name] = returnData[i][j];
        } else {
          if (inputidboll === 1) {
            dtt[columnList[j].name] = parseFloat(returnData[i][j]).toFixed(1);
          } else if (inputidboll === 2) {
            const indata = parseFloat(returnData[i][j]).toFixed(0);
            dtt[columnList[j].name] = parseFloat(indata).toLocaleString();
          } else {
            dtt[columnList[j].name] = returnData[i][j];
          }
        }
      }
    }
    data.push(dtt);
  }
  return setData(data, keyColumnNameList, columnKeyList);
};

const setData = (data, keyColumnNameList, columnKeyList) => {
  if (keyColumnNameList.length > 0 && columnKeyList.length > 0) {
    columnKeyList.forEach((v) => {
      if (v.columnKey.indexOf(',')) {
        v.columnKey1 = v.columnKey.split(',');
      } else {
        v.columnKey1 = [v.columnKey];
      }
    });
    data.forEach((v) => {
      v.flagKey = [];
    });
    columnKeyList.forEach((k) => {
      k.columnKey1.forEach((h) => {
        data.forEach((v) => {
          keyColumnNameList[0].keyColumnName.forEach((j) => {
            if (v[j] == h) {
              v.flagKey.push(h);
            }
          });
        });
      });
    });
    data.forEach((v) => {
      if (v.flagKey.length > 0) {
        v.flagKey = v.flagKey.join(',');
        columnKeyList.forEach((j) => {
          if (v.flagKey === j.columnKey) {
            v.flag = true;
          }
        });
      } else {
        v.flagKey = null;
      }
    });
    return data;
  } else {
    return data;
  }
};
export default {
  toTableData,
  toTableData2,
  toTableData3,
  toTableData4,
};
