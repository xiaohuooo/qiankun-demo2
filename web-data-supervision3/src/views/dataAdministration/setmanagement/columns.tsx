import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict';
export type TableListItem = API.DatasetListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;
const dataSourceName = dict.getItemsByDictCode('dataSource').map((item) => {
  return {
    label: item.itemText,
    value: item.itemText,
  };
});

export const baseColumns: TableColumnItem[] = [
  {
    title: '数据集分类',
    dataIndex: 'dataCategory',
    hideInTable: true,
    width: 120,
    ellipsis: true,
    resizable: false,
    formItemProps: {
      component: 'Cascader',
      componentProps: {
        changeOnSelect: true,
      },
    },
  },
  {
    title: '数据集名称',
    width: 120,
    dataIndex: 'tableCname',
    ellipsis: true,
  },
  { title: '数据分类', width: 100, dataIndex: 'branchDetails', hideInSearch: true, ellipsis: true },
  {
    title: '关键词',
    width: 120,
    hideInTable: true,
    dataIndex: 'columnCnames',
  },
  {
    title: '数据集英文名',
    width: 100,
    dataIndex: 'tableName',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.tableName} placement="topLeft">
          {record.tableName}
        </a-tooltip>
      );
    },
  },
  {
    title: '数据源',
    width: 70,
    dataIndex: 'sourceName',
    ellipsis: true,
    hideInSearch: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.sourceName} placement="topLeft">
          {record.sourceName}
        </a-tooltip>
      );
    },
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dataSourceName,
      },
    },
  },

  { title: '数据集应用场景', width: 120, dataIndex: 'applicationScenario', ellipsis: true },
  {
    title: '数据治理业务对象',
    width: 80,
    dataIndex: 'governanceObject',
    hideInSearch: true,
    ellipsis: true,
  },
  { title: '修改人', width: 90, dataIndex: 'updateUser', hideInSearch: true },
  {
    title: '修改时间',
    width: 90,
    dataIndex: 'updateTime',
    hideInSearch: true,
  },
  { title: '创建人', width: 90, dataIndex: 'createUser' },
  {
    title: '创建时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createTime',
    formItemProps: {
      component: 'RangePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
  // {
  //   title: '被使用情况',
  //   width: 60,
  //   dataIndex: 'createTime',
  // },
];
