import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: 'ID',
    width: 80,
    align: 'center',
    dataIndex: 'id',
    hideInSearch: true,
  },
  {
    title: '数据表',
    width: 140,
    ellipsis: true,
    dataIndex: 'tableCname',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '规则描述',
    width: 200,
    ellipsis: true,
    dataIndex: 'ruleDescription',
    align: 'center',
  },
  {
    title: '表字段',
    width: 280,
    dataIndex: 'visibleField',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
    customRender: ({ value }) => {
      const _data = JSON.parse(value).map((item) => {
        return item.columnCname;
      });
      return _data.join();
    },
  },
  {
    title: '创建人',
    width: 120,
    dataIndex: 'createName',
    align: 'center',
  },
  {
    title: '创建时间',
    width: 180,
    dataIndex: 'createTime',
    align: 'center',
    hideInSearch: true,
  },
];
