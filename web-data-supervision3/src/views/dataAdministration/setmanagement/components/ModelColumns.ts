import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict';
export type TableListItem = API.ModelInfo; //ModelColumnsItems;
export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns: TableColumnItem[] = [
  {
    title: 'ID',
    width: 80,
    align: 'center',
    hideInSearch: true,
    fixed: true,
    customRender({ index }) {
      return index + 1;
    },
  },
  {
    title: '模型名称',
    width: 140,
    ellipsis: true,
    dataIndex: 'modelName',
    align: 'center',
  },
  {
    title: '模型描述',
    width: 200,
    ellipsis: true,
    dataIndex: 'description',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '类型',
    width: 200,
    dataIndex: 'modelType',
    align: 'center',
    ellipsis: true,
    customRender({ record }) {
      const obj = {
        static: '静态',
        dynamic: '动态',
        algorithm: '算法',
      };
      return obj[record.modelType];
    },
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dict.getKeyValueByDictCode('MODEL_TYPE'),
      },
    },
  },
  {
    title: '数据集',
    width: 120,
    dataIndex: 'datasets',
    align: 'center',
    hideInSearch: true,
    ellipsis: true,
    customRender: ({ record }) => {
      return (record.datasets && record.datasets.join(',')) || '--';
    },
  },
  {
    title: '所属防线',
    width: 180,
    dataIndex: 'lineOfDefenseType',
    align: 'center',
    hideInSearch: true,
    customRender: ({ record }) => {
      if (record.lineOfDefenseType != null) {
        const obj = dict.getValue('lineOfDefense', record.lineOfDefenseType);
        return obj.itemText;
      } else {
        return '';
      }
    },
  },
  {
    title: '模型管理员',
    width: 180,
    dataIndex: 'targetUsernames',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建时间',
    width: 180,
    dataIndex: 'createTime',
    align: 'center',
    hideInSearch: true,
  },
];
