import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict';
export type TableListItem = API.bulletinBoardListColumns;
export type TableColumnItem = TableColumn<TableListItem>;
const chartTypeOption = [
  {
    label: '全部',
    value: '',
  },
];
dict.getItemsByDictCode('chartType').forEach((item) => {
  chartTypeOption.push({
    label: item.itemText,
    value: item.itemValue,
  });
});
export const baseColumns: TableColumnItem[] = [
  {
    title: 'ID',
    width: 80,
    align: 'center',
    dataIndex: 'widgetId',
    hideInSearch: true,
  },
  {
    title: '图表名称',
    width: 140,
    ellipsis: true,
    dataIndex: 'widgetName',
    align: 'center',
  },
  {
    title: '图表描述',
    width: 200,
    ellipsis: true,
    dataIndex: 'description',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '类型',
    width: 200,
    dataIndex: 'chartType',
    align: 'center',
    ellipsis: true,
    customRender: ({ record }) => {
      //值集转换列表显示
      if (record.chartType != null && record.chartType != undefined) {
        return dict.getItemText('chartType', record.chartType);
      }
    },
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: chartTypeOption,
      },
    },
  },
  {
    title: '数据集',
    width: 120,
    dataIndex: 'datasets',
    align: 'center',
    ellipsis: true,
    hideInSearch: true,
    customRender: ({ record }) => {
      return (record.datasets && record.datasets.join(',')) || '--';
    },
  },
  {
    title: '创建人',
    width: 180,
    dataIndex: 'createUserName',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '创建时间',
    width: 180,
    dataIndex: 'createDate',
    align: 'center',
    hideInSearch: true,
  },
];
