import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict';

export type TableListItem = API.DatasetListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '文件名称',
    dataIndex: 'fileName',
    ellipsis: true,
    width: 300,
  },
  {
    title: '文件来源',
    dataIndex: 'fileResources',
    ellipsis: true,
    width: 150,
    hideInSearch: true,
    customRender: ({ value }) => {
      return dict.getItemText('DownLoadFileSources', value);
    },
  },
  {
    title: '提交人',
    dataIndex: 'createUserName',
    ellipsis: true,
    width: 100,
  },
  {
    title: '文件生成状态',
    dataIndex: 'handlerStatus',
    hideInSearch: true,
    ellipsis: true,
    width: 100,
    customRender: ({ value }) => {
      let color = '#87d068';
      let text = '成功';

      if (value == 10) {
        color = '#87d068';
        text = '生成中';
      } else if (value == 20) {
        color = '#87d068';
        text = '成功';
      } else {
        color = '#f50';
        text = '失败';
      }

      return <a-badge color={color} text={text}></a-badge>;
    },
  },
  {
    title: '文件生成时间',
    dataIndex: 'createDate',
    hideInSearch: true,
    ellipsis: true,
    width: 200,
  },
  // {
  //   title: '操作',
  //   key: 'operation',
  //   scopedSlots: { customRender: 'operation' },
  // },
];
