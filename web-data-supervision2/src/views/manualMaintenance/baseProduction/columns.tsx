import dayjs from 'dayjs';
import type { TableColumn } from '@/components/core/dynamic-table';
import { Storage } from '@/utils/Storage';
import { ACCESS_DICT_ALL_KEY } from '@/enums/cacheEnum';

const codeValue = Storage.get(ACCESS_DICT_ALL_KEY);
const MANUAL_AREA = codeValue
  ?.find((x) => x.dictCode === 'MANUAL_AREA')
  ?.itemDtoList?.map((x, index) => {
    return {
      label: x[`itemText`],
      value: x[`itemValue`],
      key: index,
    };
  });

export type TableListItem = API.BaseProductionListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '月份',
    width: 100,
    align: 'center',
    ellipsis: true,
    dataIndex: 'ym',
    editable: false,
    formItemProps: {
      component: 'MonthPicker',
      defaultValue: dayjs().subtract(1, 'month'),
    },
  },
  {
    title: '地区',
    dataIndex: 'baseValue',
    align: 'center',
    width: 200,
    editable: false,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: MANUAL_AREA,
        mode: 'multiple',
        multiple: true,
      },
    },
  },
  {
    title: '账套',
    dataIndex: 'acctCode',
    align: 'center',
    width: 200,
    editable: false,
  },
  {
    title: '工序/子工序/机组类型',
    dataIndex: 'unitType',
    align: 'center',
    width: 200,
    editable: false,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [
          { label: '工序', value: '工序' },
          { label: '子工序', value: '子工序' },
          { label: '机组', value: '机组' },
        ],
      },
    },
  },
  {
    title: '工序/子工序/机组名称',
    dataIndex: 'unitName',
    align: 'center',
    width: 200,
    editable: false,
    hideInSearch: true,
    ellipsis: true,
    customRender: ({ record }) => {
      return record?.isExistsUnitName == '1' ? (
        <span>{record.unitName}</span>
      ) : record?.isExistsUnitName == '0' ? (
        <span style="color: red">{record.unitName}</span>
      ) : (
        <span>{record.unitName}</span>
      );
    },
  },
  {
    title: '产量 (吨)',
    align: 'center',
    dataIndex: 'output',
    width: 250,
    hideInSearch: true,
    formItemProps: {
      component: 'InputNumber',
      componentProps: {
        min: 0,
        controls: false,
      },
    },
  },
  {
    title: '更新人',
    align: 'center',
    width: 100,
    dataIndex: 'updateUserName',
    hideInSearch: true,
    editable: false,
  },
  {
    title: '更新时间',
    align: 'center',
    width: 200,
    dataIndex: 'updateDateValue',
    hideInSearch: true,
    editable: false,
  },
];
