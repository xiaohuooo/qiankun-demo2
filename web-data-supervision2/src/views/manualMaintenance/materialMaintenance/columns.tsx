import type { TableColumn } from '@/components/core/dynamic-table';
import { Storage } from '@/utils/Storage';
import { ACCESS_DICT_ALL_KEY } from '@/enums/cacheEnum';

const codeValue = Storage.get(ACCESS_DICT_ALL_KEY);
const transfer = (name, label, value) => {
  return codeValue
    ?.find((x) => x.dictCode === name)
    ?.itemDtoList?.map((x, index) => {
      return {
        label: x[label],
        value: x[value],
        key: index,
      };
    });
};

export type TableListItem = API.MaterialMaintenanceListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '指标名称',
    width: 200,
    align: 'center',
    ellipsis: true,
    dataIndex: 'targetName',
    editable: false,
  },
  {
    title: '地区',
    dataIndex: 'baseValue',
    align: 'center',
    width: 150,
    ellipsis: true,
    editable: false,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: transfer('MANUAL_AREA', 'itemText', 'itemValue'),
        mode: 'multiple',
        multiple: true,
      },
    },
  },
  {
    title: '账套',
    dataIndex: 'acctCode',
    align: 'center',
    width: 100,
    ellipsis: true,
    editable: false,
    hideInSearch: true,
  },
  {
    title: '工序/子工序/机组类型',
    dataIndex: 'unitType',
    align: 'center',
    width: 200,
    hideInSearch: true,
    editable: false,
  },
  {
    title: '工序/子工序/机组名称',
    align: 'center',
    width: 200,
    dataIndex: 'unitName',
    hideInSearch: true,
    editable: false,
    ellipsis: true,
  },
  {
    title: '成本中心代码',
    align: 'center',
    width: 400,
    dataIndex: 'costCode',
    hideInSearch: true,
    formItemProps: {
      component: 'TagInput',
      componentProps: {
        tags: [],
      },
    },
  },
  {
    title: '代码类型',
    align: 'center',
    dataIndex: 'codeType',
    hideInSearch: true,
    width: 210,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: transfer('MANUAL_CODE_TYPE', 'itemText', 'itemText'),
      },
    },
  },
  {
    title: '代码清单',
    align: 'center',
    width: 400,
    dataIndex: 'codeName',
    hideInSearch: true,
    formItemProps: {
      component: 'TagInput',
      componentProps: {
        tags: [],
      },
    },
  },
  {
    title: '更新人',
    align: 'center',
    width: 100,
    dataIndex: 'updateUserName',
    hideInSearch: true,
    editable: false,
  },
  {
    title: '更新时间',
    align: 'center',
    width: 200,
    dataIndex: 'updateDateValue',
    hideInSearch: true,
    editable: false,
  },
];
