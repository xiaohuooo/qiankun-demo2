const toTableData = (inputdata, parseFunc) => {
  const data = [];
  const columnList = inputdata.columnList;
  const returnData = inputdata.data;
  for (let i = 0; i < returnData.length; i++) {
    const dtt = {};
    for (let j = 0; j < columnList.length; j++) {
      if (parseFunc) {
        dtt[columnList[j].name] = parseFunc(columnList[j].name, returnData[i][j]);
      } else {
        dtt[columnList[j].name] = returnData[i][j];
      }
    }
    data.push(dtt);
  }
  return data;
};

const genRowSpans = (dataList, cname) => {
  if (!dataList || dataList.length == 0) {
    return [];
  }

  const indexList = [];
  indexList.push(0);

  for (let i = 1; i < dataList.length; i++) {
    if (dataList[i][cname] != dataList[i - 1][cname]) {
      indexList.push(i);
    }
  }
  indexList.push(dataList.length);

  const rowSpans = [];
  for (let j = 0; j < indexList.length - 1; j++) {
    rowSpans.push({
      start: indexList[j],
      span: indexList[j + 1] - indexList[j],
    });
  }

  return rowSpans;
};

export default {
  toTableData,
  genRowSpans,
};
