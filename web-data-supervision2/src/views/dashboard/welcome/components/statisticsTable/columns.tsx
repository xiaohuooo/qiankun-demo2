import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '模型名称',
    width: 200,
    ellipsis: true,
    align: 'center',
    dataIndex: 'modelName',
  },
  {
    title: '描述',
    dataIndex: 'content',
    align: 'center',
    width: 100,
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 100,
    ellipsis: true,
    align: 'center',
    hideInSearch: true,
  },
];
