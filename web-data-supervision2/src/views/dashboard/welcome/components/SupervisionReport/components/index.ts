export { default as LineOfDefense } from './LineOfDefense.vue';
export { default as LineOfDefenseDetail } from './LineOfDefenseDetail.vue';
export { default as Department } from './Department.vue';
export { default as DepartmentDetail } from './DepartmentDetail.vue';
