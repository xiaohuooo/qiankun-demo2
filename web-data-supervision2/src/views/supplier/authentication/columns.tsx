export const baseColumns = [
  {
    title: '企业申请日期',
    width: 200,
    align: 'center',
    ellipsis: true,
    dataIndex: 'companyApplyDate',
    hideInSearch: true,
  },
  {
    title: '企业名称',
    dataIndex: 'companyName',
    align: 'center',
    width: 200,
    ellipsis: true,
    hideInSearch: true,
  },
  { title: '统一社会信用代码', dataIndex: 'companyUniCode', align: 'center', width: 200 },
  {
    title: '认证类型代码',
    align: 'center',
    width: 200,
    dataIndex: 'companyApplyStandard',
    customRender: ({ record }) => {
      if (record.companyApplyStandard == 'basic') {
        return '基础认证';
      } else if (record.companyApplyStandard == 'universal') {
        return '通用认证';
      } else if (record.companyApplyStandard == 'architecture1') {
        return '建筑业-咨询服务认证';
      } else if (record.companyApplyStandard == 'architecture2') {
        return '建筑业-材料设备认证';
      } else if (record.companyApplyStandard == 'architecture3') {
        return '建筑业-工程建设认证';
      }
    },
  },
  { title: '评价日期', align: 'center', dataIndex: 'appraiseDate', hideInSearch: true, width: 200 },
  {
    title: '认证状态',
    align: 'center',
    dataIndex: 'appraiseResult',
    hideInSearch: true,
    width: 200,
    customRender: ({ record }) => {
      return record.appraiseResult == '1' ? '已认证' : '未认证';
    },
  }, // 1是已认证 0 是未认证
  {
    title: '本次评价失效时间',
    align: 'center',
    dataIndex: 'expiryDate',
    hideInSearch: true,
    width: 200,
  },
  {
    title: '证书地址',
    align: 'center',
    dataIndex: 'certificateUrl',
    width: 200,
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a style="color: blue" href={record.certificateUrl} target="_blank">
          {record.certificateUrl}
        </a>
      );
    },
  },
  {
    title: '企业主营产品与服务',
    align: 'center',
    dataIndex: 'certificationScope',
    ellipsis: true,
    hideInSearch: true,
    width: 200,
  },
  { title: '证书编号', align: 'center', dataIndex: 'certCode', width: 200, hideInSearch: true },
  {
    title: '运营地址',
    align: 'center',
    dataIndex: 'operatingAddress',
    width: 200,
    hideInSearch: true,
  },
  {
    title: '注册地址',
    align: 'center',
    dataIndex: 'registeredAddress',
    width: 200,
    hideInSearch: true,
  },
  {
    title: '首次认证日期',
    align: 'center',
    dataIndex: 'firstAppraiseDate',
    width: 200,
    hideInSearch: true,
  },
];
