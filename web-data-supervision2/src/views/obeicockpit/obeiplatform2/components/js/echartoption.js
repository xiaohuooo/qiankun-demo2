const echarts = (data1, data2, data3) => {
  const option = [
    {
      name: '目标',
      type: 'bar',
      data: data1,
      itemStyle: {
        color: '#57C358',
      },
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '20%',
    },
    {
      name: '实绩',
      type: 'bar',
      data: data2,
      itemStyle: {
        color: '#F0C868',
      },
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
    {
      name: '完成率',
      type: 'line',
      yAxisIndex: 1,
      data: data3,
      itemStyle: {
        normal: {
          color: '#EF6E4D',
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return `${Xiaoshu(p.value)}%`;
            },
          },
        },
      },
      tooltip: {
        valueFormatter(value) {
          return `${Xiaoshu(value)}%`;
        },
      },
    },
  ];
  return option;
};

const echarts01 = (data1, data2, data3) => {
  const option = [
    {
      name: '目标',
      type: 'bar',
      data: data1,
      itemStyle: {
        color: 'rgba(90, 162, 255, 0.2)',
      },
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '15px',
      barGap: '-75%',
    },
    {
      name: '当前',
      type: 'bar',
      data: data2,
      zlevel: '1',
      color: [
        {
          type: 'linear',
          x: 0,
          y: 0,
          x2: 0,
          y2: 1,
          colorStops: [
            { offset: 0, color: 'rgba(69, 115, 230, 1)' }, // 设置颜色渐变
            { offset: 1, color: 'rgba(90, 162, 255, 1)' },
          ],
        },
      ],
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '7px',
    },
    {
      name: '完成率',
      type: 'line',
      data: data3,
      symbolSize: 0, // 设置symbol的大小设置为0
      showSymbol: false, // 不显示symbol
      lineStyle: {
        width: 0, //设置线宽为0
        color: 'rgba(0, 0, 0, 0)', // s设置线的颜色为透明
      },
      tooltip: {
        valueFormatter(value) {
          return `${Xiaoshu(value)}%`;
        },
      },
    },
  ];
  return option;
};

const echarts02 = (data1, data2, data3) => {
  const option = [
    {
      name: '目标',
      type: 'bar',
      data: data1,
      itemStyle: {
        color: '#EF6E4D',
      },
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '20%',
    },
    {
      name: '实绩',
      type: 'bar',
      data: data2,
      itemStyle: {
        color: '#F0C868',
      },
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
    {
      name: '完成率',
      type: 'line',
      yAxisIndex: 1,
      data: data3,
      itemStyle: {
        normal: {
          color: '#EF6E4D',
          label: {
            show: true,
            position: 'top',
            textStyle: { color: 'black' },
            formatter(p) {
              return `${Xiaoshu(p.value)}%`;
            },
          },
        },
      },
      tooltip: {
        valueFormatter(value) {
          return `${Xiaoshu(value)}%`;
        },
      },
    },
  ];
  return option;
};

const echarts03 = (data1, data2) => {
  const option = [
    {
      name: '企业会员数',
      type: 'bar',
      data: data1,
      stack: 'total',
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '20%',
    },
    {
      name: '个人会员数',
      type: 'bar',
      stack: 'total',
      data: data2,
      tooltip: {
        valueFormatter(value) {
          return Xiaoshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
  ];
  return option;
};

const echarts04 = (data1, data2, data3) => {
  const option = [
    {
      name: '商城本年新增SKU',
      type: 'bar',
      data: data1,
      stack: 'total',
      itemStyle: {
        color: '#EF6E4D',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
      barWidth: '20%',
    },
    {
      name: '云库本年新增SKU',
      type: 'bar',
      data: data2,
      stack: 'total',
      itemStyle: {
        color: '#5AA2FF',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
    {
      name: '易销本年新增SKU',
      type: 'bar',
      data: data3,
      stack: 'total',
      itemStyle: {
        color: '#60C758',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
  ];
  return option;
};

const echarts05 = (data1, data2, data3, data4) => {
  const option = [
    {
      name: '累计SKU',
      type: 'line',
      data: data1,
      itemStyle: {
        color: '#EF6E4D',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
    },
    {
      name: '商城',
      type: 'bar',
      data: data2,
      stack: 'total',
      itemStyle: {
        color: '#FEC64F',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
    {
      name: '云库',
      type: 'bar',
      data: data3,
      stack: 'total',
      itemStyle: {
        color: '#57C358',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
    {
      name: '易销',
      type: 'bar',
      data: data4,
      stack: 'total',
      itemStyle: {
        color: '#5AA2FF',
      },
      tooltip: {
        valueFormatter(value) {
          return zhenshu(value);
        },
      },
      barWidth: '20%',
      barGap: '0%',
    },
  ];
  return option;
};

//保留两位有效小数
function Xiaoshu(text) {
  if (text === '-') {
    return text;
  } else if (text == undefined) {
    return 0;
  } else {
    const datatext = parseFloat(text).toFixed(2);
    return parseFloat(datatext).toLocaleString();
  }
}

function zhenshu(text) {
  if (text === '-') {
    return text;
  } else if (text == undefined) {
    return 0;
  } else {
    return parseInt(text).toLocaleString();
  }
}

export default {
  echarts,
  echarts01,
  echarts02,
  echarts03,
  echarts04,
  echarts05,
};
