import { Tag } from 'ant-design-vue';
import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.DictListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '值集名称',
    width: 80,
    ellipsis: true,
    dataIndex: 'dictName',
  },
  {
    title: '英文名',
    width: 120,
    ellipsis: true,
    dataIndex: 'dictCode',
  },
  {
    title: '值集类型',
    dataIndex: 'type',
    width: 70,
    ellipsis: true,
    key: 'type',
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [
          {
            label: '系统数据类',
            value: 'sys',
          },
          {
            label: '数据表枚举类',
            value: 'tblfield',
          },
        ],
      },
    },
    customRender: ({ record }) => {
      const label = record.type == 'sys' ? '系统数据类' : '数据表枚举类';
      return <Tag color={record.type == 'sys' ? 'success' : 'red'}>{label}</Tag>;
    },
  },
  { title: '修改人', width: 80, hideInSearch: true, dataIndex: 'updateUser', ellipsis: true },
  {
    title: '修改时间',
    dataIndex: 'updateTime',
    ellipsis: true,
    width: 120,
    hideInSearch: true,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
  { title: '创建人', width: 80, hideInSearch: true, dataIndex: 'createUser', ellipsis: true },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 120,
    hideInSearch: true,
    formItemProps: {
      component: 'DatePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
