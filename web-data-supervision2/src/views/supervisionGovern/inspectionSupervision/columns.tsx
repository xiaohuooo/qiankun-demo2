import type { TableColumn } from '@/components/core/dynamic-table';
export type TableListItem = API.DatasetListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '项目编号',
    width: 120,
    dataIndex: 'projectCode',
    ellipsis: true,
    fixed: true,
  },
  { title: '项目名称', width: 100, dataIndex: 'projectName', ellipsis: true },
  {
    title: '项目内容',
    width: 120,
    ellipsis: true,
    dataIndex: 'projectContent',
  },

  { title: '项目部门', width: 120, dataIndex: 'projectDeptName', ellipsis: true },
  {
    title: '项目责任人',
    width: 80,
    dataIndex: 'projectPerson',
    ellipsis: true,
  },
  { title: '团队成员', width: 90, dataIndex: 'projectMember', ellipsis: true, hideInSearch: true },
  {
    title: '立项理由',
    width: 120,
    dataIndex: 'reason',
    ellipsis: true,
    hideInSearch: true,
  },
  { title: '备注', width: 90, dataIndex: 'remark', ellipsis: true, hideInSearch: true },
  {
    title: '项目开始时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'projectStartDate',
    // formItemProps: {
    //   component: 'RangePicker',
    //   componentProps: {
    //     class: 'w-full',
    //   },
    // },
  },
  {
    title: '项目完成时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'projectEndDate',
  },
  {
    title: '创建人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createName',
  },
  {
    title: '创建时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'createDate',
  },
  {
    title: '更新人',
    width: 90,
    hideInSearch: true,
    dataIndex: 'updateName',
  },
  {
    title: '更新时间',
    width: 90,
    hideInSearch: true,
    dataIndex: 'updateDate',
  },
];
