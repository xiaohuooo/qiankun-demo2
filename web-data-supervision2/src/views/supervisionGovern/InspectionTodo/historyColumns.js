// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const historyColumns = [
  {
    title: '待办编号',
    width: 80,
    dataIndex: 'clueId',
    ellipsis: true,
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '问题子项编号',
    width: 100,
    dataIndex: 'problemDetailCode',
    hideInSearch: true,
  },
  {
    title: '项目名称',
    width: 120,
    dataIndex: 'projectName',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '项目类别',
    width: 80,
    dataIndex: 'projectCategory',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_TYPE', record.projectCategory);
    },
  },
  {
    title: '问题大类',
    width: 80,
    dataIndex: 'problemType',
    customRender: ({ record }) => {
      return dict.getItemText('ISSUE_LARGE_CATEGORIES', record.problemType);
    },
  },
  {
    title: '问题名称',
    width: 80,
    dataIndex: 'problemName',
    hideInSearch: true,
    ellipsis: true,
  },

  {
    title: '问题子项详情',
    width: 120,
    dataIndex: 'problemDetail',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题责任人',
    width: 80,
    dataIndex: 'problemLiabilityPerson',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题整改状态',
    width: 90,
    dataIndex: 'remediationStatus',
    customRender: ({ record }) => {
      return dict.getItemText('INSPECTION_REMEDIATION_STATUS', record.remediationStatus);
    },
  },
  { title: '当前处理人', width: 90, dataIndex: 'handleUsername', hideInSearch: true },
  { title: '待办创建人', width: 100, dataIndex: 'createName', hideInSearch: true },
  { title: '待办创建时间', width: 120, dataIndex: 'createDate', hideInSearch: true },
  { title: '待办完结时间', width: 120, dataIndex: 'closeTime', hideInSearch: true },
  { title: '耗时', width: 90, dataIndex: 'timeConsumer', hideInSearch: true },
];
