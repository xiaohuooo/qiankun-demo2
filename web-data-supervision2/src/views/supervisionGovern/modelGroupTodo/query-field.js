import dict from '@/utils/dict.js';

const queryFields = {
  fieldsTwo: [
    {
      columnCname: '待办编号',
      columnName: 'id',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '模型组详情',
      columnName: 'recordKeyChinese',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '模型组名称',
      columnName: 'modelName',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '排序字段',
      columnName: 'sort',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '待办类型',
      columnName: 'todoType',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('TODO_TYPE'),
    },

    {
      columnCname: '上级推送人',
      columnName: 'beforeUser',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '当前处理意见',
      columnName: 'handleRemark',
      newType: '1',
      example: null,
      dictCode: null,
    },
    {
      columnCname: '当前环节',
      columnName: 'todoLink',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('MODEL_GROUP_HISTORY_LINK_CODE'),
    },
    {
      columnCname: '整改状态',
      columnName: 'rectifyStatus',
      newType: '4',
      example: null,
      itemDtoList: dict.getItemsByDictCode('MODEL_GROUP_CLUES_RECTIFICATION_STATUS'),
    },
    {
      columnCname: '处理时间',
      columnName: 'updateTime',
      newType: '3',
      example: null,
      dictCode: null,
    },
  ],
};
export default queryFields;
