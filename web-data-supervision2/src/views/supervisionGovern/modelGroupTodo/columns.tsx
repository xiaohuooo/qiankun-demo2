import dict from '@/utils/dict.js';
export const baseColumns = [
  {
    title: '待办编号',
    dataIndex: 'id',
    width: 60,
    fixed: 'left',
  },
  {
    title: '模型组详情',
    dataIndex: 'recordKeyChinese',
    ellipsis: true,
    width: 120,
    fixed: 'left',
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.recordKeyChinese} placement="topLeft">
          {record.recordKeyChinese}
        </a-tooltip>
      );
    },
  },
  {
    title: '模型组名称',
    dataIndex: 'modelName',
    ellipsis: true,
    width: 80,
    fixed: 'left',
  },
  {
    title: '排序字段',
    dataIndex: 'sort',
    width: 120,
    ellipsis: true,
    fixed: 'left',
  },
  {
    title: '待办类型',
    ellipsis: true,
    dataIndex: 'todoType',
    width: 80,
  },

  {
    title: '上级推送人',
    dataIndex: 'beforeUser',
    width: 80,
    ellipsis: true,
  },
  {
    title: '当前处理意见',
    dataIndex: 'handleRemark',
    ellipsis: true,
    width: 120,
  },
  {
    title: '当前环节',
    dataIndex: 'todoLink',
    ellipsis: true,
    width: 60,
  },
  {
    title: '整改状态',
    dataIndex: 'rectifyStatus',
    ellipsis: true,
    width: 80,
    customRender: ({ record }) => {
      return dict.getItemText('MODEL_GROUP_CLUES_RECTIFICATION_STATUS', record.rectifyStatus);
    },
  },
  {
    title: '处理时间',
    dataIndex: 'updateTime',
    width: 100,
    ellipsis: true,
  },
];
