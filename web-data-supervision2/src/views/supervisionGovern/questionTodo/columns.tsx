// import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict.js';
// export type TableListItem = API.DatasetListPageResultItem;
// export type TableColumnItem = TableColumn<TableListItem>;
export const baseColumns = [
  {
    title: '待办编号',
    width: 80,
    dataIndex: 'clueId',
    ellipsis: true,
    hideInSearch: true,
    fixed: true,
  },
  {
    title: '问题编号',
    width: 120,
    dataIndex: 'problemCode',
    fixed: true,
    hideInSearch: true,
  },
  {
    title: '项目名称',
    width: 120,
    dataIndex: 'projectName',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题类型',
    width: 90,
    dataIndex: 'problemType',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_TYPE', record.problemType);
    },
  },
  {
    title: '问题责任人',
    width: 90,
    dataIndex: 'problemPerson',
    hideInSearch: true,
  },

  {
    title: '检查内容简述',
    width: 120,
    dataIndex: 'problemContent',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '发现问题简述',
    width: 120,
    dataIndex: 'problemFind',
    ellipsis: true,
    hideInSearch: true,
  },
  {
    title: '问题整改要求',
    width: 120,
    dataIndex: 'problemAsk',
    ellipsis: true,
    hideInSearch: true,
  },
  { title: '问题整改部门', width: 180, dataIndex: 'problemDeptName', hideInSearch: true },
  { title: '处理人', width: 80, dataIndex: 'handleUsername', hideInSearch: true },
  {
    title: '问题整改状态',
    width: 120,
    dataIndex: 'status',
    customRender: ({ record }) => {
      return dict.getItemText('PROBLEM_RECTIFICATION_STATUS', record.status);
    },
  },
  { title: '验收部门', width: 150, dataIndex: 'deptName', hideInSearch: true },
  { title: '上级推送人', width: 100, dataIndex: 'beforeUser', hideInSearch: true },
  { title: '上级推送时间', width: 150, dataIndex: 'updateDate', hideInSearch: true },
  { title: '待办创建人', width: 100, dataIndex: 'createName', hideInSearch: true },
  { title: '待办创建时间', width: 150, dataIndex: 'createDate', hideInSearch: true },
];
