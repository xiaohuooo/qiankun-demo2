// import dict from '@/utils/dict.js';

export const base2Columns = [
  {
    title: '分类',
    dataIndex: 'CLASS_NAME',
    key: 'CLASS_NAME',
    ellipsis: false,
    width: 100,
    rspans: [
      { start: 0, span: 17 },
      { start: 17, span: 6 },
      // { start: 1, span: 1 },
    ],
  },
  {
    title: '所属部门',
    dataIndex: 'SYS_ORG_CODE',
    key: 'SYS_ORG_CODE',
  },
  {
    title: '用户',
    dataIndex: 'USER_NAME',
    key: 'USER_NAME',
    width: '12%',
  },
  {
    title: '公司分管领导',
    dataIndex: 'COMPANY_LEADER',
    key: 'COMPANY_LEADER',
  },
  {
    title: '大客户总监',
    dataIndex: 'CUST_MANAGER',
    key: 'CUST_MANAGER',
  },
  {
    title: '考核值',
    dataIndex: 'CHK_VALUE',
    key: 'CHK_VALUE',
  },
  {
    title: '一季度',
    dataIndex: 'one',
    key: 'one',
  },
  {
    title: '二季度',
    dataIndex: 'two',
    key: 'two',
    mergeColumn: 'CHK_VALUE,two',
    emphasisColor: 'givebase2Columnstwo',
    after: 'two',
    method: {
      complex: true,
      expression: '{1}',
    },
  },
  {
    title: '三季度',
    dataIndex: 'third',
    key: 'third',
  },
  {
    title: '四季度',
    dataIndex: 'fourth',
    key: 'fourth',
    // customRender: ({ value }) => {
    //   if (value) {
    //     let color = '#87d068';
    //     if (value == 2) {
    //       color = '#87d068';
    //     } else if (value == 3) {
    //       color = '#FFFF00';
    //     } else if (value == 1) {
    //       color = '#f50';
    //     }
    //     return <a-badge color={color}></a-badge>;
    //   } else {
    //     return '--';
    //   }
    // },
  },
];
