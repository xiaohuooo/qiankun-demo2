// import dict from '@/utils/dict.js';

export const base3Columns = [
  {
    title: '分类',
    dataIndex: 'CLASS_NAME',
    key: 'CLASS_NAME',
    ellipsis: false,
    width: 100,
    rspans: [
      { start: 0, span: 17 },
      { start: 17, span: 6 },
    ],
  },
  {
    title: '用户',
    dataIndex: 'USER_NAME',
    key: 'USER_NAME',
    width: '12%',
  },
  {
    title: '公司分管领导',
    dataIndex: 'COMPANY_LEADER',
    key: 'COMPANY_LEADER',
  },
  {
    title: '大客户总监',
    dataIndex: 'CUST_MANAGER',
    key: 'CUST_MANAGER',
  },
  {
    title: '1',
    dataIndex: 'one',
    key: 'one',
  },
  {
    title: '2',
    dataIndex: 'two',
    key: 'two',
  },
  {
    title: '3',
    dataIndex: 'third',
    key: 'third',
  },
  {
    title: '4',
    dataIndex: 'fourth',
    key: 'fourth',
  },
  {
    title: '5',
    dataIndex: 'fifth',
    key: 'fifth',
  },
  {
    title: '6',
    dataIndex: 'sixth',
    key: 'sixth',
  },
  {
    title: '7',
    dataIndex: 'seventh',
    key: 'seventh',
  },
  {
    title: '8',
    dataIndex: 'eighth',
    key: 'eighth',
  },
  {
    title: '9',
    dataIndex: 'ninth',
    key: 'ninth',
  },
  {
    title: '10',
    dataIndex: 'tenth',
    key: 'tenth',
  },
  {
    title: '11',
    dataIndex: 'eleventh',
    key: 'eleventh',
  },
  {
    title: '12',
    dataIndex: 'twelve',
    key: 'twelve',
  },
];
