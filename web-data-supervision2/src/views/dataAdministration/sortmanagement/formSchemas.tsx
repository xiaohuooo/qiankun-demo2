import type { FormSchema } from '@/components/core/schema-form/';

export const dataSortSchemas: FormSchema<API.CreateDataSortParams>[] = [
  {
    field: 'categoryName',
    component: 'Input',
    label: '分类名称',
    rules: [
      { required: true, type: 'string', message: '请输入名称' },
      { max: 50, message: '最多50个字符' },
    ],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'parentId',
    component: 'TreeSelect',
    label: '父级分类',
    colProps: {
      span: 12,
    },
    componentProps: {
      fieldNames: {
        label: 'title',
        value: 'value',
      },
      getPopupContainer: () => document.body,
    },
    rules: [{ required: true, type: 'number' }],
  },
  {
    field: 'orderNum',
    component: 'InputNumber',
    label: '排序号',
    defaultValue: 255,
    componentProps: {
      style: {
        width: '100%',
      },
    },
  },
];
