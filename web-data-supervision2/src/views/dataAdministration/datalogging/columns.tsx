import type { TableColumn } from '@/components/core/dynamic-table';

export type TableListItem = API.DatasetListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: '菜单',
    dataIndex: 'operationBtn',
    width: 200,
    customRender: ({ record }) => {
      return (
        <div style="width: 255px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
          <a-tooltip title={record.operationBtn} placement="topLeft">
            {record.operationBtn}
          </a-tooltip>
        </div>
      );
    },
  },
  {
    title: '操作类型',
    width: 150,
    ellipsis: true,
    dataIndex: 'operation',
  },
  {
    title: '请求信息',
    dataIndex: 'params',
    width: 260,
    customRender: ({ record }) => {
      return (
        <div style="width: 335px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
          <a-tooltip title={record.params} placement="topLeft">
            {record.params}
          </a-tooltip>
        </div>
      );
    },
  },
  {
    title: '数据对比',
    key: 'handlerStatus',
    hideInSearch: true,
    width: 60,
    align: 'center',
    ellipsis: true,
  },
  {
    title: '用户ID',
    dataIndex: 'id',
    ellipsis: true,
    hideInSearch: true,
    width: 100,
  },
  {
    title: '操作人',
    dataIndex: 'username',
    ellipsis: true,
    width: 100,
  },
  {
    title: 'IP',
    dataIndex: 'ip',
    ellipsis: true,
    hideInSearch: true,
    width: 150,
  },
  {
    title: '操作时间',
    dataIndex: 'createTime',
    ellipsis: true,
    width: 150,
    formItemProps: {
      component: 'RangePicker',
      componentProps: {
        class: 'w-full',
      },
    },
  },
];
