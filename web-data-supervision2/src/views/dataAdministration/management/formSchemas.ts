import type { FormSchema } from '@/components/core/schema-form/';

// import type { Rule } from 'ant-design-vue/es/form';
// import { checkDSName } from '@/api/dataAdministration/management';

// const validateName = async (_rule: Rule, value: String) => {
//   const checkResult = await checkDSName(value);
//   if (!checkResult.success) {
//     return Promise.reject('名称不能重复');
//   } else {
//     return Promise.resolve();
//   }
// };

export const dsSchemas: FormSchema<API.CreateDSParams>[] = [
  {
    field: 'sourceName',
    component: 'Input',
    label: '数据源标题',
    rules: [
      {
        required: true,
        type: 'string',
      },
      // {
      //   validator: validateName,
      // },
    ],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'sourceType',
    component: 'Select',
    label: '数据源类型',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 12,
    },
  },
  {
    field: 'url',
    component: 'Input',
    label: '数据源地址',
    rules: [{ required: true, type: 'string' }],
    colProps: {
      span: 24,
    },
  },
  {
    field: 'userName',
    component: 'Input',
    label: '用户名',
    rules: [{ type: 'string' }],
    colProps: {
      span: 12,
    },
    dynamicDisabled: ({ formModel }) => {
      return formModel.sourceType == '5sAPI';
    },
  },
  {
    field: 'passWord',
    component: 'Input',
    label: '密码',
    rules: [{ type: 'string' }],
    colProps: {
      span: 12,
    },
    dynamicDisabled: ({ formModel }) => {
      return formModel.sourceType == '5sAPI';
    },
  },
];
