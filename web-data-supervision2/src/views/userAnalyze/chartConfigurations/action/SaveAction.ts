import { message } from 'ant-design-vue';
import querySql from './QuerySql';
import QueryBefore from './QueryBefore';
import NameMontage from './NameMontage';
import { chartConfigurations } from '@/store/modules/chartConfigurations';
import { updateWidget, saveWidget } from '@/api/userAnalyze/chartConfigurations';
const store = chartConfigurations();

let parmas = {} as any;
// 横纵轴数据配置
const insertContentConfig = async () => {
  const insertContent = {
    keys: [],
    values: [],
    filters: [],
    aggregateMetricsRows: [],
    aggregateMetricsFilters: [],
  } as any;
  store.insertContent.forEach((item, index) => {
    item.value.forEach((t) => {
      switch (index) {
        case 0:
          if (t.type.split(',').indexOf('catalogue') !== -1) {
            t.children.forEach((cItem) => {
              if (cItem.check) {
                insertContent.keys.push({
                  col: cItem.name,
                  colCname: NameMontage(cItem),
                  values: [],
                  sort: t.sort,
                  id: cItem.sourceId,
                  distinct: t?.distinct || false,
                  type: cItem.sourceType,
                });
              }
            });
          } else {
            if (t.columnType == '10') {
              insertContent.aggregateMetricsRows.push({
                col: t.text,
                colCname: NameMontage(t),
                values: [],
                sort: t.sort,
                id: t.sourceId,
                distinct: t?.distinct || false,
                type: t.sourceType,
              });
            } else {
              insertContent.keys.push({
                col: t.name,
                colCname: NameMontage(t),
                values: [],
                sort: t.sort,
                id: t.sourceId,
                distinct: t?.distinct || false,
                type: t.sourceType,
              });
            }
          }
          break;
        case 1:
          insertContent.values.push({
            name: '',
            cols: [
              {
                col: t.name,
                colCname: NameMontage(t),
                sort: t.sort,
                distinct: t?.distinct || false,
                dataFormat: t?.dataFormat || null,
                aggregate_type: t?.aggregateType || 'SUM',
              },
            ],
          });
          break;
        case 2:
          t.filters.forEach((filterItem) => {
            if (filterItem.filterType == '为空' || filterItem.filterType == '非空') {
              const obj = {
                col: t.name,
                colCname: t.sourceName,
                values: ['#NULL'],
                sort: t.sort || 'desc',
                id: t.sourceId,
                type: filterItem.filterType == '为空' ? '=' : '≠',
              };
              if (t.columnType == '10') {
                insertContent.aggregateMetricsFilters.push(obj);
              } else {
                insertContent.filters.push(obj);
              }
            } else if (filterItem.filterType == '包含' || filterItem.filterType == '不包含') {
              const obj = {
                col: t.name,
                colCname: t.sourceName,
                values: filterItem.values,
                sort: t.sort || 'desc',
                id: t.sourceId,
                type: filterItem.filterType == '包含' ? 'like' : 'not like',
              };
              if (t.columnType == '10') {
                insertContent.aggregateMetricsFilters.push(obj);
              } else {
                insertContent.filters.push(obj);
              }
            } else {
              const obj = {
                col: t.name,
                colCname: t.sourceName,
                values: filterItem.values,
                sort: t.sort || 'desc',
                id: t.sourceId,
                type: filterItem.filterType,
              };
              if (t.columnType == '10') {
                insertContent.aggregateMetricsFilters.push(obj);
              } else {
                insertContent.filters.push(obj);
              }
            }
          });
          break;
      }
    });
  });
  return insertContent;
};
const datasetsConfig = () => {
  const list: any = [];
  store.datasets.forEach((item) => {
    list.push(item.id);
  });
  return list;
};
const datasetParams = async () => {
  const parmas = {
    schema: {
      dimension: [], //纵轴
      measure: [], //横
      filters: [],
    },
    selects: [],
  } as any;
  store.insertContent.forEach((item, index) => {
    item.value.forEach((t) => {
      switch (index) {
        case 0:
          if (t.type.split(',').indexOf('catalogue') !== -1) {
            t.children.forEach((cItem) => {
              if (cItem.check) {
                parmas.schema.measure.push({
                  column: cItem.name,
                  id: cItem.sourceId,
                  type: 'column',
                });
              }
            });
          } else {
            parmas.schema.measure.push({
              column: t.name,
              id: t.sourceId,
              type: 'column',
            });
          }
          break;
        case 1:
          parmas.schema.dimension.push({
            column: t.name,
            id: t.sourceId,
            type: 'column',
          });

          break;
      }
    });
  });
  store.datasets.forEach((item) => {
    item.columnsList.forEach((t) => {
      parmas.selects.push(`T${t.id}_${t.columnName}`);
    });
  });
  return parmas;
};
//获取目录
const queryRinIn = () => {
  let rinIn = {};
  let flag = true;
  if (store.chartData?.config?.option?.rinIn?.children?.length) {
    rinIn = store.chartData.config.option.rinIn;
    flag = false;
  }
  store.insertContent[0].value.forEach((item) => {
    if (item.newType === 4) {
      if (flag) {
        rinIn = item;
      } else {
        item = rinIn;
      }
    }
  });

  return rinIn;
};
const paramsConfig = async () => {
  const rinIn = queryRinIn();
  parmas = {
    widget: {
      data: {
        datasetIds: datasetsConfig().join(),
        datasetId: store?.page?.data?.datasetId || '',
        config: {
          rows: [],
          // aggregateMetricsRows: [],
          // aggregateMetricsFilters: [],
          filters: [],
          ...(await insertContentConfig()),
          columns: [],
          chart_type: store.page.chartType ? store.page.chartType : 'table',
          drawBy: 'column',
          groups: [],
          option: {},
          pageData: '',
          page: {
            pageSize: store.chartData.page.pageSize,
            pageNum: 1,
          },
        },
      },
      status: '10', // 保存后上线
      type: store.page.type,
      widgetId: store.page.widgetId,
      widgetName: store.page.widgetName,
      chartType: store.page.chartType ? store.page.chartType : 'table',
      dataModelId: store.page.dataModelId,
      description: store.page.description,
      businessObjs: store.page.businessObjs,
      bizIds: store.page.bizIds,
      datasetIds: datasetsConfig().join(),
      jurisdiction: store.page.jurisdiction || 1,
      dataSourceType: store.sourceName == '数据湖' ? 20 : 10,
    },
    dataset: {
      datasetName: store.page.widgetName,
      datasetId: store?.page?.data?.datasetId || '',
      data: {
        filters: [],
        ...(await datasetParams()),
        datasource: store.datasets[0].sourceId,
        query: {
          sql: await querySql(),
          datasets: datasetsConfig().join(),
        },
        expressions: [],
      },
    },
    resourcesType: store.page.resourcesType,
    userIds: store.page.userIds,
    roleIds: store.page.roleIds,
    chartType: store.page.chartType ? store.page.chartType : 'table',
  };
  if (store.chartData?.config?.option?.filters?.length) {
    store.chartData?.config?.option?.filters.forEach((item) => {
      parmas.widget.data.config.filters.push({
        col: item.columnName,
        colCname: item.columnCName,
        values: item.values,
        id: item.id,
        type: item.filterType,
      });
    });
  }
  parmas.widget.data.config.pageData = JSON.stringify({
    filterList: store.insertContent, // 轴数据
    datasetIds: datasetsConfig(),
    page: store.chartData.page,
    tableData: store.navList[1], // 左边的目录
    addField: store.navList[0], // 左边新增字段
    sqlParams: store.sqlParams, // 关联字段表
    sourceName: store.sourceName, // 数据源
  }); // 页面上所有配置
  parmas.widget.data.config.option = {
    chartAxis: {
      max: store.echartConfig.AxisMaxNum,
      min: store.echartConfig.AxisMinimum,
      minInterval: store.echartConfig.AxisSpace,
    },
    colorTheme: store.echartConfig.colorTheme,
    chartLegend: {
      show: store.echartConfig.isShowLegend,
      orient: store.echartConfig.layout,
      left: store.echartConfig.Horizontal,
      top: store.echartConfig.Longitudinal,
      align: store.echartConfig.FontSize,
    },
    tooltip: {
      show: store.echartConfig.dataShow,
      tooltip: store.echartConfig.dataTypes,
    },
    resultLimit: store.chartData.page.pageSize,
    radarMax: store.echartConfig.Maximum,
    merge: store.echartConfig.isMergeCell,
    rinIn,
    filters: store.chartData?.config?.option?.filters,
    editorConfig: store.editorConfig,
    echartConfig: store.echartConfig,
  }; // 图表配置
  return parmas;
};
function getUrlParams(url) {
  // 通过 ? 分割获取后面的参数字符串
  const urlStr = url.split('?')[1];
  // 创建空对象存储参数
  const obj = {};
  // 再通过 & 将每一个参数单独分割出来
  const paramsArr = urlStr.split('&');
  for (let i = 0, len = paramsArr.length; i < len; i++) {
    // 再通过 = 将每一个参数分割为 key:value 的形式
    const arr = paramsArr[i].split('=');
    obj[arr[0]] = arr[1];
  }
  return obj;
}
// 保存方法
const saveAction = async () => {
  if (!QueryBefore()) {
    return;
  }
  const params = await paramsConfig();
  const query = getUrlParams(location.href) as any;
  // 模型要保存到模型位置
  if (query.toPage === 'modal') {
    params.widget.dataModelId = query.id;
    params.widget.type = '20';
  }
  if (query.toPage === 'modal' && query.widgetId == 'init') {
    // 保存接口
    const res = await saveWidget(params);
    if (res.success) {
      window.history.go(-1);
      message.success(res.message);
    } else {
      message.error(res.message);
    }
  } else {
    // 更新接口
    const res = await updateWidget(params);
    if (res.success) {
      message.success(res.message);
    } else {
      message.error(res.message);
    }
  }
};
export default saveAction;
