import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();

const storeConfig = (data, params) => {
  store.page.chartType = params.chartType;
  store.chartData.data = data.data;
  store.chartData.pageData = params?.config?.pageData ? JSON.parse(params.config.pageData) : {};
  store.chartData.config = params.config;
  store.chartData.chartType = params.chartType;
  store.chartData.page = {
    pageNum: data.pageNum || 1,
    pageSize: data.pageSize || 10,
    total: data.total || 0,
    pageCount: data.pageCount || 1,
  };
};
// 设置页面上的图表配置
const setChartConfig = (option) => {
  store.echartConfig = {
    AxisMaxNum: option.chartAxis.max,
    AxisMinimum: option.chartAxis.min,
    AxisSpace: option.chartAxis.minInterval,
    colorTheme: option.colorTheme,
    isShowLegend: option.chartLegend.show,
    layout: option.chartLegend.orient,
    Horizontal: option.chartLegend.left,
    Longitudinal: option.chartLegend.top,
    FontSize: option.chartLegend.align,
    dataShow: option.tooltip.show,
    dataTypes: option.tooltip.tooltip,
    Maximum: option.radarMax,
    isMergeCell: option.merge,
    showSummaryType: option?.echartConfig?.showSummaryType ?? '',
    showRowSummaryTitle: option?.echartConfig?.showRowSummaryTitle ?? '合计',
    showColumnSummaryTitle: option?.echartConfig?.showColumnSummaryTitle ?? '合计',
    columnSummaryPosition: option?.echartConfig?.columnSummaryPosition ?? 'left',
    rowSummaryPosition: option?.echartConfig?.rowSummaryPosition ?? 'bottom',
    summaryMethods: option?.echartConfig?.summaryMethods ?? {},
    summaryMethodsCopy: Object.assign({}, option?.echartConfig?.summaryMethods ?? {}),
    mergeColumns: option?.echartConfig?.mergeColumns ?? [],
    tableSize: option?.echartConfig?.tableSize ?? 'small',
  };
};
const chartDataConfig = (data, params) => {
  storeConfig(data, params);
  const option = data.data.dataConfig.option;
  if (option) {
    setChartConfig(option);
  }
};
export default chartDataConfig;
