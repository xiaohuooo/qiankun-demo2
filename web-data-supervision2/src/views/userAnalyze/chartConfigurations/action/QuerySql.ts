import { getSqlInfo } from '@/api/userAnalyze/chartConfigurations'; // api 接口
import { chartConfigurations } from '@/store/modules/chartConfigurations';
const store = chartConfigurations();
const unique = (arr) => {
  if (!Array.isArray(arr)) {
    console.log('type error!');
    return;
  }
  const res = [arr[0]];
  for (let i = 1; i < arr.length; i++) {
    let flag = true;
    for (let j = 0; j < res.length; j++) {
      if (arr[i].colAli === res[j].colAli) {
        flag = false;
        break;
      }
    }
    if (flag) {
      res.push(arr[i]);
    }
  }
  return res;
};
const queryConfigList = () => {
  const list = [] as any;
  store.insertContent.forEach((it) => {
    it.value.forEach((ct: any) => {
      if (ct.type == 'addField') {
        list.push({
          colAli: ct.name,
          colExp: `(${ct.text})`,
          columnType: ct?.columnType == '10' ? '10' : '',
        });
      }
    });
  });
  return list.length ? unique(list) : [];
};
const paramsCofig = () => {
  const sqlParams = {
    compositeColumnList: queryConfigList(),
    datatableList: [],
    tableRelationList: [],
    mergeCols: [],
  } as any;

  // store.sqlParams = sqlParams;
  sqlParams.tableRelationList = store.sqlParams.tableRelationList;
  store.datasets.forEach((item, index) => {
    const datatableList = {
      sourceId: item.sourceId,
      tableId: item.id,
      tableAlias: `t${index + 1}`,
      tableCName: item.tableCname,
      tableName: item.tableName,
      tableScheme: item.tableScheme,
      tableColumnList: [] as any,
    };
    item.columnsList.forEach((item) => {
      datatableList.tableColumnList.push({
        tableColumnName: item.columnName,
        tableColumnCName: item.tableColumnCName,
      });
    });
    sqlParams.datatableList.push(datatableList);
  });
  sqlParams.mergeCols = store?.echartConfig?.mergeColumns || [];
  return sqlParams;
};
const querySql = async () => {
  let previewSql: any = '';
  const data = await getSqlInfo(paramsCofig());
  if (data.code === 'OK') {
    previewSql = data.data.sqlStr;
  }
  return previewSql;
};

export default querySql;
