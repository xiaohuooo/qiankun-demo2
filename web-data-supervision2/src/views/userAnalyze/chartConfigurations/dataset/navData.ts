export default [
  {
    title: '新增字段',
    type: 'allAddField',
    drag: 'true',
    children: [],
  },
  {
    title: '拖拽目录',
    type: 'allCatalogue',
    drag: 'true',
    children: [],
  },
  {
    title: '字段类型',
    children: [
      {
        title: '文本',
        type: 'text',
        drag: 'true',
        children: [],
      },
      {
        title: '数据',
        type: 'number',
        drag: 'true',
        children: [],
      },
      {
        title: '时间',
        type: 'time',
        drag: 'true',
        children: [],
      },
    ],
  },
];
