import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict';

export type TableListItem = API.DatasetListPageResultItem;
export type TableColumnItem = TableColumn<TableListItem>;
const dataSourceName = dict.getItemsByDictCode('dataSource').map((item) => {
  return {
    label: item.itemText,
    value: item.itemText,
  };
});

export const baseColumns: TableColumnItem[] = [
  {
    title: '数据集分类',
    dataIndex: 'dataCategory',
    hideInTable: true,
    width: 120,
    resizable: false,
    formItemProps: {
      component: 'Cascader',
      componentProps: {
        changeOnSelect: true,
      },
    },
  },
  {
    title: '数据集中字段',
    width: 120,
    dataIndex: 'columnCnames',
    ellipsis: true,
    hideInTable: true,
    formItemProps: {
      component: 'TagInput',
      componentProps: {
        tags: [],
      },
    },
  },
  { title: '数据分类', width: 100, dataIndex: 'branchDetails', hideInSearch: true, ellipsis: true },
  {
    title: '数据集名称',
    width: 120,
    dataIndex: 'tableCname',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.tableCname} placement="topLeft">
          {record.tableCname}
        </a-tooltip>
      );
    },
  },
  {
    title: '数据集英文名',
    width: 100,
    hideInSearch: true,
    dataIndex: 'tableName',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.tableName} placement="topLeft">
          {record.tableName}
        </a-tooltip>
      );
    },
  },
  {
    title: '数据源',
    width: 70,
    // hideInSearch: true,
    dataIndex: 'sourceName',
    ellipsis: true,
    customRender: ({ record }) => {
      return (
        <a-tooltip title={record.sourceName} placement="topLeft">
          {record.sourceName}
        </a-tooltip>
      );
    },
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: dataSourceName,
      },
    },
  },
  {
    title: '应用场景',
    width: 120,
    dataIndex: 'applicationScenario',
    ellipsis: true,
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: [
          {
            label: '数据治理成果',
            value: '数据治理成果',
          },
          {
            label: '静态监督模型',
            value: '静态监督模型',
          },
        ],
      },
    },
  },
  {
    title: '数据治理业务对象',
    width: 80,
    dataIndex: 'governanceObject',
    hideInSearch: true,
    ellipsis: true,
  },
  { title: '修改人', width: 90, dataIndex: 'updateUser', hideInSearch: true, ellipsis: true },
  {
    title: '修改时间',
    width: 80,
    dataIndex: 'updateTime',
    hideInSearch: true,
    ellipsis: true,
  },
  { title: '创建人', width: 90, dataIndex: 'createUser', ellipsis: true, hideInSearch: true },
  // {
  //   title: '创建时间',
  //   width: 80,
  //   hideInSearch: true,
  //   dataIndex: 'createTime',
  //   ellipsis: true,
  //   formItemProps: {
  //     component: 'RangePicker',
  //     componentProps: {
  //       class: 'w-full',
  //     },
  //   },
  // },
];
