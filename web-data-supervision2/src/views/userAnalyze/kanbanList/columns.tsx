import { Tag } from 'ant-design-vue';
import type { TableColumn } from '@/components/core/dynamic-table';
import dict from '@/utils/dict';
export type TableListItem = API.kanbanListColumns;
export type TableColumnItem = TableColumn<TableListItem>;

const businessScenesOption = [
  {
    label: '全部',
    value: '',
  },
];
dict.getItemsByDictCode('bizIds').forEach((item) => {
  businessScenesOption.push({
    label: item.itemText,
    value: item.itemValue,
  });
});
const openPage = (boardId, boardName) => {
  const _href = window.location.href;
  const url = `${
    _href.split('#')[0]
  }#/userAnalyze/dataPreview?boardId=${boardId}&tabCustomName=${boardName}&preview=${true}`;
  window.open(url);
};
export const baseColumns: TableColumnItem[] = [
  {
    title: '看板分类',
    width: 100,
    dataIndex: 'dataCategoryName',
    formItemProps: {
      component: 'Cascader',
      componentProps: {
        fieldNames: {
          label: 'categoryName',
          value: 'categoryCode',
        },
        options: [],
      },
    },
  },
  {
    title: '看板名称',
    dataIndex: 'boardName',
    width: 180,
    customRender: ({ record }: any) => {
      return (
        <a style="color:#1890FF" onClick={() => openPage(record.boardId, record.boardName)}>
          {record.boardName}
        </a>
      );
    },
  },
  {
    title: '看板描述',
    width: 180,
    dataIndex: 'description',
    hideInSearch: true, // 是否关闭头部搜索
  },
  {
    title: '监督业务对象',
    width: 60,
    dataIndex: 'businessObjs',
    hideInSearch: true, // 是否关闭头部搜索
    customRender: ({ record }) => {
      //值集转换列表显示
      return (
        record.businessObjs && <Tag>{dict.getItemText('businessObject', record.businessObjs)}</Tag>
      );
    },
  },
  {
    title: '监督业务环节',
    width: 120,
    dataIndex: 'bizIds',
    customRender: ({ record }) => {
      return record.bizIds && <Tag>{dict.getItemText('bizIds', record.bizIds)}</Tag>;
    },
    formItemProps: {
      component: 'Select',
      componentProps: {
        options: businessScenesOption,
      },
    },
  },
  {
    title: '创建时间',
    width: 160,
    dataIndex: 'createDate',
    hideInSearch: true, // 是否关闭头部搜索
  },
  {
    title: '修改人',
    width: 80,
    dataIndex: 'updateUserName',
    hideInSearch: true, // 是否关闭头部搜索
  },
  {
    title: '修改时间',
    width: 160,
    dataIndex: 'updateDate',
    hideInSearch: true, // 是否关闭头部搜索
  },
];
