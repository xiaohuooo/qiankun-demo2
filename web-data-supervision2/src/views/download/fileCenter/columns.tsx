import type { TableColumn } from '@/components/core/dynamic-table';
// import { Avatar, Space, Tag } from 'ant-design-vue';

export type TableListItem = API.RoleListResultItem;
export type TableColumnItem = TableColumn<TableListItem>;

export const baseColumns: TableColumnItem[] = [
  {
    title: 'ID',
    width: 80,
    align: 'center',
    dataIndex: 'pkId',
  },
  {
    title: '文件名称',
    width: 180,
    ellipsis: true,
    dataIndex: 'fileName',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '数据表',
    width: 230,
    ellipsis: true,
    dataIndex: 'datatable',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '提交人',
    width: 150,
    dataIndex: 'createUserName',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '任务提交时间',
    width: 160,
    dataIndex: 'createDate',
    align: 'center',
    hideInSearch: true,
  },
  {
    title: '文件生成状态',
    width: 150,
    ellipsis: true,
    dataIndex: 'handlerStatus',
    align: 'center',
    hideInSearch: true,
    customRender: ({ value }) => {
      let color = '#767676';
      let item = '';
      switch (value) {
        case '10':
          item = '生成中';
          break;
        case '20':
          item = '生成完成';
          color = '#52C41A';
          break;
        case '30':
          item = '生成失败';
          color = '#EA0000';
          break;
      }
      return <a-badge color={color} text={item}></a-badge>;
    },
  },
  {
    title: '文件生成时间',
    width: 160,
    dataIndex: 'createDate',
    align: 'center',
    hideInSearch: true,
  },
];
