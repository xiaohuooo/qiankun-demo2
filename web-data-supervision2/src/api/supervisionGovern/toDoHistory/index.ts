import { request, downloadJson } from '@/utils/request';

export function getSupervisionGovern(data: API.PageRequest) {
  return request(
    {
      url: `p/process/todo/history`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getModelGroupHistory(data: API.PageRequest) {
  return request(
    {
      url: `p/process/modelGroup/todo/history`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getAllUsers() {
  return request(
    {
      url: `/role/list/allUsers`,
      method: 'get',
    },
    {
      isPromise: true,
    },
  );
}

export function getDept() {
  return request(
    {
      url: `/dept`,
      method: 'get',
    },
    {
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}

export function downloadModelList(params, filename) {
  // const url = `p/download/clues/supervise`;
  const url = `/p/process/modelGroup/todoHistoryExport`;
  downloadJson(url, params, filename);
}
export function downloadModelListForModel(params, filename) {
  const url = `p/download/clues/supervise`;
  downloadJson(url, params, filename);
}
