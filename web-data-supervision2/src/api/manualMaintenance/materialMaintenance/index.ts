import _ from 'lodash-es';
import { request, downloadJson } from '@/utils/request';

export function getMaterialMaintenanceListPage(data: API.PageRequest) {
  const params = _.cloneDeep(data);
  if (params?.baseValue) {
    params['baseList'] = params.baseValue;
    delete params.baseValue;
  }
  return request<API.TableListResult<API.MaterialMaintenanceListResult>>(
    {
      url: 'n/query/material',
      method: 'post',
      data: params,
    },
    {
      isJsonBody: true,
    },
  );
}

export function updateMaterialMaintenance(data) {
  return request(
    {
      url: `p/update/material`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}

export function exportList(data, fileName) {
  const params = _.cloneDeep(data);
  if (params?.baseValue) {
    params['baseList'] = params.baseValue;
    delete params.baseValue;
  }
  downloadJson('/p/byConditionExport/material', params, fileName);
}

export function exportAll(data, fileName) {
  downloadJson('/p/export/material', data, fileName);
}
