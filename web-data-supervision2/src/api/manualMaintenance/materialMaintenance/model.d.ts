declare namespace API {
  /** 物料维护列表项 */
  type MaterialMaintenanceListResultItem = {
    id: number;
    targetName: string;
    base: string;
    acctCode: string;
    unitType: string;
    unitName: string;
    costCode: any;
    codeType: string;
    codeName: any;
    createDate: Date;
    updateUserName: string;
  };

  /** 物料维护列表 */
  type MaterialMaintenanceListResult = MaterialMaintenanceListResultItem[];

  /** 新增物料维护参数 */
  type CreateMaterialMaintenanceParams = {
    id: number;
    costCode: string;
    codeType: string;
    codeName: string;
  };
}
