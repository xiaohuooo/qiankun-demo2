import { request } from '@/utils/request';

// ---- added by zhujun
export function getMenuAll(data: API.PageRequest) {
  return request(
    {
      url: 'menu',
      method: 'get',
      data,
    },
    {},
  );
}

export function createMenu(data: API.MenuAddParams) {
  return request(
    {
      url: 'menu',
      method: 'post',
      data,
    },
    {
      successMsg: '创建成功',
    },
  );
}

export function updateMenu(data: API.MenuUpdateParams) {
  return request(
    {
      url: 'menu',
      method: 'put',
      data,
    },
    {
      successMsg: '更新成功',
    },
  );
}

export function deleteMenu(menuId: number) {
  return request(
    {
      url: `menu/${menuId}`,
      method: 'delete',
    },
    {
      successMsg: '删除成功',
    },
  );
}
