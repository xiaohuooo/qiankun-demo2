declare namespace API {
  type MenuListResultItem = {
    createTime: string;
    updatedAt: string;
    id: number;
    parentId: number;
    name: string;
    router: string;
    perms: string;
    /** 0: '菜单', 1: '权限'  */
    type: number;
    icon: string;
    orderNum: number;
    viewPath: string;
    keepAlive: string;
    showInSide: number;
    keyPath?: number[];
  };

  /** 获取菜单列表参数 */
  type MenuListResult = MenuListResultItem[];

  /** 更新某项菜单参数 */
  type MenuUpdateParams = MenuAddParams & {
    menuId: number;
  };

  // --- added by zhujun  ---
  /** 新增菜单参数 */
  type MenuAddParams = {
    menuName: string;
    parentId: number;
    type: number;
    path: string;
    component: string;
    orderNum: number;
    showInSide: number;
    keepAlive: string;
    icon: string;
    perms: string;
  };
}
