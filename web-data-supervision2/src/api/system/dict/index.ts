import { request } from '@/utils/request';

export function getDictList(data: API.PageRequest) {
  return request<API.TableListResult<API.DictListPageResult>>(
    {
      url: `p/query/dataDict`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function createDict(data: API.CreateDictParams) {
  return request(
    {
      url: 'p/add/dataDict',
      method: 'post',
      data,
    },
    {
      // successMsg: '创建成功',
      // isJsonBody: true,
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function updateDict(data) {
  return request(
    {
      url: 'p/update/dataDict',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      // isGetDataDirectly: true,
    },
  );
}

export function deleteDict(idList) {
  return request(
    {
      url: 'p/delete/dataDict',
      method: 'post',
      data: idList,
    },
    {
      // successMsg: '更新值集成功',
      // isJsonBody: true,
      isPromise: true,
      isJsonBody: true,
    },
  );
}
