declare namespace API {
  type bulletinBoardListColumns = {
    businessScenes: any;
    widgetId: string;
    widgetName: string;
    chartType: any;
    status: string;
    datasets: string[];
    businessObjList: string[];
    dictName: string;
    dictCode: string;
    dictId: string;
    jurisdiction: string;
    type: string;
    updateUser: string;
    updateTime: string;
    createUser: string;
    createTime: string;
    itemDtoList: DictItem[];
    roleIds: any[];
    businessObjs: string[];
    bizIds: string[];
  };
  type saveWidgetParmas = {};
  type bulletinBoardListSchemas = {
    widgetName: string;
    description: string;
    businessObjs: string;
    bizIds: string;
    jurisdiction: string;
    roleIds: string[];
    userIds: string[];
  };
}
