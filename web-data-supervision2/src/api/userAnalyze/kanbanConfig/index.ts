import { message } from 'ant-design-vue';
import { request } from '@/utils/request';
// 查询全部图表
export function getWidgetList(data) {
  return request(
    {
      url: `p/widget/getWidgetList`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 查询看板信息
export function getBoardData(data) {
  return request(
    {
      url: `p/board/getBoardData`,
      method: 'get',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 获取看板图表配置
export function dashboardWidget(data) {
  return request(
    {
      url: `p/widget/dashboardWidget`,
      method: 'get',
      data,
    },
    {
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 获取图表数据
export function getAggregateData(data) {
  return request(
    {
      url: `n/dashboard/getAggregateData`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
    },
  );
}
// 保存看板
export function saveBoard(data) {
  return request(
    {
      url: `p/board/saveBoard`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
      successMsg: '保存成功',
    },
  );
}

// 下载
export function widgetIDown(data) {
  return request(
    {
      url: `p/getDatasetQuerySql/by/widgetId`,
      method: 'post',
      data: { widgetId: data.widgetId },
    },
    {
      isPromise: true,
      isGetDataDirectly: true,
    },
  ).then((res) => {
    db2({
      fileName: data.widgetName,
      querySql: res.data,
      widgetId: data.widgetId,
    }).then((r) => {
      if (r.success) {
        message.info('文件生成中，请前往文件下载中心查看');
      } else {
        message.warn(r.message);
      }
    });
  });
}
// 下载
function db2(data) {
  return request(
    {
      url: `p/file/upload/hdfs/by/db2`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: true,
      // successMsg: '文件生成中，请前往文件下载中心查看',
    },
  );
}
