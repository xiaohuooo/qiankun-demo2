import { request } from '@/utils/request';
// 物料对照列表查询
export function queryList(data) {
  return request(
    {
      url: `p/material/comparison/queryList`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 物料对照保存
export function comparisonSave(data) {
  return request(
    {
      url: `p/material/comparison/save`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 物料对照删除
export function comparisonDelet(data) {
  return request(
    {
      url: `p/material/comparison/delete`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 物料对照运行
export function comparisonRun(data) {
  return request(
    {
      url: `p/material/comparison/run`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
