declare namespace API {
  type ModelInfo = {
    belongUnit: any;
    modelBelongDept: string; //部门
    deptData: Array;
    defenseNames: string;
    generationMethod: string;
    lineOfDefenseType: string;
    modelBelongUnit: string;
    bizIds: string;
    bizSystem: string;
    defenseLine: string;
    affiliationData: string;
    boardId: null;
    businessObjs: string;
    businessPurpose: string;
    columnCname: Array;
    createTime: string;
    createUser: string;
    cronDesc: string;
    cronDetail: null;
    cronExpression: string;
    datatableColumns: Array;
    datatableIds: Array;
    datatableNames: Array;
    deptCname: string;
    description: string;
    id?: number;
    lastRunTime: string;
    modelConfig: any;
    modelName: string;
    modelType: string;
    onlineStatus?: number;
    problemDepartment: string;
    problemDepartmentField: string;
    pushTag?: number;
    runStatus?: number;
    sendTargetUserDto: null;
    sendTargetUsers: Array;
    sourceId: string;
    sourceName: string;
    suspectedRecords?: number;
    targetUsernames: string;
    updateTime: string;
    updateUser: string;
    datasets: Array;
  };
}
