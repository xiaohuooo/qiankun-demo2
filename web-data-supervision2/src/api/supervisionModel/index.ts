import { request, downloadJson } from '@/utils/request';

export function getSupervisionModel(data: API.PageRequest) {
  return request(
    {
      url: `p/query/datamodel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getAllUsers() {
  return request(
    {
      url: `/role/list/allUsers`,
      method: 'get',
    },
    {
      isPromise: true,
    },
  );
}
// 生产方式
export function getGeneration(data) {
  return request(
    {
      url: `p/model/getGenerationMethodByDept`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      // isJsonBody: true,
    },
  );
}
// 模型管理查询 后端唐骏
export function getUsersCode(data: API.PageRequest) {
  return request(
    {
      url: `user/getUsers/username`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
export function getUsers(data: API.PageRequest) {
  return request(
    {
      url: `/user/condition/getUsers`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getUsersAdmin(data: API.PageRequest) {
  return request(
    {
      url: `p/query/model/admin`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getModelResult(data: API.PageRequest) {
  return request(
    {
      url: `p/query/datamodel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getSqlApi(data: API.PageRequest) {
  return request(
    {
      url: `p/query/disciplinary/datamodel/sql/by/id`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
export function getModelGroupSqlApi(data: API.PageRequest) {
  return request(
    {
      url: `p/query/modelGroup/sql/by/id`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function postHdfs(data: API.PageRequest) {
  return request(
    {
      url: `p/file/upload/hdfs`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function downloadModelList(params, filename) {
  const url = `p/datamodel/download/model`;
  downloadJson(url, params, filename);
}

export function downloadModelResult(params, filename) {
  const url = `p/datamodel/download/supervise`;
  downloadJson(url, params, filename);
}

export function getDataSetItemById(id: number) {
  return request(
    {
      url: `p/query/dataSetItem?id=${id}`,
      method: 'post',
      data: {},
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getDataSetItemByIds(ids: any) {
  return request(
    {
      url: `p/list/dataSetItems?ids=${ids}`,
      method: 'post',
      data: {},
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function remoteCheckModelName(params: any) {
  return request(
    {
      url: `p/check/modelName`,
      method: 'post',
      data: { ...params },
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}

export function addModel(data: any) {
  return request(
    {
      url: `p/add/datamodel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function getAllRoleUsers() {
  return request(
    {
      url: `/role/list/allRoleUsers`,
      method: 'get',
    },
    {
      isPromise: true,
    },
  );
}

export function getBoardData(data: API.PageRequest) {
  return request(
    {
      url: `/p/board/getBoardData`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getFilter(data: API.PageRequest) {
  return request(
    {
      url: `p/query/model/result/filter`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function datamodel(data: API.PageRequest) {
  return request(
    {
      url: `p/saveQuery/datamodel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getlog(data) {
  return request(
    {
      url: `p/query/datamodel/last/run/log`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

export function getDatamodelSubmitHistory(data) {
  return request(
    {
      url: `p/list/datamodelSubmitHistory`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}
// 新改的模型归属单位
export function getDetail(data) {
  return request(
    {
      url: `user/username/detail`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

export function getdetail(data: API.PageRequest) {
  return request(
    {
      url: 'p/query/disciplinary/datamodel/detail/by/id',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getlogy(data) {
  return request(
    {
      url: `p/query/datamodel/process/failure/run/log`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

export function getPush(data: API.PageRequest) {
  return request(
    {
      url: 'p/process/todo/push',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function getPushSecond(data: API.PageRequest) {
  return request(
    {
      url: 'p/first/clue/push',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

// export function getTransfery(data) {
//   return request(
//     {
//       url: `p/process/todo/handle/transfer`,
//       method: 'get',
//       data,
//     },
//     {
//       isPromise: true,
//     },
//   );
// }

export function getTransferySecond(data) {
  return request(
    {
      url: `p/process/todo/push/person`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}

/**
 * 编辑模型
 * @param data
 */
export function updateModel(data: any) {
  return request(
    {
      url: `p/update/datamodel`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

/**
 * 批量更新
 * @param data
 */
export function bathUpdateModel(data: API.PageRequest) {
  return request(
    {
      url: `p/batch/Update/By/Ids`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}

/**
 * 模型上线
 * @param data
 */
export function onlineStatus(data: API.PageRequest) {
  return request(
    {
      url: `p/update/onlineStatus`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

/**
 * 批量模型上线
 * @param data
 */
export function bathOnlineStatus(data: API.PageRequest) {
  return request(
    {
      url: `p/update/batch/onlineStatus`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

/**
 * 批量删除
 * @param ids
 */
export function bathDel(ids: string) {
  return request(
    {
      url: `p/update/delFlag/batch?ids=${ids}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

/**
 * 复制模型
 * @param id
 */
export function copyModel(id: number) {
  return request(
    {
      url: `p/copy/datamodel`,
      method: 'post',
      data: { modelId: id },
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}

/**
 * 计算模型
 * @param id
 */
export function calcModel(id: number) {
  return request(
    {
      url: `p/start/process?id=${id}`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

/**
 * 动态模型试运行
 * @param data
 */
export function tryRun(data: any) {
  return request(
    {
      url: `p/datamodel/dynamic/tryRun`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function BoardList(data: API.PageRequest) {
  return request(
    {
      url: 'p/board/getBoardList',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function relation(data: API.PageRequest) {
  return request(
    {
      url: 'p/board/model/relation',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//sql查询列表
export function getSqllist(data: API.PageRequest) {
  return request(
    {
      url: 'p/sql/register/query',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
//sql登记
export function saveSql(data: API.PageRequest) {
  return request(
    {
      url: '/p/sql/register/save',
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}

export function dataTable(data: API.PageRequest) {
  return request(
    {
      url: `p/save/query/dataTable`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

export function dataSetItem(data: API.PageRequest) {
  return request(
    {
      url: `p/query/dataSetItem?id=${data.id}`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
export function getConfigFilters(data) {
  return request(
    {
      url: `/p/query/model/configFilters`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}
// 数据集预览下载
export function byDbtwo(data: any) {
  return request(
    {
      url: `p/file/upload/hdfs/by/db2`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 推送待办接收人的级联接口
export function rectifierCasValue(data) {
  return request(
    {
      url: `p/process/todo/handle/rectifier`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 推送待办接收人的级联接口
export function getDeptLead(data) {
  return request(
    {
      url: `p/process/todo/handle/deptLead`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 确认推送
export function confirmPush(data: any) {
  return request(
    {
      url: `p/first/clue/confirm/push`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
// 修改检查员
export function handleRole(data: any) {
  return request(
    {
      url: `p/process/todo/handle/role`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: false,
    },
  );
}
// 修改检查员保存
export function saveInspector(data: any) {
  return request(
    {
      url: `p/first/clue/batch/hand`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//查询模型编辑时是否可编辑模型管理员
export function checkModifySendTargetUsers(data: any) {
  return request(
    {
      url: `p/query/checkModifySendTargetUsers`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//按条件推送模型组
export function BatchPush(data) {
  return request(
    {
      url: `p/modelGroup/BatchPush`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
//设置推送条件接收人选择
export function getRecipient(data) {
  return request(
    {
      url: `p/model/automaticPush/recipient`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
    },
  );
}
// 是否订阅
export function getSubscribe(data) {
  return request(
    {
      url: `p/update/modelSubscribe`,
      method: 'get',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
