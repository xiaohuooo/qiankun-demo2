import { request, downloadJson } from '@/utils/request';

export function getSupervisionModel(data: API.PageRequest) {
  return request(
    {
      url: `p/query/file/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
      isGetDataDirectly: false,
    },
  );
}
export function getDatasetList(data: API.PageRequest) {
  return request<API.TableListResult<API.DSListPageResult>>(
    {
      url: `p/query/file/list`,
      method: 'post',
      data,
    },
    {
      isJsonBody: true,
    },
  );
}
// 下载中心批量下载
export function AllDownloadCenter(params, filename) {
  const url = `p/batch/file/export/download`;
  downloadJson(url, params, filename);
}
// 下载中心
export function DownloadCenter(params, filename) {
  const url = `p/file/export/download`;
  downloadJson(url, params, filename);
}
// 监督成功下载
export function Downloads(params, filename) {
  const url = `p/download/monitoringResTable`;
  downloadJson(url, params, filename);
}
// 监督成果批量下载
export function AllDownloads(params, filename) {
  const url = `p/batch/download/monitoringResTable`;
  downloadJson(url, params, filename);
}
// 监督成果列表
export function monitoringResDownload(data) {
  return request(
    {
      url: `/p/query/monitoringResTable`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 查看列表
export function getResDownload(data) {
  return request(
    {
      url: `p/query/monitoringResDownload?fileName=${data}`,
      method: 'get',
    },
    {
      isPromise: true,
      isGetDataDirectly: false,
    },
  );
}
// 生成按钮
export function monitoringResDetail(data) {
  return request(
    {
      url: `p/query/monitoringResDetail`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 删除
export function deleteFile(data) {
  return request(
    {
      url: `p/delete/file/log`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
