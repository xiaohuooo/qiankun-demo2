declare namespace API {
  type DatasetListPageResultItem = {
    handlerStatus: any;
    dataCategory: string;
    id: number;
    tableCname: string;
    tableName: string;
    sourceName: string;
    branchDetails: string;
    applicationScenario: string;
    governanceObject: string;
    tableName: string;
    updateUser: string;
    updateTime: string;
    createTime: string;
    createUser: string;
    operationBtn: string;
    params: string;
  };

  type DatasetListPageResult = DatasetListPageResultItem[];

  type CreateDatasetParams = {
    sourceId: string;
    sourceName: string;
    tableCname: string;
    tableName: string;
    apiName: string;
    algorithmClass: string;
    tableScheme: string;
    applicationScenario: string;
    governanceObject: string;
    dataCategory: any;
    voList: object[];
  };
}
