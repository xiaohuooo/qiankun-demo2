import { request } from '@/utils/request';
// 查询规则列表接口
export function queryDataRules(data) {
  return request(
    {
      url: `/p/datatable/authorized/rule/list`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}

// 数据规则增删改查接口
export function configRule(data) {
  return request(
    {
      url: `/p/datatable/authorized/rule/operate`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 查看规则
export function getRuleDetail(id) {
  return request(
    {
      url: `/p/datatable/authorized/rule/get?filtersId=${id}`,
      method: 'get',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}
// 查询数据集
export function getDatasetItemList(id) {
  return request(
    {
      url: `p/query/dataSetItem?id=${id}&filters=false`,
      method: 'post',
    },
    {
      isPromise: true,
      isJsonBody: true,
    },
  );
}