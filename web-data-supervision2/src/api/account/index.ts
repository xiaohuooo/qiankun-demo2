import { request } from '@/utils/request';

//  ------------zhujun 分隔线---------------------
export function getUsermenu(userId: string) {
  return request<API.VueRouter[]>(
    {
      url: `menu/${userId}`,
      method: 'get',
    },
    {},
  );
}

export function logout(userId: number) {
  const data = { id: userId };

  return request({
    url: `logout/id`,
    method: 'get',
    data,
  });
}

export function editUser(data) {
  return request(
    {
      url: 'user/profile',
      method: 'put',
      data,
    },
    {
      isPromise: true,
    },
  );
}

//特殊路径获取用户信息
export function getUsers(data) {
  return request(
    {
      url: `login/md5`,
      method: 'post',
      data,
    },
    {
      isPromise: true,
    },
  );
}
