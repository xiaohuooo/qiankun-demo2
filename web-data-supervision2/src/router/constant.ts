export const LOGIN_NAME = 'Login';
export const LOGIN_TRANSMITTER = 'transmitter';

export const REDIRECT_NAME = 'Redirect';

export const PARENT_LAYOUT_NAME = 'ParentLayout';

export const PAGE_NOT_FOUND_NAME = 'PageNotFound';

export const OBEI_COCKPIT_NAME = '/obeicockpit/obeiplatform2/OverIndex';

// 路由白名单
export const whiteNameList = [
  LOGIN_NAME,
  LOGIN_TRANSMITTER,
  'icons',
  'error',
  'error-404',
] as const; // no redirect whitelist

export type WhiteNameList = typeof whiteNameList;

export type WhiteName = typeof whiteNameList[number];
