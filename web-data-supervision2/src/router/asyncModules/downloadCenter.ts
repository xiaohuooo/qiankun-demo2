/**
 * 下载中心相关路由
 */
export default {
  'views/download/downloadCenter': () => import('@/views/download/downloadCenter/index.vue'),
  'views/download/fileCenter': () => import('@/views/download/fileCenter/index.vue'),
} as const;
