import { defineStore } from 'pinia';
import { isEmpty } from 'lodash-es';
import { store } from '@/store';
interface insertContentType {
  title: string;
  value: any[];
}

interface chartConfigurationsState {
  insertContent: insertContentType[];
  AddCatalogueFlug: boolean;
  navList: any[];
  page: any;
  datasets: any;
  chartData: any;
  pageData: any;
  fieldArray: any;
  echartConfig: any;
  sqlParams: any;
  newSqlParams: any;
  chartsResize: boolean;
  editorConfig: any;
  formatter: any;
  formatterTable: any;
  sourceName: any;
  sourceNameList: any;
}
const insertContent = [
  {
    title: '横轴',
    value: [],
  },
  {
    title: '纵轴',
    value: [],
  },
  {
    title: '过滤',
    value: [],
  },
];
const chartData = {
  loading: 'error', // error 无数据
  data: {}, // 获取到的数据
  page: {
    pageNum: 1,
    pageSize: 10,
    total: 1,
    pageCount: 1,
  },
};
export const chartConfigurations = defineStore({
  id: 'chartConfigurations',
  state: (): chartConfigurationsState => ({
    AddCatalogueFlug: false,
    insertContent, // 横纵轴配置参数
    navList: [], // 左边所有数据
    page: {},
    pageData: {},
    datasets: [],
    chartData,
    fieldArray: [], // 所有字段
    echartConfig: {}, // 图表配置项
    sqlParams: { datatableList: [], tableRelationList: [] }, //获取sql入参
    newSqlParams: {},
    chartsResize: false, // 图表是否全屏
    editorConfig: {
      valueHtml: '',
      htmlList: [],
    },
    formatter: (params, formatterList) => {
      if (params?.value?.includes(',')) params.value = params?.value?.replaceAll(',', '');
      if (params.value instanceof Array) {
        params.value = params.value[1];
      }
      if (formatterList && formatterList.length > 0) {
        formatterList.map((x) => {
          if (params.seriesName.includes(x.sourceName) && x.dataFormat) {
            if (params?.value?.includes(x.dataFormat.unit)) {
              params.value = params?.value?.replaceAll(x.dataFormat.unit, '');
            }
            switch (x.dataFormat.type) {
              case 'number':
                // 是否保留小数
                if (x.dataFormat.D || x.dataFormat.D === 0) {
                  params.value = Number(params.value).toFixed(x.dataFormat.D);
                }
                // 是否需要千分位
                if (x.dataFormat.U) {
                  params.value = (params.value || 0)
                    ?.toString()
                    .replace(/^-?\d+/g, (m) => m.replace(/(?=(?!\b)(\d{3})+$)/g, ','));
                }
                // 是否需要单位
                if (x.dataFormat.unit) {
                  params.value = `${params.value}(${x.dataFormat.unit})`;
                }
                break;
              case 'percent':
                // 保留小数
                params.value = String(params.value).replaceAll('%', '');
                if (x.dataFormat.D || x.dataFormat.D === 0) {
                  params.value = `${Number(params.value).toFixed(x.dataFormat.D)}%`;
                }
                break;
              default:
                break;
            }
          }
        });
      }
      return (
        `<span style="font-weight: bold;font-size:16px;">${params.seriesName}</span><br/><br/>` +
        `<span style="font-size:14px;">${
          params.marker + params.name
        }</span><span style="font-weight: bold;font-size:14px;margin-left: 10px;">${
          params.value
        }</span>`
      );
    },
    formatterTable: (value, name, formatterList) => {
      if (value?.includes(',')) value = value.replaceAll(',', '');
      if (formatterList && formatterList.length > 0) {
        formatterList.map((x) => {
          if (name.includes(x.sourceName) && x.dataFormat) {
            if (value?.includes(x.dataFormat.unit)) value = value.replaceAll(x.dataFormat.unit, '');
            switch (x.dataFormat.type) {
              case 'number':
                // 是否保留小数
                if (x.dataFormat.D || x.dataFormat.D === 0) {
                  value = Number(value).toFixed(x.dataFormat.D);
                }
                // 是否需要千分位
                if (x.dataFormat.U) {
                  value = (value || 0)
                    ?.toString()
                    .replace(/^-?\d+/g, (m) => m.replace(/(?=(?!\b)(\d{3})+$)/g, ','));
                }
                // 是否需要单位
                if (x.dataFormat.unit) {
                  value = `${value}(${x.dataFormat.unit})`;
                }
                break;
              case 'percent':
                // 保留小数
                value = String(value).replaceAll('%', '');
                if (x.dataFormat.D || x.dataFormat.D === 0) {
                  value = `${Number(value).toFixed(x.dataFormat.D)}%`;
                }
                break;
              default:
                break;
            }
          }
        });
      }
      return value;
    },
    sourceName: '',
    sourceNameList: [],
  }),
  actions: {
    AddCatalogueFlugConfig(itemLength) {
      this.AddCatalogueFlug = itemLength > 0;
    },
    navListConfig(data, fieldArray) {
      this.navList = data;
      this.fieldArray = fieldArray;
      if (this.pageData?.tableData?.children?.length) {
        this.navList[1] = this.pageData.tableData; // 配置目录
      }
      if (this.pageData.addField?.children?.length) {
        this.navList[0] = this.pageData.addField; // 配置新增字段
      }
    },
    pageConfig(data) {
      this.page = data;
      this.pageData = data.data.pageData ? JSON.parse(data.data.pageData) : {};
      this.sourceName = this.pageData.sourceName ? this.pageData.sourceName : '5S通用查询配置';
      this.sqlParams = { datatableList: [], tableRelationList: [] };
      if (this.pageData?.sqlParams) {
        this.sqlParams = this.pageData?.sqlParams;
      } else if (this.pageData?.sqlData) {
        this.sqlParams = this.pageData?.sqlData;
      }
      if (!isEmpty(data.data.option) && !isEmpty(data.data.option.editorConfig)) {
        this.editorConfig = data.data.option.editorConfig;
      }
    },
    setSqlParams(data) {
      this.sqlParams = data;
    },
    chartDataConfig(data) {
      this.chartData = data;
    },
    insertContentConfig(data) {
      this.insertContent = [
        {
          title: '横轴',
          value: [],
        },
        {
          title: '纵轴',
          value: [],
        },
        {
          title: '过滤',
          value: [],
        },
      ];
      let pageData = data.filterList;
      if (data.pageData) {
        pageData = JSON.parse(data.pageData);
      }
      this.insertContent.forEach((item, index) => {
        item.value = pageData[index].value;
      });
    },
    onlyAxis(newVal) {
      if (!newVal.length) {
        this.insertContent = [
          {
            title: '横轴',
            value: [],
          },
          {
            title: '纵轴',
            value: [],
          },
          {
            title: '过滤',
            value: [],
          },
        ];
        this.chartData.loading = 'error';
        this.editorConfig.valueHtml = '';
        this.chartData.data.dataConfig.option.editorConfig.valueHtml = '';
        return;
      }
      // 轴字段过滤
      this.insertContent.forEach((item) => {
        item.value.forEach((tItem, tIndex) => {
          // 目录
          if (tItem.newType === 4) {
            tItem.children.forEach((cItem, cIndex) => {
              let flag = false;
              newVal.forEach((fItem) => {
                if (cItem.name == fItem.name) {
                  flag = true;
                }
              });
              if (!flag) {
                tItem.children.splice(cIndex, 1);
              }
            });
          } else if (tItem.type === 'addField') {
            console.log(tItem, 'tItem');
            //  新增字段
          } else {
            let flag = false;
            newVal.forEach((fItem) => {
              if (tItem.name === fItem.name) {
                flag = true;
              }
            });
            if (!flag) {
              item.value.splice(tIndex, 1);
            }
          }
        });
      });
      // 目录过滤
      if (
        this.navList[1] != undefined &&
        this.navList[1].children != undefined &&
        this.navList[1].children.length > 0
      ) {
        this.navList[1]?.children?.[0]?.children.forEach((citem, cIndex) => {
          let flag = false;
          newVal.forEach((fItem) => {
            if (citem.name === fItem.name) {
              flag = true;
            }
          });
          if (!flag) {
            this.navList[1].children?.[0].children.splice(cIndex, 1);
          }
        });
      }
    },
    getnewSqlParams(newSqlParams) {
      this.newSqlParams = newSqlParams;
    },
  },
});

// 在组件setup函数外使用
export function chartConfigurationsWithOut() {
  return chartConfigurations(store);
}
