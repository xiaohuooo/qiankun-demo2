import { defineStore } from 'pinia';
import { store } from '@/store';

interface SupervisionGovernState {
  modelresultView: Boolean;
  modelData: Object;
  queryFiltershow: Boolean;
  dataPushshow: Boolean;
  numType: String;
}

export const useSupervisionGovern = defineStore({
  id: 'supervisionGovern',
  state: (): SupervisionGovernState => ({
    modelresultView: false,
    modelData: {},
    queryFiltershow: true,
    dataPushshow: false,
    numType: '',
  }),
  getters: {},
  actions: {
    setVisible(modelresultView: Boolean) {
      this.modelresultView = modelresultView;
    },
    setModelData(modelData: Object) {
      this.modelData = modelData;
    },
    setQueryFiltershow(queryFiltershow: Boolean) {
      this.queryFiltershow = queryFiltershow;
    },
    setDataPushshow(dataPushshow: Boolean) {
      this.dataPushshow = dataPushshow;
    },
    setNumType(numType: String) {
      this.numType = numType;
    },
  },
});

// Need to be used outside the setup
export function useSupervisionGovernOut() {
  return useSupervisionGovern(store);
}
