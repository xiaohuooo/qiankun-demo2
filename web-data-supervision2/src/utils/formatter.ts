import XEUtils from 'xe-utils';

/**
 * 格式化小数位数
 * @param value
 * @param digits
 */
export function formatterNum(value: number, digits: number): number {
  if (digits < 0) {
    digits = 2;
  }
  return XEUtils.toFixed(XEUtils.round(value, digits), digits);
}

/**
 * 千分位
 * @param cellValue
 */
export function commafy(cellValue: number, digits: number): string {
  if (digits < 0) {
    digits = 2;
  }
  return XEUtils.commafy(Number(cellValue), { digits });
}
