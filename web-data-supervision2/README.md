![欧贝](/src/assets/images/logo.png)

## 大数据穿透式在线系统介绍
```
 欧贝大数据穿透式在线监督平台
 大数据穿透式在线监督平台前端团队开发
```

#### 软件架构
```
基于 vue-cli5.x / vite2.x + vue3.x + antd-design-vue3.x + typescript4.x 的后台管理系统模板 、 vue3-antd-admin
```

## 安装使用

- 获取项目代码

```bash
git clone https://github.com/buqiyuan/vue3-antd-admin
```

- 运行

```bash
```
npm install

运行dev环境：npm run dev
运行test环境：npm run test
运行prod环境：npm run prod
编译dev：npm run build:dev
编译test：npm run  build:test
编译prod：npm run build:prod
```
```

- 打包

```bash
npm build
```

## vscode 配置

安装项目根目录.vscode 推荐的插件，再安装 Volar，并禁用 Vetur，重启 vscode 即可，更多详细的 vue3 IDE 配置点[这里](https://cn.vuejs.org/guide/typescript/overview.html#volar-takeover-mode)。

> 使用了 Vue3.x 全家桶、ant-design-vue3.x 和 typescript4.x，实践 vue3.x 的新特性以及玩法，不得不说 vue3.x 的 Composition API 相比于 vue2.x 的 Options API 灵活很多，让我们可以灵活地组合组件逻辑，我们可以很轻松的使用 hooks 的形式去代替以前 mixins 等的写法。更多 hooks 可以参考[vueuse](https://vueuse.org/functions.html)

## 项目简要说明

